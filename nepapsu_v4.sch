<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="13" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="13" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="rakettitiede">
<packages>
<package name="10-VSON-HOMETCH">
<smd name="11" x="0" y="0" dx="2.6" dy="1.7" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="7" x="-0.2" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="8" x="-0.6" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="9" x="-1" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="10" x="-1.4" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="1" x="-1.4" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="2" x="-1" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="3" x="-0.6" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="4" x="-0.2" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<wire x1="-1.65" y1="1.65" x2="1.65" y2="1.65" width="0.127" layer="51"/>
<wire x1="1.65" y1="1.65" x2="1.65" y2="-1.65" width="0.127" layer="51"/>
<wire x1="1.65" y1="-1.65" x2="-1.65" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-1.65" y1="-1.65" x2="-1.65" y2="1.65" width="0.127" layer="51"/>
<wire x1="-1.7" y1="1.65" x2="-1.7" y2="-1.65" width="0.25" layer="21"/>
<wire x1="1.7" y1="-1.65" x2="1.7" y2="1.65" width="0.25" layer="21"/>
<text x="-2" y="-1.3" size="0.6096" layer="21" rot="R90">&gt;NAME</text>
<polygon width="0.02" layer="31">
<vertex x="-1.15" y="-0.7"/>
<vertex x="-0.2" y="-0.7"/>
<vertex x="-0.2" y="-0.65"/>
<vertex x="-0.2" y="-0.1"/>
<vertex x="-1.15" y="-0.1"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-0.2" y="0.1"/>
<vertex x="-1.15" y="0.1"/>
<vertex x="-1.15" y="0.7"/>
<vertex x="-0.2" y="0.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="0.2" y="0.1"/>
<vertex x="1.15" y="0.1"/>
<vertex x="1.15" y="0.7"/>
<vertex x="0.2" y="0.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="0.2" y="-0.1"/>
<vertex x="1.15" y="-0.1"/>
<vertex x="1.15" y="-0.7"/>
<vertex x="0.2" y="-0.7"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-1.31" y="0.86"/>
<vertex x="1.31" y="0.86"/>
<vertex x="1.31" y="-0.86"/>
<vertex x="-1.31" y="-0.86"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-1.49" y="1.3"/>
<vertex x="-1.31" y="1.3"/>
<vertex x="-1.31" y="1.7"/>
<vertex x="-1.49" y="1.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-1.09" y="1.3"/>
<vertex x="-0.91" y="1.3"/>
<vertex x="-0.91" y="1.7"/>
<vertex x="-1.09" y="1.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-0.69" y="1.3"/>
<vertex x="-0.51" y="1.3"/>
<vertex x="-0.51" y="1.7"/>
<vertex x="-0.69" y="1.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-0.29" y="1.3"/>
<vertex x="-0.11" y="1.3"/>
<vertex x="-0.11" y="1.7"/>
<vertex x="-0.29" y="1.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="0.11" y="1.3"/>
<vertex x="1.49" y="1.3"/>
<vertex x="1.49" y="1.7"/>
<vertex x="1.31" y="1.7"/>
<vertex x="1.09" y="1.7"/>
<vertex x="0.91" y="1.7"/>
<vertex x="0.68" y="1.7"/>
<vertex x="0.11" y="1.7"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-1.51" y="2.46"/>
<vertex x="-1.29" y="2.46"/>
<vertex x="-1.29" y="1.14"/>
<vertex x="-1.51" y="1.14"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-1.11" y="1.14"/>
<vertex x="-0.89" y="1.14"/>
<vertex x="-0.89" y="2.46"/>
<vertex x="-1.11" y="2.46"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-0.71" y="1.14"/>
<vertex x="-0.49" y="1.14"/>
<vertex x="-0.49" y="2.46"/>
<vertex x="-0.71" y="2.46"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-0.31" y="2.46"/>
<vertex x="-0.09" y="2.46"/>
<vertex x="-0.09" y="1.14"/>
<vertex x="-0.31" y="1.14"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-1.49" y="-1.3"/>
<vertex x="-1.31" y="-1.3"/>
<vertex x="-1.31" y="-1.7"/>
<vertex x="-1.49" y="-1.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-1.08" y="-1.3"/>
<vertex x="-0.91" y="-1.3"/>
<vertex x="-0.91" y="-1.7"/>
<vertex x="-1.09" y="-1.7"/>
<vertex x="-1.09" y="-1.3"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-0.69" y="-1.7"/>
<vertex x="-0.51" y="-1.7"/>
<vertex x="-0.51" y="-1.3"/>
<vertex x="-0.69" y="-1.3"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-0.29" y="-1.7"/>
<vertex x="-0.11" y="-1.7"/>
<vertex x="-0.11" y="-1.3"/>
<vertex x="-0.29" y="-1.3"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="0.11" y="-1.7"/>
<vertex x="0.29" y="-1.7"/>
<vertex x="0.51" y="-1.7"/>
<vertex x="0.69" y="-1.7"/>
<vertex x="0.91" y="-1.7"/>
<vertex x="1.09" y="-1.7"/>
<vertex x="1.31" y="-1.7"/>
<vertex x="1.49" y="-1.7"/>
<vertex x="1.49" y="-1.3"/>
<vertex x="0.11" y="-1.3"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-1.51" y="-1.14"/>
<vertex x="-1.29" y="-1.14"/>
<vertex x="-1.29" y="-2.46"/>
<vertex x="-1.51" y="-2.46"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-1.11" y="-1.14"/>
<vertex x="-0.89" y="-1.14"/>
<vertex x="-0.89" y="-2.46"/>
<vertex x="-1.11" y="-2.46"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-0.71" y="-1.14"/>
<vertex x="-0.49" y="-1.14"/>
<vertex x="-0.49" y="-2.46"/>
<vertex x="-0.71" y="-2.46"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-0.31" y="-1.14"/>
<vertex x="-0.09" y="-1.14"/>
<vertex x="-0.09" y="-2.46"/>
<vertex x="-0.31" y="-2.46"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="0.1" y="1.15"/>
<vertex x="1.5" y="1.15"/>
<vertex x="1.5" y="2.45"/>
<vertex x="0.1" y="2.45"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="0.1" y="-1.15"/>
<vertex x="0.1" y="-2.45"/>
<vertex x="1.5" y="-2.45"/>
<vertex x="1.5" y="-1.15"/>
</polygon>
<smd name="6" x="0.8" y="1.8" dx="1.4" dy="1.3" layer="1" cream="no"/>
<smd name="5" x="0.8" y="-1.8" dx="1.4" dy="1.3" layer="1" cream="no"/>
</package>
<package name="SO-8">
<smd name="5" x="1.905" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="0.635" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="7" x="-0.635" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="8" x="-1.905" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="1" x="-1.905" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-0.635" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="0.635" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="1.905" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<wire x1="2.45" y1="1.95" x2="-2.45" y2="1.95" width="0.127" layer="51"/>
<wire x1="-2.45" y1="1.95" x2="-2.45" y2="-1.95" width="0.127" layer="51"/>
<wire x1="-2.45" y1="-1.95" x2="2.45" y2="-1.95" width="0.127" layer="51"/>
<wire x1="2.45" y1="-1.95" x2="2.45" y2="1.95" width="0.127" layer="51"/>
<text x="-2.85" y="-1.75" size="0.8128" layer="21" rot="R90">&gt;NAME</text>
<wire x1="-2.35" y1="1.95" x2="-2.45" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.95" x2="-2.45" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.95" x2="-2.4" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="2.35" y1="1.95" x2="2.45" y2="1.95" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.95" x2="2.45" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.95" x2="2.35" y2="-1.95" width="0.2032" layer="21"/>
<circle x="-1.9" y="-1.3" radius="0.2" width="0.127" layer="21"/>
</package>
<package name="POWER_JACK_SMD">
<wire x1="-5" y1="4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.5" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="9.8" y1="4.5" x2="9.8" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="4.5" x2="-3.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-2.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="0" y2="2.5" width="0.2032" layer="21"/>
<wire x1="0" y1="2.5" x2="2.286" y2="0" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.286" y1="0" x2="0" y2="-2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="0" y1="-2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="4.5" x2="-2" y2="4.5" width="0.2032" layer="21"/>
<wire x1="2" y1="4.5" x2="4.1" y2="4.5" width="0.2032" layer="21"/>
<wire x1="8.2" y1="4.5" x2="9.8" y2="4.5" width="0.2032" layer="21"/>
<wire x1="9.8" y1="-4.5" x2="8.1" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-4.5" x2="2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="-4.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<smd name="VIN0" x="0" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="GND" x="0" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="VIN1" x="6.1" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="P$4" x="6.1" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<hole x="0" y="0" drill="2.032"/>
<hole x="4.572" y="0" drill="2.032"/>
</package>
<package name="POWER_JACK_SLOT">
<wire x1="9.33" y1="13.7" x2="4.83" y2="13.7" width="0.2032" layer="21"/>
<wire x1="4.83" y1="13.7" x2="0.83" y2="13.7" width="0.2032" layer="51"/>
<wire x1="0.83" y1="13.7" x2="0.83" y2="12.7" width="0.2032" layer="51"/>
<wire x1="0.83" y1="12.7" x2="0.83" y2="8.4" width="0.2032" layer="21"/>
<wire x1="0.83" y1="8.4" x2="0.83" y2="6.55" width="0.2032" layer="51"/>
<wire x1="0.98" y1="13.8" x2="4.18" y2="13.8" width="0" layer="46"/>
<wire x1="0.98" y1="13.8" x2="1.48" y2="14.3" width="0" layer="46" curve="-90"/>
<wire x1="1.48" y1="14.3" x2="3.68" y2="14.3" width="0" layer="46"/>
<wire x1="3.68" y1="14.3" x2="4.18" y2="13.8" width="0" layer="46" curve="-90"/>
<wire x1="4.18" y1="13.8" x2="3.68" y2="13.3" width="0" layer="46" curve="-90"/>
<wire x1="3.68" y1="13.3" x2="1.48" y2="13.3" width="0" layer="46"/>
<wire x1="1.48" y1="13.3" x2="0.98" y2="13.8" width="0" layer="46" curve="-90"/>
<wire x1="0.98" y1="7.5" x2="1.48" y2="8" width="0" layer="46" curve="-90"/>
<wire x1="1.48" y1="8" x2="3.68" y2="8" width="0" layer="46"/>
<wire x1="3.68" y1="8" x2="4.18" y2="7.5" width="0" layer="46" curve="-90"/>
<wire x1="4.18" y1="7.5" x2="3.68" y2="7" width="0" layer="46" curve="-90"/>
<wire x1="3.68" y1="7" x2="1.48" y2="7" width="0" layer="46"/>
<wire x1="1.48" y1="7" x2="0.98" y2="7.5" width="0" layer="46" curve="-90"/>
<wire x1="7.58" y1="12.4" x2="8.08" y2="11.9" width="0" layer="46" curve="-90"/>
<wire x1="8.08" y1="11.9" x2="8.08" y2="9.7" width="0" layer="46"/>
<wire x1="8.08" y1="9.7" x2="7.58" y2="9.2" width="0" layer="46" curve="-90"/>
<wire x1="7.58" y1="9.2" x2="7.08" y2="9.7" width="0" layer="46" curve="-90"/>
<wire x1="7.08" y1="9.7" x2="7.08" y2="11.9" width="0" layer="46"/>
<wire x1="7.08" y1="11.9" x2="7.58" y2="12.4" width="0" layer="46" curve="-90"/>
<wire x1="0.83" y1="3.1" x2="0.83" y2="0.1" width="0.2032" layer="51"/>
<wire x1="9.33" y1="0.1" x2="9.33" y2="3.1" width="0.2032" layer="51"/>
<wire x1="9.33" y1="0.1" x2="0.83" y2="0.1" width="0.2032" layer="51"/>
<wire x1="0.83" y1="6.51" x2="0.83" y2="3.1" width="0.2032" layer="21"/>
<wire x1="9.33" y1="3.1" x2="9.33" y2="13.7" width="0.2032" layer="21"/>
<wire x1="0.83" y1="3.1" x2="9.33" y2="3.1" width="0.2032" layer="21"/>
<wire x1="9.33" y1="3.1" x2="9.28" y2="3.1" width="0.2032" layer="21"/>
<pad name="PWR" x="2.58" y="13.8" drill="1" diameter="2" shape="long"/>
<pad name="GND@1" x="2.58" y="7.5" drill="1" diameter="2" shape="long"/>
<pad name="GND@2" x="7.58" y="10.8" drill="1" diameter="2" shape="long" rot="R90"/>
<text x="0" y="0" size="1.778" layer="25" rot="R90">&gt;NAME</text>
<text x="11.43" y="0" size="1.778" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="POWER_JACK_PTH">
<wire x1="4.5" y1="13.7" x2="2.4" y2="13.7" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3" x2="-4.5" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="0.1" x2="4.5" y2="3" width="0.2032" layer="51"/>
<wire x1="4.5" y1="0.1" x2="-4.5" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="3" x2="4.5" y2="8.3" width="0.2032" layer="21"/>
<wire x1="4.5" y1="13.7" x2="4.5" y2="13" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3" x2="-4.5" y2="13.7" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="13.7" x2="-2.4" y2="13.7" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3" x2="4.5" y2="3" width="0.2032" layer="21"/>
<pad name="PWR" x="0" y="13.7" drill="2.9972" diameter="4.318"/>
<pad name="GND" x="0" y="7.7" drill="2.9972" diameter="4.318"/>
<pad name="GNDBREAK" x="4.7" y="10.7" drill="2.9972" diameter="4.318" rot="R90"/>
<text x="-3.81" y="5.08" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="3.81" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="POWER_JACK_COMBO">
<wire x1="-5" y1="4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.5" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="9.8" y1="4.5" x2="9.8" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="4.5" x2="-3.5" y2="2.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-2.5" x2="-3.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="2.5" x2="0" y2="2.5" width="0.2032" layer="51"/>
<wire x1="0" y1="2.5" x2="2.286" y2="0" width="0.2032" layer="51" curve="-90"/>
<wire x1="2.286" y1="0" x2="0" y2="-2.5" width="0.2032" layer="51" curve="-90"/>
<wire x1="0" y1="-2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="4.5" x2="-2" y2="4.5" width="0.2032" layer="51"/>
<wire x1="2" y1="4.5" x2="4.1" y2="4.5" width="0.2032" layer="51"/>
<wire x1="8.2" y1="4.5" x2="9.8" y2="4.5" width="0.2032" layer="51"/>
<wire x1="9.8" y1="-4.5" x2="8.1" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="4.1" y1="-4.5" x2="2" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-2" y1="-4.5" x2="-3.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="8.62" y1="-1.96" x2="8.62" y2="0.14" width="0.2032" layer="51"/>
<wire x1="-2.08" y1="7.04" x2="-4.98" y2="7.04" width="0.2032" layer="51"/>
<wire x1="-4.98" y1="-1.96" x2="-2.08" y2="-1.96" width="0.2032" layer="51"/>
<wire x1="-4.98" y1="-1.96" x2="-4.98" y2="7.04" width="0.2032" layer="51"/>
<wire x1="-2.08" y1="-1.96" x2="3.22" y2="-1.96" width="0.2032" layer="51"/>
<wire x1="8.62" y1="-1.96" x2="7.92" y2="-1.96" width="0.2032" layer="51"/>
<wire x1="-2.08" y1="7.04" x2="8.62" y2="7.04" width="0.2032" layer="51"/>
<wire x1="8.62" y1="7.04" x2="8.62" y2="4.94" width="0.2032" layer="51"/>
<wire x1="-2.08" y1="7.04" x2="-2.08" y2="-1.96" width="0.2032" layer="51"/>
<wire x1="0" y1="5.715" x2="6.35" y2="5.715" width="1.27" layer="1"/>
<wire x1="6.35" y1="5.715" x2="6.35" y2="5.08" width="1.27" layer="1"/>
<wire x1="6.35" y1="5.08" x2="8.89" y2="2.54" width="1.27" layer="1"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-1.905" width="1.27" layer="1"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-5.715" width="1.27" layer="1"/>
<wire x1="2.54" y1="-5.715" x2="0" y2="-5.715" width="1.27" layer="1"/>
<wire x1="6.35" y1="-5.715" x2="2.54" y2="-5.715" width="1.27" layer="1"/>
<wire x1="5.715" y1="-1.905" x2="2.54" y2="-1.905" width="1.27" layer="1"/>
<pad name="POWER" x="8.62" y="2.54" drill="2.9972" rot="R270"/>
<pad name="GND@1" x="2.62" y="2.54" drill="2.9972" rot="R270"/>
<pad name="GND" x="5.62" y="-2.16" drill="2.9972"/>
<smd name="POWER@1" x="0" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="GND@3" x="0" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="POWER@2" x="6.1" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="GND@4" x="6.1" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<text x="-5.08" y="7.62" size="1.778" layer="25">&gt;NAME</text>
<hole x="0" y="0" drill="1.6"/>
<hole x="5.08" y="0" drill="1.6"/>
</package>
<package name="POWER_JACK_PTH_LOCK">
<wire x1="4.3476" y1="14.2588" x2="2.4" y2="14.2588" width="0.2032" layer="21"/>
<wire x1="-4.3476" y1="3.2794" x2="-4.3476" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.3476" y1="0.1" x2="4.3476" y2="3.2794" width="0.2032" layer="51"/>
<wire x1="4.3476" y1="0.1" x2="-4.3476" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.3476" y1="3.254" x2="4.3476" y2="8.3" width="0.2032" layer="21"/>
<wire x1="4.3476" y1="14.2588" x2="4.3476" y2="13" width="0.2032" layer="21"/>
<wire x1="-4.3476" y1="3.254" x2="-4.3476" y2="14.2588" width="0.2032" layer="21"/>
<wire x1="-4.3476" y1="14.2588" x2="-2.4" y2="14.2588" width="0.2032" layer="21"/>
<wire x1="-4.3476" y1="3.254" x2="4.3476" y2="3.254" width="0.2032" layer="21"/>
<pad name="PWR" x="0" y="13.8778" drill="3.2" diameter="4.1148"/>
<pad name="GND" x="0.0254" y="6.557" drill="2.9972" diameter="4.1148"/>
<pad name="GNDBREAK" x="3.7616" y="10.7" drill="2.9972" diameter="4.1148" rot="R90"/>
<text x="-3.81" y="5.08" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="3.81" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2159" y1="12.1793" x2="0.2413" y2="15.1003" layer="51" rot="R90"/>
<rectangle x1="-0.1397" y1="6.3119" x2="0.1397" y2="8.7503" layer="51" rot="R90"/>
<rectangle x1="4.4704" y1="9.4742" x2="4.7498" y2="11.9126" layer="51" rot="R180"/>
</package>
<package name="POWER_JACK_SMD_OVERPASTE_REDBOARD_0603">
<rectangle x1="5.7531" y1="-6.6675" x2="6.8707" y2="-6.6421" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-6.6421" x2="6.9215" y2="-6.6167" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="-6.6167" x2="6.9723" y2="-6.5913" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="-6.5913" x2="6.9723" y2="-6.5659" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="-6.5659" x2="6.9977" y2="-6.5405" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-6.5405" x2="7.0231" y2="-6.5151" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-6.5151" x2="7.0231" y2="-6.4897" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4897" x2="7.0231" y2="-6.4643" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4643" x2="7.0231" y2="-6.4389" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="-6.4643" x2="0.4445" y2="-6.4389" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4389" x2="7.0231" y2="-6.4135" layer="200" rot="R180"/>
<rectangle x1="-0.8001" y1="-6.4389" x2="0.5207" y2="-6.4135" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.4135" x2="7.0231" y2="-6.3881" layer="200" rot="R180"/>
<rectangle x1="-0.8255" y1="-6.4135" x2="0.5461" y2="-6.3881" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3881" x2="7.0231" y2="-6.3627" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-6.3881" x2="0.5715" y2="-6.3627" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3627" x2="7.0231" y2="-6.3373" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-6.3627" x2="0.5969" y2="-6.3373" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3373" x2="7.0231" y2="-6.3119" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.3373" x2="0.6223" y2="-6.3119" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3119" x2="7.0231" y2="-6.2865" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.3119" x2="0.6223" y2="-6.2865" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2865" x2="7.0231" y2="-6.2611" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.2865" x2="0.6223" y2="-6.2611" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2611" x2="7.0231" y2="-6.2357" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2611" x2="0.6477" y2="-6.2357" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2357" x2="7.0231" y2="-6.2103" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2357" x2="0.6477" y2="-6.2103" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2103" x2="7.0231" y2="-6.1849" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2103" x2="0.6477" y2="-6.1849" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1849" x2="7.0231" y2="-6.1595" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1849" x2="0.6477" y2="-6.1595" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1595" x2="7.0231" y2="-6.1341" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1595" x2="0.6477" y2="-6.1341" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1341" x2="7.0231" y2="-6.1087" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1341" x2="0.6477" y2="-6.1087" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1087" x2="7.0231" y2="-6.0833" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1087" x2="0.6477" y2="-6.0833" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0833" x2="7.0231" y2="-6.0579" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0833" x2="0.6477" y2="-6.0579" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0579" x2="7.0231" y2="-6.0325" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0579" x2="0.6477" y2="-6.0325" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0325" x2="7.0231" y2="-6.0071" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0325" x2="0.6477" y2="-6.0071" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0071" x2="7.0231" y2="-5.9817" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0071" x2="0.6477" y2="-5.9817" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9817" x2="7.0231" y2="-5.9563" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9817" x2="0.6731" y2="-5.9563" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9563" x2="7.0231" y2="-5.9309" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9563" x2="0.6731" y2="-5.9309" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9309" x2="7.0231" y2="-5.9055" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9309" x2="0.6731" y2="-5.9055" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9055" x2="7.0231" y2="-5.8801" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9055" x2="0.6731" y2="-5.8801" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.8801" x2="7.0231" y2="-5.8547" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8801" x2="0.6731" y2="-5.8547" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8547" x2="7.0231" y2="-5.8293" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8547" x2="0.6731" y2="-5.8293" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8293" x2="7.0231" y2="-5.8039" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8293" x2="0.6731" y2="-5.8039" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8039" x2="7.0231" y2="-5.7785" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8039" x2="0.6731" y2="-5.7785" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7785" x2="7.0231" y2="-5.7531" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.7785" x2="0.6731" y2="-5.7531" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7531" x2="7.0231" y2="-5.7277" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7531" x2="0.6731" y2="-5.7277" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7277" x2="7.0231" y2="-5.7023" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7277" x2="0.6731" y2="-5.7023" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7023" x2="7.0231" y2="-5.6769" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7023" x2="0.6731" y2="-5.6769" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6769" x2="7.0231" y2="-5.6515" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6769" x2="0.6731" y2="-5.6515" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6515" x2="7.0231" y2="-5.6261" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6515" x2="0.6731" y2="-5.6261" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6261" x2="7.0231" y2="-5.6007" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6261" x2="0.6731" y2="-5.6007" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6007" x2="7.0231" y2="-5.5753" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6007" x2="0.6731" y2="-5.5753" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5753" x2="7.0231" y2="-5.5499" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5753" x2="0.6731" y2="-5.5499" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5499" x2="7.0231" y2="-5.5245" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5499" x2="0.6731" y2="-5.5245" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5245" x2="7.0231" y2="-5.4991" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5245" x2="0.6731" y2="-5.4991" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4991" x2="7.0231" y2="-5.4737" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4991" x2="0.6731" y2="-5.4737" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4737" x2="7.0231" y2="-5.4483" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4737" x2="0.6731" y2="-5.4483" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4483" x2="7.0231" y2="-5.4229" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4483" x2="0.6731" y2="-5.4229" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4229" x2="7.0231" y2="-5.3975" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4229" x2="0.6731" y2="-5.3975" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3975" x2="7.0231" y2="-5.3721" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3975" x2="0.6731" y2="-5.3721" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3721" x2="7.0231" y2="-5.3467" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3721" x2="0.6731" y2="-5.3467" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3467" x2="7.0231" y2="-5.3213" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3467" x2="0.6731" y2="-5.3213" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3213" x2="7.0231" y2="-5.2959" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3213" x2="0.6731" y2="-5.2959" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2959" x2="7.0231" y2="-5.2705" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2959" x2="0.6731" y2="-5.2705" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2705" x2="7.0231" y2="-5.2451" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2705" x2="0.6731" y2="-5.2451" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2451" x2="7.0231" y2="-5.2197" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2451" x2="0.6731" y2="-5.2197" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2197" x2="7.0231" y2="-5.1943" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.2197" x2="0.6731" y2="-5.1943" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1943" x2="7.0231" y2="-5.1689" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1943" x2="0.6731" y2="-5.1689" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1689" x2="7.0231" y2="-5.1435" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1689" x2="0.6731" y2="-5.1435" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1435" x2="7.0231" y2="-5.1181" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1435" x2="0.6731" y2="-5.1181" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1181" x2="7.0231" y2="-5.0927" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1181" x2="0.6731" y2="-5.0927" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0927" x2="7.0231" y2="-5.0673" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0927" x2="0.6731" y2="-5.0673" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0673" x2="7.0231" y2="-5.0419" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0673" x2="0.6731" y2="-5.0419" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0419" x2="7.0231" y2="-5.0165" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0419" x2="0.6731" y2="-5.0165" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0165" x2="6.9977" y2="-4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0165" x2="0.6731" y2="-4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-4.9911" x2="0.6731" y2="-4.9657" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-4.9657" x2="0.5207" y2="-4.9403" layer="200" rot="R180"/>
<rectangle x1="4.3815" y1="-4.7879" x2="7.0739" y2="-4.7625" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-4.7879" x2="1.3335" y2="-4.7625" layer="200" rot="R180"/>
<rectangle x1="-4.4069" y1="-4.7625" x2="7.0993" y2="-4.7371" layer="200" rot="R180"/>
<rectangle x1="-4.4577" y1="-4.7371" x2="7.0993" y2="-4.7117" layer="200" rot="R180"/>
<rectangle x1="-4.5085" y1="-4.7117" x2="7.0993" y2="-4.6863" layer="200" rot="R180"/>
<rectangle x1="-4.5593" y1="-4.6863" x2="7.0993" y2="-4.6609" layer="200" rot="R180"/>
<rectangle x1="-4.5847" y1="-4.6609" x2="7.0993" y2="-4.6355" layer="200" rot="R180"/>
<rectangle x1="-4.6355" y1="-4.6355" x2="7.0993" y2="-4.6101" layer="200" rot="R180"/>
<rectangle x1="-4.6609" y1="-4.6101" x2="7.0993" y2="-4.5847" layer="200" rot="R180"/>
<rectangle x1="-4.6863" y1="-4.5847" x2="7.0993" y2="-4.5593" layer="200" rot="R180"/>
<rectangle x1="-4.7117" y1="-4.5593" x2="7.0739" y2="-4.5339" layer="200" rot="R180"/>
<rectangle x1="-4.7371" y1="-4.5339" x2="7.0739" y2="-4.5085" layer="200" rot="R180"/>
<rectangle x1="-4.7625" y1="-4.5085" x2="7.0739" y2="-4.4831" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="-4.4831" x2="7.0739" y2="-4.4577" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="-4.4577" x2="7.0739" y2="-4.4323" layer="200" rot="R180"/>
<rectangle x1="-4.8133" y1="-4.4323" x2="7.5057" y2="-4.4069" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="-4.4069" x2="9.7155" y2="-4.3815" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="-4.3815" x2="9.7663" y2="-4.3561" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="-4.3561" x2="9.7917" y2="-4.3307" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="-4.3307" x2="9.8171" y2="-4.3053" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="-4.3053" x2="9.8425" y2="-4.2799" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="-4.2799" x2="9.8679" y2="-4.2545" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2545" x2="9.8933" y2="-4.2291" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2291" x2="9.8933" y2="-4.2037" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2037" x2="9.8933" y2="-4.1783" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1783" x2="9.8933" y2="-4.1529" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1529" x2="9.8933" y2="-4.1275" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1275" x2="9.8933" y2="-4.1021" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.1021" x2="9.8933" y2="-4.0767" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0767" x2="9.8933" y2="-4.0513" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0513" x2="9.8933" y2="-4.0259" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0259" x2="9.8933" y2="-4.0005" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0005" x2="9.8933" y2="-3.9751" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9751" x2="9.8933" y2="-3.9497" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9497" x2="9.8933" y2="-3.9243" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9243" x2="9.8933" y2="-3.8989" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.8989" x2="9.8933" y2="-3.8735" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.8735" x2="9.8933" y2="-3.8481" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.8481" x2="9.8933" y2="-3.8227" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.8227" x2="9.8933" y2="-3.7973" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7973" x2="9.8933" y2="-3.7719" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7719" x2="9.8933" y2="-3.7465" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7465" x2="9.8933" y2="-3.7211" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7211" x2="9.8933" y2="-3.6957" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6957" x2="9.8933" y2="-3.6703" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6703" x2="9.8933" y2="-3.6449" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6449" x2="9.8933" y2="-3.6195" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6195" x2="9.8933" y2="-3.5941" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5941" x2="9.8933" y2="-3.5687" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5687" x2="9.8933" y2="-3.5433" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5433" x2="9.8933" y2="-3.5179" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5179" x2="9.8933" y2="-3.4925" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4925" x2="9.8933" y2="-3.4671" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4671" x2="9.8933" y2="-3.4417" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4417" x2="9.8933" y2="-3.4163" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4163" x2="9.8933" y2="-3.3909" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3909" x2="9.8933" y2="-3.3655" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3655" x2="9.8933" y2="-3.3401" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3401" x2="9.8933" y2="-3.3147" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3147" x2="9.8933" y2="-3.2893" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2893" x2="9.8933" y2="-3.2639" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2639" x2="9.8933" y2="-3.2385" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2385" x2="9.8933" y2="-3.2131" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2131" x2="9.8933" y2="-3.1877" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1877" x2="9.8933" y2="-3.1623" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1623" x2="9.8933" y2="-3.1369" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1369" x2="9.8933" y2="-3.1115" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1115" x2="9.8933" y2="-3.0861" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0861" x2="9.8933" y2="-3.0607" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0607" x2="9.8933" y2="-3.0353" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0353" x2="9.8933" y2="-3.0099" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0099" x2="9.8933" y2="-2.9845" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9845" x2="9.8933" y2="-2.9591" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9591" x2="9.8933" y2="-2.9337" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9337" x2="9.8933" y2="-2.9083" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9083" x2="9.8933" y2="-2.8829" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8829" x2="9.8933" y2="-2.8575" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8575" x2="9.8933" y2="-2.8321" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8321" x2="9.8933" y2="-2.8067" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8067" x2="9.8933" y2="-2.7813" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7813" x2="9.8933" y2="-2.7559" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7559" x2="9.8933" y2="-2.7305" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7305" x2="9.8933" y2="-2.7051" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7051" x2="9.8933" y2="-2.6797" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6797" x2="9.8933" y2="-2.6543" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6543" x2="9.8933" y2="-2.6289" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6289" x2="9.8933" y2="-2.6035" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6035" x2="9.8933" y2="-2.5781" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5781" x2="9.8933" y2="-2.5527" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5527" x2="9.8933" y2="-2.5273" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5273" x2="9.8933" y2="-2.5019" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5019" x2="9.8933" y2="-2.4765" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4765" x2="9.8933" y2="-2.4511" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4511" x2="9.8933" y2="-2.4257" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4257" x2="9.8933" y2="-2.4003" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4003" x2="9.8933" y2="-2.3749" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3749" x2="9.8933" y2="-2.3495" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3495" x2="9.8933" y2="-2.3241" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3241" x2="9.8933" y2="-2.2987" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2987" x2="9.8933" y2="-2.2733" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2733" x2="9.8933" y2="-2.2479" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2479" x2="9.8933" y2="-2.2225" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2225" x2="9.8933" y2="-2.1971" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1971" x2="9.8933" y2="-2.1717" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1717" x2="9.8933" y2="-2.1463" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1463" x2="9.8933" y2="-2.1209" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1209" x2="9.8933" y2="-2.0955" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0955" x2="9.8933" y2="-2.0701" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0701" x2="9.8933" y2="-2.0447" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0447" x2="9.8933" y2="-2.0193" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0193" x2="9.8933" y2="-1.9939" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9939" x2="9.8933" y2="-1.9685" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9685" x2="9.8933" y2="-1.9431" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9431" x2="9.8933" y2="-1.9177" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9177" x2="9.8933" y2="-1.8923" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8923" x2="9.8933" y2="-1.8669" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8669" x2="9.8933" y2="-1.8415" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8415" x2="9.8933" y2="-1.8161" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8161" x2="9.8933" y2="-1.7907" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7907" x2="9.8933" y2="-1.7653" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7653" x2="9.8933" y2="-1.7399" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7399" x2="9.8933" y2="-1.7145" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7145" x2="9.8933" y2="-1.6891" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6891" x2="9.8933" y2="-1.6637" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6637" x2="9.8933" y2="-1.6383" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6383" x2="9.8679" y2="-1.6129" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6129" x2="8.8773" y2="-1.5875" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5875" x2="8.8773" y2="-1.5621" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5621" x2="8.8773" y2="-1.5367" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5367" x2="8.8773" y2="-1.5113" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5113" x2="8.8773" y2="-1.4859" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4859" x2="8.8773" y2="-1.4605" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4605" x2="8.8773" y2="-1.4351" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4351" x2="8.8773" y2="-1.4097" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4097" x2="8.8773" y2="-1.3843" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3843" x2="8.8773" y2="-1.3589" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3589" x2="8.8773" y2="-1.3335" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3335" x2="8.8773" y2="-1.3081" layer="200" rot="R180"/>
<rectangle x1="4.8133" y1="-1.3081" x2="8.8773" y2="-1.2827" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3081" x2="4.3815" y2="-1.2827" layer="200" rot="R180"/>
<rectangle x1="4.9149" y1="-1.2827" x2="8.8773" y2="-1.2573" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2827" x2="4.2799" y2="-1.2573" layer="200" rot="R180"/>
<rectangle x1="5.0165" y1="-1.2573" x2="8.8773" y2="-1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2573" x2="4.1783" y2="-1.2319" layer="200" rot="R180"/>
<rectangle x1="5.0927" y1="-1.2319" x2="8.8773" y2="-1.2065" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2319" x2="4.1275" y2="-1.2065" layer="200" rot="R180"/>
<rectangle x1="5.1435" y1="-1.2065" x2="8.8773" y2="-1.1811" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2065" x2="4.0513" y2="-1.1811" layer="200" rot="R180"/>
<rectangle x1="5.1943" y1="-1.1811" x2="8.8773" y2="-1.1557" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1811" x2="4.0005" y2="-1.1557" layer="200" rot="R180"/>
<rectangle x1="5.2197" y1="-1.1557" x2="8.8773" y2="-1.1303" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1557" x2="3.9497" y2="-1.1303" layer="200" rot="R180"/>
<rectangle x1="5.2705" y1="-1.1303" x2="8.8773" y2="-1.1049" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1303" x2="3.8989" y2="-1.1049" layer="200" rot="R180"/>
<rectangle x1="5.2959" y1="-1.1049" x2="8.8773" y2="-1.0795" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1049" x2="3.8735" y2="-1.0795" layer="200" rot="R180"/>
<rectangle x1="5.3467" y1="-1.0795" x2="8.8773" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="0.1143" y1="-1.0795" x2="3.8227" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0795" x2="-0.1143" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="5.3721" y1="-1.0541" x2="8.8773" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="0.2413" y1="-1.0541" x2="3.7973" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0541" x2="-0.2413" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="-1.0287" x2="8.8773" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="0.3429" y1="-1.0287" x2="3.7465" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0287" x2="-0.3429" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="5.4229" y1="-1.0033" x2="8.8773" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="0.4191" y1="-1.0033" x2="3.7211" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0033" x2="-0.4191" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-0.9779" x2="8.8773" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="0.4699" y1="-0.9779" x2="3.6957" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9779" x2="-0.4699" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-0.9525" x2="8.8773" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="0.5207" y1="-0.9525" x2="3.6703" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9525" x2="-0.5207" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="-0.9271" x2="8.8773" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="0.5715" y1="-0.9271" x2="3.6449" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9271" x2="-0.5715" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="-0.9017" x2="8.8773" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="0.6223" y1="-0.9017" x2="3.6195" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9017" x2="-0.6223" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="-0.8763" x2="8.8773" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="0.6477" y1="-0.8763" x2="3.5941" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8763" x2="-0.6477" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="-0.8509" x2="8.8773" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="0.6985" y1="-0.8509" x2="3.5687" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8509" x2="-0.6985" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-0.8255" x2="8.8773" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="0.7239" y1="-0.8255" x2="3.5433" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8255" x2="-0.7239" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-0.8001" x2="8.8773" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="0.7493" y1="-0.8001" x2="3.5179" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8001" x2="-0.7493" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="-0.7747" x2="8.8773" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="-0.7747" x2="3.4925" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7747" x2="-0.7747" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="-0.7493" x2="8.8773" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="0.8255" y1="-0.7493" x2="3.4925" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7493" x2="-0.8001" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="-0.7239" x2="8.8773" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="0.8509" y1="-0.7239" x2="3.4671" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7239" x2="-0.8255" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="-0.6985" x2="8.8773" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="-0.6985" x2="3.4671" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6985" x2="-0.8509" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="-0.6731" x2="8.8773" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="-0.6731" x2="3.4417" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6731" x2="-0.8763" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="-0.6477" x2="8.8773" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="-0.6477" x2="3.4417" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6477" x2="-0.9017" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="-0.6223" x2="8.8773" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="-0.6223" x2="3.4163" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6223" x2="-0.9017" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="-0.5969" x2="8.8773" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="-0.5969" x2="3.4163" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5969" x2="-0.9271" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="-0.5715" x2="8.8773" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="-0.5715" x2="3.3909" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5715" x2="-0.9525" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="-0.5461" x2="8.8773" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="-0.5461" x2="3.3655" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5461" x2="-0.9525" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="-0.5207" x2="8.8773" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="-0.5207" x2="3.3655" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5207" x2="-0.9779" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4953" x2="8.8773" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="-0.4953" x2="3.3401" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4953" x2="-0.9779" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4699" x2="8.8773" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="-0.4699" x2="3.3401" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4699" x2="-1.0033" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4445" x2="8.8773" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="-0.4445" x2="3.3401" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4445" x2="-1.0033" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.4191" x2="8.8773" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.4191" x2="3.3147" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4191" x2="-1.0033" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3937" x2="8.8773" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.3937" x2="3.3147" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3937" x2="-1.0033" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3683" x2="8.8773" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.3683" x2="3.3147" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3683" x2="-1.0287" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3429" x2="8.8773" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.3429" x2="3.3147" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3429" x2="-1.0287" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.3175" x2="8.8773" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.3175" x2="3.3147" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3175" x2="-1.0287" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2921" x2="8.8773" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2921" x2="3.2893" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2921" x2="-1.0287" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2667" x2="8.8773" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2667" x2="3.2893" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2667" x2="-1.0287" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2413" x2="8.8773" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2413" x2="3.2893" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2413" x2="-1.0287" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2159" x2="8.8773" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.2159" x2="3.2893" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2159" x2="-1.0541" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1905" x2="8.8773" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1905" x2="3.2893" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1905" x2="-1.0541" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1651" x2="8.8773" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1651" x2="3.2893" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1651" x2="-1.0541" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1397" x2="8.8773" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1397" x2="3.2893" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1397" x2="-1.0541" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1143" x2="8.8773" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1143" x2="3.2893" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1143" x2="-1.0541" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0889" x2="8.8773" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0889" x2="3.2893" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0889" x2="-1.0541" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0635" x2="8.8773" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0635" x2="3.2893" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0635" x2="-1.0541" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0381" x2="8.8773" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0381" x2="3.2893" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0381" x2="-1.0541" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0127" x2="8.8773" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0127" x2="3.2893" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0127" x2="-1.0541" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0127" x2="8.8773" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0127" x2="3.2893" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0127" x2="-1.0541" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0381" x2="8.8773" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0381" x2="3.2893" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0381" x2="-1.0541" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0635" x2="8.8773" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0635" x2="3.2893" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0635" x2="-1.0541" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0889" x2="8.8773" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0889" x2="3.2893" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0889" x2="-1.0541" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1143" x2="8.8773" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.1143" x2="3.2893" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1143" x2="-1.0541" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1397" x2="8.8773" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1397" x2="3.2893" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1397" x2="-1.0541" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1651" x2="8.8773" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1651" x2="3.3147" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1651" x2="-1.0541" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1905" x2="8.8773" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1905" x2="3.3147" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1905" x2="-1.0541" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.2159" x2="8.9027" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.2159" x2="3.3147" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2159" x2="-1.0541" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2413" x2="8.9027" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.2413" x2="3.3147" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2413" x2="-1.0541" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2667" x2="8.9027" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.2667" x2="3.3401" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2667" x2="-1.0541" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2921" x2="8.9027" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.2921" x2="3.3401" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2921" x2="-1.0287" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="0.3175" x2="8.9027" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.3175" x2="3.3401" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3175" x2="-1.0287" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="0.3429" x2="8.9027" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.3429" x2="3.3655" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3429" x2="-1.0287" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="0.3683" x2="8.9027" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.3683" x2="3.3909" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3683" x2="-1.0287" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="0.3937" x2="8.9027" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.3937" x2="3.3655" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3937" x2="-1.0287" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4191" x2="8.9027" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.4191" x2="3.3909" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4191" x2="-1.0033" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4445" x2="8.9027" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="0.4445" x2="3.4163" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4445" x2="-1.0033" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4699" x2="8.9027" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="0.4699" x2="3.4163" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4699" x2="-1.0033" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="0.4953" x2="8.9027" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="0.4953" x2="3.4163" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4953" x2="-0.9779" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="0.5207" x2="8.9027" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="0.5207" x2="3.4417" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5207" x2="-0.9779" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="0.5461" x2="8.9027" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="0.5461" x2="3.4417" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5461" x2="-0.9525" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="0.5715" x2="8.9027" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="0.5715" x2="3.4671" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5715" x2="-0.9525" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="0.5969" x2="8.9027" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="0.5969" x2="3.4671" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5969" x2="-0.9271" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="0.6223" x2="8.9027" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="0.6223" x2="3.4925" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6223" x2="-0.9271" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="0.6477" x2="8.9027" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="0.6477" x2="3.4925" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6477" x2="-0.9017" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="0.6731" x2="8.9027" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="0.6731" x2="3.5179" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6731" x2="-0.8763" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="0.6985" x2="8.9027" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="0.6985" x2="3.5433" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6985" x2="-0.8763" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7239" x2="8.9027" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="0.8509" y1="0.7239" x2="3.5433" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7239" x2="-0.8509" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7493" x2="8.9027" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="0.8255" y1="0.7493" x2="3.5687" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7493" x2="-0.8255" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7747" x2="8.9027" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="0.7747" x2="3.5941" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7747" x2="-0.8001" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="0.8001" x2="8.9027" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="0.8001" x2="3.6195" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8001" x2="-0.7747" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="0.8255" x2="8.9027" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="0.7747" y1="0.8255" x2="3.6195" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8255" x2="-0.7493" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="0.8509" x2="8.9027" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="0.7493" y1="0.8509" x2="3.6449" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8509" x2="-0.7239" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="0.8763" x2="8.9027" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="0.7239" y1="0.8763" x2="3.6703" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8763" x2="-0.6731" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="5.4229" y1="0.9017" x2="8.9027" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="0.6731" y1="0.9017" x2="3.6957" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9017" x2="-0.6477" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="0.9271" x2="8.9027" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="0.6477" y1="0.9271" x2="3.7211" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9271" x2="-0.5969" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="0.9525" x2="8.9027" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="0.5969" y1="0.9525" x2="3.7465" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9525" x2="-0.5461" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="5.3213" y1="0.9779" x2="8.9027" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="0.5715" y1="0.9779" x2="3.7973" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9779" x2="-0.4953" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="5.2959" y1="1.0033" x2="8.9027" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="0.5207" y1="1.0033" x2="3.8227" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0033" x2="-0.4191" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="5.2705" y1="1.0287" x2="8.9027" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="0.4445" y1="1.0287" x2="3.8481" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0287" x2="-0.3429" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="5.2197" y1="1.0541" x2="8.9027" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="0.3937" y1="1.0541" x2="3.8989" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0541" x2="-0.2413" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="5.1689" y1="1.0795" x2="8.9027" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="0.2921" y1="1.0795" x2="3.9497" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0795" x2="-0.0889" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="5.1181" y1="1.1049" x2="8.9027" y2="1.1303" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1049" x2="4.0005" y2="1.1303" layer="200" rot="R180"/>
<rectangle x1="5.0673" y1="1.1303" x2="8.9027" y2="1.1557" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1303" x2="4.0513" y2="1.1557" layer="200" rot="R180"/>
<rectangle x1="5.0165" y1="1.1557" x2="8.9027" y2="1.1811" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1557" x2="4.1275" y2="1.1811" layer="200" rot="R180"/>
<rectangle x1="4.9149" y1="1.1811" x2="8.9027" y2="1.2065" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1811" x2="4.2037" y2="1.2065" layer="200" rot="R180"/>
<rectangle x1="4.7625" y1="1.2065" x2="8.9027" y2="1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2065" x2="4.3307" y2="1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2319" x2="8.9027" y2="1.2573" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2573" x2="8.9027" y2="1.2827" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2827" x2="8.9027" y2="1.3081" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3081" x2="8.9027" y2="1.3335" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3335" x2="8.9027" y2="1.3589" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3589" x2="8.9027" y2="1.3843" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3843" x2="8.9027" y2="1.4097" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4097" x2="8.9027" y2="1.4351" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4351" x2="8.9027" y2="1.4605" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4605" x2="8.9027" y2="1.4859" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4859" x2="8.9027" y2="1.5113" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5113" x2="8.9027" y2="1.5367" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5367" x2="8.9027" y2="1.5621" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5621" x2="8.9027" y2="1.5875" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5875" x2="8.9027" y2="1.6129" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6129" x2="8.9027" y2="1.6383" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6383" x2="8.9027" y2="1.6637" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6637" x2="8.9027" y2="1.6891" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6891" x2="8.9027" y2="1.7145" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7145" x2="8.9027" y2="1.7399" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7399" x2="8.9027" y2="1.7653" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7653" x2="8.9027" y2="1.7907" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7907" x2="8.9027" y2="1.8161" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8161" x2="8.9027" y2="1.8415" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8415" x2="8.9027" y2="1.8669" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8669" x2="8.9027" y2="1.8923" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8923" x2="8.9027" y2="1.9177" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9177" x2="8.9027" y2="1.9431" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9431" x2="8.9027" y2="1.9685" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9685" x2="8.9027" y2="1.9939" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9939" x2="8.9027" y2="2.0193" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="2.0193" x2="8.9027" y2="2.0447" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0447" x2="8.9027" y2="2.0701" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0701" x2="8.9027" y2="2.0955" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0955" x2="8.9027" y2="2.1209" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1209" x2="8.9027" y2="2.1463" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1463" x2="8.9281" y2="2.1717" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1717" x2="8.9281" y2="2.1971" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1971" x2="8.9281" y2="2.2225" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2225" x2="8.9281" y2="2.2479" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2479" x2="8.9281" y2="2.2733" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2733" x2="8.9281" y2="2.2987" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2987" x2="8.9281" y2="2.3241" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3241" x2="8.9281" y2="2.3495" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3495" x2="8.9281" y2="2.3749" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3749" x2="8.9281" y2="2.4003" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4003" x2="8.9281" y2="2.4257" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4257" x2="8.9281" y2="2.4511" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4511" x2="8.9281" y2="2.4765" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4765" x2="8.9281" y2="2.5019" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5019" x2="8.9281" y2="2.5273" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5273" x2="8.9281" y2="2.5527" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5527" x2="8.9281" y2="2.5781" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5781" x2="8.9281" y2="2.6035" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6035" x2="8.9281" y2="2.6289" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6289" x2="8.9281" y2="2.6543" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6543" x2="8.9281" y2="2.6797" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6797" x2="8.9281" y2="2.7051" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7051" x2="8.9281" y2="2.7305" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7305" x2="8.9281" y2="2.7559" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7559" x2="8.9281" y2="2.7813" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7813" x2="8.9281" y2="2.8067" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8067" x2="8.9281" y2="2.8321" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8321" x2="8.9281" y2="2.8575" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8575" x2="8.9281" y2="2.8829" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8829" x2="8.9281" y2="2.9083" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9083" x2="8.9281" y2="2.9337" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9337" x2="8.9281" y2="2.9591" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9591" x2="8.9281" y2="2.9845" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9845" x2="8.9281" y2="3.0099" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0099" x2="8.9281" y2="3.0353" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0353" x2="8.9281" y2="3.0607" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0607" x2="8.9281" y2="3.0861" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0861" x2="8.9281" y2="3.1115" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1115" x2="8.9281" y2="3.1369" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1369" x2="8.9281" y2="3.1623" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1623" x2="8.9281" y2="3.1877" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1877" x2="8.9281" y2="3.2131" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2131" x2="8.9281" y2="3.2385" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2385" x2="8.9281" y2="3.2639" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2639" x2="8.9281" y2="3.2893" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2893" x2="8.9281" y2="3.3147" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3147" x2="8.9281" y2="3.3401" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3401" x2="8.9281" y2="3.3655" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3655" x2="8.9281" y2="3.3909" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3909" x2="8.9281" y2="3.4163" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4163" x2="8.9281" y2="3.4417" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4417" x2="8.9281" y2="3.4671" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4671" x2="8.9281" y2="3.4925" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4925" x2="8.9281" y2="3.5179" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5179" x2="8.9281" y2="3.5433" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5433" x2="8.9281" y2="3.5687" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5687" x2="8.9281" y2="3.5941" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5941" x2="8.9281" y2="3.6195" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6195" x2="8.9281" y2="3.6449" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6449" x2="8.9281" y2="3.6703" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6703" x2="8.9281" y2="3.6957" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6957" x2="8.9281" y2="3.7211" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7211" x2="8.9281" y2="3.7465" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7465" x2="8.9281" y2="3.7719" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7719" x2="8.9281" y2="3.7973" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7973" x2="8.9281" y2="3.8227" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8227" x2="8.9281" y2="3.8481" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8481" x2="8.9281" y2="3.8735" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8735" x2="8.9281" y2="3.8989" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8989" x2="8.9281" y2="3.9243" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.9243" x2="8.9281" y2="3.9497" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="3.9497" x2="8.9281" y2="3.9751" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="3.9751" x2="8.9281" y2="4.0005" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0005" x2="8.9281" y2="4.0259" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0259" x2="8.9281" y2="4.0513" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0513" x2="8.9281" y2="4.0767" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0767" x2="8.9281" y2="4.1021" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.1021" x2="8.9281" y2="4.1275" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.1275" x2="8.9281" y2="4.1529" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="4.1529" x2="8.9281" y2="4.1783" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="4.1783" x2="8.9281" y2="4.2037" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="4.2037" x2="8.9027" y2="4.2291" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="4.2291" x2="8.9027" y2="4.2545" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="4.2545" x2="8.8773" y2="4.2799" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="4.2799" x2="8.8773" y2="4.3053" layer="200" rot="R180"/>
<rectangle x1="-4.8133" y1="4.3053" x2="8.8519" y2="4.3307" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="4.3307" x2="8.8265" y2="4.3561" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="4.3561" x2="8.8011" y2="4.3815" layer="200" rot="R180"/>
<rectangle x1="-4.7625" y1="4.3815" x2="8.7757" y2="4.4069" layer="200" rot="R180"/>
<rectangle x1="-4.7371" y1="4.4069" x2="8.7249" y2="4.4323" layer="200" rot="R180"/>
<rectangle x1="-4.7117" y1="4.4323" x2="8.6741" y2="4.4577" layer="200" rot="R180"/>
<rectangle x1="-4.6863" y1="4.4577" x2="8.6233" y2="4.4831" layer="200" rot="R180"/>
<rectangle x1="-4.6609" y1="4.4831" x2="7.1247" y2="4.5085" layer="200" rot="R180"/>
<rectangle x1="-4.6101" y1="4.5085" x2="7.1247" y2="4.5339" layer="200" rot="R180"/>
<rectangle x1="-4.5847" y1="4.5339" x2="7.1247" y2="4.5593" layer="200" rot="R180"/>
<rectangle x1="-4.5339" y1="4.5593" x2="7.1247" y2="4.5847" layer="200" rot="R180"/>
<rectangle x1="-4.4577" y1="4.5847" x2="7.1247" y2="4.6101" layer="200" rot="R180"/>
<rectangle x1="-4.3815" y1="4.6101" x2="6.9215" y2="4.6355" layer="200" rot="R180"/>
<rectangle x1="-3.0607" y1="4.6355" x2="-1.6637" y2="4.6609" layer="200" rot="R180"/>
<rectangle x1="-3.8989" y1="4.6355" x2="-3.3655" y2="4.6609" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="4.8133" x2="7.0739" y2="4.8387" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8387" x2="7.0993" y2="4.8641" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8641" x2="7.0993" y2="4.8895" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8895" x2="7.0993" y2="4.9149" layer="200" rot="R180"/>
<rectangle x1="-0.1651" y1="4.8895" x2="0.7239" y2="4.9149" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9149" x2="7.0993" y2="4.9403" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9149" x2="0.7239" y2="4.9403" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9403" x2="7.0993" y2="4.9657" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9403" x2="0.7239" y2="4.9657" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9657" x2="7.0993" y2="4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9657" x2="0.7239" y2="4.9911" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9911" x2="7.0993" y2="5.0165" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9911" x2="0.7239" y2="5.0165" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0165" x2="7.0993" y2="5.0419" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0165" x2="0.7239" y2="5.0419" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0419" x2="7.0993" y2="5.0673" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0419" x2="0.7239" y2="5.0673" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0673" x2="7.0993" y2="5.0927" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0673" x2="0.7239" y2="5.0927" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0927" x2="7.0993" y2="5.1181" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0927" x2="0.7239" y2="5.1181" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1181" x2="7.0993" y2="5.1435" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1181" x2="0.7239" y2="5.1435" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1435" x2="7.0993" y2="5.1689" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1435" x2="0.7239" y2="5.1689" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1689" x2="7.0993" y2="5.1943" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1689" x2="0.7239" y2="5.1943" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1943" x2="7.0993" y2="5.2197" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1943" x2="0.7239" y2="5.2197" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2197" x2="7.0993" y2="5.2451" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2197" x2="0.7239" y2="5.2451" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2451" x2="7.0993" y2="5.2705" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2451" x2="0.7239" y2="5.2705" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2705" x2="7.0993" y2="5.2959" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2705" x2="0.7239" y2="5.2959" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2959" x2="7.0739" y2="5.3213" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2959" x2="0.7239" y2="5.3213" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3213" x2="7.0739" y2="5.3467" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3213" x2="0.7239" y2="5.3467" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3467" x2="7.0739" y2="5.3721" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3467" x2="0.7239" y2="5.3721" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3721" x2="7.0739" y2="5.3975" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3721" x2="0.7239" y2="5.3975" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3975" x2="7.0739" y2="5.4229" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3975" x2="0.7239" y2="5.4229" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4229" x2="7.0739" y2="5.4483" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4229" x2="0.7239" y2="5.4483" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4483" x2="7.0739" y2="5.4737" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4483" x2="0.7239" y2="5.4737" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4737" x2="7.0739" y2="5.4991" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4737" x2="0.7239" y2="5.4991" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4991" x2="7.0739" y2="5.5245" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4991" x2="0.7239" y2="5.5245" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5245" x2="7.0739" y2="5.5499" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5245" x2="0.7239" y2="5.5499" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5499" x2="7.0739" y2="5.5753" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5499" x2="0.7239" y2="5.5753" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5753" x2="7.0739" y2="5.6007" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5753" x2="0.7239" y2="5.6007" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6007" x2="7.0739" y2="5.6261" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6007" x2="0.7239" y2="5.6261" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6261" x2="7.0739" y2="5.6515" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6261" x2="0.7239" y2="5.6515" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6515" x2="7.0739" y2="5.6769" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6515" x2="0.7239" y2="5.6769" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6769" x2="7.0739" y2="5.7023" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6769" x2="0.7239" y2="5.7023" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7023" x2="7.0739" y2="5.7277" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7023" x2="0.7239" y2="5.7277" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7277" x2="7.0739" y2="5.7531" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7277" x2="0.7239" y2="5.7531" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7531" x2="7.0739" y2="5.7785" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7531" x2="0.7239" y2="5.7785" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7785" x2="7.0739" y2="5.8039" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7785" x2="0.7239" y2="5.8039" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8039" x2="7.0739" y2="5.8293" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8039" x2="0.7239" y2="5.8293" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8293" x2="7.0739" y2="5.8547" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8293" x2="0.7239" y2="5.8547" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8547" x2="7.0739" y2="5.8801" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8547" x2="0.7239" y2="5.8801" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8801" x2="7.0739" y2="5.9055" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8801" x2="0.7239" y2="5.9055" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9055" x2="7.0739" y2="5.9309" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9055" x2="0.6985" y2="5.9309" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9309" x2="7.0739" y2="5.9563" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9309" x2="0.6985" y2="5.9563" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9563" x2="7.0739" y2="5.9817" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9563" x2="0.6985" y2="5.9817" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9817" x2="7.0739" y2="6.0071" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9817" x2="0.6985" y2="6.0071" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0071" x2="7.0739" y2="6.0325" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0071" x2="0.6985" y2="6.0325" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0325" x2="7.0739" y2="6.0579" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0325" x2="0.6985" y2="6.0579" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0579" x2="7.0739" y2="6.0833" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0579" x2="0.6985" y2="6.0833" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0833" x2="7.0739" y2="6.1087" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0833" x2="0.6985" y2="6.1087" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1087" x2="7.0739" y2="6.1341" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1087" x2="0.6985" y2="6.1341" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1341" x2="7.0739" y2="6.1595" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1341" x2="0.6985" y2="6.1595" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1595" x2="7.0739" y2="6.1849" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1595" x2="0.6985" y2="6.1849" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.1849" x2="7.0739" y2="6.2103" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1849" x2="0.6985" y2="6.2103" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2103" x2="7.0485" y2="6.2357" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2103" x2="0.6985" y2="6.2357" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2357" x2="7.0485" y2="6.2611" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2357" x2="0.6985" y2="6.2611" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2611" x2="7.0485" y2="6.2865" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2611" x2="0.6985" y2="6.2865" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="6.2865" x2="7.0231" y2="6.3119" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="6.2865" x2="0.6731" y2="6.3119" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="6.3119" x2="7.0231" y2="6.3373" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="6.3119" x2="0.6731" y2="6.3373" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="6.3373" x2="6.9977" y2="6.3627" layer="200" rot="R180"/>
<rectangle x1="-0.7239" y1="6.3373" x2="0.6477" y2="6.3627" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="6.3627" x2="6.9723" y2="6.3881" layer="200" rot="R180"/>
<rectangle x1="-0.6985" y1="6.3627" x2="0.6223" y2="6.3881" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="6.3881" x2="6.9215" y2="6.4135" layer="200" rot="R180"/>
<rectangle x1="-0.6223" y1="6.3881" x2="0.5461" y2="6.4135" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="6.4135" x2="6.8707" y2="6.4389" layer="200" rot="R180"/>
<text x="9.906" y="-6.8072" size="0.0508" layer="200" font="vector" rot="MR0">//kentro/work/Production/AOI Parts/Tyler/Breadboard power 5v/barreljack.bmp</text>
<wire x1="-5" y1="4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.5" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="9.8" y1="4.5" x2="9.8" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="4.5" x2="-3.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-2.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="0" y2="2.5" width="0.2032" layer="21"/>
<wire x1="0" y1="2.5" x2="2.286" y2="0" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.286" y1="0" x2="0" y2="-2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="0" y1="-2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="4.5" x2="-2" y2="4.5" width="0.2032" layer="21"/>
<wire x1="2" y1="4.5" x2="4.1" y2="4.5" width="0.2032" layer="21"/>
<wire x1="8.2" y1="4.5" x2="9.8" y2="4.5" width="0.2032" layer="21"/>
<wire x1="9.8" y1="-4.5" x2="8.1" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-4.5" x2="2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="-4.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<smd name="VIN0" x="0" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="GND" x="0" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="VIN1" x="6.1" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="P$4" x="6.1" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<hole x="0" y="0" drill="2.032"/>
<hole x="4.572" y="0" drill="2.032"/>
<rectangle x1="7.470140625" y1="4.83108125" x2="8.41501875" y2="6.48461875" layer="31"/>
<rectangle x1="-0.481328125" y1="6.493509375" x2="0.463546875" y2="8.147046875" layer="31" rot="R90"/>
<rectangle x1="-0.570228125" y1="-8.187690625" x2="0.374646875" y2="-6.534153125" layer="31" rot="R90"/>
<rectangle x1="5.76453125" y1="-8.18515" x2="6.70940625" y2="-6.5316125" layer="31" rot="R90"/>
</package>
<package name="POWER_JACK_SMD_OVERPASTE_TOE">
<wire x1="-5" y1="4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.5" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="9.8" y1="4.5" x2="9.8" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="4.5" x2="-3.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-2.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="0" y2="2.5" width="0.2032" layer="21"/>
<wire x1="0" y1="2.5" x2="2.286" y2="0" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.286" y1="0" x2="0" y2="-2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="0" y1="-2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="4.5" x2="-2" y2="4.5" width="0.2032" layer="21"/>
<wire x1="2" y1="4.5" x2="4.1" y2="4.5" width="0.2032" layer="21"/>
<wire x1="8.2" y1="4.5" x2="9.8" y2="4.5" width="0.2032" layer="21"/>
<wire x1="9.8" y1="-4.5" x2="8.1" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-4.5" x2="2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="-4.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<smd name="VIN0" x="0" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="GND" x="0" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="VIN1" x="6.1" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="P$4" x="6.1" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<hole x="0" y="0" drill="2.032"/>
<hole x="4.572" y="0" drill="2.032"/>
<rectangle x1="-1.399540625" y1="-8.382" x2="1.399540625" y2="-6.8961" layer="31"/>
<rectangle x1="4.701540625" y1="-8.384540625" x2="7.500621875" y2="-6.898640625" layer="31"/>
<rectangle x1="4.7015375" y1="6.8961" x2="7.50061875" y2="8.382" layer="31" rot="R180"/>
<rectangle x1="-1.39954375" y1="6.8961" x2="1.3995375" y2="8.382" layer="31" rot="R180"/>
<rectangle x1="5.7531" y1="-6.6675" x2="6.8707" y2="-6.6421" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-6.6421" x2="6.9215" y2="-6.6167" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="-6.6167" x2="6.9723" y2="-6.5913" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="-6.5913" x2="6.9723" y2="-6.5659" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="-6.5659" x2="6.9977" y2="-6.5405" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-6.5405" x2="7.0231" y2="-6.5151" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-6.5151" x2="7.0231" y2="-6.4897" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4897" x2="7.0231" y2="-6.4643" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4643" x2="7.0231" y2="-6.4389" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="-6.4643" x2="0.4445" y2="-6.4389" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4389" x2="7.0231" y2="-6.4135" layer="200" rot="R180"/>
<rectangle x1="-0.8001" y1="-6.4389" x2="0.5207" y2="-6.4135" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.4135" x2="7.0231" y2="-6.3881" layer="200" rot="R180"/>
<rectangle x1="-0.8255" y1="-6.4135" x2="0.5461" y2="-6.3881" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3881" x2="7.0231" y2="-6.3627" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-6.3881" x2="0.5715" y2="-6.3627" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3627" x2="7.0231" y2="-6.3373" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-6.3627" x2="0.5969" y2="-6.3373" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3373" x2="7.0231" y2="-6.3119" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.3373" x2="0.6223" y2="-6.3119" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3119" x2="7.0231" y2="-6.2865" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.3119" x2="0.6223" y2="-6.2865" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2865" x2="7.0231" y2="-6.2611" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.2865" x2="0.6223" y2="-6.2611" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2611" x2="7.0231" y2="-6.2357" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2611" x2="0.6477" y2="-6.2357" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2357" x2="7.0231" y2="-6.2103" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2357" x2="0.6477" y2="-6.2103" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2103" x2="7.0231" y2="-6.1849" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2103" x2="0.6477" y2="-6.1849" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1849" x2="7.0231" y2="-6.1595" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1849" x2="0.6477" y2="-6.1595" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1595" x2="7.0231" y2="-6.1341" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1595" x2="0.6477" y2="-6.1341" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1341" x2="7.0231" y2="-6.1087" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1341" x2="0.6477" y2="-6.1087" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1087" x2="7.0231" y2="-6.0833" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1087" x2="0.6477" y2="-6.0833" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0833" x2="7.0231" y2="-6.0579" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0833" x2="0.6477" y2="-6.0579" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0579" x2="7.0231" y2="-6.0325" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0579" x2="0.6477" y2="-6.0325" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0325" x2="7.0231" y2="-6.0071" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0325" x2="0.6477" y2="-6.0071" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0071" x2="7.0231" y2="-5.9817" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0071" x2="0.6477" y2="-5.9817" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9817" x2="7.0231" y2="-5.9563" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9817" x2="0.6731" y2="-5.9563" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9563" x2="7.0231" y2="-5.9309" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9563" x2="0.6731" y2="-5.9309" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9309" x2="7.0231" y2="-5.9055" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9309" x2="0.6731" y2="-5.9055" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9055" x2="7.0231" y2="-5.8801" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9055" x2="0.6731" y2="-5.8801" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.8801" x2="7.0231" y2="-5.8547" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8801" x2="0.6731" y2="-5.8547" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8547" x2="7.0231" y2="-5.8293" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8547" x2="0.6731" y2="-5.8293" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8293" x2="7.0231" y2="-5.8039" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8293" x2="0.6731" y2="-5.8039" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8039" x2="7.0231" y2="-5.7785" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8039" x2="0.6731" y2="-5.7785" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7785" x2="7.0231" y2="-5.7531" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.7785" x2="0.6731" y2="-5.7531" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7531" x2="7.0231" y2="-5.7277" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7531" x2="0.6731" y2="-5.7277" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7277" x2="7.0231" y2="-5.7023" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7277" x2="0.6731" y2="-5.7023" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7023" x2="7.0231" y2="-5.6769" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7023" x2="0.6731" y2="-5.6769" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6769" x2="7.0231" y2="-5.6515" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6769" x2="0.6731" y2="-5.6515" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6515" x2="7.0231" y2="-5.6261" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6515" x2="0.6731" y2="-5.6261" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6261" x2="7.0231" y2="-5.6007" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6261" x2="0.6731" y2="-5.6007" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6007" x2="7.0231" y2="-5.5753" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6007" x2="0.6731" y2="-5.5753" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5753" x2="7.0231" y2="-5.5499" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5753" x2="0.6731" y2="-5.5499" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5499" x2="7.0231" y2="-5.5245" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5499" x2="0.6731" y2="-5.5245" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5245" x2="7.0231" y2="-5.4991" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5245" x2="0.6731" y2="-5.4991" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4991" x2="7.0231" y2="-5.4737" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4991" x2="0.6731" y2="-5.4737" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4737" x2="7.0231" y2="-5.4483" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4737" x2="0.6731" y2="-5.4483" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4483" x2="7.0231" y2="-5.4229" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4483" x2="0.6731" y2="-5.4229" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4229" x2="7.0231" y2="-5.3975" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4229" x2="0.6731" y2="-5.3975" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3975" x2="7.0231" y2="-5.3721" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3975" x2="0.6731" y2="-5.3721" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3721" x2="7.0231" y2="-5.3467" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3721" x2="0.6731" y2="-5.3467" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3467" x2="7.0231" y2="-5.3213" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3467" x2="0.6731" y2="-5.3213" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3213" x2="7.0231" y2="-5.2959" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3213" x2="0.6731" y2="-5.2959" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2959" x2="7.0231" y2="-5.2705" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2959" x2="0.6731" y2="-5.2705" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2705" x2="7.0231" y2="-5.2451" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2705" x2="0.6731" y2="-5.2451" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2451" x2="7.0231" y2="-5.2197" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2451" x2="0.6731" y2="-5.2197" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2197" x2="7.0231" y2="-5.1943" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.2197" x2="0.6731" y2="-5.1943" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1943" x2="7.0231" y2="-5.1689" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1943" x2="0.6731" y2="-5.1689" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1689" x2="7.0231" y2="-5.1435" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1689" x2="0.6731" y2="-5.1435" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1435" x2="7.0231" y2="-5.1181" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1435" x2="0.6731" y2="-5.1181" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1181" x2="7.0231" y2="-5.0927" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1181" x2="0.6731" y2="-5.0927" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0927" x2="7.0231" y2="-5.0673" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0927" x2="0.6731" y2="-5.0673" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0673" x2="7.0231" y2="-5.0419" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0673" x2="0.6731" y2="-5.0419" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0419" x2="7.0231" y2="-5.0165" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0419" x2="0.6731" y2="-5.0165" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0165" x2="6.9977" y2="-4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0165" x2="0.6731" y2="-4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-4.9911" x2="0.6731" y2="-4.9657" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-4.9657" x2="0.5207" y2="-4.9403" layer="200" rot="R180"/>
<rectangle x1="4.3815" y1="-4.7879" x2="7.0739" y2="-4.7625" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-4.7879" x2="1.3335" y2="-4.7625" layer="200" rot="R180"/>
<rectangle x1="-4.4069" y1="-4.7625" x2="7.0993" y2="-4.7371" layer="200" rot="R180"/>
<rectangle x1="-4.4577" y1="-4.7371" x2="7.0993" y2="-4.7117" layer="200" rot="R180"/>
<rectangle x1="-4.5085" y1="-4.7117" x2="7.0993" y2="-4.6863" layer="200" rot="R180"/>
<rectangle x1="-4.5593" y1="-4.6863" x2="7.0993" y2="-4.6609" layer="200" rot="R180"/>
<rectangle x1="-4.5847" y1="-4.6609" x2="7.0993" y2="-4.6355" layer="200" rot="R180"/>
<rectangle x1="-4.6355" y1="-4.6355" x2="7.0993" y2="-4.6101" layer="200" rot="R180"/>
<rectangle x1="-4.6609" y1="-4.6101" x2="7.0993" y2="-4.5847" layer="200" rot="R180"/>
<rectangle x1="-4.6863" y1="-4.5847" x2="7.0993" y2="-4.5593" layer="200" rot="R180"/>
<rectangle x1="-4.7117" y1="-4.5593" x2="7.0739" y2="-4.5339" layer="200" rot="R180"/>
<rectangle x1="-4.7371" y1="-4.5339" x2="7.0739" y2="-4.5085" layer="200" rot="R180"/>
<rectangle x1="-4.7625" y1="-4.5085" x2="7.0739" y2="-4.4831" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="-4.4831" x2="7.0739" y2="-4.4577" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="-4.4577" x2="7.0739" y2="-4.4323" layer="200" rot="R180"/>
<rectangle x1="-4.8133" y1="-4.4323" x2="7.5057" y2="-4.4069" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="-4.4069" x2="9.7155" y2="-4.3815" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="-4.3815" x2="9.7663" y2="-4.3561" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="-4.3561" x2="9.7917" y2="-4.3307" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="-4.3307" x2="9.8171" y2="-4.3053" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="-4.3053" x2="9.8425" y2="-4.2799" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="-4.2799" x2="9.8679" y2="-4.2545" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2545" x2="9.8933" y2="-4.2291" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2291" x2="9.8933" y2="-4.2037" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2037" x2="9.8933" y2="-4.1783" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1783" x2="9.8933" y2="-4.1529" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1529" x2="9.8933" y2="-4.1275" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1275" x2="9.8933" y2="-4.1021" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.1021" x2="9.8933" y2="-4.0767" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0767" x2="9.8933" y2="-4.0513" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0513" x2="9.8933" y2="-4.0259" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0259" x2="9.8933" y2="-4.0005" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0005" x2="9.8933" y2="-3.9751" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9751" x2="9.8933" y2="-3.9497" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9497" x2="9.8933" y2="-3.9243" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9243" x2="9.8933" y2="-3.8989" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.8989" x2="9.8933" y2="-3.8735" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.8735" x2="9.8933" y2="-3.8481" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.8481" x2="9.8933" y2="-3.8227" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.8227" x2="9.8933" y2="-3.7973" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7973" x2="9.8933" y2="-3.7719" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7719" x2="9.8933" y2="-3.7465" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7465" x2="9.8933" y2="-3.7211" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7211" x2="9.8933" y2="-3.6957" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6957" x2="9.8933" y2="-3.6703" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6703" x2="9.8933" y2="-3.6449" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6449" x2="9.8933" y2="-3.6195" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6195" x2="9.8933" y2="-3.5941" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5941" x2="9.8933" y2="-3.5687" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5687" x2="9.8933" y2="-3.5433" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5433" x2="9.8933" y2="-3.5179" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5179" x2="9.8933" y2="-3.4925" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4925" x2="9.8933" y2="-3.4671" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4671" x2="9.8933" y2="-3.4417" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4417" x2="9.8933" y2="-3.4163" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4163" x2="9.8933" y2="-3.3909" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3909" x2="9.8933" y2="-3.3655" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3655" x2="9.8933" y2="-3.3401" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3401" x2="9.8933" y2="-3.3147" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3147" x2="9.8933" y2="-3.2893" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2893" x2="9.8933" y2="-3.2639" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2639" x2="9.8933" y2="-3.2385" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2385" x2="9.8933" y2="-3.2131" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2131" x2="9.8933" y2="-3.1877" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1877" x2="9.8933" y2="-3.1623" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1623" x2="9.8933" y2="-3.1369" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1369" x2="9.8933" y2="-3.1115" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1115" x2="9.8933" y2="-3.0861" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0861" x2="9.8933" y2="-3.0607" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0607" x2="9.8933" y2="-3.0353" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0353" x2="9.8933" y2="-3.0099" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0099" x2="9.8933" y2="-2.9845" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9845" x2="9.8933" y2="-2.9591" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9591" x2="9.8933" y2="-2.9337" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9337" x2="9.8933" y2="-2.9083" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9083" x2="9.8933" y2="-2.8829" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8829" x2="9.8933" y2="-2.8575" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8575" x2="9.8933" y2="-2.8321" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8321" x2="9.8933" y2="-2.8067" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8067" x2="9.8933" y2="-2.7813" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7813" x2="9.8933" y2="-2.7559" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7559" x2="9.8933" y2="-2.7305" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7305" x2="9.8933" y2="-2.7051" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7051" x2="9.8933" y2="-2.6797" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6797" x2="9.8933" y2="-2.6543" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6543" x2="9.8933" y2="-2.6289" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6289" x2="9.8933" y2="-2.6035" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6035" x2="9.8933" y2="-2.5781" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5781" x2="9.8933" y2="-2.5527" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5527" x2="9.8933" y2="-2.5273" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5273" x2="9.8933" y2="-2.5019" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5019" x2="9.8933" y2="-2.4765" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4765" x2="9.8933" y2="-2.4511" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4511" x2="9.8933" y2="-2.4257" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4257" x2="9.8933" y2="-2.4003" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4003" x2="9.8933" y2="-2.3749" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3749" x2="9.8933" y2="-2.3495" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3495" x2="9.8933" y2="-2.3241" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3241" x2="9.8933" y2="-2.2987" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2987" x2="9.8933" y2="-2.2733" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2733" x2="9.8933" y2="-2.2479" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2479" x2="9.8933" y2="-2.2225" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2225" x2="9.8933" y2="-2.1971" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1971" x2="9.8933" y2="-2.1717" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1717" x2="9.8933" y2="-2.1463" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1463" x2="9.8933" y2="-2.1209" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1209" x2="9.8933" y2="-2.0955" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0955" x2="9.8933" y2="-2.0701" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0701" x2="9.8933" y2="-2.0447" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0447" x2="9.8933" y2="-2.0193" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0193" x2="9.8933" y2="-1.9939" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9939" x2="9.8933" y2="-1.9685" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9685" x2="9.8933" y2="-1.9431" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9431" x2="9.8933" y2="-1.9177" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9177" x2="9.8933" y2="-1.8923" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8923" x2="9.8933" y2="-1.8669" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8669" x2="9.8933" y2="-1.8415" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8415" x2="9.8933" y2="-1.8161" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8161" x2="9.8933" y2="-1.7907" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7907" x2="9.8933" y2="-1.7653" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7653" x2="9.8933" y2="-1.7399" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7399" x2="9.8933" y2="-1.7145" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7145" x2="9.8933" y2="-1.6891" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6891" x2="9.8933" y2="-1.6637" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6637" x2="9.8933" y2="-1.6383" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6383" x2="9.8679" y2="-1.6129" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6129" x2="8.8773" y2="-1.5875" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5875" x2="8.8773" y2="-1.5621" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5621" x2="8.8773" y2="-1.5367" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5367" x2="8.8773" y2="-1.5113" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5113" x2="8.8773" y2="-1.4859" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4859" x2="8.8773" y2="-1.4605" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4605" x2="8.8773" y2="-1.4351" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4351" x2="8.8773" y2="-1.4097" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4097" x2="8.8773" y2="-1.3843" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3843" x2="8.8773" y2="-1.3589" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3589" x2="8.8773" y2="-1.3335" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3335" x2="8.8773" y2="-1.3081" layer="200" rot="R180"/>
<rectangle x1="4.8133" y1="-1.3081" x2="8.8773" y2="-1.2827" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3081" x2="4.3815" y2="-1.2827" layer="200" rot="R180"/>
<rectangle x1="4.9149" y1="-1.2827" x2="8.8773" y2="-1.2573" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2827" x2="4.2799" y2="-1.2573" layer="200" rot="R180"/>
<rectangle x1="5.0165" y1="-1.2573" x2="8.8773" y2="-1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2573" x2="4.1783" y2="-1.2319" layer="200" rot="R180"/>
<rectangle x1="5.0927" y1="-1.2319" x2="8.8773" y2="-1.2065" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2319" x2="4.1275" y2="-1.2065" layer="200" rot="R180"/>
<rectangle x1="5.1435" y1="-1.2065" x2="8.8773" y2="-1.1811" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2065" x2="4.0513" y2="-1.1811" layer="200" rot="R180"/>
<rectangle x1="5.1943" y1="-1.1811" x2="8.8773" y2="-1.1557" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1811" x2="4.0005" y2="-1.1557" layer="200" rot="R180"/>
<rectangle x1="5.2197" y1="-1.1557" x2="8.8773" y2="-1.1303" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1557" x2="3.9497" y2="-1.1303" layer="200" rot="R180"/>
<rectangle x1="5.2705" y1="-1.1303" x2="8.8773" y2="-1.1049" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1303" x2="3.8989" y2="-1.1049" layer="200" rot="R180"/>
<rectangle x1="5.2959" y1="-1.1049" x2="8.8773" y2="-1.0795" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1049" x2="3.8735" y2="-1.0795" layer="200" rot="R180"/>
<rectangle x1="5.3467" y1="-1.0795" x2="8.8773" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="0.1143" y1="-1.0795" x2="3.8227" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0795" x2="-0.1143" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="5.3721" y1="-1.0541" x2="8.8773" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="0.2413" y1="-1.0541" x2="3.7973" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0541" x2="-0.2413" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="-1.0287" x2="8.8773" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="0.3429" y1="-1.0287" x2="3.7465" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0287" x2="-0.3429" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="5.4229" y1="-1.0033" x2="8.8773" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="0.4191" y1="-1.0033" x2="3.7211" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0033" x2="-0.4191" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-0.9779" x2="8.8773" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="0.4699" y1="-0.9779" x2="3.6957" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9779" x2="-0.4699" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-0.9525" x2="8.8773" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="0.5207" y1="-0.9525" x2="3.6703" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9525" x2="-0.5207" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="-0.9271" x2="8.8773" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="0.5715" y1="-0.9271" x2="3.6449" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9271" x2="-0.5715" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="-0.9017" x2="8.8773" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="0.6223" y1="-0.9017" x2="3.6195" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9017" x2="-0.6223" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="-0.8763" x2="8.8773" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="0.6477" y1="-0.8763" x2="3.5941" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8763" x2="-0.6477" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="-0.8509" x2="8.8773" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="0.6985" y1="-0.8509" x2="3.5687" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8509" x2="-0.6985" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-0.8255" x2="8.8773" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="0.7239" y1="-0.8255" x2="3.5433" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8255" x2="-0.7239" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-0.8001" x2="8.8773" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="0.7493" y1="-0.8001" x2="3.5179" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8001" x2="-0.7493" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="-0.7747" x2="8.8773" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="-0.7747" x2="3.4925" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7747" x2="-0.7747" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="-0.7493" x2="8.8773" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="0.8255" y1="-0.7493" x2="3.4925" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7493" x2="-0.8001" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="-0.7239" x2="8.8773" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="0.8509" y1="-0.7239" x2="3.4671" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7239" x2="-0.8255" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="-0.6985" x2="8.8773" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="-0.6985" x2="3.4671" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6985" x2="-0.8509" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="-0.6731" x2="8.8773" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="-0.6731" x2="3.4417" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6731" x2="-0.8763" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="-0.6477" x2="8.8773" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="-0.6477" x2="3.4417" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6477" x2="-0.9017" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="-0.6223" x2="8.8773" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="-0.6223" x2="3.4163" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6223" x2="-0.9017" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="-0.5969" x2="8.8773" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="-0.5969" x2="3.4163" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5969" x2="-0.9271" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="-0.5715" x2="8.8773" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="-0.5715" x2="3.3909" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5715" x2="-0.9525" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="-0.5461" x2="8.8773" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="-0.5461" x2="3.3655" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5461" x2="-0.9525" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="-0.5207" x2="8.8773" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="-0.5207" x2="3.3655" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5207" x2="-0.9779" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4953" x2="8.8773" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="-0.4953" x2="3.3401" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4953" x2="-0.9779" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4699" x2="8.8773" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="-0.4699" x2="3.3401" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4699" x2="-1.0033" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4445" x2="8.8773" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="-0.4445" x2="3.3401" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4445" x2="-1.0033" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.4191" x2="8.8773" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.4191" x2="3.3147" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4191" x2="-1.0033" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3937" x2="8.8773" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.3937" x2="3.3147" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3937" x2="-1.0033" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3683" x2="8.8773" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.3683" x2="3.3147" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3683" x2="-1.0287" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3429" x2="8.8773" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.3429" x2="3.3147" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3429" x2="-1.0287" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.3175" x2="8.8773" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.3175" x2="3.3147" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3175" x2="-1.0287" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2921" x2="8.8773" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2921" x2="3.2893" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2921" x2="-1.0287" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2667" x2="8.8773" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2667" x2="3.2893" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2667" x2="-1.0287" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2413" x2="8.8773" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2413" x2="3.2893" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2413" x2="-1.0287" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2159" x2="8.8773" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.2159" x2="3.2893" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2159" x2="-1.0541" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1905" x2="8.8773" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1905" x2="3.2893" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1905" x2="-1.0541" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1651" x2="8.8773" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1651" x2="3.2893" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1651" x2="-1.0541" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1397" x2="8.8773" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1397" x2="3.2893" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1397" x2="-1.0541" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1143" x2="8.8773" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1143" x2="3.2893" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1143" x2="-1.0541" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0889" x2="8.8773" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0889" x2="3.2893" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0889" x2="-1.0541" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0635" x2="8.8773" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0635" x2="3.2893" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0635" x2="-1.0541" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0381" x2="8.8773" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0381" x2="3.2893" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0381" x2="-1.0541" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0127" x2="8.8773" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0127" x2="3.2893" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0127" x2="-1.0541" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0127" x2="8.8773" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0127" x2="3.2893" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0127" x2="-1.0541" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0381" x2="8.8773" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0381" x2="3.2893" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0381" x2="-1.0541" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0635" x2="8.8773" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0635" x2="3.2893" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0635" x2="-1.0541" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0889" x2="8.8773" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0889" x2="3.2893" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0889" x2="-1.0541" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1143" x2="8.8773" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.1143" x2="3.2893" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1143" x2="-1.0541" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1397" x2="8.8773" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1397" x2="3.2893" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1397" x2="-1.0541" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1651" x2="8.8773" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1651" x2="3.3147" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1651" x2="-1.0541" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1905" x2="8.8773" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1905" x2="3.3147" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1905" x2="-1.0541" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.2159" x2="8.9027" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.2159" x2="3.3147" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2159" x2="-1.0541" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2413" x2="8.9027" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.2413" x2="3.3147" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2413" x2="-1.0541" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2667" x2="8.9027" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.2667" x2="3.3401" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2667" x2="-1.0541" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2921" x2="8.9027" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.2921" x2="3.3401" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2921" x2="-1.0287" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="0.3175" x2="8.9027" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.3175" x2="3.3401" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3175" x2="-1.0287" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="0.3429" x2="8.9027" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.3429" x2="3.3655" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3429" x2="-1.0287" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="0.3683" x2="8.9027" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.3683" x2="3.3909" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3683" x2="-1.0287" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="0.3937" x2="8.9027" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.3937" x2="3.3655" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3937" x2="-1.0287" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4191" x2="8.9027" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.4191" x2="3.3909" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4191" x2="-1.0033" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4445" x2="8.9027" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="0.4445" x2="3.4163" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4445" x2="-1.0033" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4699" x2="8.9027" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="0.4699" x2="3.4163" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4699" x2="-1.0033" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="0.4953" x2="8.9027" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="0.4953" x2="3.4163" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4953" x2="-0.9779" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="0.5207" x2="8.9027" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="0.5207" x2="3.4417" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5207" x2="-0.9779" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="0.5461" x2="8.9027" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="0.5461" x2="3.4417" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5461" x2="-0.9525" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="0.5715" x2="8.9027" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="0.5715" x2="3.4671" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5715" x2="-0.9525" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="0.5969" x2="8.9027" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="0.5969" x2="3.4671" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5969" x2="-0.9271" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="0.6223" x2="8.9027" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="0.6223" x2="3.4925" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6223" x2="-0.9271" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="0.6477" x2="8.9027" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="0.6477" x2="3.4925" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6477" x2="-0.9017" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="0.6731" x2="8.9027" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="0.6731" x2="3.5179" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6731" x2="-0.8763" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="0.6985" x2="8.9027" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="0.6985" x2="3.5433" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6985" x2="-0.8763" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7239" x2="8.9027" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="0.8509" y1="0.7239" x2="3.5433" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7239" x2="-0.8509" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7493" x2="8.9027" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="0.8255" y1="0.7493" x2="3.5687" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7493" x2="-0.8255" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7747" x2="8.9027" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="0.7747" x2="3.5941" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7747" x2="-0.8001" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="0.8001" x2="8.9027" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="0.8001" x2="3.6195" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8001" x2="-0.7747" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="0.8255" x2="8.9027" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="0.7747" y1="0.8255" x2="3.6195" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8255" x2="-0.7493" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="0.8509" x2="8.9027" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="0.7493" y1="0.8509" x2="3.6449" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8509" x2="-0.7239" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="0.8763" x2="8.9027" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="0.7239" y1="0.8763" x2="3.6703" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8763" x2="-0.6731" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="5.4229" y1="0.9017" x2="8.9027" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="0.6731" y1="0.9017" x2="3.6957" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9017" x2="-0.6477" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="0.9271" x2="8.9027" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="0.6477" y1="0.9271" x2="3.7211" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9271" x2="-0.5969" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="0.9525" x2="8.9027" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="0.5969" y1="0.9525" x2="3.7465" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9525" x2="-0.5461" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="5.3213" y1="0.9779" x2="8.9027" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="0.5715" y1="0.9779" x2="3.7973" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9779" x2="-0.4953" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="5.2959" y1="1.0033" x2="8.9027" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="0.5207" y1="1.0033" x2="3.8227" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0033" x2="-0.4191" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="5.2705" y1="1.0287" x2="8.9027" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="0.4445" y1="1.0287" x2="3.8481" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0287" x2="-0.3429" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="5.2197" y1="1.0541" x2="8.9027" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="0.3937" y1="1.0541" x2="3.8989" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0541" x2="-0.2413" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="5.1689" y1="1.0795" x2="8.9027" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="0.2921" y1="1.0795" x2="3.9497" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0795" x2="-0.0889" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="5.1181" y1="1.1049" x2="8.9027" y2="1.1303" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1049" x2="4.0005" y2="1.1303" layer="200" rot="R180"/>
<rectangle x1="5.0673" y1="1.1303" x2="8.9027" y2="1.1557" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1303" x2="4.0513" y2="1.1557" layer="200" rot="R180"/>
<rectangle x1="5.0165" y1="1.1557" x2="8.9027" y2="1.1811" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1557" x2="4.1275" y2="1.1811" layer="200" rot="R180"/>
<rectangle x1="4.9149" y1="1.1811" x2="8.9027" y2="1.2065" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1811" x2="4.2037" y2="1.2065" layer="200" rot="R180"/>
<rectangle x1="4.7625" y1="1.2065" x2="8.9027" y2="1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2065" x2="4.3307" y2="1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2319" x2="8.9027" y2="1.2573" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2573" x2="8.9027" y2="1.2827" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2827" x2="8.9027" y2="1.3081" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3081" x2="8.9027" y2="1.3335" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3335" x2="8.9027" y2="1.3589" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3589" x2="8.9027" y2="1.3843" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3843" x2="8.9027" y2="1.4097" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4097" x2="8.9027" y2="1.4351" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4351" x2="8.9027" y2="1.4605" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4605" x2="8.9027" y2="1.4859" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4859" x2="8.9027" y2="1.5113" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5113" x2="8.9027" y2="1.5367" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5367" x2="8.9027" y2="1.5621" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5621" x2="8.9027" y2="1.5875" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5875" x2="8.9027" y2="1.6129" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6129" x2="8.9027" y2="1.6383" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6383" x2="8.9027" y2="1.6637" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6637" x2="8.9027" y2="1.6891" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6891" x2="8.9027" y2="1.7145" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7145" x2="8.9027" y2="1.7399" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7399" x2="8.9027" y2="1.7653" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7653" x2="8.9027" y2="1.7907" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7907" x2="8.9027" y2="1.8161" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8161" x2="8.9027" y2="1.8415" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8415" x2="8.9027" y2="1.8669" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8669" x2="8.9027" y2="1.8923" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8923" x2="8.9027" y2="1.9177" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9177" x2="8.9027" y2="1.9431" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9431" x2="8.9027" y2="1.9685" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9685" x2="8.9027" y2="1.9939" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9939" x2="8.9027" y2="2.0193" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="2.0193" x2="8.9027" y2="2.0447" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0447" x2="8.9027" y2="2.0701" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0701" x2="8.9027" y2="2.0955" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0955" x2="8.9027" y2="2.1209" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1209" x2="8.9027" y2="2.1463" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1463" x2="8.9281" y2="2.1717" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1717" x2="8.9281" y2="2.1971" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1971" x2="8.9281" y2="2.2225" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2225" x2="8.9281" y2="2.2479" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2479" x2="8.9281" y2="2.2733" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2733" x2="8.9281" y2="2.2987" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2987" x2="8.9281" y2="2.3241" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3241" x2="8.9281" y2="2.3495" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3495" x2="8.9281" y2="2.3749" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3749" x2="8.9281" y2="2.4003" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4003" x2="8.9281" y2="2.4257" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4257" x2="8.9281" y2="2.4511" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4511" x2="8.9281" y2="2.4765" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4765" x2="8.9281" y2="2.5019" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5019" x2="8.9281" y2="2.5273" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5273" x2="8.9281" y2="2.5527" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5527" x2="8.9281" y2="2.5781" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5781" x2="8.9281" y2="2.6035" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6035" x2="8.9281" y2="2.6289" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6289" x2="8.9281" y2="2.6543" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6543" x2="8.9281" y2="2.6797" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6797" x2="8.9281" y2="2.7051" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7051" x2="8.9281" y2="2.7305" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7305" x2="8.9281" y2="2.7559" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7559" x2="8.9281" y2="2.7813" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7813" x2="8.9281" y2="2.8067" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8067" x2="8.9281" y2="2.8321" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8321" x2="8.9281" y2="2.8575" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8575" x2="8.9281" y2="2.8829" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8829" x2="8.9281" y2="2.9083" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9083" x2="8.9281" y2="2.9337" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9337" x2="8.9281" y2="2.9591" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9591" x2="8.9281" y2="2.9845" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9845" x2="8.9281" y2="3.0099" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0099" x2="8.9281" y2="3.0353" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0353" x2="8.9281" y2="3.0607" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0607" x2="8.9281" y2="3.0861" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0861" x2="8.9281" y2="3.1115" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1115" x2="8.9281" y2="3.1369" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1369" x2="8.9281" y2="3.1623" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1623" x2="8.9281" y2="3.1877" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1877" x2="8.9281" y2="3.2131" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2131" x2="8.9281" y2="3.2385" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2385" x2="8.9281" y2="3.2639" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2639" x2="8.9281" y2="3.2893" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2893" x2="8.9281" y2="3.3147" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3147" x2="8.9281" y2="3.3401" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3401" x2="8.9281" y2="3.3655" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3655" x2="8.9281" y2="3.3909" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3909" x2="8.9281" y2="3.4163" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4163" x2="8.9281" y2="3.4417" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4417" x2="8.9281" y2="3.4671" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4671" x2="8.9281" y2="3.4925" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4925" x2="8.9281" y2="3.5179" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5179" x2="8.9281" y2="3.5433" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5433" x2="8.9281" y2="3.5687" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5687" x2="8.9281" y2="3.5941" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5941" x2="8.9281" y2="3.6195" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6195" x2="8.9281" y2="3.6449" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6449" x2="8.9281" y2="3.6703" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6703" x2="8.9281" y2="3.6957" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6957" x2="8.9281" y2="3.7211" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7211" x2="8.9281" y2="3.7465" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7465" x2="8.9281" y2="3.7719" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7719" x2="8.9281" y2="3.7973" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7973" x2="8.9281" y2="3.8227" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8227" x2="8.9281" y2="3.8481" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8481" x2="8.9281" y2="3.8735" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8735" x2="8.9281" y2="3.8989" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8989" x2="8.9281" y2="3.9243" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.9243" x2="8.9281" y2="3.9497" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="3.9497" x2="8.9281" y2="3.9751" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="3.9751" x2="8.9281" y2="4.0005" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0005" x2="8.9281" y2="4.0259" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0259" x2="8.9281" y2="4.0513" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0513" x2="8.9281" y2="4.0767" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0767" x2="8.9281" y2="4.1021" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.1021" x2="8.9281" y2="4.1275" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.1275" x2="8.9281" y2="4.1529" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="4.1529" x2="8.9281" y2="4.1783" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="4.1783" x2="8.9281" y2="4.2037" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="4.2037" x2="8.9027" y2="4.2291" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="4.2291" x2="8.9027" y2="4.2545" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="4.2545" x2="8.8773" y2="4.2799" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="4.2799" x2="8.8773" y2="4.3053" layer="200" rot="R180"/>
<rectangle x1="-4.8133" y1="4.3053" x2="8.8519" y2="4.3307" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="4.3307" x2="8.8265" y2="4.3561" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="4.3561" x2="8.8011" y2="4.3815" layer="200" rot="R180"/>
<rectangle x1="-4.7625" y1="4.3815" x2="8.7757" y2="4.4069" layer="200" rot="R180"/>
<rectangle x1="-4.7371" y1="4.4069" x2="8.7249" y2="4.4323" layer="200" rot="R180"/>
<rectangle x1="-4.7117" y1="4.4323" x2="8.6741" y2="4.4577" layer="200" rot="R180"/>
<rectangle x1="-4.6863" y1="4.4577" x2="8.6233" y2="4.4831" layer="200" rot="R180"/>
<rectangle x1="-4.6609" y1="4.4831" x2="7.1247" y2="4.5085" layer="200" rot="R180"/>
<rectangle x1="-4.6101" y1="4.5085" x2="7.1247" y2="4.5339" layer="200" rot="R180"/>
<rectangle x1="-4.5847" y1="4.5339" x2="7.1247" y2="4.5593" layer="200" rot="R180"/>
<rectangle x1="-4.5339" y1="4.5593" x2="7.1247" y2="4.5847" layer="200" rot="R180"/>
<rectangle x1="-4.4577" y1="4.5847" x2="7.1247" y2="4.6101" layer="200" rot="R180"/>
<rectangle x1="-4.3815" y1="4.6101" x2="6.9215" y2="4.6355" layer="200" rot="R180"/>
<rectangle x1="-3.0607" y1="4.6355" x2="-1.6637" y2="4.6609" layer="200" rot="R180"/>
<rectangle x1="-3.8989" y1="4.6355" x2="-3.3655" y2="4.6609" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="4.8133" x2="7.0739" y2="4.8387" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8387" x2="7.0993" y2="4.8641" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8641" x2="7.0993" y2="4.8895" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8895" x2="7.0993" y2="4.9149" layer="200" rot="R180"/>
<rectangle x1="-0.1651" y1="4.8895" x2="0.7239" y2="4.9149" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9149" x2="7.0993" y2="4.9403" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9149" x2="0.7239" y2="4.9403" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9403" x2="7.0993" y2="4.9657" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9403" x2="0.7239" y2="4.9657" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9657" x2="7.0993" y2="4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9657" x2="0.7239" y2="4.9911" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9911" x2="7.0993" y2="5.0165" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9911" x2="0.7239" y2="5.0165" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0165" x2="7.0993" y2="5.0419" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0165" x2="0.7239" y2="5.0419" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0419" x2="7.0993" y2="5.0673" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0419" x2="0.7239" y2="5.0673" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0673" x2="7.0993" y2="5.0927" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0673" x2="0.7239" y2="5.0927" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0927" x2="7.0993" y2="5.1181" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0927" x2="0.7239" y2="5.1181" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1181" x2="7.0993" y2="5.1435" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1181" x2="0.7239" y2="5.1435" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1435" x2="7.0993" y2="5.1689" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1435" x2="0.7239" y2="5.1689" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1689" x2="7.0993" y2="5.1943" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1689" x2="0.7239" y2="5.1943" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1943" x2="7.0993" y2="5.2197" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1943" x2="0.7239" y2="5.2197" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2197" x2="7.0993" y2="5.2451" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2197" x2="0.7239" y2="5.2451" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2451" x2="7.0993" y2="5.2705" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2451" x2="0.7239" y2="5.2705" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2705" x2="7.0993" y2="5.2959" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2705" x2="0.7239" y2="5.2959" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2959" x2="7.0739" y2="5.3213" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2959" x2="0.7239" y2="5.3213" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3213" x2="7.0739" y2="5.3467" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3213" x2="0.7239" y2="5.3467" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3467" x2="7.0739" y2="5.3721" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3467" x2="0.7239" y2="5.3721" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3721" x2="7.0739" y2="5.3975" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3721" x2="0.7239" y2="5.3975" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3975" x2="7.0739" y2="5.4229" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3975" x2="0.7239" y2="5.4229" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4229" x2="7.0739" y2="5.4483" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4229" x2="0.7239" y2="5.4483" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4483" x2="7.0739" y2="5.4737" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4483" x2="0.7239" y2="5.4737" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4737" x2="7.0739" y2="5.4991" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4737" x2="0.7239" y2="5.4991" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4991" x2="7.0739" y2="5.5245" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4991" x2="0.7239" y2="5.5245" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5245" x2="7.0739" y2="5.5499" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5245" x2="0.7239" y2="5.5499" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5499" x2="7.0739" y2="5.5753" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5499" x2="0.7239" y2="5.5753" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5753" x2="7.0739" y2="5.6007" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5753" x2="0.7239" y2="5.6007" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6007" x2="7.0739" y2="5.6261" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6007" x2="0.7239" y2="5.6261" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6261" x2="7.0739" y2="5.6515" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6261" x2="0.7239" y2="5.6515" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6515" x2="7.0739" y2="5.6769" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6515" x2="0.7239" y2="5.6769" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6769" x2="7.0739" y2="5.7023" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6769" x2="0.7239" y2="5.7023" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7023" x2="7.0739" y2="5.7277" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7023" x2="0.7239" y2="5.7277" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7277" x2="7.0739" y2="5.7531" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7277" x2="0.7239" y2="5.7531" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7531" x2="7.0739" y2="5.7785" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7531" x2="0.7239" y2="5.7785" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7785" x2="7.0739" y2="5.8039" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7785" x2="0.7239" y2="5.8039" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8039" x2="7.0739" y2="5.8293" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8039" x2="0.7239" y2="5.8293" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8293" x2="7.0739" y2="5.8547" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8293" x2="0.7239" y2="5.8547" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8547" x2="7.0739" y2="5.8801" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8547" x2="0.7239" y2="5.8801" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8801" x2="7.0739" y2="5.9055" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8801" x2="0.7239" y2="5.9055" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9055" x2="7.0739" y2="5.9309" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9055" x2="0.6985" y2="5.9309" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9309" x2="7.0739" y2="5.9563" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9309" x2="0.6985" y2="5.9563" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9563" x2="7.0739" y2="5.9817" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9563" x2="0.6985" y2="5.9817" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9817" x2="7.0739" y2="6.0071" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9817" x2="0.6985" y2="6.0071" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0071" x2="7.0739" y2="6.0325" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0071" x2="0.6985" y2="6.0325" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0325" x2="7.0739" y2="6.0579" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0325" x2="0.6985" y2="6.0579" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0579" x2="7.0739" y2="6.0833" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0579" x2="0.6985" y2="6.0833" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0833" x2="7.0739" y2="6.1087" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0833" x2="0.6985" y2="6.1087" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1087" x2="7.0739" y2="6.1341" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1087" x2="0.6985" y2="6.1341" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1341" x2="7.0739" y2="6.1595" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1341" x2="0.6985" y2="6.1595" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1595" x2="7.0739" y2="6.1849" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1595" x2="0.6985" y2="6.1849" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.1849" x2="7.0739" y2="6.2103" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1849" x2="0.6985" y2="6.2103" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2103" x2="7.0485" y2="6.2357" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2103" x2="0.6985" y2="6.2357" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2357" x2="7.0485" y2="6.2611" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2357" x2="0.6985" y2="6.2611" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2611" x2="7.0485" y2="6.2865" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2611" x2="0.6985" y2="6.2865" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="6.2865" x2="7.0231" y2="6.3119" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="6.2865" x2="0.6731" y2="6.3119" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="6.3119" x2="7.0231" y2="6.3373" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="6.3119" x2="0.6731" y2="6.3373" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="6.3373" x2="6.9977" y2="6.3627" layer="200" rot="R180"/>
<rectangle x1="-0.7239" y1="6.3373" x2="0.6477" y2="6.3627" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="6.3627" x2="6.9723" y2="6.3881" layer="200" rot="R180"/>
<rectangle x1="-0.6985" y1="6.3627" x2="0.6223" y2="6.3881" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="6.3881" x2="6.9215" y2="6.4135" layer="200" rot="R180"/>
<rectangle x1="-0.6223" y1="6.3881" x2="0.5461" y2="6.4135" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="6.4135" x2="6.8707" y2="6.4389" layer="200" rot="R180"/>
<text x="9.906" y="-6.8072" size="0.0508" layer="200" font="vector" rot="MR0">//kentro/work/Production/AOI Parts/Tyler/Breadboard power 5v/barreljack.bmp</text>
</package>
<package name="POWER_JACK_PTH_BREAD">
<description>&lt;h1&gt;DC Barrel Jack Adapter - Breadboard Compatible&lt;/h1&gt;
&lt;h2&gt;PRT-10811&lt;/h2&gt;</description>
<wire x1="4.5" y1="14.6" x2="0.9" y2="14.6" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3.6" x2="-4.5" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="0.1" x2="4.5" y2="3.4" width="0.2032" layer="51"/>
<wire x1="4.5" y1="0.1" x2="-4.5" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="3.6" x2="4.5" y2="9.5" width="0.2032" layer="21"/>
<wire x1="4.5" y1="14.6" x2="4.5" y2="11.9" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3.6" x2="-4.5" y2="14.6" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="14.6" x2="-0.9" y2="14.6" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3.6" x2="4.5" y2="3.6" width="0.2032" layer="21"/>
<pad name="PWR" x="0" y="13.7" drill="1.3"/>
<pad name="GND" x="0" y="7.9" drill="1.3"/>
<pad name="GNDBREAK" x="4.8" y="10.7" drill="1.3" rot="R90"/>
<text x="-3.81" y="5.08" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="4.06" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="PJ-047AH">
<wire x1="-5.8" y1="-3.5" x2="5.8" y2="-3.5" width="0.127" layer="48"/>
<wire x1="5.8" y1="-3.5" x2="5.8" y2="3.5" width="0.127" layer="51"/>
<wire x1="5.8" y1="3.5" x2="-5.8" y2="3.5" width="0.127" layer="48"/>
<wire x1="-5.8" y1="3.5" x2="-5.8" y2="-3.5" width="0.127" layer="21"/>
<pad name="P$1" x="-2.9" y="-3.65" drill="1.9" diameter="3.2"/>
<pad name="P$2" x="-2.9" y="3.65" drill="1.9" diameter="3.2"/>
<pad name="3" x="4.5" y="3.3" drill="1.6" diameter="2.8"/>
<pad name="1" x="4.5" y="-3.3" drill="1.6" diameter="2.8"/>
<pad name="2" x="2.3" y="0" drill="1.6" diameter="2.8"/>
<wire x1="-5.8" y1="3.5" x2="-4.6" y2="3.5" width="0.127" layer="21"/>
<wire x1="-1.2" y1="3.5" x2="3.1" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.1" y1="-3.5" x2="-1.2" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-4.6" y1="-3.5" x2="-5.8" y2="-3.5" width="0.127" layer="21"/>
<wire x1="5.8" y1="2.6" x2="5.8" y2="-2.6" width="0.127" layer="21"/>
<text x="7.2" y="-2.2" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<hole x="-1.8" y="0" drill="2.2"/>
</package>
<package name="NR8040T220M">
<description>&lt;b&gt;NR8040&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<text x="-1.019" y="0.137" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.019" y="0.137" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.2" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0.2" layer="51"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.2" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.2" layer="51"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.2" layer="21"/>
<wire x1="-4" y1="-4" x2="4" y2="-4" width="0.2" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.2" layer="21"/>
<wire x1="4" y1="4" x2="-4" y2="4" width="0.2" layer="21"/>
</package>
<package name="CAPC3225X270N">
<description>&lt;b&gt;1210&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.35" y="0" dx="2.72" dy="1.22" layer="1" rot="R90"/>
<smd name="2" x="1.35" y="0" dx="2.72" dy="1.22" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.11" y1="1.51" x2="2.11" y2="1.51" width="0.05" layer="51"/>
<wire x1="2.11" y1="1.51" x2="2.11" y2="-1.51" width="0.05" layer="51"/>
<wire x1="2.11" y1="-1.51" x2="-2.11" y2="-1.51" width="0.05" layer="51"/>
<wire x1="-2.11" y1="-1.51" x2="-2.11" y2="1.51" width="0.05" layer="51"/>
<wire x1="-1.6" y1="1.25" x2="1.6" y2="1.25" width="0.1" layer="51"/>
<wire x1="1.6" y1="1.25" x2="1.6" y2="-1.25" width="0.1" layer="51"/>
<wire x1="1.6" y1="-1.25" x2="-1.6" y2="-1.25" width="0.1" layer="51"/>
<wire x1="-1.6" y1="-1.25" x2="-1.6" y2="1.25" width="0.1" layer="51"/>
<circle x="0" y="0" radius="0.14141875" width="0.2" layer="21"/>
<circle x="0" y="0" radius="0.282840625" width="0.2" layer="21"/>
<circle x="0" y="0" radius="0.360553125" width="0.2" layer="21"/>
</package>
<package name="QFN40P300X300X90-21N">
<description>&lt;b&gt;20 VQFN 3x3&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.5" y="0.8" dx="0.8" dy="0.2" layer="1" cream="no"/>
<smd name="2" x="-1.5" y="0.4" dx="0.8" dy="0.2" layer="1" cream="no"/>
<smd name="3" x="-1.5" y="0" dx="0.8" dy="0.2" layer="1" cream="no"/>
<smd name="4" x="-1.5" y="-0.4" dx="0.8" dy="0.2" layer="1" cream="no"/>
<smd name="5" x="-1.5" y="-0.8" dx="0.8" dy="0.2" layer="1" cream="no"/>
<smd name="6" x="-0.8" y="-1.5" dx="0.8" dy="0.2" layer="1" rot="R90" cream="no"/>
<smd name="7" x="-0.4" y="-1.5" dx="0.8" dy="0.2" layer="1" rot="R90" cream="no"/>
<smd name="8" x="0" y="-1.5" dx="0.8" dy="0.2" layer="1" rot="R90" cream="no"/>
<smd name="9" x="0.4" y="-1.5" dx="0.8" dy="0.2" layer="1" rot="R90" cream="no"/>
<smd name="10" x="0.8" y="-1.5" dx="0.8" dy="0.2" layer="1" rot="R90" cream="no"/>
<smd name="11" x="1.5" y="-0.8" dx="0.8" dy="0.2" layer="1" cream="no"/>
<smd name="12" x="1.5" y="-0.4" dx="0.8" dy="0.2" layer="1" cream="no"/>
<smd name="13" x="1.5" y="0" dx="0.8" dy="0.2" layer="1" cream="no"/>
<smd name="14" x="1.5" y="0.4" dx="0.8" dy="0.2" layer="1" cream="no"/>
<smd name="15" x="1.5" y="0.8" dx="0.8" dy="0.2" layer="1" cream="no"/>
<smd name="16" x="0.8" y="1.5" dx="0.8" dy="0.2" layer="1" rot="R90" cream="no"/>
<smd name="17" x="0.4" y="1.5" dx="0.8" dy="0.2" layer="1" rot="R90" cream="no"/>
<smd name="18" x="0" y="1.5" dx="0.8" dy="0.2" layer="1" rot="R90" cream="no"/>
<smd name="19" x="-0.4" y="1.5" dx="0.8" dy="0.2" layer="1" rot="R90" cream="no"/>
<smd name="20" x="-0.8" y="1.5" dx="0.8" dy="0.2" layer="1" rot="R90" cream="no"/>
<smd name="21" x="0" y="0" dx="1.8" dy="1.8" layer="1" rot="R90" cream="no"/>
<text x="0.1" y="2.7" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.125" y1="2.125" x2="2.125" y2="2.125" width="0.05" layer="51"/>
<wire x1="2.125" y1="2.125" x2="2.125" y2="-2.125" width="0.05" layer="51"/>
<wire x1="2.125" y1="-2.125" x2="-2.125" y2="-2.125" width="0.05" layer="51"/>
<wire x1="-2.125" y1="-2.125" x2="-2.125" y2="2.125" width="0.05" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="1.1" x2="-1.1" y2="1.5" width="0.1" layer="51"/>
<circle x="-1.9" y="1.4" radius="0.1" width="0.2" layer="25"/>
<rectangle x1="-0.8" y1="0.2" x2="-0.2" y2="0.8" layer="31"/>
<rectangle x1="0.2" y1="0.2" x2="0.8" y2="0.8" layer="31"/>
<rectangle x1="-0.8" y1="-0.8" x2="-0.2" y2="-0.2" layer="31"/>
<rectangle x1="0.2" y1="-0.8" x2="0.8" y2="-0.2" layer="31"/>
<rectangle x1="-1.7" y1="0.7" x2="-1.1" y2="0.9" layer="31"/>
<rectangle x1="-1.7" y1="0.3" x2="-1.1" y2="0.5" layer="31"/>
<rectangle x1="-1.7" y1="-0.1" x2="-1.1" y2="0.1" layer="31"/>
<rectangle x1="-1.7" y1="-0.5" x2="-1.1" y2="-0.3" layer="31"/>
<rectangle x1="-1.7" y1="-0.9" x2="-1.1" y2="-0.7" layer="31"/>
<rectangle x1="-0.9" y1="1.1" x2="-0.7" y2="1.7" layer="31"/>
<rectangle x1="-0.5" y1="1.1" x2="-0.3" y2="1.7" layer="31"/>
<rectangle x1="-0.1" y1="1.1" x2="0.1" y2="1.7" layer="31"/>
<rectangle x1="0.3" y1="1.1" x2="0.5" y2="1.7" layer="31"/>
<rectangle x1="0.7" y1="1.1" x2="0.9" y2="1.7" layer="31"/>
<rectangle x1="1.1" y1="0.7" x2="1.7" y2="0.9" layer="31"/>
<rectangle x1="1.1" y1="0.3" x2="1.7" y2="0.5" layer="31"/>
<rectangle x1="1.1" y1="-0.1" x2="1.7" y2="0.1" layer="31"/>
<rectangle x1="1.1" y1="-0.5" x2="1.7" y2="-0.3" layer="31"/>
<rectangle x1="1.1" y1="-0.9" x2="1.7" y2="-0.7" layer="31"/>
<rectangle x1="0.7" y1="-1.7" x2="0.9" y2="-1.1" layer="31"/>
<rectangle x1="0.3" y1="-1.7" x2="0.5" y2="-1.1" layer="31"/>
<rectangle x1="-0.1" y1="-1.7" x2="0.1" y2="-1.1" layer="31"/>
<rectangle x1="-0.5" y1="-1.7" x2="-0.3" y2="-1.1" layer="31"/>
<rectangle x1="-0.9" y1="-1.7" x2="-0.7" y2="-1.1" layer="31"/>
</package>
<package name="SOIC127P600X175-8N">
<description>&lt;b&gt;8-Pin Narrow Body SOIC&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.712" y="1.905" dx="1.525" dy="0.7" layer="1"/>
<smd name="2" x="-2.712" y="0.635" dx="1.525" dy="0.7" layer="1"/>
<smd name="3" x="-2.712" y="-0.635" dx="1.525" dy="0.7" layer="1"/>
<smd name="4" x="-2.712" y="-1.905" dx="1.525" dy="0.7" layer="1"/>
<smd name="5" x="2.712" y="-1.905" dx="1.525" dy="0.7" layer="1"/>
<smd name="6" x="2.712" y="-0.635" dx="1.525" dy="0.7" layer="1"/>
<smd name="7" x="2.712" y="0.635" dx="1.525" dy="0.7" layer="1"/>
<smd name="8" x="2.712" y="1.905" dx="1.525" dy="0.7" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.725" y1="2.75" x2="3.725" y2="2.75" width="0.05" layer="51"/>
<wire x1="3.725" y1="2.75" x2="3.725" y2="-2.75" width="0.05" layer="51"/>
<wire x1="3.725" y1="-2.75" x2="-3.725" y2="-2.75" width="0.05" layer="51"/>
<wire x1="-3.725" y1="-2.75" x2="-3.725" y2="2.75" width="0.05" layer="51"/>
<wire x1="-1.95" y1="2.45" x2="1.95" y2="2.45" width="0.1" layer="51"/>
<wire x1="1.95" y1="2.45" x2="1.95" y2="-2.45" width="0.1" layer="51"/>
<wire x1="1.95" y1="-2.45" x2="-1.95" y2="-2.45" width="0.1" layer="51"/>
<wire x1="-1.95" y1="-2.45" x2="-1.95" y2="2.45" width="0.1" layer="51"/>
<wire x1="-1.95" y1="1.18" x2="-0.68" y2="2.45" width="0.1" layer="51"/>
<wire x1="-1.6" y1="2.45" x2="1.6" y2="2.45" width="0.2" layer="21"/>
<wire x1="1.6" y1="2.45" x2="1.6" y2="-2.45" width="0.2" layer="21"/>
<wire x1="1.6" y1="-2.45" x2="-1.6" y2="-2.45" width="0.2" layer="21"/>
<wire x1="-1.6" y1="-2.45" x2="-1.6" y2="2.45" width="0.2" layer="21"/>
<wire x1="-3.475" y1="2.605" x2="-1.95" y2="2.605" width="0.2" layer="21"/>
</package>
<package name="RFM0505S">
<description>&lt;b&gt;RFM&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="-3.81" y="-1.5" drill="1" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="-1.5" drill="1" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="-1.5" drill="1" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-1.5" drill="1" shape="long" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-5.75" y1="3.25" x2="5.75" y2="3.25" width="0.2" layer="51"/>
<wire x1="5.75" y1="3.25" x2="5.75" y2="-2.75" width="0.2" layer="51"/>
<wire x1="5.75" y1="-2.75" x2="-5.75" y2="-2.75" width="0.2" layer="51"/>
<wire x1="-5.75" y1="-2.75" x2="-5.75" y2="3.25" width="0.2" layer="51"/>
<wire x1="-5.75" y1="3.25" x2="5.75" y2="3.25" width="0.2" layer="21"/>
<wire x1="5.75" y1="3.25" x2="5.75" y2="-2.75" width="0.2" layer="21"/>
<wire x1="-5.75" y1="-2.75" x2="-5.75" y2="3.25" width="0.2" layer="21"/>
<wire x1="-6.75" y1="4.25" x2="6.75" y2="4.25" width="0.1" layer="51"/>
<wire x1="6.75" y1="4.25" x2="6.75" y2="-4.25" width="0.1" layer="51"/>
<wire x1="6.75" y1="-4.25" x2="-6.75" y2="-4.25" width="0.1" layer="51"/>
<wire x1="-6.75" y1="-4.25" x2="-6.75" y2="4.25" width="0.1" layer="51"/>
<wire x1="-5.2" y1="-3.15" x2="-5" y2="-3.15" width="0.254" layer="21" curve="-180"/>
<wire x1="-5" y1="-3.15" x2="-5.2" y2="-3.15" width="0.254" layer="21" curve="-180"/>
</package>
<package name="SOP250P700X270-4N">
<description>&lt;b&gt;MBS&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-3.025" y="1.25" dx="1.85" dy="0.95" layer="1"/>
<smd name="2" x="-3.025" y="-1.25" dx="1.85" dy="0.95" layer="1"/>
<smd name="3" x="3.025" y="-1.25" dx="1.85" dy="0.95" layer="1"/>
<smd name="4" x="3.025" y="1.25" dx="1.85" dy="0.95" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.2" y1="2.7" x2="4.2" y2="2.7" width="0.05" layer="51"/>
<wire x1="4.2" y1="2.7" x2="4.2" y2="-2.7" width="0.05" layer="51"/>
<wire x1="4.2" y1="-2.7" x2="-4.2" y2="-2.7" width="0.05" layer="51"/>
<wire x1="-4.2" y1="-2.7" x2="-4.2" y2="2.7" width="0.05" layer="51"/>
<wire x1="-2" y1="2.35" x2="2" y2="2.35" width="0.1" layer="51"/>
<wire x1="2" y1="2.35" x2="2" y2="-2.35" width="0.1" layer="51"/>
<wire x1="2" y1="-2.35" x2="-2" y2="-2.35" width="0.1" layer="51"/>
<wire x1="-2" y1="-2.35" x2="-2" y2="2.35" width="0.1" layer="51"/>
<wire x1="-2" y1="-0.15" x2="0.5" y2="2.35" width="0.1" layer="51"/>
<wire x1="-1.75" y1="2.35" x2="1.75" y2="2.35" width="0.2" layer="21"/>
<wire x1="1.75" y1="2.35" x2="1.75" y2="-2.35" width="0.2" layer="21"/>
<wire x1="1.75" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2" layer="21"/>
<wire x1="-1.75" y1="-2.35" x2="-1.75" y2="2.35" width="0.2" layer="21"/>
<wire x1="-3.95" y1="2.075" x2="-2.1" y2="2.075" width="0.2" layer="21"/>
</package>
<package name="PA3856005NLT">
<description>&lt;b&gt;PA3856.005NLT&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="5" y="-7.67" dx="2.4" dy="1.22" layer="1" rot="R90"/>
<smd name="2" x="2.5" y="-7.67" dx="2.4" dy="1.22" layer="1" rot="R90"/>
<smd name="3" x="0" y="-7.67" dx="2.4" dy="1.22" layer="1" rot="R90"/>
<smd name="4" x="-2.5" y="-7.67" dx="2.4" dy="1.22" layer="1" rot="R90"/>
<smd name="5" x="-5" y="-7.67" dx="2.4" dy="1.22" layer="1" rot="R90"/>
<smd name="6" x="-5" y="8.09" dx="2.4" dy="1.22" layer="1" rot="R90"/>
<smd name="7" x="-2.5" y="8.09" dx="2.4" dy="1.22" layer="1" rot="R90"/>
<smd name="8" x="0" y="8.09" dx="2.4" dy="1.22" layer="1" rot="R90"/>
<smd name="9" x="2.5" y="8.09" dx="2.4" dy="1.22" layer="1" rot="R90"/>
<smd name="10" x="5" y="8.09" dx="2.4" dy="1.22" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-7.25" y1="6.71" x2="7.25" y2="6.71" width="0.2" layer="51"/>
<wire x1="7.25" y1="6.71" x2="7.25" y2="-6.29" width="0.2" layer="51"/>
<wire x1="7.25" y1="-6.29" x2="-7.25" y2="-6.29" width="0.2" layer="51"/>
<wire x1="-7.25" y1="-6.29" x2="-7.25" y2="6.71" width="0.2" layer="51"/>
<wire x1="-7.25" y1="6.71" x2="7.25" y2="6.71" width="0.1" layer="21"/>
<wire x1="7.25" y1="6.71" x2="7.25" y2="-6.29" width="0.1" layer="21"/>
<wire x1="7.25" y1="-6.29" x2="-7.25" y2="-6.29" width="0.1" layer="21"/>
<wire x1="-7.25" y1="-6.29" x2="-7.25" y2="6.71" width="0.1" layer="21"/>
<wire x1="-8.25" y1="10.29" x2="8.25" y2="10.29" width="0.1" layer="51"/>
<wire x1="8.25" y1="10.29" x2="8.25" y2="-10.29" width="0.1" layer="51"/>
<wire x1="8.25" y1="-10.29" x2="-8.25" y2="-10.29" width="0.1" layer="51"/>
<wire x1="-8.25" y1="-10.29" x2="-8.25" y2="10.29" width="0.1" layer="51"/>
<wire x1="4.9" y1="-9.19" x2="4.9" y2="-9.19" width="0.2" layer="21"/>
<wire x1="4.9" y1="-9.19" x2="5.1" y2="-9.19" width="0.2" layer="21" curve="-180"/>
<wire x1="5.1" y1="-9.19" x2="5.1" y2="-9.19" width="0.2" layer="21"/>
<wire x1="5.1" y1="-9.19" x2="4.9" y2="-9.19" width="0.2" layer="21" curve="-180"/>
</package>
<package name="SOIC127P600X175-9N">
<description>&lt;b&gt;8-Lead SOP (Exposed Pad)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.714" y="1.905" dx="1.522" dy="0.7" layer="1"/>
<smd name="2" x="-2.714" y="0.635" dx="1.522" dy="0.7" layer="1"/>
<smd name="3" x="-2.714" y="-0.635" dx="1.522" dy="0.7" layer="1"/>
<smd name="4" x="-2.714" y="-1.905" dx="1.522" dy="0.7" layer="1"/>
<smd name="5" x="2.714" y="-1.905" dx="1.522" dy="0.7" layer="1"/>
<smd name="6" x="2.714" y="-0.635" dx="1.522" dy="0.7" layer="1"/>
<smd name="7" x="2.714" y="0.635" dx="1.522" dy="0.7" layer="1"/>
<smd name="8" x="2.714" y="1.905" dx="1.522" dy="0.7" layer="1"/>
<smd name="9" x="0" y="0" dx="2.3" dy="2.3" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.725" y1="2.752" x2="3.725" y2="2.752" width="0.05" layer="51"/>
<wire x1="3.725" y1="2.752" x2="3.725" y2="-2.752" width="0.05" layer="51"/>
<wire x1="3.725" y1="-2.752" x2="-3.725" y2="-2.752" width="0.05" layer="51"/>
<wire x1="-3.725" y1="-2.752" x2="-3.725" y2="2.752" width="0.05" layer="51"/>
<wire x1="-1.952" y1="2.451" x2="1.952" y2="2.451" width="0.1" layer="51"/>
<wire x1="1.952" y1="2.451" x2="1.952" y2="-2.451" width="0.1" layer="51"/>
<wire x1="1.952" y1="-2.451" x2="-1.952" y2="-2.451" width="0.1" layer="51"/>
<wire x1="-1.952" y1="-2.451" x2="-1.952" y2="2.451" width="0.1" layer="51"/>
<wire x1="-1.952" y1="1.181" x2="-0.682" y2="2.451" width="0.1" layer="51"/>
<wire x1="-3.475" y1="2.605" x2="-1.952" y2="2.605" width="0.2" layer="21"/>
</package>
<package name="SSOP6-P-0.65A(US6)">
<description>&lt;b&gt;SSOP6-P-0.65A (US6)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.65" y="-0.95" dx="0.8" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="0" y="-0.95" dx="0.8" dy="0.4" layer="1" rot="R90"/>
<smd name="3" x="0.65" y="-0.95" dx="0.8" dy="0.4" layer="1" rot="R90"/>
<smd name="4" x="0.65" y="0.95" dx="0.8" dy="0.4" layer="1" rot="R90"/>
<smd name="5" x="0" y="0.95" dx="0.8" dy="0.4" layer="1" rot="R90"/>
<smd name="6" x="-0.65" y="0.95" dx="0.8" dy="0.4" layer="1" rot="R90"/>
<text x="0.4" y="1.816" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0.4" y="1.816" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.2" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.2" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.2" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.2" layer="51"/>
<wire x1="-1" y1="0.625" x2="-1" y2="-0.625" width="0.2" layer="21"/>
<wire x1="1" y1="-0.625" x2="1" y2="0.625" width="0.2" layer="21"/>
<circle x="-0.644" y="-1.645" radius="0.065" width="0.2" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="CSD95379">
<pin name="SKIP#" x="-15.24" y="12.7" length="middle" direction="in"/>
<pin name="N/A" x="-15.24" y="7.62" length="middle" direction="nc"/>
<pin name="VDD" x="-15.24" y="2.54" length="middle" direction="pwr"/>
<pin name="PGND" x="-15.24" y="-2.54" length="middle" direction="pwr"/>
<pin name="VSW" x="-15.24" y="-7.62" length="middle" direction="out"/>
<pin name="VIN" x="15.24" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="BOOT_R" x="15.24" y="-2.54" length="middle" direction="sup" rot="R180"/>
<pin name="BOOT" x="15.24" y="2.54" length="middle" direction="sup" rot="R180"/>
<pin name="N/A2" x="15.24" y="7.62" length="middle" direction="nc" rot="R180"/>
<pin name="PWM" x="15.24" y="12.7" length="middle" direction="in" rot="R180"/>
<pin name="PGND_PAD" x="0" y="-22.86" length="middle" rot="R90"/>
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-10.16" y="19.05" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="16.51" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DMC3025LSD">
<wire x1="-2.3876" y1="-5.207" x2="-2.3876" y2="-10.16" width="0.254" layer="94"/>
<wire x1="1.27" y1="-5.715" x2="-0.7366" y2="-5.715" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="-9.525" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-9.525" x2="1.27" y2="-9.525" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="-5.715" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-5.715" x2="3.81" y2="-5.715" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-9.525" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-9.525" x2="1.27" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-3.81" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-7.62" x2="0.508" y2="-8.128" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-8.128" x2="0.508" y2="-7.112" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-7.112" x2="-0.762" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="0.381" y1="-7.62" x2="1.27" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="0.381" y1="-7.366" x2="-0.508" y2="-7.62" width="0.3048" layer="94"/>
<wire x1="-0.508" y1="-7.62" x2="0.381" y2="-7.874" width="0.3048" layer="94"/>
<wire x1="0.381" y1="-7.874" x2="0.381" y2="-7.62" width="0.3048" layer="94"/>
<wire x1="0.381" y1="-7.62" x2="0.127" y2="-7.62" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-5.715" x2="3.81" y2="-6.858" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-6.858" x2="3.81" y2="-9.525" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-6.858" x2="3.175" y2="-8.255" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-8.255" x2="4.445" y2="-8.255" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-8.255" x2="3.81" y2="-6.858" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-6.858" x2="3.81" y2="-6.858" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-6.858" x2="4.445" y2="-6.858" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-6.858" x2="4.699" y2="-6.604" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-6.858" x2="2.921" y2="-7.112" width="0.1524" layer="94"/>
<circle x="1.27" y="-9.525" radius="0.127" width="0.4064" layer="94"/>
<circle x="1.27" y="-5.715" radius="0.127" width="0.4064" layer="94"/>
<text x="7.112" y="-3.556" size="1.778" layer="94">D2</text>
<text x="6.858" y="-12.7" size="1.778" layer="94">S2</text>
<text x="-9.652" y="-10.668" size="1.778" layer="94">G2</text>
<rectangle x1="-1.524" y1="-10.16" x2="-0.762" y2="-8.89" layer="94"/>
<rectangle x1="-1.524" y1="-6.35" x2="-0.762" y2="-5.08" layer="94"/>
<rectangle x1="-1.524" y1="-8.509" x2="-0.762" y2="-6.731" layer="94"/>
<wire x1="-2.3876" y1="3.937" x2="-2.3876" y2="8.89" width="0.254" layer="94"/>
<wire x1="1.27" y1="4.445" x2="-0.7366" y2="4.445" width="0.1524" layer="94"/>
<wire x1="1.27" y1="6.35" x2="1.27" y2="8.255" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="8.255" x2="1.27" y2="8.255" width="0.1524" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="4.445" width="0.1524" layer="94"/>
<wire x1="1.27" y1="4.445" x2="3.81" y2="4.445" width="0.1524" layer="94"/>
<wire x1="3.81" y1="8.255" x2="1.27" y2="8.255" width="0.1524" layer="94"/>
<wire x1="1.27" y1="8.255" x2="1.27" y2="8.89" width="0.1524" layer="94"/>
<wire x1="3.81" y1="8.255" x2="3.81" y2="7.112" width="0.1524" layer="94"/>
<wire x1="3.81" y1="7.112" x2="3.81" y2="4.445" width="0.1524" layer="94"/>
<wire x1="3.81" y1="7.112" x2="4.445" y2="5.715" width="0.1524" layer="94"/>
<wire x1="4.445" y1="5.715" x2="3.175" y2="5.715" width="0.1524" layer="94"/>
<wire x1="3.175" y1="5.715" x2="3.81" y2="7.112" width="0.1524" layer="94"/>
<wire x1="4.445" y1="7.112" x2="3.81" y2="7.112" width="0.1524" layer="94"/>
<wire x1="3.81" y1="7.112" x2="3.175" y2="7.112" width="0.1524" layer="94"/>
<wire x1="3.175" y1="7.112" x2="2.921" y2="7.366" width="0.1524" layer="94"/>
<wire x1="4.445" y1="7.112" x2="4.699" y2="6.858" width="0.1524" layer="94"/>
<wire x1="1.27" y1="6.35" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="0" y1="5.842" x2="0" y2="6.858" width="0.1524" layer="94"/>
<wire x1="0" y1="6.858" x2="1.27" y2="6.35" width="0.1524" layer="94"/>
<wire x1="0.127" y1="6.35" x2="-0.762" y2="6.35" width="0.1524" layer="94"/>
<wire x1="0.127" y1="6.604" x2="1.016" y2="6.35" width="0.3048" layer="94"/>
<wire x1="1.016" y1="6.35" x2="0.127" y2="6.096" width="0.3048" layer="94"/>
<wire x1="0.127" y1="6.096" x2="0.127" y2="6.35" width="0.3048" layer="94"/>
<wire x1="0.127" y1="6.35" x2="0.381" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-3.81" y2="6.35" width="0.1524" layer="94"/>
<circle x="1.27" y="8.255" radius="0.127" width="0.4064" layer="94"/>
<circle x="1.27" y="4.445" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="16.764" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="13.716" size="1.778" layer="96">&gt;VALUE</text>
<text x="7.112" y="2.54" size="1.778" layer="94" rot="MR180">D1</text>
<text x="6.858" y="11.176" size="1.778" layer="94" rot="MR180">S1</text>
<text x="-9.398" y="9.398" size="1.778" layer="94" rot="MR180">G1</text>
<rectangle x1="-1.524" y1="7.62" x2="-0.762" y2="8.89" layer="94"/>
<rectangle x1="-1.524" y1="3.81" x2="-0.762" y2="5.08" layer="94"/>
<rectangle x1="-1.524" y1="5.461" x2="-0.762" y2="7.239" layer="94"/>
<text x="-3.556" y="11.684" size="1.9304" layer="94" rot="MR180">P-FET</text>
<text x="-3.556" y="-4.572" size="1.9304" layer="94">N-FET</text>
<wire x1="-6.35" y1="12.7" x2="6.35" y2="12.7" width="0.254" layer="94"/>
<wire x1="6.35" y1="-12.7" x2="-6.35" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-12.7" x2="-6.35" y2="12.7" width="0.254" layer="94"/>
<wire x1="6.35" y1="12.7" x2="6.35" y2="-12.7" width="0.254" layer="94"/>
<pin name="D1" x="8.89" y="3.81" visible="off" rot="R180"/>
<pin name="G1" x="-8.89" y="6.35" visible="off" length="middle"/>
<pin name="S1" x="8.89" y="8.89" visible="off" rot="R180"/>
<pin name="S2" x="8.89" y="-10.16" visible="off" rot="R180"/>
<pin name="D2" x="8.89" y="-5.08" visible="off" rot="R180"/>
<pin name="G2" x="-8.89" y="-7.62" visible="off" length="middle"/>
</symbol>
<symbol name="POWERJACK">
<wire x1="-10.16" y1="2.54" x2="-7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.1524" layer="94"/>
<text x="-10.16" y="10.16" size="1.778" layer="96">&gt;Value</text>
<text x="-10.16" y="0" size="1.778" layer="95">&gt;Name</text>
<rectangle x1="-10.16" y1="6.858" x2="0" y2="8.382" layer="94"/>
<pin name="GNDBREAK" x="2.54" y="5.08" visible="off" length="short" rot="R180"/>
<pin name="GND" x="2.54" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="PWR" x="2.54" y="7.62" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="NR8040T220M">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-175.4"/>
<wire x1="1.27" y1="0" x2="3.81" y2="0" width="0.254" layer="94" curve="-175.4"/>
<wire x1="3.81" y1="0" x2="6.35" y2="0" width="0.254" layer="94" curve="-175.4"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-1.27" y="0" visible="off" length="point"/>
<pin name="2" x="6.35" y="0" visible="off" length="point" rot="R180"/>
</symbol>
<symbol name="CL32B106KBJNNNE">
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.508" layer="94"/>
<wire x1="3.302" y1="1.27" x2="3.302" y2="-1.27" width="0.508" layer="94"/>
<wire x1="1.27" y1="0" x2="1.778" y2="0" width="0.254" layer="94"/>
<wire x1="3.302" y1="0" x2="3.81" y2="0" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-1.27" y="0" visible="off" length="short"/>
<pin name="2" x="6.35" y="0" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="ATTINY416-MFR">
<wire x1="5.08" y1="25.4" x2="38.1" y2="25.4" width="0.254" layer="94"/>
<wire x1="38.1" y1="-20.32" x2="38.1" y2="25.4" width="0.254" layer="94"/>
<wire x1="38.1" y1="-20.32" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="25.4" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<text x="39.37" y="30.48" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="39.37" y="27.94" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="PA2" x="0" y="0" length="middle"/>
<pin name="EXTCLK_/PA3" x="0" y="-2.54" length="middle"/>
<pin name="GND" x="0" y="-5.08" length="middle"/>
<pin name="VDD" x="0" y="-7.62" length="middle"/>
<pin name="PA4" x="0" y="-10.16" length="middle"/>
<pin name="PA5" x="15.24" y="-25.4" length="middle" rot="R90"/>
<pin name="PA6" x="17.78" y="-25.4" length="middle" rot="R90"/>
<pin name="PA7" x="20.32" y="-25.4" length="middle" rot="R90"/>
<pin name="PB5" x="22.86" y="-25.4" length="middle" rot="R90"/>
<pin name="PB4" x="25.4" y="-25.4" length="middle" rot="R90"/>
<pin name="PC0" x="43.18" y="0" length="middle" rot="R180"/>
<pin name="PB0" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="PB1" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="PB2/TOSC2" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="PB3/TOSC1" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="EP" x="15.24" y="30.48" length="middle" rot="R270"/>
<pin name="PA1" x="17.78" y="30.48" length="middle" rot="R270"/>
<pin name="PA0/!RESET!/UPDI" x="20.32" y="30.48" length="middle" rot="R270"/>
<pin name="PC3" x="22.86" y="30.48" length="middle" rot="R270"/>
<pin name="PC2" x="25.4" y="30.48" length="middle" rot="R270"/>
<pin name="PC1" x="27.94" y="30.48" length="middle" rot="R270"/>
</symbol>
<symbol name="SI8620BB-B-IS">
<wire x1="5.08" y1="2.54" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<text x="24.13" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="24.13" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="VDD1" x="0" y="0" length="middle" direction="pwr"/>
<pin name="A1" x="0" y="-2.54" length="middle" direction="in"/>
<pin name="A2" x="0" y="-5.08" length="middle" direction="in"/>
<pin name="GND1" x="0" y="-7.62" length="middle" direction="pwr"/>
<pin name="VDD2" x="27.94" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="B1" x="27.94" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="B2" x="27.94" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="GND2" x="27.94" y="-7.62" length="middle" direction="pwr" rot="R180"/>
</symbol>
<symbol name="RFM-0505S">
<wire x1="5.08" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-5.08" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="26.67" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="26.67" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="-VIN" x="0" y="0" length="middle" direction="pwr"/>
<pin name="+VIN" x="0" y="-2.54" length="middle" direction="pwr"/>
<pin name="-VOUT" x="30.48" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="+VOUT" x="30.48" y="-2.54" length="middle" direction="pwr" rot="R180"/>
</symbol>
<symbol name="MB1S">
<wire x1="5.08" y1="2.54" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="-5.08" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="19.05" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="19.05" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="+" x="0" y="0" length="middle"/>
<pin name="-" x="0" y="-2.54" length="middle"/>
<pin name="~2" x="22.86" y="-2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="~1" x="22.86" y="0" length="middle" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="PA3856.006NLT">
<wire x1="5.08" y1="2.54" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="-12.7" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="-12.7" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<text x="19.05" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="19.05" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" length="middle"/>
<pin name="2" x="0" y="-2.54" length="middle"/>
<pin name="3" x="0" y="-5.08" length="middle"/>
<pin name="4" x="0" y="-7.62" length="middle"/>
<pin name="5" x="0" y="-10.16" length="middle"/>
<pin name="6" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="7" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="8" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="9" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="10" x="22.86" y="-10.16" length="middle" rot="R180"/>
</symbol>
<symbol name="RT8295BHZSP">
<wire x1="-12.7" y1="12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="-12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-12.7" y1="12.7" x2="-12.7" y2="-12.7" width="0.254" layer="94"/>
<text x="8.89" y="17.78" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="8.89" y="15.24" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="BOOT" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="VIN" x="-17.78" y="10.16" length="middle"/>
<pin name="SW" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="GND" x="-17.78" y="-7.62" length="middle"/>
<pin name="EP_(GND)" x="-17.78" y="-10.16" length="middle"/>
<pin name="SS" x="-17.78" y="0" length="middle"/>
<pin name="EN" x="-17.78" y="2.54" length="middle"/>
<pin name="COMP" x="15.24" y="-10.16" length="middle" rot="R180"/>
<pin name="FB" x="15.24" y="-2.54" length="middle" rot="R180"/>
</symbol>
<symbol name="SSM6N7002CFU,LF">
<wire x1="5.08" y1="2.54" x2="30.48" y2="2.54" width="0.254" layer="94"/>
<wire x1="30.48" y1="-7.62" x2="30.48" y2="2.54" width="0.254" layer="94"/>
<wire x1="30.48" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="31.75" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="31.75" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="SOURCE1" x="0" y="0" length="middle"/>
<pin name="GATE1" x="0" y="-2.54" length="middle"/>
<pin name="DRAIN2" x="35.56" y="-5.08" length="middle" rot="R180"/>
<pin name="SOURCE2" x="35.56" y="0" length="middle" rot="R180"/>
<pin name="GATE2" x="35.56" y="-2.54" length="middle" rot="R180"/>
<pin name="DRAIN1" x="0" y="-5.08" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CSD95379Q3M-HOMETCH" prefix="U">
<gates>
<gate name="G$1" symbol="CSD95379" x="2.54" y="-7.62"/>
</gates>
<devices>
<device name="" package="10-VSON-HOMETCH">
<connects>
<connect gate="G$1" pin="BOOT" pad="8"/>
<connect gate="G$1" pin="BOOT_R" pad="7"/>
<connect gate="G$1" pin="N/A" pad="2"/>
<connect gate="G$1" pin="N/A2" pad="9"/>
<connect gate="G$1" pin="PGND" pad="4"/>
<connect gate="G$1" pin="PGND_PAD" pad="11"/>
<connect gate="G$1" pin="PWM" pad="10"/>
<connect gate="G$1" pin="SKIP#" pad="1"/>
<connect gate="G$1" pin="VDD" pad="3"/>
<connect gate="G$1" pin="VIN" pad="6"/>
<connect gate="G$1" pin="VSW" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DMC3025LSD" prefix="Q">
<gates>
<gate name="G$1" symbol="DMC3025LSD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO-8">
<connects>
<connect gate="G$1" pin="D1" pad="5 6"/>
<connect gate="G$1" pin="D2" pad="7 8"/>
<connect gate="G$1" pin="G1" pad="4"/>
<connect gate="G$1" pin="G2" pad="2"/>
<connect gate="G$1" pin="S1" pad="3"/>
<connect gate="G$1" pin="S2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="POWER_JACK" prefix="J">
<description>&lt;b&gt;Power Jack&lt;/b&gt;
This is the standard 5.5mm barrel jack for power.&lt;br&gt;
The PTH is the most common, proven, reliable, footprint.&lt;br&gt;
The Slot footprint only works if the mill layer is transmitted to the PCB fab house so be warned.&lt;br&gt;

Mating wall wart : TOL-00298 (and others)</description>
<gates>
<gate name="G$1" symbol="POWERJACK" x="7.62" y="-2.54"/>
</gates>
<devices>
<device name="SMD" package="POWER_JACK_SMD">
<connects>
<connect gate="G$1" pin="GND" pad="P$4"/>
<connect gate="G$1" pin="GNDBREAK" pad="GND"/>
<connect gate="G$1" pin="PWR" pad="VIN0"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08106"/>
<attribute name="SF_ID" value="PRT-12748" constant="no"/>
<attribute name="VALUE" value="5.5x2.1mm Barrel" constant="no"/>
</technology>
</technologies>
</device>
<device name="SLT" package="POWER_JACK_SLOT">
<connects>
<connect gate="G$1" pin="GND" pad="GND@2"/>
<connect gate="G$1" pin="GNDBREAK" pad="GND@1"/>
<connect gate="G$1" pin="PWR" pad="PWR"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="PRT-00119" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH" package="POWER_JACK_PTH">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GNDBREAK" pad="GNDBREAK"/>
<connect gate="G$1" pin="PWR" pad="PWR"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08197"/>
</technology>
</technologies>
</device>
<device name="COMBO" package="POWER_JACK_COMBO">
<connects>
<connect gate="G$1" pin="GND" pad="GND@1"/>
<connect gate="G$1" pin="GNDBREAK" pad="GND"/>
<connect gate="G$1" pin="PWR" pad="POWER"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_LOCK" package="POWER_JACK_PTH_LOCK">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GNDBREAK" pad="GNDBREAK"/>
<connect gate="G$1" pin="PWR" pad="PWR"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08197" constant="no"/>
<attribute name="SF_ID" value="PRT-00119" constant="no"/>
</technology>
</technologies>
</device>
<device name="1" package="POWER_JACK_SMD_OVERPASTE_REDBOARD_0603">
<connects>
<connect gate="G$1" pin="GND" pad="P$4"/>
<connect gate="G$1" pin="GNDBREAK" pad="GND"/>
<connect gate="G$1" pin="PWR" pad="VIN0"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOE" package="POWER_JACK_SMD_OVERPASTE_TOE">
<connects>
<connect gate="G$1" pin="GND" pad="P$4"/>
<connect gate="G$1" pin="GNDBREAK" pad="GND"/>
<connect gate="G$1" pin="PWR" pad="VIN0"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08106" constant="no"/>
<attribute name="SF_ID" value="PRT-12748" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH_BREAD" package="POWER_JACK_PTH_BREAD">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GNDBREAK" pad="GNDBREAK"/>
<connect gate="G$1" pin="PWR" pad="PWR"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="PRT-10811" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="PJ-047AH">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="GNDBREAK" pad="3"/>
<connect gate="G$1" pin="PWR" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NR8040T220M" prefix="L">
<description>&lt;b&gt;TAIYO YUDEN - NR8040T220M - INDUCTOR, SHIELDED, 22UH, 2.2A, SMD, FULL REEL&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/2/NR8040T220M.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="NR8040T220M" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NR8040T220M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="NR8040T220M" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="http://www.arrow.com/en/products/nr8040t220m/taiyo-yuden" constant="no"/>
<attribute name="DESCRIPTION" value="TAIYO YUDEN - NR8040T220M - INDUCTOR, SHIELDED, 22UH, 2.2A, SMD, FULL REEL" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TAIYO YUDEN" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="NR8040T220M" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CL32B106KBJNNNE" prefix="C">
<description>&lt;b&gt;Samsung Electro-Mechanics 1210 CL 10F Ceramic Multilayer Capacitor, 50 V, +125C, X7R Dielectric, 10% SMD&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://docs-europe.electrocomponents.com/webdocs/13d6/0900766b813d6930.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="CL32B106KBJNNNE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC3225X270N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="CL32B106KBJNNNE" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/cl32b106kbjnnne/samsung-electro-mechanics" constant="no"/>
<attribute name="DESCRIPTION" value="Samsung Electro-Mechanics 1210 CL 10F Ceramic Multilayer Capacitor, 50 V, +125C, X7R Dielectric, 10% SMD" constant="no"/>
<attribute name="HEIGHT" value="2.7mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Samsung Electro-Mechanics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CL32B106KBJNNNE" constant="no"/>
<attribute name="RS_PART_NUMBER" value="7661214P" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/7661214P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATTINY416-MFR" prefix="IC">
<description>&lt;b&gt;MICROCHIP - ATTINY416-MFR - MCU, 8BIT, AVR, 20MHZ, VQFN-20&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/2/ATTINY416-MFR.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ATTINY416-MFR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN40P300X300X90-21N">
<connects>
<connect gate="G$1" pin="EP" pad="21"/>
<connect gate="G$1" pin="EXTCLK_/PA3" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="PA0/!RESET!/UPDI" pad="19"/>
<connect gate="G$1" pin="PA1" pad="20"/>
<connect gate="G$1" pin="PA2" pad="1"/>
<connect gate="G$1" pin="PA4" pad="5"/>
<connect gate="G$1" pin="PA5" pad="6"/>
<connect gate="G$1" pin="PA6" pad="7"/>
<connect gate="G$1" pin="PA7" pad="8"/>
<connect gate="G$1" pin="PB0" pad="14"/>
<connect gate="G$1" pin="PB1" pad="13"/>
<connect gate="G$1" pin="PB2/TOSC2" pad="12"/>
<connect gate="G$1" pin="PB3/TOSC1" pad="11"/>
<connect gate="G$1" pin="PB4" pad="10"/>
<connect gate="G$1" pin="PB5" pad="9"/>
<connect gate="G$1" pin="PC0" pad="15"/>
<connect gate="G$1" pin="PC1" pad="16"/>
<connect gate="G$1" pin="PC2" pad="17"/>
<connect gate="G$1" pin="PC3" pad="18"/>
<connect gate="G$1" pin="VDD" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="ATTINY416-MFR" constant="no"/>
<attribute name="ARROW_PRICE-STOCK" value="https://www.arrow.com/en/products/attiny416-mfr/microchip-technology" constant="no"/>
<attribute name="DESCRIPTION" value="MICROCHIP - ATTINY416-MFR - MCU, 8BIT, AVR, 20MHZ, VQFN-20" constant="no"/>
<attribute name="HEIGHT" value="0.9mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Microchip" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ATTINY416-MFR" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SI8620BB-B-IS" prefix="IC">
<description>&lt;b&gt;Silicon Labs Si8620BB-B-IS PCB SMT 2-channel Digital Isolator, 2.5 kV, 8-Pin SOIC&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.silabs.com/documents/public/data-sheets/si861x-2x-datasheet.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="SI8620BB-B-IS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-8N">
<connects>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="B1" pad="7"/>
<connect gate="G$1" pin="B2" pad="6"/>
<connect gate="G$1" pin="GND1" pad="4"/>
<connect gate="G$1" pin="GND2" pad="5"/>
<connect gate="G$1" pin="VDD1" pad="1"/>
<connect gate="G$1" pin="VDD2" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="Si8620BB-B-IS" constant="no"/>
<attribute name="ARROW_PRICE-STOCK" value="https://www.arrow.com/en/products/si8620bb-b-is/silicon-labs" constant="no"/>
<attribute name="DESCRIPTION" value="Silicon Labs Si8620BB-B-IS PCB SMT 2-channel Digital Isolator, 2.5 kV, 8-Pin SOIC" constant="no"/>
<attribute name="HEIGHT" value="1.75mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Silicon Labs" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="Si8620BB-B-IS" constant="no"/>
<attribute name="RS_PART_NUMBER" value="8232189P" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="http://uk.rs-online.com/web/p/products/8232189P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RFM-0505S" prefix="PS">
<description>&lt;b&gt;RECOM POWER - RFM-0505S - DC-DC CONVERTER, 5V, 0.2A&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.recom-power.com/pdf/Econoline/RFM.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="RFM-0505S" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RFM0505S">
<connects>
<connect gate="G$1" pin="+VIN" pad="2"/>
<connect gate="G$1" pin="+VOUT" pad="4"/>
<connect gate="G$1" pin="-VIN" pad="1"/>
<connect gate="G$1" pin="-VOUT" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="RFM-0505S" constant="no"/>
<attribute name="ARROW_PRICE-STOCK" value="https://www.arrow.com/en/products/rfm-0505s/recom-power" constant="no"/>
<attribute name="DESCRIPTION" value="RECOM POWER - RFM-0505S - DC-DC CONVERTER, 5V, 0.2A" constant="no"/>
<attribute name="HEIGHT" value="10mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="RECOM Power" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="RFM-0505S" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MB1S" prefix="IC">
<description>&lt;b&gt;MULTICOMP - MB1S - BRIDGE RECTIFIER, 0.5A, 100V, MBS&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.farnell.com/datasheets/1441719.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="MB1S" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP250P700X270-4N">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
<connect gate="G$1" pin="~1" pad="3"/>
<connect gate="G$1" pin="~2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="MB1S" constant="no"/>
<attribute name="ARROW_PRICE-STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="MULTICOMP - MB1S - BRIDGE RECTIFIER, 0.5A, 100V, MBS" constant="no"/>
<attribute name="HEIGHT" value="2.7mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="MULTICOMP" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="MB1S" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PA3856.006NLT" prefix="T">
<description>&lt;b&gt;Power Transformers SMD HiFreq WireWound 128uH .426Ohms&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://productfinder.pulseeng.com/products/datasheets/P719.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="PA3856.006NLT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PA3856005NLT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="PA3856.006NLT" constant="no"/>
<attribute name="ARROW_PRICE-STOCK" value="https://www.arrow.com/en/products/pa3856.006nlt/pulse-electronics-corporation" constant="no"/>
<attribute name="DESCRIPTION" value="Power Transformers SMD HiFreq WireWound 128uH .426Ohms" constant="no"/>
<attribute name="HEIGHT" value="14mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Pulse" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="PA3856.006NLT" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RT8295BHZSP" prefix="IC">
<description>&lt;b&gt;RICHTEK - RT8295BHZSP - DC/DC CONV, SYNC BUCK, 1.2MHZ, SOP-8&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.richtek.com/assets/product_file/RT8295B/DS8295B-04.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="RT8295BHZSP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-9N">
<connects>
<connect gate="G$1" pin="BOOT" pad="1"/>
<connect gate="G$1" pin="COMP" pad="6"/>
<connect gate="G$1" pin="EN" pad="7"/>
<connect gate="G$1" pin="EP_(GND)" pad="9"/>
<connect gate="G$1" pin="FB" pad="5"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SS" pad="8"/>
<connect gate="G$1" pin="SW" pad="3"/>
<connect gate="G$1" pin="VIN" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="RT8295BHZSP" constant="no"/>
<attribute name="ARROW_PRICE-STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="RICHTEK - RT8295BHZSP - DC/DC CONV, SYNC BUCK, 1.2MHZ, SOP-8" constant="no"/>
<attribute name="HEIGHT" value="1.753mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="RICHTEK" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="RT8295BHZSP" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SSM6N7002CFU,LF" prefix="Q">
<description>&lt;b&gt;MOSFET Small-Signal MOSFET 2-in-1&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://toshiba.semicon-storage.com/info/docget.jsp?did=29875&amp;prodName=SSM6N7002CFU"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="SSM6N7002CFU,LF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SSOP6-P-0.65A(US6)">
<connects>
<connect gate="G$1" pin="DRAIN1" pad="6"/>
<connect gate="G$1" pin="DRAIN2" pad="3"/>
<connect gate="G$1" pin="GATE1" pad="2"/>
<connect gate="G$1" pin="GATE2" pad="5"/>
<connect gate="G$1" pin="SOURCE1" pad="1"/>
<connect gate="G$1" pin="SOURCE2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="SSM6N7002CFU,LF" constant="no"/>
<attribute name="ARROW_PRICE-STOCK" value="https://www.arrow.com/en/products/ssm6n7002cfulf/toshiba" constant="no"/>
<attribute name="DESCRIPTION" value="MOSFET Small-Signal MOSFET 2-in-1" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Toshiba" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="SSM6N7002CFU,LF" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dp_devices">
<description>Dangerous Prototypes Standard PCB sizes
http://dangerousprototypes.com</description>
<packages>
<package name="C805">
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.127" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.127" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="12">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.016" layer="27" ratio="12">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
</package>
<package name="C402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="C603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1.1" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8302" y2="0.4801" layer="51" rot="R180"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<circle x="0" y="0" radius="0.1" width="0.127" layer="21"/>
<wire x1="-0.8" y1="0.75" x2="0.8" y2="0.75" width="0.2" layer="21"/>
<wire x1="-0.8" y1="-0.75" x2="0.8" y2="-0.75" width="0.2" layer="21"/>
</package>
<package name="C1206">
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-2.305" y="1.47" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.405" y="-2.64" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<circle x="0" y="0" radius="0.15" width="0.3048" layer="21"/>
<wire x1="-1.7" y1="1.1" x2="1.7" y2="1.1" width="0.1016" layer="21"/>
<wire x1="-1.7" y1="-1.1" x2="1.7" y2="-1.1" width="0.1016" layer="21"/>
</package>
<package name="C0.1PTH">
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.8575" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="R805">
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
</package>
<package name="R603">
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1.1" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<wire x1="-0.8" y1="0.75" x2="0.8" y2="0.75" width="0.2" layer="21"/>
<wire x1="0.8" y1="-0.75" x2="-0.8" y2="-0.75" width="0.2" layer="21"/>
</package>
<package name="R402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="R1206">
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="RTH025W">
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.8575" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="TO-220-2">
<wire x1="4.826" y1="-1.778" x2="5.08" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-1.778" x2="-4.826" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.524" x2="-4.826" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.397" x2="5.08" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.524" x2="-5.08" y2="1.397" width="0.1524" layer="21"/>
<circle x="-4.6228" y="-1.1684" radius="0.254" width="0" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-3.3782" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="-1.27" y="-1.27" size="1.27" layer="51" font="vector" ratio="10">1</text>
<text x="3.81" y="-1.27" size="1.27" layer="51" font="vector" ratio="10">2</text>
<rectangle x1="-5.334" y1="1.27" x2="-3.429" y2="2.54" layer="21"/>
<rectangle x1="-3.429" y1="1.778" x2="-1.651" y2="2.54" layer="21"/>
<rectangle x1="-1.651" y1="1.27" x2="-0.889" y2="2.54" layer="21"/>
<rectangle x1="-0.889" y1="1.778" x2="0.889" y2="2.54" layer="21"/>
<rectangle x1="0.889" y1="1.27" x2="1.651" y2="2.54" layer="21"/>
<rectangle x1="1.651" y1="1.778" x2="3.429" y2="2.54" layer="21"/>
<rectangle x1="3.429" y1="1.27" x2="5.334" y2="2.54" layer="21"/>
<rectangle x1="-3.429" y1="1.27" x2="-1.651" y2="1.778" layer="51"/>
<rectangle x1="-0.889" y1="1.27" x2="0.889" y2="1.778" layer="21"/>
<rectangle x1="1.651" y1="1.27" x2="3.429" y2="1.778" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR_NPOL" prefix="C" uservalue="yes">
<description>Non-Polarized capacitor in various packages</description>
<gates>
<gate name="C" symbol="CAP" x="0" y="-2.54"/>
</gates>
<devices>
<device name="-0805" package="C805">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402" package="C402">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="C603">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206" package="C1206">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PTH_0.1&quot;" package="C0.1PTH">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0805" package="R805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="R805"/>
</technologies>
</device>
<device name="-0603" package="R603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402" package="R402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PTH-0.4" package="RTH025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TO-220-2" package="TO-220-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="GND-ISO">
<description>Isolated ground</description>
<pin name="GND-ISO" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-2.032" y1="0" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.524" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="1.27" y1="2.032" x2="1.27" y2="0.508" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND-ISO" prefix="GND-ISO">
<description>Isolated ground</description>
<gates>
<gate name="G$1" symbol="GND-ISO" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microbuilder">
<description>&lt;h2&gt;&lt;b&gt;microBuilder.eu&lt;/b&gt; Eagle Footprint Library&lt;/h2&gt;

&lt;p&gt;Footprints for common components used in our projects and products.  This is the same library that we use internally, and it is regularly updated.  The newest version can always be found at &lt;b&gt;www.microBuilder.eu&lt;/b&gt;.  If you find this library useful, please feel free to purchase something from our online store. Please also note that all holes are optimised for metric drill bits!&lt;/p&gt;

&lt;h3&gt;Obligatory Warning&lt;/h3&gt;
&lt;p&gt;While it probably goes without saying, there are no guarantees that the footprints or schematic symbols in this library are flawless, and we make no promises of fitness for production, prototyping or any other purpose. These libraries are provided for information puposes only, and are used at your own discretion.  While we make every effort to produce accurate footprints, and many of the items found in this library have be proven in production, we can't make any promises of suitability for a specific purpose. If you do find any errors, though, please feel free to contact us at www.microbuilder.eu to let us know about it so that we can update the library accordingly!&lt;/p&gt;

&lt;h3&gt;License&lt;/h3&gt;
&lt;p&gt;This work is placed in the public domain, and may be freely used for commercial and non-commercial work with the following conditions:&lt;/p&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
&lt;/p&gt;</description>
<packages>
<package name="SOT23-WIDE">
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.2032" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.6724" y1="-0.6524" x2="-1.6724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="-1.6724" y1="0.6604" x2="-0.7136" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="1.6724" y1="0.6604" x2="1.6724" y2="-0.6524" width="0.2032" layer="21"/>
<wire x1="0.7136" y1="0.6604" x2="1.6724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="0.2224" y1="-0.6604" x2="-0.2364" y2="-0.6604" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1" dx="1" dy="1.27" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="1" dy="1.27" layer="1"/>
<smd name="1" x="-0.95" y="-1" dx="1" dy="1.27" layer="1"/>
<text x="1.905" y="0" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1.905" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="TO252">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;&lt;p&gt;
TS-003</description>
<wire x1="3.2766" y1="3.8354" x2="3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-2.159" x2="-3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-2.159" x2="-3.2766" y2="3.8354" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="3.835" x2="3.2774" y2="3.8346" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="3.937" x2="-2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="4.6482" x2="-2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="5.1054" x2="2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="5.1054" x2="2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="4.6482" x2="2.5654" y2="3.937" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.937" x2="-2.5654" y2="3.937" width="0.2032" layer="51"/>
<smd name="3" x="0" y="2.5" dx="5.4" dy="6.2" layer="1"/>
<smd name="1" x="-2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<smd name="2" x="2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<text x="-3.81" y="-2.54" size="0.8128" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.7178" y1="-5.1562" x2="-1.8542" y2="-2.2606" layer="51"/>
<rectangle x1="1.8542" y1="-5.1562" x2="2.7178" y2="-2.2606" layer="51"/>
<rectangle x1="-0.4318" y1="-3.0226" x2="0.4318" y2="-2.2606" layer="21"/>
<polygon width="0.2032" layer="51">
<vertex x="-2.5654" y="3.937"/>
<vertex x="-2.5654" y="4.6482"/>
<vertex x="-2.1082" y="5.1054"/>
<vertex x="2.1082" y="5.1054"/>
<vertex x="2.5654" y="4.6482"/>
<vertex x="2.5654" y="3.937"/>
</polygon>
</package>
<package name="1X02_OVAL">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02_ROUND">
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02_SMT">
<description>&lt;p&gt;&lt;b&gt;Pin Headers&lt;/b&gt;&lt;br/&gt;
2 Pin, 0.1"/2.54mm pitch, SMT&lt;/p&gt;</description>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.2032" layer="51"/>
<smd name="1" x="-1.27" y="1.27" dx="1" dy="3.5" layer="1"/>
<smd name="2" x="1.27" y="-1.27" dx="1" dy="3.5" layer="1"/>
<text x="-2.6162" y="3.25" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-4.5" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SOT23-R">
<description>&lt;b&gt;SOT23&lt;/b&gt; - Reflow soldering</description>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-0.7" x2="-1.5" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.6" x2="-0.6" y2="0.6" width="0.2032" layer="21"/>
<wire x1="1.5" y1="0.6" x2="1.5" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.6" y1="0.6" x2="1.5" y2="0.6" width="0.2032" layer="21"/>
<wire x1="0.4" y1="-0.7" x2="-0.4" y2="-0.7" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1.2" dx="0.7" dy="1.1" layer="1"/>
<smd name="2" x="0.95" y="-1.2" dx="0.7" dy="1.1" layer="1"/>
<smd name="1" x="-0.95" y="-1.2" dx="0.7" dy="1.1" layer="1"/>
<text x="1.778" y="-0.127" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1.778" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<text x="0.5" y="-0.4" size="1.016" layer="21" ratio="20" rot="R90">N</text>
</package>
</packages>
<symbols>
<symbol name="MOSFET-P">
<wire x1="-1.778" y1="-0.762" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-3.175" x2="-1.778" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="-1.778" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="1.905" x2="-1.778" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="-1.778" y2="3.175" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.778" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="0.762" x2="0.762" y2="0.508" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0.508" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.508" x2="2.032" y2="0.254" width="0.1524" layer="94"/>
<circle x="0" y="2.54" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-2.54" radius="0.3592" width="0" layer="94"/>
<text x="1.27" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="0.635" y="-3.81" size="0.8128" layer="93">D</text>
<text x="0.635" y="3.175" size="0.8128" layer="93">S</text>
<text x="-3.81" y="1.27" size="0.8128" layer="93">G</text>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="1.27" y="0.508"/>
<vertex x="1.778" y="-0.254"/>
<vertex x="0.762" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="0" y="0"/>
<vertex x="-1.016" y="0.762"/>
<vertex x="-1.016" y="-0.762"/>
</polygon>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOSFET-P" prefix="Q" uservalue="yes">
<description>&lt;b&gt;P-Channel Mosfet&lt;/b&gt;
&lt;p&gt;&lt;b&gt;LEGEND&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;
&lt;b&gt;VDS&lt;/b&gt;: Voltage Drain-Source&lt;br/&gt;
&lt;b&gt;ID&lt;/b&gt;: Drain Current&lt;br/&gt;
&lt;b&gt;RDS(ON)&lt;/b&gt;: Drain-Source On-State Resistance&lt;br/&gt;
&lt;b&gt;VGS(TH)&lt;/b&gt;: Gate-Source Threshold Voltage&lt;br/&gt;
&lt;b&gt;CISS&lt;/b&gt;: Drain-Source Input Capacitance
&lt;/p&gt;
&lt;p&gt;
&lt;b&gt;SOT-23&lt;/b&gt;
&lt;table border="0" width="90%" cellspacing="0" cellpadding="5"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
&lt;td&gt;Name&lt;/td&gt;
&lt;td&gt;VDS&lt;/td&gt;
&lt;td&gt;ID&lt;/td&gt;
&lt;td&gt;RDS(ON)&lt;/td&gt;
&lt;td&gt;VGS(TH)&lt;/td&gt;
&lt;td&gt;CISS&lt;/td&gt;
&lt;td&gt;Order Number&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;IRLML5103&lt;/td&gt;
&lt;td&gt;30V&lt;/td&gt;
&lt;td&gt;760mA&lt;/td&gt;
&lt;td&gt;600 mOhm&lt;/td&gt;
&lt;td&gt;--&lt;/td&gt;
&lt;td&gt;75pF @ 25V&lt;/td&gt;
&lt;td&gt;Digikey: IRLML5103PBFCT-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;IRLML6401&lt;/td&gt;
&lt;td&gt;12V&lt;/td&gt;
&lt;td&gt;4.3A&lt;/td&gt;
&lt;td&gt;50 mOhm&lt;/td&gt;
&lt;td&gt;950mV @ 250µA&lt;/td&gt;
&lt;td&gt;830pF @ 10V&lt;/td&gt;
&lt;td&gt;Digikey: IRLML6401PBFTR-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;NTR0202PL&lt;/td&gt;
&lt;td&gt;20V&lt;/td&gt;
&lt;td&gt;400mA&lt;/td&gt;
&lt;td&gt;800 mOhm&lt;/td&gt;
&lt;td&gt;2.3V @ 250uA&lt;/td&gt;
&lt;td&gt;70pF @ 5V&lt;/td&gt;
&lt;td&gt;Digikey: NTR0202PLT1GOSTR-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;NTR4101PT1G&lt;/td&gt;
&lt;td&gt;20V&lt;/td&gt;
&lt;td&gt;1.8A&lt;/td&gt;
&lt;td&gt;85 mOhm&lt;/td&gt;
&lt;td&gt;1.2V @ 250uA&lt;/td&gt;
&lt;td&gt;675pF @ 10V&lt;/td&gt;
&lt;td&gt;Digikey: NTR4101PT1GOSCT-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;DMP2004K&lt;/td&gt;
&lt;td&gt;20V&lt;/td&gt;
&lt;td&gt;600mA&lt;/td&gt;
&lt;td&gt;900 mOhm&lt;/td&gt;
&lt;td&gt;1V @ 250uA&lt;/td&gt;
&lt;td&gt;175pF @ 16V&lt;/td&gt;
&lt;td&gt;Digikey: DMP2004KDICT-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;PMV65XP&lt;/td&gt;
&lt;td&gt;20V&lt;/td&gt;
&lt;td&gt;3.9A&lt;/td&gt;
&lt;td&gt;76 mOhm&lt;/td&gt;
&lt;td&gt;950mV @ 1mA&lt;/td&gt;
&lt;td&gt;725pF @ 20V&lt;/td&gt;
&lt;td&gt;Digikey: 568-2358-2-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt; 
&lt;b&gt;TO-252&lt;/b&gt;
&lt;table border="0" width="90%" cellspacing="0" cellpadding="5"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
&lt;td&gt;Name&lt;/td&gt;
&lt;td&gt;VDS&lt;/td&gt;
&lt;td&gt;ID&lt;/td&gt;
&lt;td&gt;RDS(ON)&lt;/td&gt;
&lt;td&gt;VGS(TH)&lt;/td&gt;
&lt;td&gt;CISS&lt;/td&gt;
&lt;td&gt;Order Number&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;AOD417&lt;/td&gt;
&lt;td&gt;30V&lt;/td&gt;
&lt;td&gt;25A&lt;/td&gt;
&lt;td&gt;34 mOhm&lt;/td&gt;
&lt;td&gt;3V @ 250µA&lt;/td&gt;
&lt;td&gt;920pF @ 15V&lt;/td&gt;
&lt;td&gt;Digikey: 785-1106-2-ND&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="MOSFET-P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-R">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WIDE" package="SOT23-WIDE">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO252" package="TO252">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02_OVAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROUND" package="1X02_ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMT" package="1X02_SMT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lio">
<packages>
<package name="SSOP5">
<wire x1="1.45" y1="0.8104" x2="1.45" y2="-0.8104" width="0.2032" layer="21"/>
<wire x1="-1.45" y1="-0.8104" x2="-1.45" y2="0.8104" width="0.2032" layer="21"/>
<circle x="-1.0922" y="-0.37465" radius="0.0359" width="0.254" layer="21"/>
<smd name="1" x="-0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<text x="-0.8255" y="2.159" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9525" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="BD48XXX">
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="-5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="-7.62" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="2.54" y="-10.16" length="middle" direction="pwr" rot="R90"/>
<pin name="VOUT" x="-2.54" y="-10.16" length="middle" direction="oc" rot="R90"/>
<pin name="VDD" x="0" y="-10.16" length="middle" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BD48XXX" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="BD48XXX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SSOP5">
<connects>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="VDD" pad="2"/>
<connect gate="G$1" pin="VOUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="prg_header">
<description>Connectorless programming header</description>
<packages>
<package name="AVX_9188_6-WAY_MATING_GUIDE">
<smd name="2" x="-2" y="1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<smd name="6" x="2" y="1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<smd name="3" x="-1" y="-1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<smd name="1" x="-3" y="-1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<smd name="4" x="1" y="-1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<smd name="5" x="3" y="-1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<text x="-6.096" y="2.794" size="1.016" layer="25">&gt;NAME</text>
<text x="-6.096" y="-3.81" size="1.016" layer="27">&gt;VALUE</text>
<hole x="-5.334" y="0" drill="1"/>
<hole x="5.334" y="0" drill="1"/>
<circle x="-3.97" y="-0.93" radius="0.14141875" width="0.3048" layer="21"/>
</package>
<package name="AVX_9188_6-WAY_SMT">
<description>&lt;b&gt;AVX 9188 Staggered SOLO Stacker 6-Way&lt;/b&gt;
&lt;p&gt;SMT PCB Footprint&lt;/P&gt;
&lt;a href="http://www.avx.com/docs/Catalogs/9188.pdf"&gt;http://www.avx.com/docs/Catalogs/9188.pdf&lt;/a&gt;</description>
<wire x1="-3.65" y1="2.05" x2="3.65" y2="2.05" width="0.127" layer="21"/>
<wire x1="3.65" y1="2.05" x2="3.65" y2="-2.05" width="0.127" layer="21"/>
<wire x1="3.65" y1="-2.05" x2="-3.65" y2="-2.05" width="0.127" layer="21"/>
<wire x1="-3.65" y1="-2.05" x2="-3.65" y2="2.05" width="0.127" layer="21"/>
<smd name="2" x="-2" y="0.95" dx="0.6" dy="1" layer="1"/>
<smd name="6" x="2" y="0.95" dx="0.6" dy="1" layer="1"/>
<smd name="3" x="-1" y="-0.95" dx="0.6" dy="1" layer="1"/>
<smd name="1" x="-3" y="-0.95" dx="0.6" dy="1" layer="1"/>
<smd name="4" x="1" y="-0.95" dx="0.6" dy="1" layer="1"/>
<smd name="5" x="3" y="-0.95" dx="0.6" dy="1" layer="1"/>
<text x="-3" y="2.5" size="1" layer="25">&gt;NAME</text>
<text x="-3" y="-3.5" size="1" layer="27">&gt;VALUE</text>
</package>
<package name="AVX_9188_6-WAY_TINY">
<smd name="2" x="-2" y="1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<smd name="6" x="2" y="1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<smd name="3" x="-1" y="-1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<smd name="1" x="-3" y="-1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<smd name="4" x="1" y="-1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<smd name="5" x="3" y="-1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<text x="-6.096" y="2.794" size="1.016" layer="25">&gt;NAME</text>
<text x="-6.096" y="-3.81" size="1.016" layer="27">&gt;VALUE</text>
<hole x="-5.334" y="0" drill="1"/>
<hole x="5.334" y="0" drill="1"/>
<circle x="-3.97" y="-0.93" radius="0.14141875" width="0.3048" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="AVR_ISP">
<wire x1="-5.08" y1="-5.08" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="-4.318" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.064" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="8.89" y="0.635" size="1.27" layer="94">MOSI</text>
<text x="-11.938" y="-2.032" size="1.27" layer="94">RESET</text>
<text x="-11.938" y="0.508" size="1.27" layer="94">SCK</text>
<text x="-11.938" y="3.302" size="1.27" layer="94">MISO</text>
<text x="8.89" y="3.048" size="1.27" layer="94">+5</text>
<text x="8.89" y="-2.032" size="1.27" layer="94">GND</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" direction="pas" function="dot"/>
<pin name="2" x="10.16" y="2.54" visible="pad" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-7.62" y="0" visible="pad" direction="pas" function="dot"/>
<pin name="4" x="10.16" y="0" visible="pad" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-7.62" y="-2.54" visible="pad" direction="pas" function="dot"/>
<pin name="6" x="10.16" y="-2.54" visible="pad" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ISPTOUCH" prefix="J">
<description>&lt;b&gt;ISPtouch for AVR Microcontrollers&lt;/b&gt;
&lt;p&gt;This 6-pin ISP connector for AVR programming does not require any part on the PCB, but an adapter for a common 6-pin programmer.&lt;/p&gt;
</description>
<gates>
<gate name="G$1" symbol="AVR_ISP" x="0" y="0"/>
</gates>
<devices>
<device name="_HEADER" package="AVX_9188_6-WAY_MATING_GUIDE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ADAPTER" package="AVX_9188_6-WAY_SMT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AVX_9188_6-WAY_TINY" package="AVX_9188_6-WAY_TINY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3P-LOC" urn="urn:adsk.eagle:symbol:13887/1" library_version="1">
<wire x1="256.54" y1="24.13" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="256.54" y1="3.81" x2="246.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="3.81" x2="161.29" y2="3.81" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="387.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3P-LOC" urn="urn:adsk.eagle:component:13945/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>A3 Portrait Location</description>
<gates>
<gate name="G$1" symbol="A3P-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="10118194-0001LF">
<packages>
<package name="FRAMATOME_10118194-0001LF">
<wire x1="-3.5" y1="2.85" x2="3.5" y2="2.85" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-1.25" x2="-3.5" y2="-2.1" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-2.1" x2="-3.5" y2="-2.15" width="0.127" layer="21"/>
<wire x1="3.5" y1="-1.25" x2="3.5" y2="-2.15" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-1.45" x2="4.5" y2="-1.45" width="0.127" layer="21"/>
<text x="4.25973125" y="-1.503440625" size="1.2729" layer="51">PCB Edge</text>
<wire x1="-3.5" y1="-2.1" x2="-4" y2="-2.6" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.9" y1="-2.9" x2="-3.1" y2="-2.1" width="0.127" layer="21" curve="90"/>
<wire x1="-3.1" y1="-2.1" x2="-3.1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-3.1" y1="-1.8" x2="-2.8" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-1.8" x2="-2.4" y2="-2.2" width="0.127" layer="21" curve="90"/>
<wire x1="3.5" y1="-1.25" x2="3.5" y2="-2.1" width="0.127" layer="21"/>
<wire x1="3.5" y1="-2.1" x2="3.5" y2="-2.15" width="0.127" layer="21"/>
<wire x1="3.5" y1="-2.1" x2="4" y2="-2.6" width="0.127" layer="21" curve="90"/>
<wire x1="3.9" y1="-2.9" x2="3.1" y2="-2.1" width="0.127" layer="21" curve="-90"/>
<wire x1="3.1" y1="-2.1" x2="3.1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="3.1" y1="-1.8" x2="2.8" y2="-1.8" width="0.127" layer="21"/>
<wire x1="2.8" y1="-1.8" x2="2.4" y2="-2.2" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.4" y1="-2.2" x2="2.3" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-2.5" y1="3.7" x2="3.8" y2="3.7" width="0.127" layer="39"/>
<wire x1="3.8" y1="3.7" x2="3.8" y2="1.2" width="0.127" layer="39"/>
<wire x1="3.8" y1="1.2" x2="4.7" y2="1.2" width="0.127" layer="39"/>
<wire x1="4.7" y1="1.2" x2="4.7" y2="-1.2" width="0.127" layer="39"/>
<wire x1="4.7" y1="-1.2" x2="4.3" y2="-1.2" width="0.127" layer="39"/>
<wire x1="4.3" y1="-1.2" x2="4.3" y2="-3.2" width="0.127" layer="39"/>
<wire x1="4.3" y1="-3.2" x2="-4.3" y2="-3.2" width="0.127" layer="39"/>
<wire x1="-4.3" y1="-3.2" x2="-4.3" y2="-1.2" width="0.127" layer="39"/>
<wire x1="-4.3" y1="-1.2" x2="-4.7" y2="-1.2" width="0.127" layer="39"/>
<wire x1="-4.7" y1="-1.2" x2="-4.7" y2="1.2" width="0.127" layer="39"/>
<wire x1="-4.7" y1="1.2" x2="-3.8" y2="1.2" width="0.127" layer="39"/>
<wire x1="-3.8" y1="1.2" x2="-3.8" y2="3.7" width="0.127" layer="39"/>
<wire x1="-3.8" y1="3.7" x2="-1.8" y2="3.7" width="0.127" layer="39"/>
<text x="-4.50903125" y="-0.90180625" size="1.27255" layer="25" rot="R90">&gt;NAME</text>
<text x="5.904759375" y="0.10008125" size="1.27101875" layer="27" rot="R90">&gt;VALUE</text>
<smd name="1" x="-1.3" y="2.675" dx="0.45" dy="1.35" layer="1" rot="R180"/>
<smd name="2" x="-0.65" y="2.675" dx="0.45" dy="1.35" layer="1" rot="R180"/>
<smd name="3" x="0" y="2.675" dx="0.45" dy="1.35" layer="1" rot="R180"/>
<smd name="4" x="0.65" y="2.675" dx="0.45" dy="1.35" layer="1" rot="R180"/>
<smd name="5" x="1.3" y="2.675" dx="0.45" dy="1.35" layer="1" rot="R180"/>
<pad name="P$6" x="-2.5" y="2.7" drill="0.9"/>
<pad name="P$7" x="2.5" y="2.7" drill="0.9"/>
<pad name="P$8" x="-3.5" y="0" drill="1.2"/>
<pad name="P$9" x="3.5" y="0" drill="1.2"/>
<smd name="P$10" x="-1" y="0" dx="1.5" dy="1.55" layer="1"/>
<smd name="P$11" x="1" y="0" dx="1.5" dy="1.55" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="10118194-0001LF">
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-7.38786875" y="5.604590625" size="1.27376875" layer="95">&gt;NAME</text>
<text x="-7.63473125" y="-7.63473125" size="1.272459375" layer="96">&gt;VALUE</text>
<pin name="D+" x="-12.7" y="2.54" length="middle"/>
<pin name="D-" x="-12.7" y="0" length="middle"/>
<pin name="ID" x="-12.7" y="-2.54" length="middle" direction="pas"/>
<pin name="GND" x="12.7" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD" x="12.7" y="2.54" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10118194-0001LF" prefix="J">
<description>Conn Micro USB Type B RCP 5 POS 0.65mm Solder RA SMD 5 Terminal 1 Port T/R</description>
<gates>
<gate name="G$1" symbol="10118194-0001LF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FRAMATOME_10118194-0001LF">
<connects>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="ID" pad="4"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Warning"/>
<attribute name="DESCRIPTION" value=" Micro Usb, 2.0 Type b, Rcpt, Smt "/>
<attribute name="MF" value="Amphenol"/>
<attribute name="MP" value="10118194-0001LF"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="0.26 USD"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="A750MS108M1AAAE013">
<description>&lt;KEMET Aluminium Polymer Capacitor 1000F 10V dc 10mm A750 Series Aluminium, Through Hole Polymer, 20%&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPPRD500W110D1000H1300">
<description>&lt;b&gt;Bulk&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.5"/>
<pad name="2" x="5" y="0" drill="1.2" diameter="2.5"/>
<text x="2.4" y="7" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="2.6" y="-6.7" size="1.27" layer="27" align="center">&gt;VALUE</text>
<circle x="2.5" y="0" radius="5" width="0.2" layer="25"/>
<circle x="2.5" y="0" radius="5" width="0.1" layer="51"/>
<text x="-0.5" y="1.3" size="1.4224" layer="21">+</text>
<wire x1="6.5" y1="2.9" x2="6.5" y2="-2.9" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="A750MS108M1AAAE013">
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="5.842" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.842" y2="2.54" width="0.254" layer="94"/>
<wire x1="4.572" y1="1.27" x2="3.556" y2="1.27" width="0.254" layer="94"/>
<wire x1="4.064" y1="1.778" x2="4.064" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<text x="-1.27" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-1.27" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="+" x="0" y="0" visible="off" length="short"/>
<pin name="-" x="12.7" y="0" visible="off" length="short" rot="R180"/>
<polygon width="0.254" layer="94">
<vertex x="7.62" y="2.54"/>
<vertex x="7.62" y="-2.54"/>
<vertex x="6.858" y="-2.54"/>
<vertex x="6.858" y="2.54"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="A750MS108M1AAAE013" prefix="C">
<description>&lt;b&gt;KEMET Aluminium Polymer Capacitor 1000F 10V dc 10mm A750 Series Aluminium, Through Hole Polymer, 20%&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://uk.rs-online.com/web/p/products/1734860"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="A750MS108M1AAAE013" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPPRD500W110D1000H1300">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="A750MS108M1AAAE013" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="KEMET Aluminium Polymer Capacitor 1000F 10V dc 10mm A750 Series Aluminium, Through Hole Polymer, 20%" constant="no"/>
<attribute name="HEIGHT" value="13mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="A750MS108M1AAAE013" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="A750KS477M1CAAE013">
<description>&lt;KEMET Aluminium Polymer Capacitor 470F 16V dc 8mm A750 Series Aluminium, Through Hole Polymer, 20% 8 (Dia.) x 12mm&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPPRD350W60D800H1200">
<description>&lt;b&gt;Bulk&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="0.9" diameter="2.4" rot="R90"/>
<pad name="2" x="3.5" y="0" drill="0.9" diameter="2.4" rot="R90"/>
<text x="1.7" y="5.6" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="1.6" y="-5.4" size="1.27" layer="27" align="center">&gt;VALUE</text>
<circle x="1.75" y="0" radius="4" width="0.2" layer="25"/>
<circle x="1.75" y="0" radius="4" width="0.1" layer="51"/>
<text x="-0.6" y="1.2" size="1.4224" layer="21">+</text>
<wire x1="4.9" y1="2.4" x2="4.9" y2="-2.4" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="A750KS477M1CAAE013">
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="5.842" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.842" y2="2.54" width="0.254" layer="94"/>
<wire x1="4.572" y1="1.27" x2="3.556" y2="1.27" width="0.254" layer="94"/>
<wire x1="4.064" y1="1.778" x2="4.064" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<text x="8.89" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="8.89" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="+" x="0" y="0" visible="off" length="short"/>
<pin name="-" x="12.7" y="0" visible="off" length="short" rot="R180"/>
<polygon width="0.254" layer="94">
<vertex x="7.62" y="2.54"/>
<vertex x="7.62" y="-2.54"/>
<vertex x="6.858" y="-2.54"/>
<vertex x="6.858" y="2.54"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="A750KS477M1CAAE013" prefix="C">
<description>&lt;b&gt;KEMET Aluminium Polymer Capacitor 470F 16V dc 8mm A750 Series Aluminium, Through Hole Polymer, 20% 8 (Dia.) x 12mm&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://uk.rs-online.com/web/p/products/1726599"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="A750KS477M1CAAE013" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPPRD350W60D800H1200">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="A750KS477M1CAAE013" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="KEMET Aluminium Polymer Capacitor 470F 16V dc 8mm A750 Series Aluminium, Through Hole Polymer, 20% 8 (Dia.) x 12mm" constant="no"/>
<attribute name="HEIGHT" value="12mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="A750KS477M1CAAE013" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="tagsu_button">
<packages>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FIDUCIAL" prefix="FID">
<description>For use by pick and place machines to calibrate the vision/machine, 1mm
&lt;p&gt;By microbuilder.eu&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U3" library="rakettitiede" deviceset="CSD95379Q3M-HOMETCH" device="" value="CSD95379Q3M"/>
<part name="U2" library="rakettitiede" deviceset="CSD95379Q3M-HOMETCH" device="" value="CSD95379Q3M"/>
<part name="Q3" library="rakettitiede" deviceset="DMC3025LSD" device=""/>
<part name="Q2" library="rakettitiede" deviceset="DMC3025LSD" device=""/>
<part name="C20" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.1u"/>
<part name="C7" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.1u"/>
<part name="GND11" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND12" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND9" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND10" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="Q1" library="microbuilder" deviceset="MOSFET-P" device="TO252" value="DMP4047SK3-13"/>
<part name="U1" library="lio" deviceset="BD48XXX" device="" value="BD4853G"/>
<part name="R1" library="dp_devices" deviceset="RESISTOR" device="-0603" value="47k"/>
<part name="R5" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="R6" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="C3" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.1u"/>
<part name="JP1" library="microbuilder" deviceset="HEADER-1X2" device="" value="5VDC OUT"/>
<part name="GND4" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND6" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND7" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND2" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND3" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="J3" library="prg_header" deviceset="ISPTOUCH" device="_HEADER"/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3P-LOC" device=""/>
<part name="J2" library="10118194-0001LF" deviceset="10118194-0001LF" device=""/>
<part name="J1" library="rakettitiede" deviceset="POWER_JACK" device="" value="BARREL 5.5MM"/>
<part name="JP2" library="microbuilder" deviceset="HEADER-1X2" device="ROUND" value="50/60Hz"/>
<part name="R2" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="C8" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.47u"/>
<part name="C21" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.47u"/>
<part name="C2" library="A750MS108M1AAAE013" deviceset="A750MS108M1AAAE013" device="" value="1000u"/>
<part name="C13" library="A750KS477M1CAAE013" deviceset="A750KS477M1CAAE013" device="" value="470u"/>
<part name="L1" library="rakettitiede" deviceset="NR8040T220M" device="" value="22u"/>
<part name="C25" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C26" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C27" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C10" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C11" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C12" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="GND-ISO14" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO9" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO8" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="R7" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="R8" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="GND-ISO12" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO13" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="IC2" library="rakettitiede" deviceset="ATTINY416-MFR" device=""/>
<part name="IC1" library="rakettitiede" deviceset="SI8620BB-B-IS" device=""/>
<part name="C9" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="4.7u"/>
<part name="C22" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="4.7u"/>
<part name="PS1" library="rakettitiede" deviceset="RFM-0505S" device=""/>
<part name="IC3" library="rakettitiede" deviceset="MB1S" device="" value="KMB23S"/>
<part name="T1" library="rakettitiede" deviceset="PA3856.006NLT" device=""/>
<part name="C5" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.1u"/>
<part name="GND8" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND-ISO1" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="C4" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="GND5" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND-ISO4" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO6" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="C6" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.1u"/>
<part name="R4" library="dp_devices" deviceset="RESISTOR" device="-0603" value="10k"/>
<part name="R3" library="dp_devices" deviceset="RESISTOR" device="-0603" value="33k"/>
<part name="FID1" library="tagsu_button" deviceset="FIDUCIAL" device=""/>
<part name="FID2" library="tagsu_button" deviceset="FIDUCIAL" device=""/>
<part name="FID3" library="tagsu_button" deviceset="FIDUCIAL" device=""/>
<part name="GND-ISO5" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO7" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO2" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO3" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="IC4" library="rakettitiede" deviceset="RT8295BHZSP" device=""/>
<part name="C16" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="R12" library="dp_devices" deviceset="RESISTOR" device="-0603" value="100k"/>
<part name="C19" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.1u"/>
<part name="GND-ISO16" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="C15" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.1u"/>
<part name="L2" library="rakettitiede" deviceset="NR8040T220M" device="" value="10u"/>
<part name="C17" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C18" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="GND-ISO17" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="C23" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="820p"/>
<part name="C24" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="NA"/>
<part name="R14" library="dp_devices" deviceset="RESISTOR" device="-0603" value="32k"/>
<part name="GND-ISO18" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="R9" library="dp_devices" deviceset="RESISTOR" device="-0603" value="295k"/>
<part name="R11" library="dp_devices" deviceset="RESISTOR" device="-0603" value="100k"/>
<part name="R10" library="dp_devices" deviceset="RESISTOR" device="-0603" value="25k"/>
<part name="GND-ISO15" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="JP3" library="microbuilder" deviceset="HEADER-1X2" device="" value="9VAC OUT"/>
<part name="R13" library="dp_devices" deviceset="RESISTOR" device="-1206" value="470"/>
<part name="C14" library="A750KS477M1CAAE013" deviceset="A750KS477M1CAAE013" device="" value="470u"/>
<part name="C1" library="A750MS108M1AAAE013" deviceset="A750MS108M1AAAE013" device="" value="1000u"/>
<part name="GND1" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND-ISO10" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO11" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="Q4" library="rakettitiede" deviceset="SSM6N7002CFU,LF" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="-30.48" y1="135.89" x2="-30.48" y2="243.84" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-30.48" y1="243.84" x2="73.66" y2="243.84" width="0.1524" layer="94" style="shortdash"/>
<wire x1="73.66" y1="243.84" x2="73.66" y2="135.89" width="0.1524" layer="94" style="shortdash"/>
<wire x1="214.63" y1="243.84" x2="214.63" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="214.63" y1="-2.54" x2="72.39" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="72.39" y1="-2.54" x2="72.39" y2="132.08" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-30.48" y1="344.17" x2="121.92" y2="344.17" width="0.1524" layer="94"/>
<wire x1="121.92" y1="344.17" x2="121.92" y2="289.56" width="0.1524" layer="94"/>
<wire x1="121.92" y1="289.56" x2="-30.48" y2="289.56" width="0.1524" layer="94"/>
<wire x1="-30.48" y1="289.56" x2="-30.48" y2="344.17" width="0.1524" layer="94"/>
<text x="-27.94" y="340.36" size="1.778" layer="94">5VDC input/output, overvoltage protection and 5VDC filtering</text>
<wire x1="72.39" y1="132.08" x2="-30.48" y2="132.08" width="0.1524" layer="94"/>
<wire x1="-30.48" y1="132.08" x2="-30.48" y2="-2.54" width="0.1524" layer="94"/>
<text x="162.56" y="281.94" size="1.778" layer="94">MCU programming header</text>
<text x="127" y="-27.94" size="1.4224" layer="94">NepaPSU is a 5VDC to 5VDC &amp; 9VAC
power supply unit for C64

(C) 2018 Jari Tulilahti &amp; Peter Mitchell
Licensed under CERN OHL
see LICENSE for more info</text>
<text x="-5.08" y="312.42" size="1.778" layer="95" rot="R180">MICROUSB</text>
<text x="-21.59" y="5.08" size="1.778" layer="94">10uF caps:
Samsung CL32A106MOJNNNE</text>
<text x="-24.13" y="72.39" size="1.778" layer="94">10uF caps:
Samsung CL32A106MOJNNNE</text>
<text x="130.81" y="129.54" size="1.778" layer="94" rot="MR180">H-Bridge (polarity flip)</text>
<wire x1="-30.48" y1="-2.54" x2="72.39" y2="-2.54" width="0.1524" layer="94"/>
<text x="74.93" y="97.79" size="1.778" layer="94">Transformer:
1-3: 0.47 | 6-9: 1 | 7-10: 1

Secondaries in series: 1:4.3</text>
<text x="102.87" y="281.94" size="1.778" layer="94">50 / 60 Hertz switch</text>
<wire x1="100.33" y1="285.75" x2="156.21" y2="285.75" width="0.1524" layer="94" style="shortdash"/>
<wire x1="156.21" y1="285.75" x2="156.21" y2="247.65" width="0.1524" layer="94" style="shortdash"/>
<wire x1="156.21" y1="247.65" x2="100.33" y2="247.65" width="0.1524" layer="94" style="shortdash"/>
<wire x1="100.33" y1="247.65" x2="100.33" y2="285.75" width="0.1524" layer="94" style="shortdash"/>
<wire x1="160.02" y1="285.75" x2="214.63" y2="285.75" width="0.1524" layer="94" style="shortdash"/>
<wire x1="214.63" y1="285.75" x2="214.63" y2="247.65" width="0.1524" layer="94" style="shortdash"/>
<wire x1="214.63" y1="247.65" x2="160.02" y2="247.65" width="0.1524" layer="94" style="shortdash"/>
<wire x1="160.02" y1="247.65" x2="160.02" y2="285.75" width="0.1524" layer="94" style="shortdash"/>
<text x="129.54" y="340.36" size="1.778" layer="94">Isolated 5VDC to MCU</text>
<text x="-7.62" y="-17.78" size="2.54" layer="94">Isolated side marked with dashed box!</text>
<wire x1="127" y1="344.17" x2="127" y2="289.56" width="0.1524" layer="94"/>
<wire x1="127" y1="289.56" x2="172.72" y2="289.56" width="0.1524" layer="94"/>
<wire x1="172.72" y1="289.56" x2="168.91" y2="289.56" width="0.1524" layer="94" style="shortdash"/>
<wire x1="172.72" y1="289.56" x2="214.63" y2="289.56" width="0.1524" layer="94" style="shortdash"/>
<wire x1="214.63" y1="289.56" x2="214.63" y2="344.17" width="0.1524" layer="94" style="shortdash"/>
<wire x1="214.63" y1="344.17" x2="168.91" y2="344.17" width="0.1524" layer="94" style="shortdash"/>
<wire x1="168.91" y1="344.17" x2="168.91" y2="289.56" width="0.1524" layer="94" style="shortdash"/>
<wire x1="168.91" y1="344.17" x2="127" y2="344.17" width="0.1524" layer="94"/>
<wire x1="27.94" y1="247.65" x2="27.94" y2="285.75" width="0.1524" layer="94" style="shortdash"/>
<wire x1="27.94" y1="285.75" x2="96.52" y2="285.75" width="0.1524" layer="94" style="shortdash"/>
<wire x1="96.52" y1="285.75" x2="96.52" y2="247.65" width="0.1524" layer="94" style="shortdash"/>
<wire x1="96.52" y1="247.65" x2="27.94" y2="247.65" width="0.1524" layer="94" style="shortdash"/>
<wire x1="27.94" y1="285.75" x2="-30.48" y2="285.75" width="0.1524" layer="94"/>
<wire x1="-30.48" y1="285.75" x2="-30.48" y2="247.65" width="0.1524" layer="94"/>
<wire x1="-30.48" y1="247.65" x2="27.94" y2="247.65" width="0.1524" layer="94"/>
<text x="-27.94" y="281.94" size="1.778" layer="94">Digital isolator</text>
<text x="19.05" y="279.4" size="1.778" layer="94">OUT      IN</text>
<wire x1="-11.43" y1="-21.59" x2="20.32" y2="-21.59" width="0.1524" layer="94" style="shortdash"/>
<wire x1="20.32" y1="-21.59" x2="20.32" y2="-11.43" width="0.1524" layer="94" style="shortdash"/>
<wire x1="20.32" y1="-11.43" x2="-11.43" y2="-11.43" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-11.43" y1="-11.43" x2="-11.43" y2="-21.59" width="0.1524" layer="94" style="shortdash"/>
<text x="15.24" y="227.33" size="1.778" layer="94">Voltage monitoring</text>
<text x="-27.94" y="240.03" size="1.778" layer="94">Main controller</text>
<text x="-27.94" y="128.27" size="1.778" layer="94">Power stages</text>
<wire x1="-30.48" y1="135.89" x2="73.66" y2="135.89" width="0.1524" layer="94" style="shortdash"/>
<wire x1="20.32" y1="-11.43" x2="74.93" y2="-11.43" width="0.1524" layer="94"/>
<wire x1="74.93" y1="-11.43" x2="74.93" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="74.93" y1="-21.59" x2="20.32" y2="-21.59" width="0.1524" layer="94"/>
<text x="137.16" y="325.12" size="1.6764" layer="94">CL32A106MOJNNNE</text>
<text x="114.3" y="88.9" size="1.778" layer="94">NR8040T220M</text>
<text x="48.26" y="330.2" size="1.778" layer="94">A750MS108M1AAAE013</text>
<text x="-25.4" y="334.01" size="1.778" layer="94">CUI inc PJ-047AH</text>
<text x="-2.54" y="297.18" size="1.778" layer="94" rot="R180">10118194-0001LF</text>
<wire x1="77.47" y1="243.84" x2="214.63" y2="243.84" width="0.1524" layer="94" style="shortdash"/>
<wire x1="77.47" y1="132.08" x2="77.47" y2="243.84" width="0.1524" layer="94" style="shortdash"/>
<wire x1="72.39" y1="132.08" x2="77.47" y2="132.08" width="0.1524" layer="94" style="shortdash"/>
<text x="179.07" y="-15.24" size="2.54" layer="94">V4.2</text>
<text x="81.28" y="213.36" size="6.4516" layer="94">Work
in
Progress</text>
<text x="190.5" y="35.56" size="1.778" layer="94">Load
resistor</text>
<text x="190.5" y="66.04" size="1.778" layer="94">DAC-sine
feedback
gen.</text>
<text x="115.57" y="16.51" size="1.778" layer="94">Buck converter
sine generator</text>
</plain>
<instances>
<instance part="U3" gate="G$1" x="21.59" y="35.56" smashed="yes" rot="MR0">
<attribute name="NAME" x="31.75" y="54.61" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="31.75" y="52.07" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="U2" gate="G$1" x="21.59" y="102.87" smashed="yes" rot="MR0">
<attribute name="NAME" x="31.75" y="121.92" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="31.75" y="119.38" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="Q3" gate="G$1" x="191.77" y="151.13" smashed="yes">
<attribute name="NAME" x="185.42" y="167.894" size="1.778" layer="95"/>
<attribute name="VALUE" x="185.42" y="164.846" size="1.778" layer="96"/>
</instance>
<instance part="Q2" gate="G$1" x="102.87" y="151.13" smashed="yes" rot="MR0">
<attribute name="NAME" x="109.22" y="167.894" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="109.22" y="164.846" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C20" gate="C" x="-1.27" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="-1.27" y="43.18" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="1.27" y="35.56" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C7" gate="C" x="-1.27" y="105.41" smashed="yes" rot="R90">
<attribute name="NAME" x="-1.27" y="110.49" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="1.27" y="102.87" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND11" gate="1" x="21.59" y="5.08" smashed="yes">
<attribute name="VALUE" x="19.05" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="GND12" gate="1" x="44.45" y="5.08" smashed="yes">
<attribute name="VALUE" x="41.91" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="21.59" y="72.39" smashed="yes">
<attribute name="VALUE" x="19.05" y="69.85" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="44.45" y="72.39" smashed="yes">
<attribute name="VALUE" x="41.91" y="69.85" size="1.778" layer="96"/>
</instance>
<instance part="Q1" gate="G$1" x="35.56" y="328.93" smashed="yes" rot="R90">
<attribute name="VALUE" x="39.37" y="332.74" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="39.37" y="335.28" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="U1" gate="G$1" x="39.37" y="306.07" smashed="yes" rot="R270">
<attribute name="NAME" x="34.29" y="297.942" size="1.778" layer="95"/>
<attribute name="VALUE" x="34.29" y="295.91" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="22.86" y="321.31" smashed="yes">
<attribute name="NAME" x="19.05" y="322.8086" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="19.05" y="318.008" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R5" gate="G$1" x="127" y="163.83" smashed="yes" rot="R270">
<attribute name="NAME" x="129.54" y="165.1" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="VALUE" x="129.54" y="161.29" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R6" gate="G$1" x="166.37" y="163.83" smashed="yes" rot="R270">
<attribute name="NAME" x="168.91" y="165.1" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="VALUE" x="168.91" y="161.29" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C3" gate="C" x="86.36" y="321.31" smashed="yes">
<attribute name="NAME" x="85.852" y="326.644" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="86.36" y="320.294" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JP1" gate="G$1" x="110.49" y="326.39" smashed="yes">
<attribute name="NAME" x="104.14" y="332.105" size="1.778" layer="95"/>
<attribute name="VALUE" x="104.14" y="321.31" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="97.79" y="313.69" smashed="yes" rot="MR0">
<attribute name="VALUE" x="100.33" y="311.15" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND6" gate="1" x="7.62" y="295.91" smashed="yes">
<attribute name="VALUE" x="5.08" y="293.37" size="1.778" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="17.78" y="295.91" smashed="yes" rot="MR0">
<attribute name="VALUE" x="20.32" y="293.37" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND2" gate="1" x="72.39" y="313.69" smashed="yes" rot="MR0">
<attribute name="VALUE" x="74.93" y="311.15" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND3" gate="1" x="86.36" y="313.69" smashed="yes" rot="MR0">
<attribute name="VALUE" x="88.9" y="311.15" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="J3" gate="G$1" x="185.42" y="265.43" smashed="yes">
<attribute name="NAME" x="193.548" y="277.368" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="193.294" y="274.32" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="FRAME1" gate="G$1" x="-38.1" y="-35.56" smashed="yes">
<attribute name="DRAWING_NAME" x="179.07" y="-20.32" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="179.07" y="-25.4" size="2.286" layer="94"/>
<attribute name="SHEET" x="192.405" y="-30.48" size="2.54" layer="94"/>
</instance>
<instance part="J2" gate="G$1" x="-12.7" y="303.53" smashed="yes">
<attribute name="NAME" x="-5.31213125" y="314.435409375" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J1" gate="G$1" x="-15.24" y="321.31" smashed="yes">
<attribute name="VALUE" x="-25.4" y="331.47" size="1.778" layer="96"/>
<attribute name="NAME" x="-25.4" y="321.31" size="1.778" layer="95"/>
</instance>
<instance part="JP2" gate="G$1" x="144.78" y="264.16" smashed="yes">
<attribute name="NAME" x="138.43" y="269.875" size="1.778" layer="95"/>
<attribute name="VALUE" x="138.43" y="259.08" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="128.27" y="267.97" smashed="yes" rot="R180">
<attribute name="NAME" x="132.08" y="266.4714" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="132.08" y="271.272" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="C8" gate="C" x="44.45" y="105.41" smashed="yes">
<attribute name="NAME" x="45.72" y="109.22" size="1.778" layer="95"/>
<attribute name="VALUE" x="44.45" y="104.14" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C21" gate="C" x="44.45" y="38.1" smashed="yes">
<attribute name="NAME" x="45.72" y="41.91" size="1.778" layer="95"/>
<attribute name="VALUE" x="44.45" y="36.83" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C2" gate="G$1" x="72.39" y="328.93" smashed="yes" rot="R270">
<attribute name="NAME" x="71.882" y="325.628" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="72.136" y="319.786" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C13" gate="G$1" x="130.81" y="85.09" smashed="yes" rot="R270">
<attribute name="NAME" x="131.318" y="83.058" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="131.318" y="75.692" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="L1" gate="G$1" x="118.11" y="85.09" smashed="yes">
<attribute name="NAME" x="119.38" y="87.63" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="119.38" y="83.566" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C25" gate="G$1" x="-10.16" y="16.51" smashed="yes" rot="R90">
<attribute name="NAME" x="-10.16" y="21.59" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="-10.16" y="16.51" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C26" gate="G$1" x="-2.54" y="16.51" smashed="yes" rot="R90">
<attribute name="NAME" x="-2.54" y="21.59" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="-2.54" y="16.51" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C27" gate="G$1" x="5.08" y="16.51" smashed="yes" rot="R90">
<attribute name="NAME" x="5.08" y="21.59" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="5.08" y="16.51" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C10" gate="G$1" x="-10.16" y="85.09" smashed="yes" rot="R90">
<attribute name="NAME" x="-10.16" y="90.17" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="-10.16" y="85.09" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C11" gate="G$1" x="-2.54" y="85.09" smashed="yes" rot="R90">
<attribute name="NAME" x="-2.54" y="90.17" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="-2.54" y="85.09" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C12" gate="G$1" x="5.08" y="85.09" smashed="yes" rot="R90">
<attribute name="NAME" x="5.08" y="90.17" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="5.08" y="85.09" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="GND-ISO14" gate="G$1" x="140.97" y="68.58" smashed="yes" rot="MR0">
<attribute name="VALUE" x="135.89" y="66.04" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GND-ISO9" gate="G$1" x="166.37" y="138.43" smashed="yes" rot="MR0">
<attribute name="VALUE" x="161.29" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="GND-ISO8" gate="G$1" x="127" y="138.43" smashed="yes">
<attribute name="VALUE" x="121.92" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="114.3" y="134.62" smashed="yes" rot="R270">
<attribute name="NAME" x="111.76" y="133.35" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="111.76" y="132.08" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="R8" gate="G$1" x="180.34" y="134.62" smashed="yes" rot="R270">
<attribute name="NAME" x="182.88" y="134.62" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="VALUE" x="183.134" y="129.794" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND-ISO12" gate="G$1" x="114.3" y="125.73" smashed="yes">
<attribute name="VALUE" x="109.22" y="121.92" size="1.778" layer="96"/>
</instance>
<instance part="GND-ISO13" gate="G$1" x="180.34" y="125.73" smashed="yes">
<attribute name="VALUE" x="175.26" y="121.92" size="1.778" layer="96"/>
</instance>
<instance part="IC2" gate="G$1" x="2.54" y="185.42" smashed="yes" rot="MR180">
<attribute name="NAME" x="36.83" y="157.48" size="1.778" layer="95" rot="MR180" align="center-left"/>
<attribute name="VALUE" x="34.29" y="182.88" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="IC1" gate="G$1" x="41.91" y="267.97" smashed="yes" rot="MR0">
<attribute name="NAME" x="36.83" y="275.59" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="36.83" y="273.05" size="1.778" layer="96" rot="MR0" align="center-left"/>
</instance>
<instance part="C9" gate="C" x="55.88" y="105.41" smashed="yes">
<attribute name="NAME" x="55.88" y="110.49" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="55.88" y="104.14" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C22" gate="C" x="55.88" y="38.1" smashed="yes">
<attribute name="NAME" x="55.88" y="43.18" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="55.88" y="36.83" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PS1" gate="G$1" x="154.94" y="314.96" smashed="yes" rot="MR180">
<attribute name="NAME" x="180.34" y="325.12" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="180.34" y="322.58" size="1.778" layer="96" rot="MR0" align="center-left"/>
</instance>
<instance part="IC3" gate="G$1" x="114.3" y="85.09" smashed="yes" rot="MR0">
<attribute name="NAME" x="109.22" y="77.47" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="109.22" y="90.17" size="1.778" layer="96" rot="MR0" align="center-left"/>
</instance>
<instance part="T1" gate="G$1" x="60.96" y="87.63" smashed="yes">
<attribute name="NAME" x="60.96" y="95.25" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="60.96" y="92.71" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C5" gate="C" x="-8.89" y="262.89" smashed="yes">
<attribute name="NAME" x="-10.668" y="268.224" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-10.16" y="261.874" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND8" gate="1" x="-8.89" y="255.27" smashed="yes">
<attribute name="VALUE" x="-11.43" y="252.73" size="1.778" layer="96"/>
</instance>
<instance part="GND-ISO1" gate="G$1" x="196.85" y="307.34" smashed="yes" rot="MR0">
<attribute name="VALUE" x="191.77" y="303.53" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="148.59" y="313.69" smashed="yes" rot="R90">
<attribute name="NAME" x="148.082" y="319.278" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="148.59" y="313.436" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="GND5" gate="1" x="143.51" y="307.34" smashed="yes">
<attribute name="VALUE" x="140.97" y="304.8" size="1.778" layer="96"/>
</instance>
<instance part="GND-ISO4" gate="G$1" x="76.2" y="255.27" smashed="yes" rot="MR0">
<attribute name="VALUE" x="71.12" y="251.46" size="1.778" layer="96"/>
</instance>
<instance part="GND-ISO6" gate="G$1" x="-1.27" y="179.07" smashed="yes" rot="MR0">
<attribute name="VALUE" x="-6.35" y="176.53" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="C6" gate="C" x="-10.16" y="189.23" smashed="yes" rot="R180">
<attribute name="NAME" x="-8.382" y="183.896" size="1.778" layer="95"/>
<attribute name="VALUE" x="-8.89" y="190.246" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="33.02" y="219.71" smashed="yes">
<attribute name="NAME" x="35.56" y="220.98" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="35.56" y="223.52" size="1.778" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="R3" gate="G$1" x="22.86" y="219.71" smashed="yes">
<attribute name="NAME" x="25.4" y="220.98" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="25.4" y="223.52" size="1.778" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="FID1" gate="G$1" x="92.71" y="-16.51" smashed="yes"/>
<instance part="FID2" gate="G$1" x="99.06" y="-16.51" smashed="yes"/>
<instance part="FID3" gate="G$1" x="105.41" y="-16.51" smashed="yes"/>
<instance part="GND-ISO5" gate="G$1" x="38.1" y="214.63" smashed="yes" rot="MR0">
<attribute name="VALUE" x="33.02" y="212.09" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GND-ISO7" gate="G$1" x="17.78" y="149.86" smashed="yes" rot="MR0">
<attribute name="VALUE" x="12.7" y="147.32" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GND-ISO2" gate="G$1" x="121.92" y="259.08" smashed="yes" rot="MR0">
<attribute name="VALUE" x="116.84" y="256.54" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GND-ISO3" gate="G$1" x="203.2" y="259.08" smashed="yes" rot="MR0">
<attribute name="VALUE" x="198.12" y="256.54" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="IC4" gate="G$1" x="128.27" y="43.18" smashed="yes">
<attribute name="NAME" x="133.35" y="58.42" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="118.11" y="27.94" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C16" gate="G$1" x="93.98" y="41.91" smashed="yes" rot="R270">
<attribute name="NAME" x="92.71" y="41.91" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="92.71" y="36.83" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="R12" gate="G$1" x="104.14" y="45.72" smashed="yes">
<attribute name="NAME" x="105.41" y="49.53" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="VALUE" x="97.79" y="49.53" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="C19" gate="C" x="104.14" y="40.64" smashed="yes" rot="MR180">
<attribute name="NAME" x="102.362" y="35.306" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="102.87" y="41.656" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND-ISO16" gate="G$1" x="104.14" y="27.94" smashed="yes" rot="MR0">
<attribute name="VALUE" x="99.06" y="25.4" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="C15" gate="C" x="146.05" y="50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="147.32" y="54.61" size="1.778" layer="95"/>
<attribute name="VALUE" x="147.32" y="53.34" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="L2" gate="G$1" x="152.4" y="45.72" smashed="yes">
<attribute name="NAME" x="151.13" y="48.26" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="156.21" y="48.26" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C17" gate="G$1" x="166.37" y="41.91" smashed="yes" rot="R270">
<attribute name="NAME" x="165.1" y="41.91" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="165.1" y="36.83" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C18" gate="G$1" x="176.53" y="41.91" smashed="yes" rot="R270">
<attribute name="NAME" x="175.26" y="41.91" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="175.26" y="36.83" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="GND-ISO17" gate="G$1" x="176.53" y="26.67" smashed="yes" rot="MR0">
<attribute name="VALUE" x="171.45" y="24.13" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="C23" gate="C" x="156.21" y="27.94" smashed="yes">
<attribute name="NAME" x="158.75" y="30.48" size="1.778" layer="95"/>
<attribute name="VALUE" x="158.75" y="29.21" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="C24" gate="C" x="149.86" y="22.86" smashed="yes" rot="MR0">
<attribute name="NAME" x="147.32" y="24.13" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="147.32" y="22.86" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R14" gate="G$1" x="156.21" y="16.51" smashed="yes" rot="R90">
<attribute name="NAME" x="158.75" y="19.05" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="VALUE" x="158.75" y="16.51" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="GND-ISO18" gate="G$1" x="156.21" y="6.35" smashed="yes" rot="MR0">
<attribute name="VALUE" x="151.13" y="3.81" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="R9" gate="G$1" x="184.15" y="77.47" smashed="yes">
<attribute name="NAME" x="181.61" y="81.28" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="VALUE" x="181.61" y="74.93" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="R11" gate="G$1" x="184.15" y="59.69" smashed="yes" rot="R180">
<attribute name="NAME" x="181.61" y="63.5" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="VALUE" x="181.61" y="57.15" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="R10" gate="G$1" x="168.91" y="69.85" smashed="yes" rot="R90">
<attribute name="NAME" x="171.45" y="72.39" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="VALUE" x="171.45" y="69.85" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="GND-ISO15" gate="G$1" x="168.91" y="60.96" smashed="yes" rot="MR0">
<attribute name="VALUE" x="163.83" y="58.42" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP3" gate="G$1" x="194.31" y="195.58" smashed="yes">
<attribute name="NAME" x="187.96" y="201.295" size="1.778" layer="95"/>
<attribute name="VALUE" x="187.96" y="190.5" size="1.778" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="186.69" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="185.42" y="43.18" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="185.42" y="40.64" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="C14" gate="G$1" x="140.97" y="85.09" smashed="yes" rot="R270">
<attribute name="NAME" x="141.478" y="83.058" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="141.478" y="75.692" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C1" gate="G$1" x="59.69" y="328.93" smashed="yes" rot="R270">
<attribute name="NAME" x="59.182" y="325.628" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="59.436" y="319.786" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="GND1" gate="1" x="59.69" y="313.69" smashed="yes" rot="MR0">
<attribute name="VALUE" x="62.23" y="311.15" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND-ISO10" gate="G$1" x="90.17" y="133.35" smashed="yes" rot="MR0">
<attribute name="VALUE" x="85.09" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="GND-ISO11" gate="G$1" x="204.47" y="133.35" smashed="yes" rot="MR0">
<attribute name="VALUE" x="199.39" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="Q4" gate="G$1" x="165.1" y="148.59" smashed="yes" rot="R180">
<attribute name="NAME" x="158.75" y="158.75" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="158.75" y="143.51" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="PSOUT2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VSW"/>
<wire x1="59.69" y1="27.94" x2="36.83" y2="27.94" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="3"/>
<wire x1="60.96" y1="82.55" x2="59.69" y2="82.55" width="0.1524" layer="91"/>
<wire x1="59.69" y1="82.55" x2="59.69" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BOOT" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BOOT"/>
<pinref part="C7" gate="C" pin="2"/>
<wire x1="6.35" y1="105.41" x2="1.27" y2="105.41" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BOOT_R" class="0">
<segment>
<pinref part="C7" gate="C" pin="1"/>
<wire x1="-6.35" y1="105.41" x2="-8.89" y2="105.41" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="105.41" x2="-8.89" y2="100.33" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="BOOT_R"/>
<wire x1="-8.89" y1="100.33" x2="6.35" y2="100.33" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PGND_PAD"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="21.59" y1="80.01" x2="21.59" y2="78.74" width="0.1524" layer="91"/>
<wire x1="21.59" y1="78.74" x2="21.59" y2="74.93" width="0.1524" layer="91"/>
<wire x1="5.08" y1="78.74" x2="21.59" y2="78.74" width="0.1524" layer="91"/>
<junction x="21.59" y="78.74"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="5.08" y1="78.74" x2="-2.54" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="78.74" x2="-10.16" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="78.74" x2="-10.16" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="83.82" x2="-2.54" y2="78.74" width="0.1524" layer="91"/>
<junction x="-2.54" y="78.74"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="5.08" y1="83.82" x2="5.08" y2="78.74" width="0.1524" layer="91"/>
<junction x="5.08" y="78.74"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PGND_PAD"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="21.59" y1="12.7" x2="21.59" y2="11.43" width="0.1524" layer="91"/>
<wire x1="21.59" y1="11.43" x2="21.59" y2="7.62" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="21.59" y1="11.43" x2="5.08" y2="11.43" width="0.1524" layer="91"/>
<wire x1="5.08" y1="11.43" x2="-2.54" y2="11.43" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="11.43" x2="-10.16" y2="11.43" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="11.43" x2="-10.16" y2="15.24" width="0.1524" layer="91"/>
<junction x="21.59" y="11.43"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="15.24" x2="-2.54" y2="11.43" width="0.1524" layer="91"/>
<junction x="-2.54" y="11.43"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="5.08" y1="15.24" x2="5.08" y2="11.43" width="0.1524" layer="91"/>
<junction x="5.08" y="11.43"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="PGND"/>
<wire x1="44.45" y1="7.62" x2="44.45" y2="33.02" width="0.1524" layer="91"/>
<wire x1="44.45" y1="33.02" x2="36.83" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C21" gate="C" pin="2"/>
<wire x1="44.45" y1="33.02" x2="44.45" y2="35.56" width="0.1524" layer="91"/>
<junction x="44.45" y="33.02"/>
<pinref part="C22" gate="C" pin="2"/>
<wire x1="55.88" y1="35.56" x2="55.88" y2="33.02" width="0.1524" layer="91"/>
<wire x1="55.88" y1="33.02" x2="44.45" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="U2" gate="G$1" pin="PGND"/>
<wire x1="44.45" y1="74.93" x2="44.45" y2="100.33" width="0.1524" layer="91"/>
<wire x1="44.45" y1="100.33" x2="36.83" y2="100.33" width="0.1524" layer="91"/>
<pinref part="C8" gate="C" pin="2"/>
<wire x1="44.45" y1="102.87" x2="44.45" y2="100.33" width="0.1524" layer="91"/>
<junction x="44.45" y="100.33"/>
<pinref part="C9" gate="C" pin="2"/>
<wire x1="55.88" y1="102.87" x2="55.88" y2="100.33" width="0.1524" layer="91"/>
<wire x1="55.88" y1="100.33" x2="44.45" y2="100.33" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="29.21" y1="303.53" x2="17.78" y2="303.53" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="17.78" y1="303.53" x2="17.78" y2="298.45" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="C" pin="2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="86.36" y1="316.23" x2="86.36" y2="318.77" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="97.79" y1="316.23" x2="97.79" y2="326.39" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="97.79" y1="326.39" x2="107.95" y2="326.39" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C2" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="-12.7" y1="323.85" x2="7.62" y2="323.85" width="0.1524" layer="91"/>
<wire x1="7.62" y1="323.85" x2="7.62" y2="300.99" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="GND"/>
<wire x1="7.62" y1="300.99" x2="7.62" y2="298.45" width="0.1524" layer="91"/>
<wire x1="0" y1="300.99" x2="7.62" y2="300.99" width="0.1524" layer="91"/>
<junction x="7.62" y="300.99"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND2"/>
<wire x1="13.97" y1="260.35" x2="-8.89" y2="260.35" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="260.35" x2="-8.89" y2="257.81" width="0.1524" layer="91"/>
<pinref part="C5" gate="C" pin="2"/>
<junction x="-8.89" y="260.35"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="PS1" gate="G$1" pin="-VIN"/>
<wire x1="154.94" y1="314.96" x2="152.4" y2="314.96" width="0.1524" layer="91"/>
<wire x1="152.4" y1="314.96" x2="152.4" y2="311.15" width="0.1524" layer="91"/>
<wire x1="152.4" y1="311.15" x2="148.59" y2="311.15" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="148.59" y1="311.15" x2="143.51" y2="311.15" width="0.1524" layer="91"/>
<wire x1="148.59" y1="312.42" x2="148.59" y2="311.15" width="0.1524" layer="91"/>
<junction x="148.59" y="311.15"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="143.51" y1="311.15" x2="143.51" y2="309.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="-"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
</net>
<net name="PWM" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PWM"/>
<wire x1="6.35" y1="115.57" x2="1.27" y2="115.57" width="0.1524" layer="91"/>
<label x="1.27" y="115.57" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="B1"/>
<wire x1="13.97" y1="265.43" x2="12.7" y2="265.43" width="0.1524" layer="91"/>
<label x="12.7" y="265.43" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NPWM" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PWM"/>
<wire x1="6.35" y1="48.26" x2="1.27" y2="48.26" width="0.1524" layer="91"/>
<label x="1.27" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="B2"/>
<wire x1="13.97" y1="262.89" x2="5.08" y2="262.89" width="0.1524" layer="91"/>
<label x="5.08" y="262.89" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="5VDC" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SKIP#"/>
<wire x1="36.83" y1="115.57" x2="40.64" y2="115.57" width="0.1524" layer="91"/>
<wire x1="40.64" y1="115.57" x2="40.64" y2="113.03" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VDD"/>
<wire x1="40.64" y1="113.03" x2="40.64" y2="105.41" width="0.1524" layer="91"/>
<wire x1="40.64" y1="105.41" x2="36.83" y2="105.41" width="0.1524" layer="91"/>
<wire x1="40.64" y1="113.03" x2="44.45" y2="113.03" width="0.1524" layer="91"/>
<junction x="40.64" y="113.03"/>
<label x="44.45" y="116.84" size="1.778" layer="95" xref="yes"/>
<pinref part="C8" gate="C" pin="1"/>
<wire x1="44.45" y1="113.03" x2="44.45" y2="116.84" width="0.1524" layer="91"/>
<wire x1="44.45" y1="110.49" x2="44.45" y2="113.03" width="0.1524" layer="91"/>
<junction x="44.45" y="113.03"/>
<pinref part="C9" gate="C" pin="1"/>
<wire x1="44.45" y1="113.03" x2="55.88" y2="113.03" width="0.1524" layer="91"/>
<wire x1="55.88" y1="113.03" x2="55.88" y2="110.49" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="SKIP#"/>
<wire x1="36.83" y1="48.26" x2="40.64" y2="48.26" width="0.1524" layer="91"/>
<wire x1="40.64" y1="48.26" x2="40.64" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VDD"/>
<wire x1="40.64" y1="45.72" x2="40.64" y2="38.1" width="0.1524" layer="91"/>
<wire x1="40.64" y1="38.1" x2="36.83" y2="38.1" width="0.1524" layer="91"/>
<junction x="40.64" y="45.72"/>
<wire x1="40.64" y1="45.72" x2="44.45" y2="45.72" width="0.1524" layer="91"/>
<label x="44.45" y="50.8" size="1.778" layer="95" xref="yes"/>
<pinref part="C21" gate="C" pin="1"/>
<wire x1="44.45" y1="45.72" x2="44.45" y2="50.8" width="0.1524" layer="91"/>
<wire x1="44.45" y1="43.18" x2="44.45" y2="45.72" width="0.1524" layer="91"/>
<junction x="44.45" y="45.72"/>
<pinref part="C22" gate="C" pin="1"/>
<wire x1="44.45" y1="45.72" x2="55.88" y2="45.72" width="0.1524" layer="91"/>
<wire x1="55.88" y1="45.72" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="+"/>
<pinref part="JP1" gate="G$1" pin="1"/>
<junction x="72.39" y="328.93"/>
<wire x1="72.39" y1="328.93" x2="86.36" y2="328.93" width="0.1524" layer="91"/>
<wire x1="86.36" y1="328.93" x2="107.95" y2="328.93" width="0.1524" layer="91"/>
<pinref part="C3" gate="C" pin="1"/>
<wire x1="86.36" y1="326.39" x2="86.36" y2="328.93" width="0.1524" layer="91"/>
<junction x="86.36" y="328.93"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="40.64" y1="328.93" x2="59.69" y2="328.93" width="0.1524" layer="91"/>
<wire x1="59.69" y1="328.93" x2="72.39" y2="328.93" width="0.1524" layer="91"/>
<wire x1="86.36" y1="328.93" x2="86.36" y2="337.82" width="0.1524" layer="91"/>
<wire x1="86.36" y1="337.82" x2="90.17" y2="337.82" width="0.1524" layer="91"/>
<label x="90.17" y="337.82" size="1.778" layer="95" xref="yes"/>
<pinref part="C1" gate="G$1" pin="+"/>
<junction x="59.69" y="328.93"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VIN"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="6.35" y1="27.94" x2="5.08" y2="27.94" width="0.1524" layer="91"/>
<wire x1="5.08" y1="27.94" x2="-2.54" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="27.94" x2="-10.16" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="27.94" x2="-10.16" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="22.86" x2="-2.54" y2="27.94" width="0.1524" layer="91"/>
<junction x="-2.54" y="27.94"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="5.08" y1="22.86" x2="5.08" y2="27.94" width="0.1524" layer="91"/>
<junction x="5.08" y="27.94"/>
<wire x1="-10.16" y1="27.94" x2="-12.7" y2="27.94" width="0.1524" layer="91"/>
<junction x="-10.16" y="27.94"/>
<label x="-12.7" y="27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VIN"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="6.35" y1="95.25" x2="5.08" y2="95.25" width="0.1524" layer="91"/>
<wire x1="5.08" y1="95.25" x2="-2.54" y2="95.25" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="95.25" x2="-10.16" y2="95.25" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="95.25" x2="-10.16" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="91.44" x2="-2.54" y2="95.25" width="0.1524" layer="91"/>
<junction x="-2.54" y="95.25"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="5.08" y1="91.44" x2="5.08" y2="95.25" width="0.1524" layer="91"/>
<junction x="5.08" y="95.25"/>
<wire x1="-10.16" y1="95.25" x2="-12.7" y2="95.25" width="0.1524" layer="91"/>
<junction x="-10.16" y="95.25"/>
<label x="-12.7" y="95.25" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VDD2"/>
<wire x1="13.97" y1="267.97" x2="-8.89" y2="267.97" width="0.1524" layer="91"/>
<pinref part="C5" gate="C" pin="1"/>
<wire x1="-8.89" y1="267.97" x2="-8.89" y2="270.51" width="0.1524" layer="91"/>
<junction x="-8.89" y="267.97"/>
<wire x1="-8.89" y1="270.51" x2="-16.51" y2="270.51" width="0.1524" layer="91"/>
<label x="-16.51" y="270.51" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PS1" gate="G$1" pin="+VIN"/>
<wire x1="154.94" y1="317.5" x2="152.4" y2="317.5" width="0.1524" layer="91"/>
<wire x1="152.4" y1="317.5" x2="152.4" y2="321.31" width="0.1524" layer="91"/>
<wire x1="152.4" y1="321.31" x2="148.59" y2="321.31" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="148.59" y1="321.31" x2="143.51" y2="321.31" width="0.1524" layer="91"/>
<wire x1="148.59" y1="320.04" x2="148.59" y2="321.31" width="0.1524" layer="91"/>
<junction x="148.59" y="321.31"/>
<label x="143.51" y="321.31" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ISO_GND" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="S2"/>
<wire x1="90.17" y1="140.97" x2="93.98" y2="140.97" width="0.1524" layer="91"/>
<wire x1="90.17" y1="140.97" x2="90.17" y2="135.89" width="0.1524" layer="91"/>
<pinref part="GND-ISO10" gate="G$1" pin="GND-ISO"/>
</segment>
<segment>
<pinref part="GND-ISO12" gate="G$1" pin="GND-ISO"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="114.3" y1="128.27" x2="114.3" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND-ISO13" gate="G$1" pin="GND-ISO"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="180.34" y1="128.27" x2="180.34" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PS1" gate="G$1" pin="-VOUT"/>
<wire x1="185.42" y1="314.96" x2="187.96" y2="314.96" width="0.1524" layer="91"/>
<wire x1="187.96" y1="314.96" x2="187.96" y2="311.15" width="0.1524" layer="91"/>
<wire x1="187.96" y1="311.15" x2="196.85" y2="311.15" width="0.1524" layer="91"/>
<pinref part="GND-ISO1" gate="G$1" pin="GND-ISO"/>
<wire x1="196.85" y1="309.88" x2="196.85" y2="311.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND-ISO4" gate="G$1" pin="GND-ISO"/>
<pinref part="IC1" gate="G$1" pin="GND1"/>
<wire x1="41.91" y1="260.35" x2="76.2" y2="260.35" width="0.1524" layer="91"/>
<wire x1="76.2" y1="260.35" x2="76.2" y2="257.81" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<wire x1="2.54" y1="190.5" x2="-1.27" y2="190.5" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="190.5" x2="-1.27" y2="182.88" width="0.1524" layer="91"/>
<pinref part="GND-ISO6" gate="G$1" pin="GND-ISO"/>
<pinref part="C6" gate="C" pin="1"/>
<wire x1="-1.27" y1="182.88" x2="-1.27" y2="181.61" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="184.15" x2="-10.16" y2="182.88" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="182.88" x2="-1.27" y2="182.88" width="0.1524" layer="91"/>
<junction x="-1.27" y="182.88"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="GND-ISO5" gate="G$1" pin="GND-ISO"/>
<wire x1="38.1" y1="217.17" x2="38.1" y2="219.71" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="EP"/>
<pinref part="GND-ISO7" gate="G$1" pin="GND-ISO"/>
<wire x1="17.78" y1="154.94" x2="17.78" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="2"/>
<wire x1="142.24" y1="264.16" x2="134.62" y2="264.16" width="0.1524" layer="91"/>
<wire x1="134.62" y1="264.16" x2="134.62" y2="262.89" width="0.1524" layer="91"/>
<wire x1="134.62" y1="262.89" x2="121.92" y2="262.89" width="0.1524" layer="91"/>
<pinref part="GND-ISO2" gate="G$1" pin="GND-ISO"/>
<wire x1="121.92" y1="262.89" x2="121.92" y2="261.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="6"/>
<wire x1="195.58" y1="262.89" x2="203.2" y2="262.89" width="0.1524" layer="91"/>
<wire x1="203.2" y1="262.89" x2="203.2" y2="261.62" width="0.1524" layer="91"/>
<pinref part="GND-ISO3" gate="G$1" pin="GND-ISO"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="GND"/>
<pinref part="C19" gate="C" pin="1"/>
<wire x1="110.49" y1="35.56" x2="104.14" y2="35.56" width="0.1524" layer="91"/>
<wire x1="104.14" y1="35.56" x2="104.14" y2="33.02" width="0.1524" layer="91"/>
<junction x="104.14" y="35.56"/>
<pinref part="IC4" gate="G$1" pin="EP_(GND)"/>
<wire x1="104.14" y1="33.02" x2="110.49" y2="33.02" width="0.1524" layer="91"/>
<wire x1="104.14" y1="33.02" x2="93.98" y2="33.02" width="0.1524" layer="91"/>
<junction x="104.14" y="33.02"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="93.98" y1="33.02" x2="93.98" y2="35.56" width="0.1524" layer="91"/>
<wire x1="104.14" y1="33.02" x2="104.14" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND-ISO16" gate="G$1" pin="GND-ISO"/>
</segment>
<segment>
<pinref part="C24" gate="C" pin="2"/>
<wire x1="149.86" y1="20.32" x2="149.86" y2="10.16" width="0.1524" layer="91"/>
<wire x1="149.86" y1="10.16" x2="156.21" y2="10.16" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="156.21" y1="10.16" x2="156.21" y2="11.43" width="0.1524" layer="91"/>
<wire x1="156.21" y1="10.16" x2="156.21" y2="8.89" width="0.1524" layer="91"/>
<junction x="156.21" y="10.16"/>
<pinref part="GND-ISO18" gate="G$1" pin="GND-ISO"/>
</segment>
<segment>
<pinref part="GND-ISO15" gate="G$1" pin="GND-ISO"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="168.91" y1="63.5" x2="168.91" y2="64.77" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND-ISO14" gate="G$1" pin="GND-ISO"/>
<pinref part="C13" gate="G$1" pin="-"/>
<pinref part="IC3" gate="G$1" pin="-"/>
<wire x1="130.81" y1="72.39" x2="115.57" y2="72.39" width="0.1524" layer="91"/>
<wire x1="115.57" y1="72.39" x2="115.57" y2="82.55" width="0.1524" layer="91"/>
<wire x1="115.57" y1="82.55" x2="114.3" y2="82.55" width="0.1524" layer="91"/>
<junction x="130.81" y="72.39"/>
<pinref part="C14" gate="G$1" pin="-"/>
<wire x1="140.97" y1="72.39" x2="130.81" y2="72.39" width="0.1524" layer="91"/>
<wire x1="140.97" y1="71.12" x2="140.97" y2="72.39" width="0.1524" layer="91"/>
<junction x="140.97" y="72.39"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="176.53" y1="35.56" x2="176.53" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="176.53" y1="33.02" x2="166.37" y2="33.02" width="0.1524" layer="91"/>
<wire x1="166.37" y1="33.02" x2="166.37" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="186.69" y1="33.02" x2="186.69" y2="35.56" width="0.1524" layer="91"/>
<wire x1="176.53" y1="33.02" x2="186.69" y2="33.02" width="0.1524" layer="91"/>
<junction x="176.53" y="33.02"/>
<pinref part="GND-ISO17" gate="G$1" pin="GND-ISO"/>
<wire x1="176.53" y1="29.21" x2="176.53" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q3" gate="G$1" pin="S2"/>
<wire x1="200.66" y1="140.97" x2="204.47" y2="140.97" width="0.1524" layer="91"/>
<pinref part="GND-ISO11" gate="G$1" pin="GND-ISO"/>
<wire x1="204.47" y1="135.89" x2="204.47" y2="140.97" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q4" gate="G$1" pin="SOURCE2"/>
<wire x1="129.54" y1="148.59" x2="127" y2="148.59" width="0.1524" layer="91"/>
<wire x1="127" y1="148.59" x2="127" y2="140.97" width="0.1524" layer="91"/>
<pinref part="GND-ISO8" gate="G$1" pin="GND-ISO"/>
</segment>
<segment>
<pinref part="Q4" gate="G$1" pin="SOURCE1"/>
<pinref part="GND-ISO9" gate="G$1" pin="GND-ISO"/>
<wire x1="165.1" y1="148.59" x2="166.37" y2="148.59" width="0.1524" layer="91"/>
<wire x1="166.37" y1="148.59" x2="166.37" y2="140.97" width="0.1524" layer="91"/>
</segment>
</net>
<net name="9VAC_OUT2" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="D2"/>
<wire x1="204.47" y1="151.13" x2="208.28" y2="151.13" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="D1"/>
<wire x1="200.66" y1="154.94" x2="204.47" y2="154.94" width="0.1524" layer="91"/>
<wire x1="204.47" y1="154.94" x2="204.47" y2="151.13" width="0.1524" layer="91"/>
<junction x="204.47" y="151.13"/>
<wire x1="204.47" y1="151.13" x2="204.47" y2="146.05" width="0.1524" layer="91"/>
<wire x1="204.47" y1="146.05" x2="200.66" y2="146.05" width="0.1524" layer="91"/>
<label x="208.28" y="151.13" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="2"/>
<wire x1="191.77" y1="195.58" x2="185.42" y2="195.58" width="0.1524" layer="91"/>
<wire x1="185.42" y1="195.58" x2="185.42" y2="191.77" width="0.1524" layer="91"/>
<wire x1="185.42" y1="191.77" x2="180.34" y2="191.77" width="0.1524" layer="91"/>
<label x="173.99" y="191.77" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="180.34" y1="191.77" x2="173.99" y2="191.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5VDC_PSU" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="PWR"/>
<wire x1="-12.7" y1="328.93" x2="1.27" y2="328.93" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="VDD"/>
<wire x1="0" y1="306.07" x2="1.27" y2="306.07" width="0.1524" layer="91"/>
<wire x1="1.27" y1="306.07" x2="1.27" y2="328.93" width="0.1524" layer="91"/>
<label x="11.43" y="328.93" size="1.4224" layer="95" rot="R270" xref="yes"/>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="17.78" y1="321.31" x2="17.78" y2="328.93" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<junction x="17.78" y="321.31"/>
<wire x1="17.78" y1="328.93" x2="30.48" y2="328.93" width="0.1524" layer="91"/>
<wire x1="17.78" y1="321.31" x2="17.78" y2="306.07" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="29.21" y1="306.07" x2="17.78" y2="306.07" width="0.1524" layer="91"/>
<wire x1="1.27" y1="328.93" x2="17.78" y2="328.93" width="0.1524" layer="91"/>
<junction x="1.27" y="328.93"/>
<junction x="17.78" y="328.93"/>
</segment>
</net>
<net name="BOOT2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="BOOT"/>
<pinref part="C20" gate="C" pin="2"/>
<wire x1="6.35" y1="38.1" x2="1.27" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BOOT_R2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="BOOT_R"/>
<wire x1="6.35" y1="33.02" x2="-8.89" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="33.02" x2="-8.89" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C20" gate="C" pin="1"/>
<wire x1="-8.89" y1="38.1" x2="-6.35" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SW_50_60" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="123.19" y1="267.97" x2="121.92" y2="267.97" width="0.1524" layer="91"/>
<label x="121.92" y="267.97" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PC1"/>
<wire x1="30.48" y1="154.94" x2="30.48" y2="153.67" width="0.1524" layer="91"/>
<wire x1="30.48" y1="153.67" x2="33.02" y2="153.67" width="0.1524" layer="91"/>
<label x="33.02" y="153.67" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="114.3" y1="85.09" x2="116.84" y2="85.09" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<pinref part="IC3" gate="G$1" pin="+"/>
</segment>
</net>
<net name="SPWM1" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="G2"/>
<wire x1="180.34" y1="143.51" x2="182.88" y2="143.51" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="180.34" y1="139.7" x2="180.34" y2="143.51" width="0.1524" layer="91"/>
<junction x="180.34" y="143.51"/>
<wire x1="180.34" y1="143.51" x2="179.07" y2="143.51" width="0.1524" layer="91"/>
<label x="179.07" y="143.51" size="1.6764" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PB0"/>
<wire x1="45.72" y1="187.96" x2="59.69" y2="187.96" width="0.1524" layer="91"/>
<label x="59.69" y="187.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<label x="125.73" y="151.13" size="1.6764" layer="95" rot="R180" xref="yes"/>
<pinref part="Q4" gate="G$1" pin="GATE2"/>
<wire x1="129.54" y1="151.13" x2="125.73" y2="151.13" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VOUT"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="27.94" y1="321.31" x2="29.21" y2="321.31" width="0.1524" layer="91"/>
<wire x1="29.21" y1="321.31" x2="29.21" y2="308.61" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="33.02" y1="323.85" x2="33.02" y2="321.31" width="0.1524" layer="91"/>
<wire x1="33.02" y1="321.31" x2="29.21" y2="321.31" width="0.1524" layer="91"/>
<junction x="29.21" y="321.31"/>
</segment>
</net>
<net name="SPWM2" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="G2"/>
<wire x1="111.76" y1="143.51" x2="114.3" y2="143.51" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="114.3" y1="139.7" x2="114.3" y2="143.51" width="0.1524" layer="91"/>
<junction x="114.3" y="143.51"/>
<wire x1="114.3" y1="143.51" x2="115.57" y2="143.51" width="0.1524" layer="91"/>
<label x="115.57" y="143.51" size="1.6764" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PB1"/>
<wire x1="45.72" y1="190.5" x2="48.26" y2="190.5" width="0.1524" layer="91"/>
<label x="48.26" y="190.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="168.91" y1="151.13" x2="165.1" y2="151.13" width="0.1524" layer="91"/>
<label x="168.91" y="151.13" size="1.6764" layer="95" xref="yes"/>
<pinref part="Q4" gate="G$1" pin="GATE1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="142.24" y1="266.7" x2="134.62" y2="266.7" width="0.1524" layer="91"/>
<wire x1="134.62" y1="266.7" x2="134.62" y2="267.97" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="134.62" y1="267.97" x2="133.35" y2="267.97" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="N/A"/>
<wire x1="36.83" y1="110.49" x2="38.1" y2="110.49" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="N/A2"/>
<wire x1="6.35" y1="110.49" x2="5.08" y2="110.49" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="N/A2"/>
<wire x1="6.35" y1="43.18" x2="5.08" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="N/A"/>
<wire x1="36.83" y1="43.18" x2="38.1" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="9VAC_OUT1" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="D2"/>
<pinref part="Q2" gate="G$1" pin="D1"/>
<wire x1="93.98" y1="154.94" x2="90.17" y2="154.94" width="0.1524" layer="91"/>
<wire x1="90.17" y1="154.94" x2="90.17" y2="151.13" width="0.1524" layer="91"/>
<wire x1="90.17" y1="151.13" x2="86.36" y2="151.13" width="0.1524" layer="91"/>
<junction x="90.17" y="151.13"/>
<wire x1="90.17" y1="151.13" x2="90.17" y2="146.05" width="0.1524" layer="91"/>
<wire x1="90.17" y1="146.05" x2="93.98" y2="146.05" width="0.1524" layer="91"/>
<label x="86.36" y="151.13" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="1"/>
<wire x1="191.77" y1="198.12" x2="185.42" y2="198.12" width="0.1524" layer="91"/>
<wire x1="185.42" y1="198.12" x2="185.42" y2="204.47" width="0.1524" layer="91"/>
<wire x1="185.42" y1="204.47" x2="180.34" y2="204.47" width="0.1524" layer="91"/>
<label x="173.99" y="204.47" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="180.34" y1="204.47" x2="173.99" y2="204.47" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PGATE1" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="G1"/>
<wire x1="111.76" y1="157.48" x2="127" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="127" y1="158.75" x2="127" y2="157.48" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="DRAIN2"/>
<wire x1="129.54" y1="153.67" x2="127" y2="153.67" width="0.1524" layer="91"/>
<wire x1="127" y1="153.67" x2="127" y2="157.48" width="0.1524" layer="91"/>
<junction x="127" y="157.48"/>
</segment>
</net>
<net name="PGATE2" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="166.37" y1="158.75" x2="166.37" y2="157.48" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="G1"/>
<wire x1="182.88" y1="157.48" x2="166.37" y2="157.48" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="DRAIN1"/>
<wire x1="165.1" y1="153.67" x2="166.37" y2="153.67" width="0.1524" layer="91"/>
<wire x1="166.37" y1="153.67" x2="166.37" y2="157.48" width="0.1524" layer="91"/>
<junction x="166.37" y="157.48"/>
</segment>
</net>
<net name="PSOUT1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VSW"/>
<wire x1="36.83" y1="95.25" x2="59.69" y2="95.25" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="1"/>
<wire x1="60.96" y1="87.63" x2="59.69" y2="87.63" width="0.1524" layer="91"/>
<wire x1="59.69" y1="87.63" x2="59.69" y2="95.25" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UPDI" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="177.8" y1="267.97" x2="171.45" y2="267.97" width="0.1524" layer="91"/>
<label x="171.45" y="267.97" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PA0/!RESET!/UPDI"/>
<wire x1="22.86" y1="154.94" x2="22.86" y2="149.86" width="0.1524" layer="91"/>
<wire x1="22.86" y1="149.86" x2="33.02" y2="149.86" width="0.1524" layer="91"/>
<label x="33.02" y="149.86" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="T1" gate="G$1" pin="9"/>
<wire x1="83.82" y1="80.01" x2="85.09" y2="80.01" width="0.1524" layer="91"/>
<wire x1="85.09" y1="80.01" x2="85.09" y2="85.09" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="7"/>
<wire x1="85.09" y1="85.09" x2="83.82" y2="85.09" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="T1" gate="G$1" pin="10"/>
<wire x1="83.82" y1="77.47" x2="88.9" y2="77.47" width="0.1524" layer="91"/>
<wire x1="88.9" y1="77.47" x2="88.9" y2="82.55" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="~2"/>
<wire x1="88.9" y1="82.55" x2="91.44" y2="82.55" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="~1"/>
<wire x1="91.44" y1="85.09" x2="88.9" y2="85.09" width="0.1524" layer="91"/>
<wire x1="88.9" y1="85.09" x2="88.9" y2="87.63" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="6"/>
<wire x1="88.9" y1="87.63" x2="83.82" y2="87.63" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ISO_5V" class="0">
<segment>
<pinref part="PS1" gate="G$1" pin="+VOUT"/>
<wire x1="185.42" y1="317.5" x2="187.96" y2="317.5" width="0.1524" layer="91"/>
<wire x1="187.96" y1="317.5" x2="187.96" y2="321.31" width="0.1524" layer="91"/>
<wire x1="187.96" y1="321.31" x2="196.85" y2="321.31" width="0.1524" layer="91"/>
<label x="196.85" y="321.31" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VDD1"/>
<wire x1="41.91" y1="267.97" x2="76.2" y2="267.97" width="0.1524" layer="91"/>
<label x="76.2" y="267.97" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VDD"/>
<wire x1="2.54" y1="193.04" x2="-10.16" y2="193.04" width="0.1524" layer="91"/>
<label x="-13.97" y="193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="C6" gate="C" pin="2"/>
<wire x1="-10.16" y1="193.04" x2="-13.97" y2="193.04" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="191.77" x2="-10.16" y2="193.04" width="0.1524" layer="91"/>
<junction x="-10.16" y="193.04"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="195.58" y1="267.97" x2="200.66" y2="267.97" width="0.1524" layer="91"/>
<label x="200.66" y="267.97" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="ISO_PWM" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="A1"/>
<wire x1="41.91" y1="265.43" x2="43.18" y2="265.43" width="0.1524" layer="91"/>
<label x="43.18" y="265.43" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PA4"/>
<wire x1="2.54" y1="195.58" x2="1.27" y2="195.58" width="0.1524" layer="91"/>
<wire x1="1.27" y1="195.58" x2="1.27" y2="208.28" width="0.1524" layer="91"/>
<wire x1="1.27" y1="208.28" x2="-2.54" y2="208.28" width="0.1524" layer="91"/>
<label x="-2.54" y="208.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ISO_NPWM" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="A2"/>
<wire x1="41.91" y1="262.89" x2="57.15" y2="262.89" width="0.1524" layer="91"/>
<label x="57.15" y="262.89" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PA5"/>
<wire x1="17.78" y1="210.82" x2="17.78" y2="212.09" width="0.1524" layer="91"/>
<wire x1="17.78" y1="212.09" x2="-2.54" y2="212.09" width="0.1524" layer="91"/>
<label x="-2.54" y="212.09" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VOLMON" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="PB4"/>
<wire x1="27.94" y1="210.82" x2="27.94" y2="219.71" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="R3" gate="G$1" pin="2"/>
<junction x="27.94" y="219.71"/>
</segment>
</net>
<net name="ISO_13V" class="0">
<segment>
<wire x1="130.81" y1="85.09" x2="140.97" y2="85.09" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="+"/>
<junction x="130.81" y="85.09"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="140.97" y1="85.09" x2="151.13" y2="85.09" width="0.1524" layer="91"/>
<wire x1="124.46" y1="85.09" x2="130.81" y2="85.09" width="0.1524" layer="91"/>
<label x="151.13" y="85.09" size="1.778" layer="95" xref="yes"/>
<pinref part="C14" gate="G$1" pin="+"/>
<junction x="140.97" y="85.09"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="VIN"/>
<wire x1="110.49" y1="53.34" x2="93.98" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="93.98" y1="43.18" x2="93.98" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="93.98" y1="45.72" x2="93.98" y2="53.34" width="0.1524" layer="91"/>
<wire x1="99.06" y1="45.72" x2="93.98" y2="45.72" width="0.1524" layer="91"/>
<junction x="93.98" y="45.72"/>
<wire x1="93.98" y1="53.34" x2="91.44" y2="53.34" width="0.1524" layer="91"/>
<junction x="93.98" y="53.34"/>
<label x="91.44" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="127" y1="170.18" x2="127" y2="168.91" width="0.1524" layer="91"/>
<wire x1="127" y1="170.18" x2="128.27" y2="170.18" width="0.1524" layer="91"/>
<label x="128.27" y="170.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="166.37" y1="168.91" x2="166.37" y2="170.18" width="0.1524" layer="91"/>
<wire x1="166.37" y1="170.18" x2="165.1" y2="170.18" width="0.1524" layer="91"/>
<label x="165.1" y="170.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="17.78" y1="219.71" x2="15.24" y2="219.71" width="0.1524" layer="91"/>
<label x="15.24" y="219.71" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="EN"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="110.49" y1="45.72" x2="109.22" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="SS"/>
<pinref part="C19" gate="C" pin="2"/>
<wire x1="110.49" y1="43.18" x2="104.14" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ISO_12V7_SINE" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="158.75" y1="45.72" x2="166.37" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="166.37" y1="43.18" x2="166.37" y2="45.72" width="0.1524" layer="91"/>
<junction x="166.37" y="45.72"/>
<wire x1="166.37" y1="45.72" x2="176.53" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="176.53" y1="43.18" x2="176.53" y2="45.72" width="0.1524" layer="91"/>
<junction x="176.53" y="45.72"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="210.82" y1="77.47" x2="189.23" y2="77.47" width="0.1524" layer="91"/>
<wire x1="210.82" y1="77.47" x2="210.82" y2="52.07" width="0.1524" layer="91"/>
<wire x1="210.82" y1="52.07" x2="176.53" y2="52.07" width="0.1524" layer="91"/>
<wire x1="176.53" y1="52.07" x2="176.53" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="186.69" y1="45.72" x2="189.23" y2="45.72" width="0.1524" layer="91"/>
<label x="189.23" y="45.72" size="1.778" layer="95" xref="yes"/>
<wire x1="176.53" y1="45.72" x2="186.69" y2="45.72" width="0.1524" layer="91"/>
<junction x="186.69" y="45.72"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="S1"/>
<wire x1="91.44" y1="160.02" x2="93.98" y2="160.02" width="0.1524" layer="91"/>
<wire x1="91.44" y1="160.02" x2="91.44" y2="175.26" width="0.1524" layer="91"/>
<label x="91.44" y="175.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="204.47" y1="160.02" x2="204.47" y2="175.26" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="S1"/>
<wire x1="200.66" y1="160.02" x2="204.47" y2="160.02" width="0.1524" layer="91"/>
<label x="204.47" y="175.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ISO_DACOUT" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="PA6"/>
<wire x1="20.32" y1="210.82" x2="20.32" y2="215.9" width="0.1524" layer="91"/>
<wire x1="20.32" y1="215.9" x2="-2.54" y2="215.9" width="0.1524" layer="91"/>
<label x="-2.54" y="215.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="189.23" y1="59.69" x2="190.5" y2="59.69" width="0.1524" layer="91"/>
<label x="190.5" y="59.69" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="BOOT"/>
<pinref part="C15" gate="C" pin="2"/>
<wire x1="143.51" y1="53.34" x2="146.05" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="C15" gate="C" pin="1"/>
<pinref part="IC4" gate="G$1" pin="SW"/>
<wire x1="146.05" y1="45.72" x2="143.51" y2="45.72" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="146.05" y1="45.72" x2="151.13" y2="45.72" width="0.1524" layer="91"/>
<junction x="146.05" y="45.72"/>
</segment>
</net>
<net name="BUCK_FB" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="FB"/>
<wire x1="143.51" y1="40.64" x2="144.78" y2="40.64" width="0.1524" layer="91"/>
<label x="144.78" y="40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="179.07" y1="59.69" x2="179.07" y2="77.47" width="0.1524" layer="91"/>
<wire x1="179.07" y1="77.47" x2="168.91" y2="77.47" width="0.1524" layer="91"/>
<junction x="179.07" y="77.47"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="168.91" y1="77.47" x2="166.37" y2="77.47" width="0.1524" layer="91"/>
<wire x1="168.91" y1="74.93" x2="168.91" y2="77.47" width="0.1524" layer="91"/>
<junction x="168.91" y="77.47"/>
<label x="166.37" y="77.47" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="C23" gate="C" pin="1"/>
<pinref part="IC4" gate="G$1" pin="COMP"/>
<wire x1="156.21" y1="33.02" x2="149.86" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C24" gate="C" pin="1"/>
<wire x1="149.86" y1="33.02" x2="143.51" y2="33.02" width="0.1524" layer="91"/>
<wire x1="149.86" y1="27.94" x2="149.86" y2="33.02" width="0.1524" layer="91"/>
<junction x="149.86" y="33.02"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="C23" gate="C" pin="2"/>
<wire x1="156.21" y1="21.59" x2="156.21" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="102,1,6.35,33.02,BOOT_R,BOOT_R2,,,,"/>
<approved hash="102,1,6.35,38.1,BOOT,BOOT2,,,,"/>
<approved hash="102,1,140.97,71.12,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,166.37,140.97,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,127,140.97,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,114.3,128.27,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,180.34,128.27,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,196.85,309.88,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,76.2,257.81,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,-1.27,181.61,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,38.1,217.17,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,17.78,152.4,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,121.92,261.62,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,203.2,261.62,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,104.14,30.48,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,176.53,29.21,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,156.21,8.89,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,168.91,63.5,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,90.17,135.89,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,204.47,135.89,GND-ISO,ISO_GND,,,,"/>
<approved hash="201,1,6.35,38.1,BOOT,BOOT2\, BOOT,,,,"/>
<approved hash="201,1,6.35,33.02,BOOT_R,BOOT_R2\, BOOT_R,,,,"/>
<approved hash="103,1,36.83,43.18,U3,N/A,N$31,,,"/>
<approved hash="104,1,36.83,38.1,U3,VDD,5VDC,,,"/>
<approved hash="104,1,36.83,33.02,U3,PGND,GND,,,"/>
<approved hash="103,1,6.35,43.18,U3,N/A2,N$30,,,"/>
<approved hash="103,1,36.83,110.49,U2,N/A,N$26,,,"/>
<approved hash="104,1,36.83,105.41,U2,VDD,5VDC,,,"/>
<approved hash="104,1,36.83,100.33,U2,PGND,GND,,,"/>
<approved hash="103,1,6.35,110.49,U2,N/A2,N$27,,,"/>
<approved hash="104,1,29.21,306.07,U1,VDD,5VDC_PSU,,,"/>
<approved hash="104,1,0,306.07,J2,VDD,5VDC_PSU,,,"/>
<approved hash="104,1,41.91,267.97,IC1,VDD1,ISO_5V,,,"/>
<approved hash="104,1,41.91,260.35,IC1,GND1,ISO_GND,,,"/>
<approved hash="104,1,13.97,267.97,IC1,VDD2,5VDC,,,"/>
<approved hash="104,1,13.97,260.35,IC1,GND2,GND,,,"/>
<approved hash="104,1,154.94,314.96,PS1,-VIN,GND,,,"/>
<approved hash="104,1,154.94,317.5,PS1,+VIN,5VDC,,,"/>
<approved hash="104,1,185.42,314.96,PS1,-VOUT,ISO_GND,,,"/>
<approved hash="104,1,185.42,317.5,PS1,+VOUT,ISO_5V,,,"/>
<approved hash="106,1,36.83,110.49,N$26,,,,,"/>
<approved hash="106,1,6.35,110.49,N$27,,,,,"/>
<approved hash="106,1,6.35,43.18,N$30,,,,,"/>
<approved hash="106,1,36.83,43.18,N$31,,,,,"/>
<approved hash="113,1,91.971,158.011,FRAME1,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
