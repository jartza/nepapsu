NepaPSU
=======

Work-in-progress PSU for Commodore 64.

Latest board can be seen here: https://gitlab.com/jartza/nepapsu/blob/master/nepapsu_v4_top.png

Latest schematics: https://gitlab.com/jartza/nepapsu/blob/master/nepapsu_v4.pdf

Firmware for MCU: https://gitlab.com/jartza/nepapsu-firmware/

## Version 4 is in the making which will be more efficient and component count has decreased. ##