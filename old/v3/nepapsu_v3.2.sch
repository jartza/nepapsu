<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="13" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="13" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="rakettitiede">
<packages>
<package name="10-VSON-HOMETCH">
<smd name="11" x="0" y="0" dx="2.6" dy="1.7" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="6_3" x="0.2" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="7" x="-0.2" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="6_2" x="0.6" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="8" x="-0.6" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="6_1" x="1" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="9" x="-1" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="10" x="-1.4" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="6" x="1.4" y="1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="1" x="-1.4" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="2" x="-1" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="3" x="-0.6" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="4" x="-0.2" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="5" x="0.2" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="5_1" x="0.6" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="5_2" x="1" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="5_3" x="1.4" y="-1.8" dx="0.2" dy="1.3" layer="1" rot="R180" stop="no" thermals="no" cream="no"/>
<wire x1="-1.65" y1="1.65" x2="1.65" y2="1.65" width="0.127" layer="51"/>
<wire x1="1.65" y1="1.65" x2="1.65" y2="-1.65" width="0.127" layer="51"/>
<wire x1="1.65" y1="-1.65" x2="-1.65" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-1.65" y1="-1.65" x2="-1.65" y2="1.65" width="0.127" layer="51"/>
<wire x1="-1.7" y1="1.65" x2="-1.7" y2="-1.65" width="0.25" layer="21"/>
<wire x1="1.7" y1="-1.65" x2="1.7" y2="1.65" width="0.25" layer="21"/>
<text x="-2" y="-1.3" size="0.6096" layer="21" rot="R90">&gt;NAME</text>
<polygon width="0.02" layer="31">
<vertex x="-1.15" y="-0.7"/>
<vertex x="-0.2" y="-0.7"/>
<vertex x="-0.2" y="-0.65"/>
<vertex x="-0.2" y="-0.1"/>
<vertex x="-1.15" y="-0.1"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-0.2" y="0.1"/>
<vertex x="-1.15" y="0.1"/>
<vertex x="-1.15" y="0.7"/>
<vertex x="-0.2" y="0.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="0.2" y="0.1"/>
<vertex x="1.15" y="0.1"/>
<vertex x="1.15" y="0.7"/>
<vertex x="0.2" y="0.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="0.2" y="-0.1"/>
<vertex x="1.15" y="-0.1"/>
<vertex x="1.15" y="-0.7"/>
<vertex x="0.2" y="-0.7"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-1.31" y="0.86"/>
<vertex x="1.31" y="0.86"/>
<vertex x="1.31" y="-0.86"/>
<vertex x="-1.31" y="-0.86"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-1.49" y="1.3"/>
<vertex x="-1.31" y="1.3"/>
<vertex x="-1.31" y="1.7"/>
<vertex x="-1.49" y="1.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-1.09" y="1.3"/>
<vertex x="-0.91" y="1.3"/>
<vertex x="-0.91" y="1.7"/>
<vertex x="-1.09" y="1.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-0.69" y="1.3"/>
<vertex x="-0.51" y="1.3"/>
<vertex x="-0.51" y="1.7"/>
<vertex x="-0.69" y="1.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-0.29" y="1.3"/>
<vertex x="-0.11" y="1.3"/>
<vertex x="-0.11" y="1.7"/>
<vertex x="-0.29" y="1.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="0.11" y="1.3"/>
<vertex x="1.49" y="1.3"/>
<vertex x="1.49" y="1.7"/>
<vertex x="1.31" y="1.7"/>
<vertex x="1.31" y="1.59"/>
<vertex x="1.09" y="1.59"/>
<vertex x="1.09" y="1.7"/>
<vertex x="0.91" y="1.7"/>
<vertex x="0.91" y="1.59"/>
<vertex x="0.69" y="1.59"/>
<vertex x="0.68" y="1.59"/>
<vertex x="0.68" y="1.7"/>
<vertex x="0.51" y="1.7"/>
<vertex x="0.51" y="1.59"/>
<vertex x="0.29" y="1.59"/>
<vertex x="0.29" y="1.7"/>
<vertex x="0.11" y="1.7"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-1.51" y="2.46"/>
<vertex x="-1.29" y="2.46"/>
<vertex x="-1.29" y="1.14"/>
<vertex x="-1.51" y="1.14"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-1.11" y="1.14"/>
<vertex x="-0.89" y="1.14"/>
<vertex x="-0.89" y="2.46"/>
<vertex x="-1.11" y="2.46"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-0.71" y="1.14"/>
<vertex x="-0.49" y="1.14"/>
<vertex x="-0.49" y="2.46"/>
<vertex x="-0.71" y="2.46"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-0.31" y="2.46"/>
<vertex x="-0.09" y="2.46"/>
<vertex x="-0.09" y="1.14"/>
<vertex x="-0.31" y="1.14"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-1.49" y="-1.3"/>
<vertex x="-1.31" y="-1.3"/>
<vertex x="-1.31" y="-1.7"/>
<vertex x="-1.49" y="-1.7"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-1.08" y="-1.3"/>
<vertex x="-0.91" y="-1.3"/>
<vertex x="-0.91" y="-1.7"/>
<vertex x="-1.09" y="-1.7"/>
<vertex x="-1.09" y="-1.3"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-0.69" y="-1.7"/>
<vertex x="-0.51" y="-1.7"/>
<vertex x="-0.51" y="-1.3"/>
<vertex x="-0.69" y="-1.3"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="-0.29" y="-1.7"/>
<vertex x="-0.11" y="-1.7"/>
<vertex x="-0.11" y="-1.3"/>
<vertex x="-0.29" y="-1.3"/>
</polygon>
<polygon width="0.02" layer="31">
<vertex x="0.11" y="-1.7"/>
<vertex x="0.29" y="-1.7"/>
<vertex x="0.29" y="-1.59"/>
<vertex x="0.51" y="-1.59"/>
<vertex x="0.51" y="-1.7"/>
<vertex x="0.69" y="-1.7"/>
<vertex x="0.69" y="-1.59"/>
<vertex x="0.91" y="-1.59"/>
<vertex x="0.91" y="-1.7"/>
<vertex x="1.09" y="-1.7"/>
<vertex x="1.09" y="-1.59"/>
<vertex x="1.31" y="-1.59"/>
<vertex x="1.31" y="-1.7"/>
<vertex x="1.49" y="-1.7"/>
<vertex x="1.49" y="-1.3"/>
<vertex x="0.11" y="-1.3"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-1.51" y="-1.14"/>
<vertex x="-1.29" y="-1.14"/>
<vertex x="-1.29" y="-2.46"/>
<vertex x="-1.51" y="-2.46"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-1.11" y="-1.14"/>
<vertex x="-0.89" y="-1.14"/>
<vertex x="-0.89" y="-2.46"/>
<vertex x="-1.11" y="-2.46"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-0.71" y="-1.14"/>
<vertex x="-0.49" y="-1.14"/>
<vertex x="-0.49" y="-2.46"/>
<vertex x="-0.71" y="-2.46"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="-0.31" y="-1.14"/>
<vertex x="-0.09" y="-1.14"/>
<vertex x="-0.09" y="-2.46"/>
<vertex x="-0.31" y="-2.46"/>
</polygon>
<smd name="6_4" x="0.4" y="1.8" dx="0.2" dy="1.3" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="6_5" x="0.8" y="1.8" dx="0.2" dy="1.3" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="6_6" x="1.2" y="1.8" dx="0.2" dy="1.3" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="P$4" x="0.4" y="-1.8" dx="0.2" dy="1.3" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="P$5" x="0.8" y="-1.8" dx="0.2" dy="1.3" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="P$6" x="1.2" y="-1.8" dx="0.2" dy="1.3" layer="1" stop="no" thermals="no" cream="no"/>
<polygon width="0.02" layer="29">
<vertex x="0.1" y="1.15"/>
<vertex x="1.5" y="1.15"/>
<vertex x="1.5" y="2.45"/>
<vertex x="0.1" y="2.45"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="0.1" y="-1.15"/>
<vertex x="0.1" y="-2.45"/>
<vertex x="1.5" y="-2.45"/>
<vertex x="1.5" y="-1.15"/>
</polygon>
</package>
<package name="SO-8">
<smd name="5" x="1.905" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="0.635" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="7" x="-0.635" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="8" x="-1.905" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="1" x="-1.905" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-0.635" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="0.635" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="1.905" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<wire x1="2.45" y1="1.95" x2="-2.45" y2="1.95" width="0.127" layer="51"/>
<wire x1="-2.45" y1="1.95" x2="-2.45" y2="-1.95" width="0.127" layer="51"/>
<wire x1="-2.45" y1="-1.95" x2="2.45" y2="-1.95" width="0.127" layer="51"/>
<wire x1="2.45" y1="-1.95" x2="2.45" y2="1.95" width="0.127" layer="51"/>
<text x="-2.85" y="-1.75" size="0.8128" layer="21" rot="R90">&gt;NAME</text>
<wire x1="-2.35" y1="1.95" x2="-2.45" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.95" x2="-2.45" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.95" x2="-2.4" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="2.35" y1="1.95" x2="2.45" y2="1.95" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.95" x2="2.45" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.95" x2="2.35" y2="-1.95" width="0.2032" layer="21"/>
<circle x="-1.9" y="-1.3" radius="0.2" width="0.127" layer="21"/>
</package>
<package name="POWER_JACK_SMD">
<wire x1="-5" y1="4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.5" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="9.8" y1="4.5" x2="9.8" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="4.5" x2="-3.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-2.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="0" y2="2.5" width="0.2032" layer="21"/>
<wire x1="0" y1="2.5" x2="2.286" y2="0" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.286" y1="0" x2="0" y2="-2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="0" y1="-2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="4.5" x2="-2" y2="4.5" width="0.2032" layer="21"/>
<wire x1="2" y1="4.5" x2="4.1" y2="4.5" width="0.2032" layer="21"/>
<wire x1="8.2" y1="4.5" x2="9.8" y2="4.5" width="0.2032" layer="21"/>
<wire x1="9.8" y1="-4.5" x2="8.1" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-4.5" x2="2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="-4.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<smd name="VIN0" x="0" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="GND" x="0" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="VIN1" x="6.1" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="P$4" x="6.1" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<hole x="0" y="0" drill="2.032"/>
<hole x="4.572" y="0" drill="2.032"/>
</package>
<package name="POWER_JACK_SLOT">
<wire x1="9.33" y1="13.7" x2="4.83" y2="13.7" width="0.2032" layer="21"/>
<wire x1="4.83" y1="13.7" x2="0.83" y2="13.7" width="0.2032" layer="51"/>
<wire x1="0.83" y1="13.7" x2="0.83" y2="12.7" width="0.2032" layer="51"/>
<wire x1="0.83" y1="12.7" x2="0.83" y2="8.4" width="0.2032" layer="21"/>
<wire x1="0.83" y1="8.4" x2="0.83" y2="6.55" width="0.2032" layer="51"/>
<wire x1="0.98" y1="13.8" x2="4.18" y2="13.8" width="0" layer="46"/>
<wire x1="0.98" y1="13.8" x2="1.48" y2="14.3" width="0" layer="46" curve="-90"/>
<wire x1="1.48" y1="14.3" x2="3.68" y2="14.3" width="0" layer="46"/>
<wire x1="3.68" y1="14.3" x2="4.18" y2="13.8" width="0" layer="46" curve="-90"/>
<wire x1="4.18" y1="13.8" x2="3.68" y2="13.3" width="0" layer="46" curve="-90"/>
<wire x1="3.68" y1="13.3" x2="1.48" y2="13.3" width="0" layer="46"/>
<wire x1="1.48" y1="13.3" x2="0.98" y2="13.8" width="0" layer="46" curve="-90"/>
<wire x1="0.98" y1="7.5" x2="1.48" y2="8" width="0" layer="46" curve="-90"/>
<wire x1="1.48" y1="8" x2="3.68" y2="8" width="0" layer="46"/>
<wire x1="3.68" y1="8" x2="4.18" y2="7.5" width="0" layer="46" curve="-90"/>
<wire x1="4.18" y1="7.5" x2="3.68" y2="7" width="0" layer="46" curve="-90"/>
<wire x1="3.68" y1="7" x2="1.48" y2="7" width="0" layer="46"/>
<wire x1="1.48" y1="7" x2="0.98" y2="7.5" width="0" layer="46" curve="-90"/>
<wire x1="7.58" y1="12.4" x2="8.08" y2="11.9" width="0" layer="46" curve="-90"/>
<wire x1="8.08" y1="11.9" x2="8.08" y2="9.7" width="0" layer="46"/>
<wire x1="8.08" y1="9.7" x2="7.58" y2="9.2" width="0" layer="46" curve="-90"/>
<wire x1="7.58" y1="9.2" x2="7.08" y2="9.7" width="0" layer="46" curve="-90"/>
<wire x1="7.08" y1="9.7" x2="7.08" y2="11.9" width="0" layer="46"/>
<wire x1="7.08" y1="11.9" x2="7.58" y2="12.4" width="0" layer="46" curve="-90"/>
<wire x1="0.83" y1="3.1" x2="0.83" y2="0.1" width="0.2032" layer="51"/>
<wire x1="9.33" y1="0.1" x2="9.33" y2="3.1" width="0.2032" layer="51"/>
<wire x1="9.33" y1="0.1" x2="0.83" y2="0.1" width="0.2032" layer="51"/>
<wire x1="0.83" y1="6.51" x2="0.83" y2="3.1" width="0.2032" layer="21"/>
<wire x1="9.33" y1="3.1" x2="9.33" y2="13.7" width="0.2032" layer="21"/>
<wire x1="0.83" y1="3.1" x2="9.33" y2="3.1" width="0.2032" layer="21"/>
<wire x1="9.33" y1="3.1" x2="9.28" y2="3.1" width="0.2032" layer="21"/>
<pad name="PWR" x="2.58" y="13.8" drill="1" diameter="2" shape="long"/>
<pad name="GND@1" x="2.58" y="7.5" drill="1" diameter="2" shape="long"/>
<pad name="GND@2" x="7.58" y="10.8" drill="1" diameter="2" shape="long" rot="R90"/>
<text x="0" y="0" size="1.778" layer="25" rot="R90">&gt;NAME</text>
<text x="11.43" y="0" size="1.778" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="POWER_JACK_PTH">
<wire x1="4.5" y1="13.7" x2="2.4" y2="13.7" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3" x2="-4.5" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="0.1" x2="4.5" y2="3" width="0.2032" layer="51"/>
<wire x1="4.5" y1="0.1" x2="-4.5" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="3" x2="4.5" y2="8.3" width="0.2032" layer="21"/>
<wire x1="4.5" y1="13.7" x2="4.5" y2="13" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3" x2="-4.5" y2="13.7" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="13.7" x2="-2.4" y2="13.7" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3" x2="4.5" y2="3" width="0.2032" layer="21"/>
<pad name="PWR" x="0" y="13.7" drill="2.9972" diameter="4.318"/>
<pad name="GND" x="0" y="7.7" drill="2.9972" diameter="4.318"/>
<pad name="GNDBREAK" x="4.7" y="10.7" drill="2.9972" diameter="4.318" rot="R90"/>
<text x="-3.81" y="5.08" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="3.81" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="POWER_JACK_COMBO">
<wire x1="-5" y1="4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.5" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="9.8" y1="4.5" x2="9.8" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="4.5" x2="-3.5" y2="2.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-2.5" x2="-3.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="2.5" x2="0" y2="2.5" width="0.2032" layer="51"/>
<wire x1="0" y1="2.5" x2="2.286" y2="0" width="0.2032" layer="51" curve="-90"/>
<wire x1="2.286" y1="0" x2="0" y2="-2.5" width="0.2032" layer="51" curve="-90"/>
<wire x1="0" y1="-2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="4.5" x2="-2" y2="4.5" width="0.2032" layer="51"/>
<wire x1="2" y1="4.5" x2="4.1" y2="4.5" width="0.2032" layer="51"/>
<wire x1="8.2" y1="4.5" x2="9.8" y2="4.5" width="0.2032" layer="51"/>
<wire x1="9.8" y1="-4.5" x2="8.1" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="4.1" y1="-4.5" x2="2" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-2" y1="-4.5" x2="-3.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="8.62" y1="-1.96" x2="8.62" y2="0.14" width="0.2032" layer="51"/>
<wire x1="-2.08" y1="7.04" x2="-4.98" y2="7.04" width="0.2032" layer="51"/>
<wire x1="-4.98" y1="-1.96" x2="-2.08" y2="-1.96" width="0.2032" layer="51"/>
<wire x1="-4.98" y1="-1.96" x2="-4.98" y2="7.04" width="0.2032" layer="51"/>
<wire x1="-2.08" y1="-1.96" x2="3.22" y2="-1.96" width="0.2032" layer="51"/>
<wire x1="8.62" y1="-1.96" x2="7.92" y2="-1.96" width="0.2032" layer="51"/>
<wire x1="-2.08" y1="7.04" x2="8.62" y2="7.04" width="0.2032" layer="51"/>
<wire x1="8.62" y1="7.04" x2="8.62" y2="4.94" width="0.2032" layer="51"/>
<wire x1="-2.08" y1="7.04" x2="-2.08" y2="-1.96" width="0.2032" layer="51"/>
<wire x1="0" y1="5.715" x2="6.35" y2="5.715" width="1.27" layer="1"/>
<wire x1="6.35" y1="5.715" x2="6.35" y2="5.08" width="1.27" layer="1"/>
<wire x1="6.35" y1="5.08" x2="8.89" y2="2.54" width="1.27" layer="1"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-1.905" width="1.27" layer="1"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-5.715" width="1.27" layer="1"/>
<wire x1="2.54" y1="-5.715" x2="0" y2="-5.715" width="1.27" layer="1"/>
<wire x1="6.35" y1="-5.715" x2="2.54" y2="-5.715" width="1.27" layer="1"/>
<wire x1="5.715" y1="-1.905" x2="2.54" y2="-1.905" width="1.27" layer="1"/>
<pad name="POWER" x="8.62" y="2.54" drill="2.9972" rot="R270"/>
<pad name="GND@1" x="2.62" y="2.54" drill="2.9972" rot="R270"/>
<pad name="GND" x="5.62" y="-2.16" drill="2.9972"/>
<smd name="POWER@1" x="0" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="GND@3" x="0" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="POWER@2" x="6.1" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="GND@4" x="6.1" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<text x="-5.08" y="7.62" size="1.778" layer="25">&gt;NAME</text>
<hole x="0" y="0" drill="1.6"/>
<hole x="5.08" y="0" drill="1.6"/>
</package>
<package name="POWER_JACK_PTH_LOCK">
<wire x1="4.3476" y1="14.2588" x2="2.4" y2="14.2588" width="0.2032" layer="21"/>
<wire x1="-4.3476" y1="3.2794" x2="-4.3476" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.3476" y1="0.1" x2="4.3476" y2="3.2794" width="0.2032" layer="51"/>
<wire x1="4.3476" y1="0.1" x2="-4.3476" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.3476" y1="3.254" x2="4.3476" y2="8.3" width="0.2032" layer="21"/>
<wire x1="4.3476" y1="14.2588" x2="4.3476" y2="13" width="0.2032" layer="21"/>
<wire x1="-4.3476" y1="3.254" x2="-4.3476" y2="14.2588" width="0.2032" layer="21"/>
<wire x1="-4.3476" y1="14.2588" x2="-2.4" y2="14.2588" width="0.2032" layer="21"/>
<wire x1="-4.3476" y1="3.254" x2="4.3476" y2="3.254" width="0.2032" layer="21"/>
<pad name="PWR" x="0" y="13.8778" drill="3.2" diameter="4.1148"/>
<pad name="GND" x="0.0254" y="6.557" drill="2.9972" diameter="4.1148"/>
<pad name="GNDBREAK" x="3.7616" y="10.7" drill="2.9972" diameter="4.1148" rot="R90"/>
<text x="-3.81" y="5.08" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="3.81" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2159" y1="12.1793" x2="0.2413" y2="15.1003" layer="51" rot="R90"/>
<rectangle x1="-0.1397" y1="6.3119" x2="0.1397" y2="8.7503" layer="51" rot="R90"/>
<rectangle x1="4.4704" y1="9.4742" x2="4.7498" y2="11.9126" layer="51" rot="R180"/>
</package>
<package name="POWER_JACK_SMD_OVERPASTE_REDBOARD_0603">
<rectangle x1="5.7531" y1="-6.6675" x2="6.8707" y2="-6.6421" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-6.6421" x2="6.9215" y2="-6.6167" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="-6.6167" x2="6.9723" y2="-6.5913" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="-6.5913" x2="6.9723" y2="-6.5659" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="-6.5659" x2="6.9977" y2="-6.5405" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-6.5405" x2="7.0231" y2="-6.5151" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-6.5151" x2="7.0231" y2="-6.4897" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4897" x2="7.0231" y2="-6.4643" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4643" x2="7.0231" y2="-6.4389" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="-6.4643" x2="0.4445" y2="-6.4389" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4389" x2="7.0231" y2="-6.4135" layer="200" rot="R180"/>
<rectangle x1="-0.8001" y1="-6.4389" x2="0.5207" y2="-6.4135" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.4135" x2="7.0231" y2="-6.3881" layer="200" rot="R180"/>
<rectangle x1="-0.8255" y1="-6.4135" x2="0.5461" y2="-6.3881" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3881" x2="7.0231" y2="-6.3627" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-6.3881" x2="0.5715" y2="-6.3627" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3627" x2="7.0231" y2="-6.3373" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-6.3627" x2="0.5969" y2="-6.3373" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3373" x2="7.0231" y2="-6.3119" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.3373" x2="0.6223" y2="-6.3119" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3119" x2="7.0231" y2="-6.2865" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.3119" x2="0.6223" y2="-6.2865" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2865" x2="7.0231" y2="-6.2611" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.2865" x2="0.6223" y2="-6.2611" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2611" x2="7.0231" y2="-6.2357" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2611" x2="0.6477" y2="-6.2357" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2357" x2="7.0231" y2="-6.2103" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2357" x2="0.6477" y2="-6.2103" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2103" x2="7.0231" y2="-6.1849" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2103" x2="0.6477" y2="-6.1849" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1849" x2="7.0231" y2="-6.1595" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1849" x2="0.6477" y2="-6.1595" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1595" x2="7.0231" y2="-6.1341" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1595" x2="0.6477" y2="-6.1341" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1341" x2="7.0231" y2="-6.1087" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1341" x2="0.6477" y2="-6.1087" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1087" x2="7.0231" y2="-6.0833" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1087" x2="0.6477" y2="-6.0833" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0833" x2="7.0231" y2="-6.0579" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0833" x2="0.6477" y2="-6.0579" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0579" x2="7.0231" y2="-6.0325" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0579" x2="0.6477" y2="-6.0325" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0325" x2="7.0231" y2="-6.0071" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0325" x2="0.6477" y2="-6.0071" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0071" x2="7.0231" y2="-5.9817" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0071" x2="0.6477" y2="-5.9817" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9817" x2="7.0231" y2="-5.9563" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9817" x2="0.6731" y2="-5.9563" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9563" x2="7.0231" y2="-5.9309" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9563" x2="0.6731" y2="-5.9309" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9309" x2="7.0231" y2="-5.9055" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9309" x2="0.6731" y2="-5.9055" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9055" x2="7.0231" y2="-5.8801" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9055" x2="0.6731" y2="-5.8801" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.8801" x2="7.0231" y2="-5.8547" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8801" x2="0.6731" y2="-5.8547" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8547" x2="7.0231" y2="-5.8293" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8547" x2="0.6731" y2="-5.8293" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8293" x2="7.0231" y2="-5.8039" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8293" x2="0.6731" y2="-5.8039" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8039" x2="7.0231" y2="-5.7785" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8039" x2="0.6731" y2="-5.7785" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7785" x2="7.0231" y2="-5.7531" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.7785" x2="0.6731" y2="-5.7531" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7531" x2="7.0231" y2="-5.7277" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7531" x2="0.6731" y2="-5.7277" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7277" x2="7.0231" y2="-5.7023" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7277" x2="0.6731" y2="-5.7023" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7023" x2="7.0231" y2="-5.6769" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7023" x2="0.6731" y2="-5.6769" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6769" x2="7.0231" y2="-5.6515" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6769" x2="0.6731" y2="-5.6515" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6515" x2="7.0231" y2="-5.6261" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6515" x2="0.6731" y2="-5.6261" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6261" x2="7.0231" y2="-5.6007" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6261" x2="0.6731" y2="-5.6007" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6007" x2="7.0231" y2="-5.5753" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6007" x2="0.6731" y2="-5.5753" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5753" x2="7.0231" y2="-5.5499" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5753" x2="0.6731" y2="-5.5499" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5499" x2="7.0231" y2="-5.5245" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5499" x2="0.6731" y2="-5.5245" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5245" x2="7.0231" y2="-5.4991" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5245" x2="0.6731" y2="-5.4991" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4991" x2="7.0231" y2="-5.4737" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4991" x2="0.6731" y2="-5.4737" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4737" x2="7.0231" y2="-5.4483" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4737" x2="0.6731" y2="-5.4483" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4483" x2="7.0231" y2="-5.4229" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4483" x2="0.6731" y2="-5.4229" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4229" x2="7.0231" y2="-5.3975" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4229" x2="0.6731" y2="-5.3975" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3975" x2="7.0231" y2="-5.3721" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3975" x2="0.6731" y2="-5.3721" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3721" x2="7.0231" y2="-5.3467" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3721" x2="0.6731" y2="-5.3467" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3467" x2="7.0231" y2="-5.3213" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3467" x2="0.6731" y2="-5.3213" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3213" x2="7.0231" y2="-5.2959" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3213" x2="0.6731" y2="-5.2959" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2959" x2="7.0231" y2="-5.2705" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2959" x2="0.6731" y2="-5.2705" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2705" x2="7.0231" y2="-5.2451" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2705" x2="0.6731" y2="-5.2451" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2451" x2="7.0231" y2="-5.2197" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2451" x2="0.6731" y2="-5.2197" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2197" x2="7.0231" y2="-5.1943" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.2197" x2="0.6731" y2="-5.1943" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1943" x2="7.0231" y2="-5.1689" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1943" x2="0.6731" y2="-5.1689" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1689" x2="7.0231" y2="-5.1435" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1689" x2="0.6731" y2="-5.1435" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1435" x2="7.0231" y2="-5.1181" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1435" x2="0.6731" y2="-5.1181" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1181" x2="7.0231" y2="-5.0927" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1181" x2="0.6731" y2="-5.0927" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0927" x2="7.0231" y2="-5.0673" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0927" x2="0.6731" y2="-5.0673" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0673" x2="7.0231" y2="-5.0419" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0673" x2="0.6731" y2="-5.0419" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0419" x2="7.0231" y2="-5.0165" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0419" x2="0.6731" y2="-5.0165" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0165" x2="6.9977" y2="-4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0165" x2="0.6731" y2="-4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-4.9911" x2="0.6731" y2="-4.9657" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-4.9657" x2="0.5207" y2="-4.9403" layer="200" rot="R180"/>
<rectangle x1="4.3815" y1="-4.7879" x2="7.0739" y2="-4.7625" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-4.7879" x2="1.3335" y2="-4.7625" layer="200" rot="R180"/>
<rectangle x1="-4.4069" y1="-4.7625" x2="7.0993" y2="-4.7371" layer="200" rot="R180"/>
<rectangle x1="-4.4577" y1="-4.7371" x2="7.0993" y2="-4.7117" layer="200" rot="R180"/>
<rectangle x1="-4.5085" y1="-4.7117" x2="7.0993" y2="-4.6863" layer="200" rot="R180"/>
<rectangle x1="-4.5593" y1="-4.6863" x2="7.0993" y2="-4.6609" layer="200" rot="R180"/>
<rectangle x1="-4.5847" y1="-4.6609" x2="7.0993" y2="-4.6355" layer="200" rot="R180"/>
<rectangle x1="-4.6355" y1="-4.6355" x2="7.0993" y2="-4.6101" layer="200" rot="R180"/>
<rectangle x1="-4.6609" y1="-4.6101" x2="7.0993" y2="-4.5847" layer="200" rot="R180"/>
<rectangle x1="-4.6863" y1="-4.5847" x2="7.0993" y2="-4.5593" layer="200" rot="R180"/>
<rectangle x1="-4.7117" y1="-4.5593" x2="7.0739" y2="-4.5339" layer="200" rot="R180"/>
<rectangle x1="-4.7371" y1="-4.5339" x2="7.0739" y2="-4.5085" layer="200" rot="R180"/>
<rectangle x1="-4.7625" y1="-4.5085" x2="7.0739" y2="-4.4831" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="-4.4831" x2="7.0739" y2="-4.4577" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="-4.4577" x2="7.0739" y2="-4.4323" layer="200" rot="R180"/>
<rectangle x1="-4.8133" y1="-4.4323" x2="7.5057" y2="-4.4069" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="-4.4069" x2="9.7155" y2="-4.3815" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="-4.3815" x2="9.7663" y2="-4.3561" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="-4.3561" x2="9.7917" y2="-4.3307" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="-4.3307" x2="9.8171" y2="-4.3053" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="-4.3053" x2="9.8425" y2="-4.2799" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="-4.2799" x2="9.8679" y2="-4.2545" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2545" x2="9.8933" y2="-4.2291" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2291" x2="9.8933" y2="-4.2037" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2037" x2="9.8933" y2="-4.1783" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1783" x2="9.8933" y2="-4.1529" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1529" x2="9.8933" y2="-4.1275" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1275" x2="9.8933" y2="-4.1021" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.1021" x2="9.8933" y2="-4.0767" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0767" x2="9.8933" y2="-4.0513" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0513" x2="9.8933" y2="-4.0259" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0259" x2="9.8933" y2="-4.0005" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0005" x2="9.8933" y2="-3.9751" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9751" x2="9.8933" y2="-3.9497" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9497" x2="9.8933" y2="-3.9243" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9243" x2="9.8933" y2="-3.8989" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.8989" x2="9.8933" y2="-3.8735" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.8735" x2="9.8933" y2="-3.8481" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.8481" x2="9.8933" y2="-3.8227" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.8227" x2="9.8933" y2="-3.7973" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7973" x2="9.8933" y2="-3.7719" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7719" x2="9.8933" y2="-3.7465" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7465" x2="9.8933" y2="-3.7211" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7211" x2="9.8933" y2="-3.6957" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6957" x2="9.8933" y2="-3.6703" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6703" x2="9.8933" y2="-3.6449" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6449" x2="9.8933" y2="-3.6195" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6195" x2="9.8933" y2="-3.5941" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5941" x2="9.8933" y2="-3.5687" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5687" x2="9.8933" y2="-3.5433" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5433" x2="9.8933" y2="-3.5179" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5179" x2="9.8933" y2="-3.4925" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4925" x2="9.8933" y2="-3.4671" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4671" x2="9.8933" y2="-3.4417" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4417" x2="9.8933" y2="-3.4163" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4163" x2="9.8933" y2="-3.3909" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3909" x2="9.8933" y2="-3.3655" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3655" x2="9.8933" y2="-3.3401" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3401" x2="9.8933" y2="-3.3147" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3147" x2="9.8933" y2="-3.2893" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2893" x2="9.8933" y2="-3.2639" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2639" x2="9.8933" y2="-3.2385" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2385" x2="9.8933" y2="-3.2131" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2131" x2="9.8933" y2="-3.1877" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1877" x2="9.8933" y2="-3.1623" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1623" x2="9.8933" y2="-3.1369" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1369" x2="9.8933" y2="-3.1115" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1115" x2="9.8933" y2="-3.0861" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0861" x2="9.8933" y2="-3.0607" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0607" x2="9.8933" y2="-3.0353" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0353" x2="9.8933" y2="-3.0099" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0099" x2="9.8933" y2="-2.9845" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9845" x2="9.8933" y2="-2.9591" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9591" x2="9.8933" y2="-2.9337" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9337" x2="9.8933" y2="-2.9083" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9083" x2="9.8933" y2="-2.8829" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8829" x2="9.8933" y2="-2.8575" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8575" x2="9.8933" y2="-2.8321" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8321" x2="9.8933" y2="-2.8067" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8067" x2="9.8933" y2="-2.7813" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7813" x2="9.8933" y2="-2.7559" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7559" x2="9.8933" y2="-2.7305" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7305" x2="9.8933" y2="-2.7051" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7051" x2="9.8933" y2="-2.6797" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6797" x2="9.8933" y2="-2.6543" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6543" x2="9.8933" y2="-2.6289" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6289" x2="9.8933" y2="-2.6035" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6035" x2="9.8933" y2="-2.5781" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5781" x2="9.8933" y2="-2.5527" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5527" x2="9.8933" y2="-2.5273" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5273" x2="9.8933" y2="-2.5019" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5019" x2="9.8933" y2="-2.4765" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4765" x2="9.8933" y2="-2.4511" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4511" x2="9.8933" y2="-2.4257" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4257" x2="9.8933" y2="-2.4003" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4003" x2="9.8933" y2="-2.3749" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3749" x2="9.8933" y2="-2.3495" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3495" x2="9.8933" y2="-2.3241" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3241" x2="9.8933" y2="-2.2987" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2987" x2="9.8933" y2="-2.2733" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2733" x2="9.8933" y2="-2.2479" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2479" x2="9.8933" y2="-2.2225" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2225" x2="9.8933" y2="-2.1971" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1971" x2="9.8933" y2="-2.1717" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1717" x2="9.8933" y2="-2.1463" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1463" x2="9.8933" y2="-2.1209" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1209" x2="9.8933" y2="-2.0955" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0955" x2="9.8933" y2="-2.0701" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0701" x2="9.8933" y2="-2.0447" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0447" x2="9.8933" y2="-2.0193" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0193" x2="9.8933" y2="-1.9939" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9939" x2="9.8933" y2="-1.9685" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9685" x2="9.8933" y2="-1.9431" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9431" x2="9.8933" y2="-1.9177" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9177" x2="9.8933" y2="-1.8923" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8923" x2="9.8933" y2="-1.8669" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8669" x2="9.8933" y2="-1.8415" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8415" x2="9.8933" y2="-1.8161" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8161" x2="9.8933" y2="-1.7907" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7907" x2="9.8933" y2="-1.7653" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7653" x2="9.8933" y2="-1.7399" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7399" x2="9.8933" y2="-1.7145" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7145" x2="9.8933" y2="-1.6891" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6891" x2="9.8933" y2="-1.6637" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6637" x2="9.8933" y2="-1.6383" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6383" x2="9.8679" y2="-1.6129" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6129" x2="8.8773" y2="-1.5875" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5875" x2="8.8773" y2="-1.5621" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5621" x2="8.8773" y2="-1.5367" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5367" x2="8.8773" y2="-1.5113" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5113" x2="8.8773" y2="-1.4859" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4859" x2="8.8773" y2="-1.4605" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4605" x2="8.8773" y2="-1.4351" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4351" x2="8.8773" y2="-1.4097" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4097" x2="8.8773" y2="-1.3843" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3843" x2="8.8773" y2="-1.3589" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3589" x2="8.8773" y2="-1.3335" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3335" x2="8.8773" y2="-1.3081" layer="200" rot="R180"/>
<rectangle x1="4.8133" y1="-1.3081" x2="8.8773" y2="-1.2827" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3081" x2="4.3815" y2="-1.2827" layer="200" rot="R180"/>
<rectangle x1="4.9149" y1="-1.2827" x2="8.8773" y2="-1.2573" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2827" x2="4.2799" y2="-1.2573" layer="200" rot="R180"/>
<rectangle x1="5.0165" y1="-1.2573" x2="8.8773" y2="-1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2573" x2="4.1783" y2="-1.2319" layer="200" rot="R180"/>
<rectangle x1="5.0927" y1="-1.2319" x2="8.8773" y2="-1.2065" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2319" x2="4.1275" y2="-1.2065" layer="200" rot="R180"/>
<rectangle x1="5.1435" y1="-1.2065" x2="8.8773" y2="-1.1811" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2065" x2="4.0513" y2="-1.1811" layer="200" rot="R180"/>
<rectangle x1="5.1943" y1="-1.1811" x2="8.8773" y2="-1.1557" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1811" x2="4.0005" y2="-1.1557" layer="200" rot="R180"/>
<rectangle x1="5.2197" y1="-1.1557" x2="8.8773" y2="-1.1303" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1557" x2="3.9497" y2="-1.1303" layer="200" rot="R180"/>
<rectangle x1="5.2705" y1="-1.1303" x2="8.8773" y2="-1.1049" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1303" x2="3.8989" y2="-1.1049" layer="200" rot="R180"/>
<rectangle x1="5.2959" y1="-1.1049" x2="8.8773" y2="-1.0795" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1049" x2="3.8735" y2="-1.0795" layer="200" rot="R180"/>
<rectangle x1="5.3467" y1="-1.0795" x2="8.8773" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="0.1143" y1="-1.0795" x2="3.8227" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0795" x2="-0.1143" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="5.3721" y1="-1.0541" x2="8.8773" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="0.2413" y1="-1.0541" x2="3.7973" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0541" x2="-0.2413" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="-1.0287" x2="8.8773" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="0.3429" y1="-1.0287" x2="3.7465" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0287" x2="-0.3429" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="5.4229" y1="-1.0033" x2="8.8773" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="0.4191" y1="-1.0033" x2="3.7211" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0033" x2="-0.4191" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-0.9779" x2="8.8773" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="0.4699" y1="-0.9779" x2="3.6957" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9779" x2="-0.4699" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-0.9525" x2="8.8773" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="0.5207" y1="-0.9525" x2="3.6703" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9525" x2="-0.5207" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="-0.9271" x2="8.8773" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="0.5715" y1="-0.9271" x2="3.6449" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9271" x2="-0.5715" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="-0.9017" x2="8.8773" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="0.6223" y1="-0.9017" x2="3.6195" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9017" x2="-0.6223" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="-0.8763" x2="8.8773" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="0.6477" y1="-0.8763" x2="3.5941" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8763" x2="-0.6477" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="-0.8509" x2="8.8773" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="0.6985" y1="-0.8509" x2="3.5687" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8509" x2="-0.6985" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-0.8255" x2="8.8773" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="0.7239" y1="-0.8255" x2="3.5433" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8255" x2="-0.7239" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-0.8001" x2="8.8773" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="0.7493" y1="-0.8001" x2="3.5179" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8001" x2="-0.7493" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="-0.7747" x2="8.8773" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="-0.7747" x2="3.4925" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7747" x2="-0.7747" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="-0.7493" x2="8.8773" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="0.8255" y1="-0.7493" x2="3.4925" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7493" x2="-0.8001" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="-0.7239" x2="8.8773" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="0.8509" y1="-0.7239" x2="3.4671" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7239" x2="-0.8255" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="-0.6985" x2="8.8773" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="-0.6985" x2="3.4671" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6985" x2="-0.8509" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="-0.6731" x2="8.8773" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="-0.6731" x2="3.4417" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6731" x2="-0.8763" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="-0.6477" x2="8.8773" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="-0.6477" x2="3.4417" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6477" x2="-0.9017" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="-0.6223" x2="8.8773" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="-0.6223" x2="3.4163" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6223" x2="-0.9017" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="-0.5969" x2="8.8773" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="-0.5969" x2="3.4163" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5969" x2="-0.9271" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="-0.5715" x2="8.8773" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="-0.5715" x2="3.3909" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5715" x2="-0.9525" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="-0.5461" x2="8.8773" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="-0.5461" x2="3.3655" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5461" x2="-0.9525" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="-0.5207" x2="8.8773" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="-0.5207" x2="3.3655" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5207" x2="-0.9779" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4953" x2="8.8773" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="-0.4953" x2="3.3401" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4953" x2="-0.9779" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4699" x2="8.8773" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="-0.4699" x2="3.3401" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4699" x2="-1.0033" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4445" x2="8.8773" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="-0.4445" x2="3.3401" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4445" x2="-1.0033" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.4191" x2="8.8773" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.4191" x2="3.3147" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4191" x2="-1.0033" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3937" x2="8.8773" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.3937" x2="3.3147" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3937" x2="-1.0033" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3683" x2="8.8773" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.3683" x2="3.3147" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3683" x2="-1.0287" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3429" x2="8.8773" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.3429" x2="3.3147" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3429" x2="-1.0287" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.3175" x2="8.8773" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.3175" x2="3.3147" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3175" x2="-1.0287" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2921" x2="8.8773" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2921" x2="3.2893" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2921" x2="-1.0287" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2667" x2="8.8773" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2667" x2="3.2893" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2667" x2="-1.0287" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2413" x2="8.8773" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2413" x2="3.2893" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2413" x2="-1.0287" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2159" x2="8.8773" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.2159" x2="3.2893" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2159" x2="-1.0541" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1905" x2="8.8773" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1905" x2="3.2893" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1905" x2="-1.0541" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1651" x2="8.8773" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1651" x2="3.2893" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1651" x2="-1.0541" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1397" x2="8.8773" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1397" x2="3.2893" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1397" x2="-1.0541" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1143" x2="8.8773" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1143" x2="3.2893" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1143" x2="-1.0541" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0889" x2="8.8773" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0889" x2="3.2893" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0889" x2="-1.0541" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0635" x2="8.8773" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0635" x2="3.2893" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0635" x2="-1.0541" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0381" x2="8.8773" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0381" x2="3.2893" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0381" x2="-1.0541" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0127" x2="8.8773" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0127" x2="3.2893" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0127" x2="-1.0541" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0127" x2="8.8773" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0127" x2="3.2893" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0127" x2="-1.0541" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0381" x2="8.8773" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0381" x2="3.2893" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0381" x2="-1.0541" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0635" x2="8.8773" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0635" x2="3.2893" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0635" x2="-1.0541" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0889" x2="8.8773" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0889" x2="3.2893" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0889" x2="-1.0541" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1143" x2="8.8773" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.1143" x2="3.2893" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1143" x2="-1.0541" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1397" x2="8.8773" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1397" x2="3.2893" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1397" x2="-1.0541" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1651" x2="8.8773" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1651" x2="3.3147" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1651" x2="-1.0541" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1905" x2="8.8773" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1905" x2="3.3147" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1905" x2="-1.0541" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.2159" x2="8.9027" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.2159" x2="3.3147" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2159" x2="-1.0541" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2413" x2="8.9027" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.2413" x2="3.3147" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2413" x2="-1.0541" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2667" x2="8.9027" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.2667" x2="3.3401" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2667" x2="-1.0541" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2921" x2="8.9027" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.2921" x2="3.3401" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2921" x2="-1.0287" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="0.3175" x2="8.9027" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.3175" x2="3.3401" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3175" x2="-1.0287" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="0.3429" x2="8.9027" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.3429" x2="3.3655" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3429" x2="-1.0287" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="0.3683" x2="8.9027" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.3683" x2="3.3909" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3683" x2="-1.0287" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="0.3937" x2="8.9027" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.3937" x2="3.3655" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3937" x2="-1.0287" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4191" x2="8.9027" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.4191" x2="3.3909" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4191" x2="-1.0033" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4445" x2="8.9027" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="0.4445" x2="3.4163" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4445" x2="-1.0033" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4699" x2="8.9027" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="0.4699" x2="3.4163" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4699" x2="-1.0033" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="0.4953" x2="8.9027" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="0.4953" x2="3.4163" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4953" x2="-0.9779" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="0.5207" x2="8.9027" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="0.5207" x2="3.4417" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5207" x2="-0.9779" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="0.5461" x2="8.9027" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="0.5461" x2="3.4417" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5461" x2="-0.9525" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="0.5715" x2="8.9027" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="0.5715" x2="3.4671" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5715" x2="-0.9525" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="0.5969" x2="8.9027" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="0.5969" x2="3.4671" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5969" x2="-0.9271" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="0.6223" x2="8.9027" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="0.6223" x2="3.4925" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6223" x2="-0.9271" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="0.6477" x2="8.9027" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="0.6477" x2="3.4925" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6477" x2="-0.9017" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="0.6731" x2="8.9027" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="0.6731" x2="3.5179" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6731" x2="-0.8763" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="0.6985" x2="8.9027" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="0.6985" x2="3.5433" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6985" x2="-0.8763" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7239" x2="8.9027" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="0.8509" y1="0.7239" x2="3.5433" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7239" x2="-0.8509" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7493" x2="8.9027" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="0.8255" y1="0.7493" x2="3.5687" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7493" x2="-0.8255" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7747" x2="8.9027" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="0.7747" x2="3.5941" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7747" x2="-0.8001" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="0.8001" x2="8.9027" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="0.8001" x2="3.6195" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8001" x2="-0.7747" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="0.8255" x2="8.9027" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="0.7747" y1="0.8255" x2="3.6195" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8255" x2="-0.7493" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="0.8509" x2="8.9027" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="0.7493" y1="0.8509" x2="3.6449" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8509" x2="-0.7239" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="0.8763" x2="8.9027" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="0.7239" y1="0.8763" x2="3.6703" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8763" x2="-0.6731" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="5.4229" y1="0.9017" x2="8.9027" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="0.6731" y1="0.9017" x2="3.6957" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9017" x2="-0.6477" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="0.9271" x2="8.9027" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="0.6477" y1="0.9271" x2="3.7211" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9271" x2="-0.5969" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="0.9525" x2="8.9027" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="0.5969" y1="0.9525" x2="3.7465" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9525" x2="-0.5461" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="5.3213" y1="0.9779" x2="8.9027" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="0.5715" y1="0.9779" x2="3.7973" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9779" x2="-0.4953" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="5.2959" y1="1.0033" x2="8.9027" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="0.5207" y1="1.0033" x2="3.8227" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0033" x2="-0.4191" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="5.2705" y1="1.0287" x2="8.9027" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="0.4445" y1="1.0287" x2="3.8481" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0287" x2="-0.3429" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="5.2197" y1="1.0541" x2="8.9027" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="0.3937" y1="1.0541" x2="3.8989" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0541" x2="-0.2413" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="5.1689" y1="1.0795" x2="8.9027" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="0.2921" y1="1.0795" x2="3.9497" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0795" x2="-0.0889" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="5.1181" y1="1.1049" x2="8.9027" y2="1.1303" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1049" x2="4.0005" y2="1.1303" layer="200" rot="R180"/>
<rectangle x1="5.0673" y1="1.1303" x2="8.9027" y2="1.1557" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1303" x2="4.0513" y2="1.1557" layer="200" rot="R180"/>
<rectangle x1="5.0165" y1="1.1557" x2="8.9027" y2="1.1811" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1557" x2="4.1275" y2="1.1811" layer="200" rot="R180"/>
<rectangle x1="4.9149" y1="1.1811" x2="8.9027" y2="1.2065" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1811" x2="4.2037" y2="1.2065" layer="200" rot="R180"/>
<rectangle x1="4.7625" y1="1.2065" x2="8.9027" y2="1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2065" x2="4.3307" y2="1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2319" x2="8.9027" y2="1.2573" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2573" x2="8.9027" y2="1.2827" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2827" x2="8.9027" y2="1.3081" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3081" x2="8.9027" y2="1.3335" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3335" x2="8.9027" y2="1.3589" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3589" x2="8.9027" y2="1.3843" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3843" x2="8.9027" y2="1.4097" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4097" x2="8.9027" y2="1.4351" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4351" x2="8.9027" y2="1.4605" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4605" x2="8.9027" y2="1.4859" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4859" x2="8.9027" y2="1.5113" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5113" x2="8.9027" y2="1.5367" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5367" x2="8.9027" y2="1.5621" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5621" x2="8.9027" y2="1.5875" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5875" x2="8.9027" y2="1.6129" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6129" x2="8.9027" y2="1.6383" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6383" x2="8.9027" y2="1.6637" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6637" x2="8.9027" y2="1.6891" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6891" x2="8.9027" y2="1.7145" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7145" x2="8.9027" y2="1.7399" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7399" x2="8.9027" y2="1.7653" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7653" x2="8.9027" y2="1.7907" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7907" x2="8.9027" y2="1.8161" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8161" x2="8.9027" y2="1.8415" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8415" x2="8.9027" y2="1.8669" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8669" x2="8.9027" y2="1.8923" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8923" x2="8.9027" y2="1.9177" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9177" x2="8.9027" y2="1.9431" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9431" x2="8.9027" y2="1.9685" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9685" x2="8.9027" y2="1.9939" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9939" x2="8.9027" y2="2.0193" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="2.0193" x2="8.9027" y2="2.0447" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0447" x2="8.9027" y2="2.0701" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0701" x2="8.9027" y2="2.0955" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0955" x2="8.9027" y2="2.1209" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1209" x2="8.9027" y2="2.1463" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1463" x2="8.9281" y2="2.1717" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1717" x2="8.9281" y2="2.1971" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1971" x2="8.9281" y2="2.2225" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2225" x2="8.9281" y2="2.2479" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2479" x2="8.9281" y2="2.2733" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2733" x2="8.9281" y2="2.2987" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2987" x2="8.9281" y2="2.3241" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3241" x2="8.9281" y2="2.3495" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3495" x2="8.9281" y2="2.3749" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3749" x2="8.9281" y2="2.4003" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4003" x2="8.9281" y2="2.4257" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4257" x2="8.9281" y2="2.4511" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4511" x2="8.9281" y2="2.4765" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4765" x2="8.9281" y2="2.5019" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5019" x2="8.9281" y2="2.5273" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5273" x2="8.9281" y2="2.5527" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5527" x2="8.9281" y2="2.5781" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5781" x2="8.9281" y2="2.6035" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6035" x2="8.9281" y2="2.6289" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6289" x2="8.9281" y2="2.6543" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6543" x2="8.9281" y2="2.6797" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6797" x2="8.9281" y2="2.7051" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7051" x2="8.9281" y2="2.7305" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7305" x2="8.9281" y2="2.7559" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7559" x2="8.9281" y2="2.7813" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7813" x2="8.9281" y2="2.8067" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8067" x2="8.9281" y2="2.8321" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8321" x2="8.9281" y2="2.8575" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8575" x2="8.9281" y2="2.8829" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8829" x2="8.9281" y2="2.9083" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9083" x2="8.9281" y2="2.9337" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9337" x2="8.9281" y2="2.9591" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9591" x2="8.9281" y2="2.9845" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9845" x2="8.9281" y2="3.0099" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0099" x2="8.9281" y2="3.0353" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0353" x2="8.9281" y2="3.0607" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0607" x2="8.9281" y2="3.0861" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0861" x2="8.9281" y2="3.1115" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1115" x2="8.9281" y2="3.1369" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1369" x2="8.9281" y2="3.1623" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1623" x2="8.9281" y2="3.1877" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1877" x2="8.9281" y2="3.2131" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2131" x2="8.9281" y2="3.2385" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2385" x2="8.9281" y2="3.2639" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2639" x2="8.9281" y2="3.2893" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2893" x2="8.9281" y2="3.3147" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3147" x2="8.9281" y2="3.3401" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3401" x2="8.9281" y2="3.3655" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3655" x2="8.9281" y2="3.3909" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3909" x2="8.9281" y2="3.4163" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4163" x2="8.9281" y2="3.4417" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4417" x2="8.9281" y2="3.4671" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4671" x2="8.9281" y2="3.4925" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4925" x2="8.9281" y2="3.5179" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5179" x2="8.9281" y2="3.5433" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5433" x2="8.9281" y2="3.5687" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5687" x2="8.9281" y2="3.5941" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5941" x2="8.9281" y2="3.6195" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6195" x2="8.9281" y2="3.6449" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6449" x2="8.9281" y2="3.6703" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6703" x2="8.9281" y2="3.6957" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6957" x2="8.9281" y2="3.7211" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7211" x2="8.9281" y2="3.7465" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7465" x2="8.9281" y2="3.7719" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7719" x2="8.9281" y2="3.7973" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7973" x2="8.9281" y2="3.8227" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8227" x2="8.9281" y2="3.8481" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8481" x2="8.9281" y2="3.8735" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8735" x2="8.9281" y2="3.8989" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8989" x2="8.9281" y2="3.9243" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.9243" x2="8.9281" y2="3.9497" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="3.9497" x2="8.9281" y2="3.9751" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="3.9751" x2="8.9281" y2="4.0005" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0005" x2="8.9281" y2="4.0259" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0259" x2="8.9281" y2="4.0513" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0513" x2="8.9281" y2="4.0767" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0767" x2="8.9281" y2="4.1021" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.1021" x2="8.9281" y2="4.1275" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.1275" x2="8.9281" y2="4.1529" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="4.1529" x2="8.9281" y2="4.1783" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="4.1783" x2="8.9281" y2="4.2037" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="4.2037" x2="8.9027" y2="4.2291" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="4.2291" x2="8.9027" y2="4.2545" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="4.2545" x2="8.8773" y2="4.2799" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="4.2799" x2="8.8773" y2="4.3053" layer="200" rot="R180"/>
<rectangle x1="-4.8133" y1="4.3053" x2="8.8519" y2="4.3307" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="4.3307" x2="8.8265" y2="4.3561" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="4.3561" x2="8.8011" y2="4.3815" layer="200" rot="R180"/>
<rectangle x1="-4.7625" y1="4.3815" x2="8.7757" y2="4.4069" layer="200" rot="R180"/>
<rectangle x1="-4.7371" y1="4.4069" x2="8.7249" y2="4.4323" layer="200" rot="R180"/>
<rectangle x1="-4.7117" y1="4.4323" x2="8.6741" y2="4.4577" layer="200" rot="R180"/>
<rectangle x1="-4.6863" y1="4.4577" x2="8.6233" y2="4.4831" layer="200" rot="R180"/>
<rectangle x1="-4.6609" y1="4.4831" x2="7.1247" y2="4.5085" layer="200" rot="R180"/>
<rectangle x1="-4.6101" y1="4.5085" x2="7.1247" y2="4.5339" layer="200" rot="R180"/>
<rectangle x1="-4.5847" y1="4.5339" x2="7.1247" y2="4.5593" layer="200" rot="R180"/>
<rectangle x1="-4.5339" y1="4.5593" x2="7.1247" y2="4.5847" layer="200" rot="R180"/>
<rectangle x1="-4.4577" y1="4.5847" x2="7.1247" y2="4.6101" layer="200" rot="R180"/>
<rectangle x1="-4.3815" y1="4.6101" x2="6.9215" y2="4.6355" layer="200" rot="R180"/>
<rectangle x1="-3.0607" y1="4.6355" x2="-1.6637" y2="4.6609" layer="200" rot="R180"/>
<rectangle x1="-3.8989" y1="4.6355" x2="-3.3655" y2="4.6609" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="4.8133" x2="7.0739" y2="4.8387" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8387" x2="7.0993" y2="4.8641" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8641" x2="7.0993" y2="4.8895" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8895" x2="7.0993" y2="4.9149" layer="200" rot="R180"/>
<rectangle x1="-0.1651" y1="4.8895" x2="0.7239" y2="4.9149" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9149" x2="7.0993" y2="4.9403" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9149" x2="0.7239" y2="4.9403" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9403" x2="7.0993" y2="4.9657" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9403" x2="0.7239" y2="4.9657" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9657" x2="7.0993" y2="4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9657" x2="0.7239" y2="4.9911" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9911" x2="7.0993" y2="5.0165" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9911" x2="0.7239" y2="5.0165" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0165" x2="7.0993" y2="5.0419" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0165" x2="0.7239" y2="5.0419" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0419" x2="7.0993" y2="5.0673" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0419" x2="0.7239" y2="5.0673" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0673" x2="7.0993" y2="5.0927" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0673" x2="0.7239" y2="5.0927" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0927" x2="7.0993" y2="5.1181" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0927" x2="0.7239" y2="5.1181" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1181" x2="7.0993" y2="5.1435" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1181" x2="0.7239" y2="5.1435" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1435" x2="7.0993" y2="5.1689" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1435" x2="0.7239" y2="5.1689" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1689" x2="7.0993" y2="5.1943" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1689" x2="0.7239" y2="5.1943" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1943" x2="7.0993" y2="5.2197" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1943" x2="0.7239" y2="5.2197" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2197" x2="7.0993" y2="5.2451" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2197" x2="0.7239" y2="5.2451" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2451" x2="7.0993" y2="5.2705" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2451" x2="0.7239" y2="5.2705" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2705" x2="7.0993" y2="5.2959" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2705" x2="0.7239" y2="5.2959" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2959" x2="7.0739" y2="5.3213" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2959" x2="0.7239" y2="5.3213" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3213" x2="7.0739" y2="5.3467" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3213" x2="0.7239" y2="5.3467" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3467" x2="7.0739" y2="5.3721" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3467" x2="0.7239" y2="5.3721" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3721" x2="7.0739" y2="5.3975" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3721" x2="0.7239" y2="5.3975" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3975" x2="7.0739" y2="5.4229" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3975" x2="0.7239" y2="5.4229" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4229" x2="7.0739" y2="5.4483" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4229" x2="0.7239" y2="5.4483" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4483" x2="7.0739" y2="5.4737" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4483" x2="0.7239" y2="5.4737" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4737" x2="7.0739" y2="5.4991" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4737" x2="0.7239" y2="5.4991" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4991" x2="7.0739" y2="5.5245" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4991" x2="0.7239" y2="5.5245" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5245" x2="7.0739" y2="5.5499" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5245" x2="0.7239" y2="5.5499" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5499" x2="7.0739" y2="5.5753" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5499" x2="0.7239" y2="5.5753" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5753" x2="7.0739" y2="5.6007" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5753" x2="0.7239" y2="5.6007" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6007" x2="7.0739" y2="5.6261" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6007" x2="0.7239" y2="5.6261" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6261" x2="7.0739" y2="5.6515" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6261" x2="0.7239" y2="5.6515" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6515" x2="7.0739" y2="5.6769" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6515" x2="0.7239" y2="5.6769" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6769" x2="7.0739" y2="5.7023" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6769" x2="0.7239" y2="5.7023" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7023" x2="7.0739" y2="5.7277" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7023" x2="0.7239" y2="5.7277" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7277" x2="7.0739" y2="5.7531" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7277" x2="0.7239" y2="5.7531" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7531" x2="7.0739" y2="5.7785" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7531" x2="0.7239" y2="5.7785" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7785" x2="7.0739" y2="5.8039" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7785" x2="0.7239" y2="5.8039" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8039" x2="7.0739" y2="5.8293" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8039" x2="0.7239" y2="5.8293" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8293" x2="7.0739" y2="5.8547" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8293" x2="0.7239" y2="5.8547" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8547" x2="7.0739" y2="5.8801" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8547" x2="0.7239" y2="5.8801" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8801" x2="7.0739" y2="5.9055" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8801" x2="0.7239" y2="5.9055" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9055" x2="7.0739" y2="5.9309" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9055" x2="0.6985" y2="5.9309" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9309" x2="7.0739" y2="5.9563" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9309" x2="0.6985" y2="5.9563" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9563" x2="7.0739" y2="5.9817" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9563" x2="0.6985" y2="5.9817" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9817" x2="7.0739" y2="6.0071" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9817" x2="0.6985" y2="6.0071" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0071" x2="7.0739" y2="6.0325" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0071" x2="0.6985" y2="6.0325" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0325" x2="7.0739" y2="6.0579" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0325" x2="0.6985" y2="6.0579" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0579" x2="7.0739" y2="6.0833" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0579" x2="0.6985" y2="6.0833" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0833" x2="7.0739" y2="6.1087" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0833" x2="0.6985" y2="6.1087" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1087" x2="7.0739" y2="6.1341" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1087" x2="0.6985" y2="6.1341" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1341" x2="7.0739" y2="6.1595" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1341" x2="0.6985" y2="6.1595" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1595" x2="7.0739" y2="6.1849" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1595" x2="0.6985" y2="6.1849" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.1849" x2="7.0739" y2="6.2103" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1849" x2="0.6985" y2="6.2103" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2103" x2="7.0485" y2="6.2357" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2103" x2="0.6985" y2="6.2357" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2357" x2="7.0485" y2="6.2611" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2357" x2="0.6985" y2="6.2611" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2611" x2="7.0485" y2="6.2865" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2611" x2="0.6985" y2="6.2865" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="6.2865" x2="7.0231" y2="6.3119" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="6.2865" x2="0.6731" y2="6.3119" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="6.3119" x2="7.0231" y2="6.3373" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="6.3119" x2="0.6731" y2="6.3373" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="6.3373" x2="6.9977" y2="6.3627" layer="200" rot="R180"/>
<rectangle x1="-0.7239" y1="6.3373" x2="0.6477" y2="6.3627" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="6.3627" x2="6.9723" y2="6.3881" layer="200" rot="R180"/>
<rectangle x1="-0.6985" y1="6.3627" x2="0.6223" y2="6.3881" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="6.3881" x2="6.9215" y2="6.4135" layer="200" rot="R180"/>
<rectangle x1="-0.6223" y1="6.3881" x2="0.5461" y2="6.4135" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="6.4135" x2="6.8707" y2="6.4389" layer="200" rot="R180"/>
<text x="9.906" y="-6.8072" size="0.0508" layer="200" font="vector" rot="MR0">//kentro/work/Production/AOI Parts/Tyler/Breadboard power 5v/barreljack.bmp</text>
<wire x1="-5" y1="4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.5" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="9.8" y1="4.5" x2="9.8" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="4.5" x2="-3.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-2.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="0" y2="2.5" width="0.2032" layer="21"/>
<wire x1="0" y1="2.5" x2="2.286" y2="0" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.286" y1="0" x2="0" y2="-2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="0" y1="-2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="4.5" x2="-2" y2="4.5" width="0.2032" layer="21"/>
<wire x1="2" y1="4.5" x2="4.1" y2="4.5" width="0.2032" layer="21"/>
<wire x1="8.2" y1="4.5" x2="9.8" y2="4.5" width="0.2032" layer="21"/>
<wire x1="9.8" y1="-4.5" x2="8.1" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-4.5" x2="2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="-4.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<smd name="VIN0" x="0" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="GND" x="0" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="VIN1" x="6.1" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="P$4" x="6.1" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<hole x="0" y="0" drill="2.032"/>
<hole x="4.572" y="0" drill="2.032"/>
<rectangle x1="7.470140625" y1="4.83108125" x2="8.41501875" y2="6.48461875" layer="31"/>
<rectangle x1="-0.481328125" y1="6.493509375" x2="0.463546875" y2="8.147046875" layer="31" rot="R90"/>
<rectangle x1="-0.570228125" y1="-8.187690625" x2="0.374646875" y2="-6.534153125" layer="31" rot="R90"/>
<rectangle x1="5.76453125" y1="-8.18515" x2="6.70940625" y2="-6.5316125" layer="31" rot="R90"/>
</package>
<package name="POWER_JACK_SMD_OVERPASTE_TOE">
<wire x1="-5" y1="4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.5" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="9.8" y1="4.5" x2="9.8" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-4.5" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="4.5" x2="-3.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-2.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="0" y2="2.5" width="0.2032" layer="21"/>
<wire x1="0" y1="2.5" x2="2.286" y2="0" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.286" y1="0" x2="0" y2="-2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="0" y1="-2.5" x2="-3.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="4.5" x2="-2" y2="4.5" width="0.2032" layer="21"/>
<wire x1="2" y1="4.5" x2="4.1" y2="4.5" width="0.2032" layer="21"/>
<wire x1="8.2" y1="4.5" x2="9.8" y2="4.5" width="0.2032" layer="21"/>
<wire x1="9.8" y1="-4.5" x2="8.1" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-4.5" x2="2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="-4.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<smd name="VIN0" x="0" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="GND" x="0" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="VIN1" x="6.1" y="5.7" dx="2.8" dy="2.4" layer="1"/>
<smd name="P$4" x="6.1" y="-5.7" dx="2.8" dy="2.4" layer="1"/>
<hole x="0" y="0" drill="2.032"/>
<hole x="4.572" y="0" drill="2.032"/>
<rectangle x1="-1.399540625" y1="-8.382" x2="1.399540625" y2="-6.8961" layer="31"/>
<rectangle x1="4.701540625" y1="-8.384540625" x2="7.500621875" y2="-6.898640625" layer="31"/>
<rectangle x1="4.7015375" y1="6.8961" x2="7.50061875" y2="8.382" layer="31" rot="R180"/>
<rectangle x1="-1.39954375" y1="6.8961" x2="1.3995375" y2="8.382" layer="31" rot="R180"/>
<rectangle x1="5.7531" y1="-6.6675" x2="6.8707" y2="-6.6421" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-6.6421" x2="6.9215" y2="-6.6167" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="-6.6167" x2="6.9723" y2="-6.5913" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="-6.5913" x2="6.9723" y2="-6.5659" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="-6.5659" x2="6.9977" y2="-6.5405" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-6.5405" x2="7.0231" y2="-6.5151" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-6.5151" x2="7.0231" y2="-6.4897" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4897" x2="7.0231" y2="-6.4643" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4643" x2="7.0231" y2="-6.4389" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="-6.4643" x2="0.4445" y2="-6.4389" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-6.4389" x2="7.0231" y2="-6.4135" layer="200" rot="R180"/>
<rectangle x1="-0.8001" y1="-6.4389" x2="0.5207" y2="-6.4135" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.4135" x2="7.0231" y2="-6.3881" layer="200" rot="R180"/>
<rectangle x1="-0.8255" y1="-6.4135" x2="0.5461" y2="-6.3881" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3881" x2="7.0231" y2="-6.3627" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-6.3881" x2="0.5715" y2="-6.3627" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3627" x2="7.0231" y2="-6.3373" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-6.3627" x2="0.5969" y2="-6.3373" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3373" x2="7.0231" y2="-6.3119" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.3373" x2="0.6223" y2="-6.3119" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.3119" x2="7.0231" y2="-6.2865" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.3119" x2="0.6223" y2="-6.2865" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2865" x2="7.0231" y2="-6.2611" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-6.2865" x2="0.6223" y2="-6.2611" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2611" x2="7.0231" y2="-6.2357" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2611" x2="0.6477" y2="-6.2357" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2357" x2="7.0231" y2="-6.2103" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2357" x2="0.6477" y2="-6.2103" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.2103" x2="7.0231" y2="-6.1849" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.2103" x2="0.6477" y2="-6.1849" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1849" x2="7.0231" y2="-6.1595" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1849" x2="0.6477" y2="-6.1595" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1595" x2="7.0231" y2="-6.1341" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1595" x2="0.6477" y2="-6.1341" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1341" x2="7.0231" y2="-6.1087" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1341" x2="0.6477" y2="-6.1087" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.1087" x2="7.0231" y2="-6.0833" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.1087" x2="0.6477" y2="-6.0833" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0833" x2="7.0231" y2="-6.0579" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0833" x2="0.6477" y2="-6.0579" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0579" x2="7.0231" y2="-6.0325" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0579" x2="0.6477" y2="-6.0325" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0325" x2="7.0231" y2="-6.0071" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0325" x2="0.6477" y2="-6.0071" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-6.0071" x2="7.0231" y2="-5.9817" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-6.0071" x2="0.6477" y2="-5.9817" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9817" x2="7.0231" y2="-5.9563" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9817" x2="0.6731" y2="-5.9563" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9563" x2="7.0231" y2="-5.9309" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9563" x2="0.6731" y2="-5.9309" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9309" x2="7.0231" y2="-5.9055" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9309" x2="0.6731" y2="-5.9055" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.9055" x2="7.0231" y2="-5.8801" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.9055" x2="0.6731" y2="-5.8801" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="-5.8801" x2="7.0231" y2="-5.8547" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8801" x2="0.6731" y2="-5.8547" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8547" x2="7.0231" y2="-5.8293" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8547" x2="0.6731" y2="-5.8293" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8293" x2="7.0231" y2="-5.8039" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8293" x2="0.6731" y2="-5.8039" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.8039" x2="7.0231" y2="-5.7785" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.8039" x2="0.6731" y2="-5.7785" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7785" x2="7.0231" y2="-5.7531" layer="200" rot="R180"/>
<rectangle x1="-0.9017" y1="-5.7785" x2="0.6731" y2="-5.7531" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7531" x2="7.0231" y2="-5.7277" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7531" x2="0.6731" y2="-5.7277" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7277" x2="7.0231" y2="-5.7023" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7277" x2="0.6731" y2="-5.7023" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.7023" x2="7.0231" y2="-5.6769" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.7023" x2="0.6731" y2="-5.6769" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6769" x2="7.0231" y2="-5.6515" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6769" x2="0.6731" y2="-5.6515" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6515" x2="7.0231" y2="-5.6261" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6515" x2="0.6731" y2="-5.6261" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6261" x2="7.0231" y2="-5.6007" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6261" x2="0.6731" y2="-5.6007" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.6007" x2="7.0231" y2="-5.5753" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.6007" x2="0.6731" y2="-5.5753" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5753" x2="7.0231" y2="-5.5499" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5753" x2="0.6731" y2="-5.5499" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5499" x2="7.0231" y2="-5.5245" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5499" x2="0.6731" y2="-5.5245" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.5245" x2="7.0231" y2="-5.4991" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.5245" x2="0.6731" y2="-5.4991" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4991" x2="7.0231" y2="-5.4737" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4991" x2="0.6731" y2="-5.4737" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4737" x2="7.0231" y2="-5.4483" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4737" x2="0.6731" y2="-5.4483" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4483" x2="7.0231" y2="-5.4229" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4483" x2="0.6731" y2="-5.4229" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.4229" x2="7.0231" y2="-5.3975" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.4229" x2="0.6731" y2="-5.3975" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3975" x2="7.0231" y2="-5.3721" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3975" x2="0.6731" y2="-5.3721" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3721" x2="7.0231" y2="-5.3467" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3721" x2="0.6731" y2="-5.3467" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3467" x2="7.0231" y2="-5.3213" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3467" x2="0.6731" y2="-5.3213" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.3213" x2="7.0231" y2="-5.2959" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.3213" x2="0.6731" y2="-5.2959" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2959" x2="7.0231" y2="-5.2705" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2959" x2="0.6731" y2="-5.2705" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2705" x2="7.0231" y2="-5.2451" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2705" x2="0.6731" y2="-5.2451" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2451" x2="7.0231" y2="-5.2197" layer="200" rot="R180"/>
<rectangle x1="-0.8763" y1="-5.2451" x2="0.6731" y2="-5.2197" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.2197" x2="7.0231" y2="-5.1943" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.2197" x2="0.6731" y2="-5.1943" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1943" x2="7.0231" y2="-5.1689" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1943" x2="0.6731" y2="-5.1689" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1689" x2="7.0231" y2="-5.1435" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1689" x2="0.6731" y2="-5.1435" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1435" x2="7.0231" y2="-5.1181" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1435" x2="0.6731" y2="-5.1181" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.1181" x2="7.0231" y2="-5.0927" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.1181" x2="0.6731" y2="-5.0927" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0927" x2="7.0231" y2="-5.0673" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0927" x2="0.6731" y2="-5.0673" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0673" x2="7.0231" y2="-5.0419" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0673" x2="0.6731" y2="-5.0419" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0419" x2="7.0231" y2="-5.0165" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0419" x2="0.6731" y2="-5.0165" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-5.0165" x2="6.9977" y2="-4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-5.0165" x2="0.6731" y2="-4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-4.9911" x2="0.6731" y2="-4.9657" layer="200" rot="R180"/>
<rectangle x1="-0.8509" y1="-4.9657" x2="0.5207" y2="-4.9403" layer="200" rot="R180"/>
<rectangle x1="4.3815" y1="-4.7879" x2="7.0739" y2="-4.7625" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-4.7879" x2="1.3335" y2="-4.7625" layer="200" rot="R180"/>
<rectangle x1="-4.4069" y1="-4.7625" x2="7.0993" y2="-4.7371" layer="200" rot="R180"/>
<rectangle x1="-4.4577" y1="-4.7371" x2="7.0993" y2="-4.7117" layer="200" rot="R180"/>
<rectangle x1="-4.5085" y1="-4.7117" x2="7.0993" y2="-4.6863" layer="200" rot="R180"/>
<rectangle x1="-4.5593" y1="-4.6863" x2="7.0993" y2="-4.6609" layer="200" rot="R180"/>
<rectangle x1="-4.5847" y1="-4.6609" x2="7.0993" y2="-4.6355" layer="200" rot="R180"/>
<rectangle x1="-4.6355" y1="-4.6355" x2="7.0993" y2="-4.6101" layer="200" rot="R180"/>
<rectangle x1="-4.6609" y1="-4.6101" x2="7.0993" y2="-4.5847" layer="200" rot="R180"/>
<rectangle x1="-4.6863" y1="-4.5847" x2="7.0993" y2="-4.5593" layer="200" rot="R180"/>
<rectangle x1="-4.7117" y1="-4.5593" x2="7.0739" y2="-4.5339" layer="200" rot="R180"/>
<rectangle x1="-4.7371" y1="-4.5339" x2="7.0739" y2="-4.5085" layer="200" rot="R180"/>
<rectangle x1="-4.7625" y1="-4.5085" x2="7.0739" y2="-4.4831" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="-4.4831" x2="7.0739" y2="-4.4577" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="-4.4577" x2="7.0739" y2="-4.4323" layer="200" rot="R180"/>
<rectangle x1="-4.8133" y1="-4.4323" x2="7.5057" y2="-4.4069" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="-4.4069" x2="9.7155" y2="-4.3815" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="-4.3815" x2="9.7663" y2="-4.3561" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="-4.3561" x2="9.7917" y2="-4.3307" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="-4.3307" x2="9.8171" y2="-4.3053" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="-4.3053" x2="9.8425" y2="-4.2799" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="-4.2799" x2="9.8679" y2="-4.2545" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2545" x2="9.8933" y2="-4.2291" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2291" x2="9.8933" y2="-4.2037" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="-4.2037" x2="9.8933" y2="-4.1783" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1783" x2="9.8933" y2="-4.1529" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1529" x2="9.8933" y2="-4.1275" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="-4.1275" x2="9.8933" y2="-4.1021" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.1021" x2="9.8933" y2="-4.0767" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0767" x2="9.8933" y2="-4.0513" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0513" x2="9.8933" y2="-4.0259" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0259" x2="9.8933" y2="-4.0005" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-4.0005" x2="9.8933" y2="-3.9751" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9751" x2="9.8933" y2="-3.9497" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9497" x2="9.8933" y2="-3.9243" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.9243" x2="9.8933" y2="-3.8989" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.8989" x2="9.8933" y2="-3.8735" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-3.8735" x2="9.8933" y2="-3.8481" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.8481" x2="9.8933" y2="-3.8227" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.8227" x2="9.8933" y2="-3.7973" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7973" x2="9.8933" y2="-3.7719" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7719" x2="9.8933" y2="-3.7465" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7465" x2="9.8933" y2="-3.7211" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.7211" x2="9.8933" y2="-3.6957" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6957" x2="9.8933" y2="-3.6703" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6703" x2="9.8933" y2="-3.6449" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6449" x2="9.8933" y2="-3.6195" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.6195" x2="9.8933" y2="-3.5941" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5941" x2="9.8933" y2="-3.5687" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5687" x2="9.8933" y2="-3.5433" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5433" x2="9.8933" y2="-3.5179" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.5179" x2="9.8933" y2="-3.4925" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4925" x2="9.8933" y2="-3.4671" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4671" x2="9.8933" y2="-3.4417" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4417" x2="9.8933" y2="-3.4163" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.4163" x2="9.8933" y2="-3.3909" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3909" x2="9.8933" y2="-3.3655" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3655" x2="9.8933" y2="-3.3401" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3401" x2="9.8933" y2="-3.3147" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.3147" x2="9.8933" y2="-3.2893" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2893" x2="9.8933" y2="-3.2639" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2639" x2="9.8933" y2="-3.2385" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2385" x2="9.8933" y2="-3.2131" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.2131" x2="9.8933" y2="-3.1877" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1877" x2="9.8933" y2="-3.1623" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1623" x2="9.8933" y2="-3.1369" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1369" x2="9.8933" y2="-3.1115" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.1115" x2="9.8933" y2="-3.0861" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0861" x2="9.8933" y2="-3.0607" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0607" x2="9.8933" y2="-3.0353" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0353" x2="9.8933" y2="-3.0099" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-3.0099" x2="9.8933" y2="-2.9845" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9845" x2="9.8933" y2="-2.9591" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9591" x2="9.8933" y2="-2.9337" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9337" x2="9.8933" y2="-2.9083" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.9083" x2="9.8933" y2="-2.8829" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8829" x2="9.8933" y2="-2.8575" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8575" x2="9.8933" y2="-2.8321" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8321" x2="9.8933" y2="-2.8067" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.8067" x2="9.8933" y2="-2.7813" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7813" x2="9.8933" y2="-2.7559" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7559" x2="9.8933" y2="-2.7305" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7305" x2="9.8933" y2="-2.7051" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.7051" x2="9.8933" y2="-2.6797" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6797" x2="9.8933" y2="-2.6543" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6543" x2="9.8933" y2="-2.6289" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6289" x2="9.8933" y2="-2.6035" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.6035" x2="9.8933" y2="-2.5781" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5781" x2="9.8933" y2="-2.5527" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5527" x2="9.8933" y2="-2.5273" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5273" x2="9.8933" y2="-2.5019" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.5019" x2="9.8933" y2="-2.4765" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4765" x2="9.8933" y2="-2.4511" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4511" x2="9.8933" y2="-2.4257" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4257" x2="9.8933" y2="-2.4003" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.4003" x2="9.8933" y2="-2.3749" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3749" x2="9.8933" y2="-2.3495" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3495" x2="9.8933" y2="-2.3241" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.3241" x2="9.8933" y2="-2.2987" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2987" x2="9.8933" y2="-2.2733" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2733" x2="9.8933" y2="-2.2479" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2479" x2="9.8933" y2="-2.2225" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.2225" x2="9.8933" y2="-2.1971" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1971" x2="9.8933" y2="-2.1717" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1717" x2="9.8933" y2="-2.1463" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1463" x2="9.8933" y2="-2.1209" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.1209" x2="9.8933" y2="-2.0955" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0955" x2="9.8933" y2="-2.0701" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0701" x2="9.8933" y2="-2.0447" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0447" x2="9.8933" y2="-2.0193" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-2.0193" x2="9.8933" y2="-1.9939" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9939" x2="9.8933" y2="-1.9685" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9685" x2="9.8933" y2="-1.9431" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9431" x2="9.8933" y2="-1.9177" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.9177" x2="9.8933" y2="-1.8923" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8923" x2="9.8933" y2="-1.8669" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8669" x2="9.8933" y2="-1.8415" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8415" x2="9.8933" y2="-1.8161" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.8161" x2="9.8933" y2="-1.7907" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7907" x2="9.8933" y2="-1.7653" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7653" x2="9.8933" y2="-1.7399" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7399" x2="9.8933" y2="-1.7145" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.7145" x2="9.8933" y2="-1.6891" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6891" x2="9.8933" y2="-1.6637" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6637" x2="9.8933" y2="-1.6383" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6383" x2="9.8679" y2="-1.6129" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.6129" x2="8.8773" y2="-1.5875" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5875" x2="8.8773" y2="-1.5621" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5621" x2="8.8773" y2="-1.5367" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5367" x2="8.8773" y2="-1.5113" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.5113" x2="8.8773" y2="-1.4859" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4859" x2="8.8773" y2="-1.4605" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4605" x2="8.8773" y2="-1.4351" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4351" x2="8.8773" y2="-1.4097" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.4097" x2="8.8773" y2="-1.3843" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3843" x2="8.8773" y2="-1.3589" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3589" x2="8.8773" y2="-1.3335" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3335" x2="8.8773" y2="-1.3081" layer="200" rot="R180"/>
<rectangle x1="4.8133" y1="-1.3081" x2="8.8773" y2="-1.2827" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.3081" x2="4.3815" y2="-1.2827" layer="200" rot="R180"/>
<rectangle x1="4.9149" y1="-1.2827" x2="8.8773" y2="-1.2573" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2827" x2="4.2799" y2="-1.2573" layer="200" rot="R180"/>
<rectangle x1="5.0165" y1="-1.2573" x2="8.8773" y2="-1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2573" x2="4.1783" y2="-1.2319" layer="200" rot="R180"/>
<rectangle x1="5.0927" y1="-1.2319" x2="8.8773" y2="-1.2065" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2319" x2="4.1275" y2="-1.2065" layer="200" rot="R180"/>
<rectangle x1="5.1435" y1="-1.2065" x2="8.8773" y2="-1.1811" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.2065" x2="4.0513" y2="-1.1811" layer="200" rot="R180"/>
<rectangle x1="5.1943" y1="-1.1811" x2="8.8773" y2="-1.1557" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1811" x2="4.0005" y2="-1.1557" layer="200" rot="R180"/>
<rectangle x1="5.2197" y1="-1.1557" x2="8.8773" y2="-1.1303" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1557" x2="3.9497" y2="-1.1303" layer="200" rot="R180"/>
<rectangle x1="5.2705" y1="-1.1303" x2="8.8773" y2="-1.1049" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1303" x2="3.8989" y2="-1.1049" layer="200" rot="R180"/>
<rectangle x1="5.2959" y1="-1.1049" x2="8.8773" y2="-1.0795" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.1049" x2="3.8735" y2="-1.0795" layer="200" rot="R180"/>
<rectangle x1="5.3467" y1="-1.0795" x2="8.8773" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="0.1143" y1="-1.0795" x2="3.8227" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0795" x2="-0.1143" y2="-1.0541" layer="200" rot="R180"/>
<rectangle x1="5.3721" y1="-1.0541" x2="8.8773" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="0.2413" y1="-1.0541" x2="3.7973" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0541" x2="-0.2413" y2="-1.0287" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="-1.0287" x2="8.8773" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="0.3429" y1="-1.0287" x2="3.7465" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0287" x2="-0.3429" y2="-1.0033" layer="200" rot="R180"/>
<rectangle x1="5.4229" y1="-1.0033" x2="8.8773" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="0.4191" y1="-1.0033" x2="3.7211" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-1.0033" x2="-0.4191" y2="-0.9779" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="-0.9779" x2="8.8773" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="0.4699" y1="-0.9779" x2="3.6957" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9779" x2="-0.4699" y2="-0.9525" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="-0.9525" x2="8.8773" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="0.5207" y1="-0.9525" x2="3.6703" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9525" x2="-0.5207" y2="-0.9271" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="-0.9271" x2="8.8773" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="0.5715" y1="-0.9271" x2="3.6449" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9271" x2="-0.5715" y2="-0.9017" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="-0.9017" x2="8.8773" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="0.6223" y1="-0.9017" x2="3.6195" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.9017" x2="-0.6223" y2="-0.8763" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="-0.8763" x2="8.8773" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="0.6477" y1="-0.8763" x2="3.5941" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8763" x2="-0.6477" y2="-0.8509" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="-0.8509" x2="8.8773" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="0.6985" y1="-0.8509" x2="3.5687" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8509" x2="-0.6985" y2="-0.8255" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-0.8255" x2="8.8773" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="0.7239" y1="-0.8255" x2="3.5433" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8255" x2="-0.7239" y2="-0.8001" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="-0.8001" x2="8.8773" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="0.7493" y1="-0.8001" x2="3.5179" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.8001" x2="-0.7493" y2="-0.7747" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="-0.7747" x2="8.8773" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="-0.7747" x2="3.4925" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7747" x2="-0.7747" y2="-0.7493" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="-0.7493" x2="8.8773" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="0.8255" y1="-0.7493" x2="3.4925" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7493" x2="-0.8001" y2="-0.7239" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="-0.7239" x2="8.8773" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="0.8509" y1="-0.7239" x2="3.4671" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.7239" x2="-0.8255" y2="-0.6985" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="-0.6985" x2="8.8773" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="-0.6985" x2="3.4671" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6985" x2="-0.8509" y2="-0.6731" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="-0.6731" x2="8.8773" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="-0.6731" x2="3.4417" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6731" x2="-0.8763" y2="-0.6477" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="-0.6477" x2="8.8773" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="-0.6477" x2="3.4417" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6477" x2="-0.9017" y2="-0.6223" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="-0.6223" x2="8.8773" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="-0.6223" x2="3.4163" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.6223" x2="-0.9017" y2="-0.5969" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="-0.5969" x2="8.8773" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="-0.5969" x2="3.4163" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5969" x2="-0.9271" y2="-0.5715" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="-0.5715" x2="8.8773" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="-0.5715" x2="3.3909" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5715" x2="-0.9525" y2="-0.5461" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="-0.5461" x2="8.8773" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="-0.5461" x2="3.3655" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5461" x2="-0.9525" y2="-0.5207" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="-0.5207" x2="8.8773" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="-0.5207" x2="3.3655" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="-4.9911" y1="-0.5207" x2="-0.9779" y2="-0.4953" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4953" x2="8.8773" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="-0.4953" x2="3.3401" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4953" x2="-0.9779" y2="-0.4699" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4699" x2="8.8773" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="-0.4699" x2="3.3401" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4699" x2="-1.0033" y2="-0.4445" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="-0.4445" x2="8.8773" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="-0.4445" x2="3.3401" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4445" x2="-1.0033" y2="-0.4191" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.4191" x2="8.8773" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.4191" x2="3.3147" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.4191" x2="-1.0033" y2="-0.3937" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3937" x2="8.8773" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.3937" x2="3.3147" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3937" x2="-1.0033" y2="-0.3683" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3683" x2="8.8773" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="-0.3683" x2="3.3147" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3683" x2="-1.0287" y2="-0.3429" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="-0.3429" x2="8.8773" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.3429" x2="3.3147" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3429" x2="-1.0287" y2="-0.3175" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.3175" x2="8.8773" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.3175" x2="3.3147" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.3175" x2="-1.0287" y2="-0.2921" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2921" x2="8.8773" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2921" x2="3.2893" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2921" x2="-1.0287" y2="-0.2667" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2667" x2="8.8773" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2667" x2="3.2893" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2667" x2="-1.0287" y2="-0.2413" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2413" x2="8.8773" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="-0.2413" x2="3.2893" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2413" x2="-1.0287" y2="-0.2159" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.2159" x2="8.8773" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.2159" x2="3.2893" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.2159" x2="-1.0541" y2="-0.1905" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1905" x2="8.8773" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1905" x2="3.2893" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1905" x2="-1.0541" y2="-0.1651" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1651" x2="8.8773" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1651" x2="3.2893" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1651" x2="-1.0541" y2="-0.1397" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1397" x2="8.8773" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1397" x2="3.2893" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1397" x2="-1.0541" y2="-0.1143" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.1143" x2="8.8773" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.1143" x2="3.2893" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.1143" x2="-1.0541" y2="-0.0889" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0889" x2="8.8773" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0889" x2="3.2893" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0889" x2="-1.0541" y2="-0.0635" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0635" x2="8.8773" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0635" x2="3.2893" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0635" x2="-1.0541" y2="-0.0381" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0381" x2="8.8773" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0381" x2="3.2893" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0381" x2="-1.0541" y2="-0.0127" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="-0.0127" x2="8.8773" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="-0.0127" x2="3.2893" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="-0.0127" x2="-1.0541" y2="0.0127" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0127" x2="8.8773" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0127" x2="3.2893" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0127" x2="-1.0541" y2="0.0381" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0381" x2="8.8773" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0381" x2="3.2893" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0381" x2="-1.0541" y2="0.0635" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0635" x2="8.8773" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0635" x2="3.2893" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0635" x2="-1.0541" y2="0.0889" layer="200" rot="R180"/>
<rectangle x1="5.8801" y1="0.0889" x2="8.8773" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.0889" x2="3.2893" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.0889" x2="-1.0541" y2="0.1143" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1143" x2="8.8773" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="1.1049" y1="0.1143" x2="3.2893" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1143" x2="-1.0541" y2="0.1397" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1397" x2="8.8773" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1397" x2="3.2893" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1397" x2="-1.0541" y2="0.1651" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1651" x2="8.8773" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1651" x2="3.3147" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1651" x2="-1.0541" y2="0.1905" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.1905" x2="8.8773" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.1905" x2="3.3147" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.1905" x2="-1.0541" y2="0.2159" layer="200" rot="R180"/>
<rectangle x1="5.8547" y1="0.2159" x2="8.9027" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.2159" x2="3.3147" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2159" x2="-1.0541" y2="0.2413" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2413" x2="8.9027" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="1.0795" y1="0.2413" x2="3.3147" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2413" x2="-1.0541" y2="0.2667" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2667" x2="8.9027" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.2667" x2="3.3401" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2667" x2="-1.0541" y2="0.2921" layer="200" rot="R180"/>
<rectangle x1="5.8293" y1="0.2921" x2="8.9027" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.2921" x2="3.3401" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.2921" x2="-1.0287" y2="0.3175" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="0.3175" x2="8.9027" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.3175" x2="3.3401" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3175" x2="-1.0287" y2="0.3429" layer="200" rot="R180"/>
<rectangle x1="5.8039" y1="0.3429" x2="8.9027" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="1.0541" y1="0.3429" x2="3.3655" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3429" x2="-1.0287" y2="0.3683" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="0.3683" x2="8.9027" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.3683" x2="3.3909" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3683" x2="-1.0287" y2="0.3937" layer="200" rot="R180"/>
<rectangle x1="5.7785" y1="0.3937" x2="8.9027" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.3937" x2="3.3655" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.3937" x2="-1.0287" y2="0.4191" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4191" x2="8.9027" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="1.0287" y1="0.4191" x2="3.3909" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4191" x2="-1.0033" y2="0.4445" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4445" x2="8.9027" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="0.4445" x2="3.4163" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4445" x2="-1.0033" y2="0.4699" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="0.4699" x2="8.9027" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="1.0033" y1="0.4699" x2="3.4163" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4699" x2="-1.0033" y2="0.4953" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="0.4953" x2="8.9027" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="0.4953" x2="3.4163" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.4953" x2="-0.9779" y2="0.5207" layer="200" rot="R180"/>
<rectangle x1="5.7277" y1="0.5207" x2="8.9027" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="0.9779" y1="0.5207" x2="3.4417" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5207" x2="-0.9779" y2="0.5461" layer="200" rot="R180"/>
<rectangle x1="5.7023" y1="0.5461" x2="8.9027" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="0.5461" x2="3.4417" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5461" x2="-0.9525" y2="0.5715" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="0.5715" x2="8.9027" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="0.9525" y1="0.5715" x2="3.4671" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5715" x2="-0.9525" y2="0.5969" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="0.5969" x2="8.9027" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="0.5969" x2="3.4671" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.5969" x2="-0.9271" y2="0.6223" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="0.6223" x2="8.9027" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="0.9271" y1="0.6223" x2="3.4925" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6223" x2="-0.9271" y2="0.6477" layer="200" rot="R180"/>
<rectangle x1="5.6515" y1="0.6477" x2="8.9027" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="0.6477" x2="3.4925" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6477" x2="-0.9017" y2="0.6731" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="0.6731" x2="8.9027" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="0.9017" y1="0.6731" x2="3.5179" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6731" x2="-0.8763" y2="0.6985" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="0.6985" x2="8.9027" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="0.8763" y1="0.6985" x2="3.5433" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.6985" x2="-0.8763" y2="0.7239" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7239" x2="8.9027" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="0.8509" y1="0.7239" x2="3.5433" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7239" x2="-0.8509" y2="0.7493" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7493" x2="8.9027" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="0.8255" y1="0.7493" x2="3.5687" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7493" x2="-0.8255" y2="0.7747" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="0.7747" x2="8.9027" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="0.7747" x2="3.5941" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.7747" x2="-0.8001" y2="0.8001" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="0.8001" x2="8.9027" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="0.8001" y1="0.8001" x2="3.6195" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8001" x2="-0.7747" y2="0.8255" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="0.8255" x2="8.9027" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="0.7747" y1="0.8255" x2="3.6195" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8255" x2="-0.7493" y2="0.8509" layer="200" rot="R180"/>
<rectangle x1="5.4737" y1="0.8509" x2="8.9027" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="0.7493" y1="0.8509" x2="3.6449" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8509" x2="-0.7239" y2="0.8763" layer="200" rot="R180"/>
<rectangle x1="5.4483" y1="0.8763" x2="8.9027" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="0.7239" y1="0.8763" x2="3.6703" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.8763" x2="-0.6731" y2="0.9017" layer="200" rot="R180"/>
<rectangle x1="5.4229" y1="0.9017" x2="8.9027" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="0.6731" y1="0.9017" x2="3.6957" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9017" x2="-0.6477" y2="0.9271" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="0.9271" x2="8.9027" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="0.6477" y1="0.9271" x2="3.7211" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9271" x2="-0.5969" y2="0.9525" layer="200" rot="R180"/>
<rectangle x1="5.3975" y1="0.9525" x2="8.9027" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="0.5969" y1="0.9525" x2="3.7465" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9525" x2="-0.5461" y2="0.9779" layer="200" rot="R180"/>
<rectangle x1="5.3213" y1="0.9779" x2="8.9027" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="0.5715" y1="0.9779" x2="3.7973" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="0.9779" x2="-0.4953" y2="1.0033" layer="200" rot="R180"/>
<rectangle x1="5.2959" y1="1.0033" x2="8.9027" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="0.5207" y1="1.0033" x2="3.8227" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0033" x2="-0.4191" y2="1.0287" layer="200" rot="R180"/>
<rectangle x1="5.2705" y1="1.0287" x2="8.9027" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="0.4445" y1="1.0287" x2="3.8481" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0287" x2="-0.3429" y2="1.0541" layer="200" rot="R180"/>
<rectangle x1="5.2197" y1="1.0541" x2="8.9027" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="0.3937" y1="1.0541" x2="3.8989" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0541" x2="-0.2413" y2="1.0795" layer="200" rot="R180"/>
<rectangle x1="5.1689" y1="1.0795" x2="8.9027" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="0.2921" y1="1.0795" x2="3.9497" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.0795" x2="-0.0889" y2="1.1049" layer="200" rot="R180"/>
<rectangle x1="5.1181" y1="1.1049" x2="8.9027" y2="1.1303" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1049" x2="4.0005" y2="1.1303" layer="200" rot="R180"/>
<rectangle x1="5.0673" y1="1.1303" x2="8.9027" y2="1.1557" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1303" x2="4.0513" y2="1.1557" layer="200" rot="R180"/>
<rectangle x1="5.0165" y1="1.1557" x2="8.9027" y2="1.1811" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1557" x2="4.1275" y2="1.1811" layer="200" rot="R180"/>
<rectangle x1="4.9149" y1="1.1811" x2="8.9027" y2="1.2065" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.1811" x2="4.2037" y2="1.2065" layer="200" rot="R180"/>
<rectangle x1="4.7625" y1="1.2065" x2="8.9027" y2="1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2065" x2="4.3307" y2="1.2319" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2319" x2="8.9027" y2="1.2573" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2573" x2="8.9027" y2="1.2827" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.2827" x2="8.9027" y2="1.3081" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3081" x2="8.9027" y2="1.3335" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3335" x2="8.9027" y2="1.3589" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3589" x2="8.9027" y2="1.3843" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.3843" x2="8.9027" y2="1.4097" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4097" x2="8.9027" y2="1.4351" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4351" x2="8.9027" y2="1.4605" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4605" x2="8.9027" y2="1.4859" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.4859" x2="8.9027" y2="1.5113" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5113" x2="8.9027" y2="1.5367" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5367" x2="8.9027" y2="1.5621" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5621" x2="8.9027" y2="1.5875" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.5875" x2="8.9027" y2="1.6129" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6129" x2="8.9027" y2="1.6383" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6383" x2="8.9027" y2="1.6637" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6637" x2="8.9027" y2="1.6891" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.6891" x2="8.9027" y2="1.7145" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7145" x2="8.9027" y2="1.7399" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7399" x2="8.9027" y2="1.7653" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7653" x2="8.9027" y2="1.7907" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.7907" x2="8.9027" y2="1.8161" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8161" x2="8.9027" y2="1.8415" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8415" x2="8.9027" y2="1.8669" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8669" x2="8.9027" y2="1.8923" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.8923" x2="8.9027" y2="1.9177" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9177" x2="8.9027" y2="1.9431" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9431" x2="8.9027" y2="1.9685" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9685" x2="8.9027" y2="1.9939" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="1.9939" x2="8.9027" y2="2.0193" layer="200" rot="R180"/>
<rectangle x1="-4.9657" y1="2.0193" x2="8.9027" y2="2.0447" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0447" x2="8.9027" y2="2.0701" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0701" x2="8.9027" y2="2.0955" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.0955" x2="8.9027" y2="2.1209" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1209" x2="8.9027" y2="2.1463" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1463" x2="8.9281" y2="2.1717" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1717" x2="8.9281" y2="2.1971" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.1971" x2="8.9281" y2="2.2225" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2225" x2="8.9281" y2="2.2479" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2479" x2="8.9281" y2="2.2733" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2733" x2="8.9281" y2="2.2987" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.2987" x2="8.9281" y2="2.3241" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3241" x2="8.9281" y2="2.3495" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3495" x2="8.9281" y2="2.3749" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.3749" x2="8.9281" y2="2.4003" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4003" x2="8.9281" y2="2.4257" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4257" x2="8.9281" y2="2.4511" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4511" x2="8.9281" y2="2.4765" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.4765" x2="8.9281" y2="2.5019" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5019" x2="8.9281" y2="2.5273" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5273" x2="8.9281" y2="2.5527" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5527" x2="8.9281" y2="2.5781" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.5781" x2="8.9281" y2="2.6035" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6035" x2="8.9281" y2="2.6289" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6289" x2="8.9281" y2="2.6543" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6543" x2="8.9281" y2="2.6797" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.6797" x2="8.9281" y2="2.7051" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7051" x2="8.9281" y2="2.7305" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7305" x2="8.9281" y2="2.7559" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7559" x2="8.9281" y2="2.7813" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.7813" x2="8.9281" y2="2.8067" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8067" x2="8.9281" y2="2.8321" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8321" x2="8.9281" y2="2.8575" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8575" x2="8.9281" y2="2.8829" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.8829" x2="8.9281" y2="2.9083" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9083" x2="8.9281" y2="2.9337" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9337" x2="8.9281" y2="2.9591" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9591" x2="8.9281" y2="2.9845" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="2.9845" x2="8.9281" y2="3.0099" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0099" x2="8.9281" y2="3.0353" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0353" x2="8.9281" y2="3.0607" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0607" x2="8.9281" y2="3.0861" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.0861" x2="8.9281" y2="3.1115" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1115" x2="8.9281" y2="3.1369" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1369" x2="8.9281" y2="3.1623" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1623" x2="8.9281" y2="3.1877" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.1877" x2="8.9281" y2="3.2131" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2131" x2="8.9281" y2="3.2385" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2385" x2="8.9281" y2="3.2639" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2639" x2="8.9281" y2="3.2893" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.2893" x2="8.9281" y2="3.3147" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3147" x2="8.9281" y2="3.3401" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3401" x2="8.9281" y2="3.3655" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3655" x2="8.9281" y2="3.3909" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.3909" x2="8.9281" y2="3.4163" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4163" x2="8.9281" y2="3.4417" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4417" x2="8.9281" y2="3.4671" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4671" x2="8.9281" y2="3.4925" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.4925" x2="8.9281" y2="3.5179" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5179" x2="8.9281" y2="3.5433" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5433" x2="8.9281" y2="3.5687" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5687" x2="8.9281" y2="3.5941" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.5941" x2="8.9281" y2="3.6195" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6195" x2="8.9281" y2="3.6449" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6449" x2="8.9281" y2="3.6703" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6703" x2="8.9281" y2="3.6957" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.6957" x2="8.9281" y2="3.7211" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7211" x2="8.9281" y2="3.7465" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7465" x2="8.9281" y2="3.7719" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7719" x2="8.9281" y2="3.7973" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.7973" x2="8.9281" y2="3.8227" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8227" x2="8.9281" y2="3.8481" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8481" x2="8.9281" y2="3.8735" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8735" x2="8.9281" y2="3.8989" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.8989" x2="8.9281" y2="3.9243" layer="200" rot="R180"/>
<rectangle x1="-4.9403" y1="3.9243" x2="8.9281" y2="3.9497" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="3.9497" x2="8.9281" y2="3.9751" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="3.9751" x2="8.9281" y2="4.0005" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0005" x2="8.9281" y2="4.0259" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0259" x2="8.9281" y2="4.0513" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0513" x2="8.9281" y2="4.0767" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.0767" x2="8.9281" y2="4.1021" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.1021" x2="8.9281" y2="4.1275" layer="200" rot="R180"/>
<rectangle x1="-4.9149" y1="4.1275" x2="8.9281" y2="4.1529" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="4.1529" x2="8.9281" y2="4.1783" layer="200" rot="R180"/>
<rectangle x1="-4.8895" y1="4.1783" x2="8.9281" y2="4.2037" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="4.2037" x2="8.9027" y2="4.2291" layer="200" rot="R180"/>
<rectangle x1="-4.8641" y1="4.2291" x2="8.9027" y2="4.2545" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="4.2545" x2="8.8773" y2="4.2799" layer="200" rot="R180"/>
<rectangle x1="-4.8387" y1="4.2799" x2="8.8773" y2="4.3053" layer="200" rot="R180"/>
<rectangle x1="-4.8133" y1="4.3053" x2="8.8519" y2="4.3307" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="4.3307" x2="8.8265" y2="4.3561" layer="200" rot="R180"/>
<rectangle x1="-4.7879" y1="4.3561" x2="8.8011" y2="4.3815" layer="200" rot="R180"/>
<rectangle x1="-4.7625" y1="4.3815" x2="8.7757" y2="4.4069" layer="200" rot="R180"/>
<rectangle x1="-4.7371" y1="4.4069" x2="8.7249" y2="4.4323" layer="200" rot="R180"/>
<rectangle x1="-4.7117" y1="4.4323" x2="8.6741" y2="4.4577" layer="200" rot="R180"/>
<rectangle x1="-4.6863" y1="4.4577" x2="8.6233" y2="4.4831" layer="200" rot="R180"/>
<rectangle x1="-4.6609" y1="4.4831" x2="7.1247" y2="4.5085" layer="200" rot="R180"/>
<rectangle x1="-4.6101" y1="4.5085" x2="7.1247" y2="4.5339" layer="200" rot="R180"/>
<rectangle x1="-4.5847" y1="4.5339" x2="7.1247" y2="4.5593" layer="200" rot="R180"/>
<rectangle x1="-4.5339" y1="4.5593" x2="7.1247" y2="4.5847" layer="200" rot="R180"/>
<rectangle x1="-4.4577" y1="4.5847" x2="7.1247" y2="4.6101" layer="200" rot="R180"/>
<rectangle x1="-4.3815" y1="4.6101" x2="6.9215" y2="4.6355" layer="200" rot="R180"/>
<rectangle x1="-3.0607" y1="4.6355" x2="-1.6637" y2="4.6609" layer="200" rot="R180"/>
<rectangle x1="-3.8989" y1="4.6355" x2="-3.3655" y2="4.6609" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="4.8133" x2="7.0739" y2="4.8387" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8387" x2="7.0993" y2="4.8641" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8641" x2="7.0993" y2="4.8895" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.8895" x2="7.0993" y2="4.9149" layer="200" rot="R180"/>
<rectangle x1="-0.1651" y1="4.8895" x2="0.7239" y2="4.9149" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9149" x2="7.0993" y2="4.9403" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9149" x2="0.7239" y2="4.9403" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9403" x2="7.0993" y2="4.9657" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9403" x2="0.7239" y2="4.9657" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9657" x2="7.0993" y2="4.9911" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9657" x2="0.7239" y2="4.9911" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="4.9911" x2="7.0993" y2="5.0165" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="4.9911" x2="0.7239" y2="5.0165" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0165" x2="7.0993" y2="5.0419" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0165" x2="0.7239" y2="5.0419" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0419" x2="7.0993" y2="5.0673" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0419" x2="0.7239" y2="5.0673" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0673" x2="7.0993" y2="5.0927" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0673" x2="0.7239" y2="5.0927" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.0927" x2="7.0993" y2="5.1181" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.0927" x2="0.7239" y2="5.1181" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1181" x2="7.0993" y2="5.1435" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1181" x2="0.7239" y2="5.1435" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1435" x2="7.0993" y2="5.1689" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1435" x2="0.7239" y2="5.1689" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1689" x2="7.0993" y2="5.1943" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1689" x2="0.7239" y2="5.1943" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.1943" x2="7.0993" y2="5.2197" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.1943" x2="0.7239" y2="5.2197" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2197" x2="7.0993" y2="5.2451" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2197" x2="0.7239" y2="5.2451" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2451" x2="7.0993" y2="5.2705" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2451" x2="0.7239" y2="5.2705" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2705" x2="7.0993" y2="5.2959" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2705" x2="0.7239" y2="5.2959" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.2959" x2="7.0739" y2="5.3213" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.2959" x2="0.7239" y2="5.3213" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3213" x2="7.0739" y2="5.3467" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3213" x2="0.7239" y2="5.3467" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3467" x2="7.0739" y2="5.3721" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3467" x2="0.7239" y2="5.3721" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3721" x2="7.0739" y2="5.3975" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3721" x2="0.7239" y2="5.3975" layer="200" rot="R180"/>
<rectangle x1="5.4991" y1="5.3975" x2="7.0739" y2="5.4229" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.3975" x2="0.7239" y2="5.4229" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4229" x2="7.0739" y2="5.4483" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4229" x2="0.7239" y2="5.4483" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4483" x2="7.0739" y2="5.4737" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4483" x2="0.7239" y2="5.4737" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4737" x2="7.0739" y2="5.4991" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4737" x2="0.7239" y2="5.4991" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.4991" x2="7.0739" y2="5.5245" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.4991" x2="0.7239" y2="5.5245" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5245" x2="7.0739" y2="5.5499" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5245" x2="0.7239" y2="5.5499" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5499" x2="7.0739" y2="5.5753" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5499" x2="0.7239" y2="5.5753" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.5753" x2="7.0739" y2="5.6007" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.5753" x2="0.7239" y2="5.6007" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6007" x2="7.0739" y2="5.6261" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6007" x2="0.7239" y2="5.6261" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6261" x2="7.0739" y2="5.6515" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6261" x2="0.7239" y2="5.6515" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6515" x2="7.0739" y2="5.6769" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6515" x2="0.7239" y2="5.6769" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.6769" x2="7.0739" y2="5.7023" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.6769" x2="0.7239" y2="5.7023" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7023" x2="7.0739" y2="5.7277" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7023" x2="0.7239" y2="5.7277" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7277" x2="7.0739" y2="5.7531" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7277" x2="0.7239" y2="5.7531" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7531" x2="7.0739" y2="5.7785" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7531" x2="0.7239" y2="5.7785" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.7785" x2="7.0739" y2="5.8039" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.7785" x2="0.7239" y2="5.8039" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8039" x2="7.0739" y2="5.8293" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8039" x2="0.7239" y2="5.8293" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8293" x2="7.0739" y2="5.8547" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8293" x2="0.7239" y2="5.8547" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8547" x2="7.0739" y2="5.8801" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8547" x2="0.7239" y2="5.8801" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.8801" x2="7.0739" y2="5.9055" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.8801" x2="0.7239" y2="5.9055" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9055" x2="7.0739" y2="5.9309" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9055" x2="0.6985" y2="5.9309" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9309" x2="7.0739" y2="5.9563" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9309" x2="0.6985" y2="5.9563" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9563" x2="7.0739" y2="5.9817" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9563" x2="0.6985" y2="5.9817" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="5.9817" x2="7.0739" y2="6.0071" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="5.9817" x2="0.6985" y2="6.0071" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0071" x2="7.0739" y2="6.0325" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0071" x2="0.6985" y2="6.0325" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0325" x2="7.0739" y2="6.0579" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0325" x2="0.6985" y2="6.0579" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0579" x2="7.0739" y2="6.0833" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0579" x2="0.6985" y2="6.0833" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.0833" x2="7.0739" y2="6.1087" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.0833" x2="0.6985" y2="6.1087" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1087" x2="7.0739" y2="6.1341" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1087" x2="0.6985" y2="6.1341" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1341" x2="7.0739" y2="6.1595" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1341" x2="0.6985" y2="6.1595" layer="200" rot="R180"/>
<rectangle x1="5.5245" y1="6.1595" x2="7.0739" y2="6.1849" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1595" x2="0.6985" y2="6.1849" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.1849" x2="7.0739" y2="6.2103" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.1849" x2="0.6985" y2="6.2103" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2103" x2="7.0485" y2="6.2357" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2103" x2="0.6985" y2="6.2357" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2357" x2="7.0485" y2="6.2611" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2357" x2="0.6985" y2="6.2611" layer="200" rot="R180"/>
<rectangle x1="5.5499" y1="6.2611" x2="7.0485" y2="6.2865" layer="200" rot="R180"/>
<rectangle x1="-0.7747" y1="6.2611" x2="0.6985" y2="6.2865" layer="200" rot="R180"/>
<rectangle x1="5.5753" y1="6.2865" x2="7.0231" y2="6.3119" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="6.2865" x2="0.6731" y2="6.3119" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="6.3119" x2="7.0231" y2="6.3373" layer="200" rot="R180"/>
<rectangle x1="-0.7493" y1="6.3119" x2="0.6731" y2="6.3373" layer="200" rot="R180"/>
<rectangle x1="5.6007" y1="6.3373" x2="6.9977" y2="6.3627" layer="200" rot="R180"/>
<rectangle x1="-0.7239" y1="6.3373" x2="0.6477" y2="6.3627" layer="200" rot="R180"/>
<rectangle x1="5.6261" y1="6.3627" x2="6.9723" y2="6.3881" layer="200" rot="R180"/>
<rectangle x1="-0.6985" y1="6.3627" x2="0.6223" y2="6.3881" layer="200" rot="R180"/>
<rectangle x1="5.6769" y1="6.3881" x2="6.9215" y2="6.4135" layer="200" rot="R180"/>
<rectangle x1="-0.6223" y1="6.3881" x2="0.5461" y2="6.4135" layer="200" rot="R180"/>
<rectangle x1="5.7531" y1="6.4135" x2="6.8707" y2="6.4389" layer="200" rot="R180"/>
<text x="9.906" y="-6.8072" size="0.0508" layer="200" font="vector" rot="MR0">//kentro/work/Production/AOI Parts/Tyler/Breadboard power 5v/barreljack.bmp</text>
</package>
<package name="POWER_JACK_PTH_BREAD">
<description>&lt;h1&gt;DC Barrel Jack Adapter - Breadboard Compatible&lt;/h1&gt;
&lt;h2&gt;PRT-10811&lt;/h2&gt;</description>
<wire x1="4.5" y1="14.6" x2="0.9" y2="14.6" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3.6" x2="-4.5" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="0.1" x2="4.5" y2="3.4" width="0.2032" layer="51"/>
<wire x1="4.5" y1="0.1" x2="-4.5" y2="0.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="3.6" x2="4.5" y2="9.5" width="0.2032" layer="21"/>
<wire x1="4.5" y1="14.6" x2="4.5" y2="11.9" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3.6" x2="-4.5" y2="14.6" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="14.6" x2="-0.9" y2="14.6" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3.6" x2="4.5" y2="3.6" width="0.2032" layer="21"/>
<pad name="PWR" x="0" y="13.7" drill="1.3"/>
<pad name="GND" x="0" y="7.9" drill="1.3"/>
<pad name="GNDBREAK" x="4.8" y="10.7" drill="1.3" rot="R90"/>
<text x="-3.81" y="5.08" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="4.06" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="PJ-047AH">
<wire x1="-5.8" y1="-3.5" x2="5.8" y2="-3.5" width="0.127" layer="48"/>
<wire x1="5.8" y1="-3.5" x2="5.8" y2="3.5" width="0.127" layer="51"/>
<wire x1="5.8" y1="3.5" x2="-5.8" y2="3.5" width="0.127" layer="48"/>
<wire x1="-5.8" y1="3.5" x2="-5.8" y2="-3.5" width="0.127" layer="21"/>
<pad name="P$1" x="-2.9" y="-3.65" drill="1.9" diameter="3.2"/>
<pad name="P$2" x="-2.9" y="3.65" drill="1.9" diameter="3.2"/>
<pad name="3" x="4.5" y="3.3" drill="1.6" diameter="2.8"/>
<pad name="1" x="4.5" y="-3.3" drill="1.6" diameter="2.8"/>
<pad name="2" x="2.3" y="0" drill="1.6" diameter="2.8"/>
<wire x1="-5.8" y1="3.5" x2="-4.6" y2="3.5" width="0.127" layer="21"/>
<wire x1="-1.2" y1="3.5" x2="3.1" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.1" y1="-3.5" x2="-1.2" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-4.6" y1="-3.5" x2="-5.8" y2="-3.5" width="0.127" layer="21"/>
<wire x1="5.8" y1="2.6" x2="5.8" y2="-2.6" width="0.127" layer="21"/>
<text x="7.2" y="-2.2" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<hole x="-1.8" y="0" drill="2.2"/>
</package>
<package name="SOIC127P600X150-9N">
<description>&lt;b&gt;SO-8(EP)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.688" y="1.905" dx="1.525" dy="0.65" layer="1"/>
<smd name="2" x="-2.688" y="0.635" dx="1.525" dy="0.65" layer="1"/>
<smd name="3" x="-2.688" y="-0.635" dx="1.525" dy="0.65" layer="1"/>
<smd name="4" x="-2.688" y="-1.905" dx="1.525" dy="0.65" layer="1"/>
<smd name="5" x="2.688" y="-1.905" dx="1.525" dy="0.65" layer="1"/>
<smd name="6" x="2.688" y="-0.635" dx="1.525" dy="0.65" layer="1"/>
<smd name="7" x="2.688" y="0.635" dx="1.525" dy="0.65" layer="1"/>
<smd name="8" x="2.688" y="1.905" dx="1.525" dy="0.65" layer="1"/>
<smd name="9" x="0" y="0" dx="3.35" dy="2.71" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.7" y1="2.725" x2="3.7" y2="2.725" width="0.05" layer="51"/>
<wire x1="3.7" y1="2.725" x2="3.7" y2="-2.725" width="0.05" layer="51"/>
<wire x1="3.7" y1="-2.725" x2="-3.7" y2="-2.725" width="0.05" layer="51"/>
<wire x1="-3.7" y1="-2.725" x2="-3.7" y2="2.725" width="0.05" layer="51"/>
<wire x1="-1.925" y1="2.438" x2="1.925" y2="2.438" width="0.1" layer="51"/>
<wire x1="1.925" y1="2.438" x2="1.925" y2="-2.438" width="0.1" layer="51"/>
<wire x1="1.925" y1="-2.438" x2="-1.925" y2="-2.438" width="0.1" layer="51"/>
<wire x1="-1.925" y1="-2.438" x2="-1.925" y2="2.438" width="0.1" layer="51"/>
<wire x1="-1.925" y1="1.168" x2="-0.655" y2="2.438" width="0.1" layer="51"/>
<wire x1="-3.45" y1="2.58" x2="-1.925" y2="2.58" width="0.2" layer="21"/>
</package>
<package name="SOT95P237X112-3N">
<description>&lt;b&gt;SOT95P237X112-3N&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.0922" y="0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="2" x="-1.0922" y="-0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="3" x="1.0922" y="0" dx="1.3208" dy="0.508" layer="1"/>
<text x="-3.4544" y="2.54" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-3.4544" y="-4.445" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="0.7112" y1="0.5842" x2="0.7112" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-0.2794" y1="-1.524" x2="0.7112" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.7112" y1="-1.524" x2="0.7112" y2="-0.5842" width="0.1524" layer="21"/>
<wire x1="0.7112" y1="1.524" x2="0.3048" y2="1.524" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.524" x2="-0.2794" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-0.7112" y1="0.3302" x2="-0.7112" y2="-0.3302" width="0.1524" layer="21"/>
</package>
<package name="SRF12808R2Y">
<description>&lt;b&gt;SRF1280-8R2Y&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.715" y="4.5" dx="5" dy="2.15" layer="1" rot="R90"/>
<smd name="2" x="1.715" y="4.5" dx="5" dy="2.15" layer="1" rot="R90"/>
<smd name="3" x="1.715" y="-4.5" dx="5" dy="2.15" layer="1" rot="R90"/>
<smd name="4" x="-1.715" y="-4.5" dx="5" dy="2.15" layer="1" rot="R90"/>
<text x="0" y="0.175" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="4.75" y="0.525" size="1.27" layer="27" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-6.25" y1="6.25" x2="6.25" y2="6.25" width="0.2" layer="51"/>
<wire x1="6.25" y1="6.25" x2="6.25" y2="-6.25" width="0.2" layer="51"/>
<wire x1="6.25" y1="-6.25" x2="-6.25" y2="-6.25" width="0.2" layer="51"/>
<wire x1="-6.25" y1="-6.25" x2="-6.25" y2="6.25" width="0.2" layer="51"/>
<wire x1="-3.43" y1="6.25" x2="-6.25" y2="6.25" width="0.2032" layer="21"/>
<wire x1="-6.25" y1="6.25" x2="-6.25" y2="-6.25" width="0.2032" layer="21"/>
<wire x1="-6.25" y1="-6.25" x2="-3.43" y2="-6.25" width="0.2032" layer="21"/>
<wire x1="3.43" y1="6.25" x2="6.25" y2="6.25" width="0.2032" layer="21"/>
<wire x1="6.25" y1="6.25" x2="6.25" y2="-6.25" width="0.2032" layer="21"/>
<wire x1="6.25" y1="-6.25" x2="3.43" y2="-6.25" width="0.2032" layer="21"/>
<circle x="-3.6" y="6.875" radius="0.22360625" width="0.1524" layer="21"/>
</package>
<package name="SOIC127P700X230-16N">
<description>&lt;b&gt;SO16_1&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-3.25" y="4.445" dx="1.65" dy="0.7" layer="1"/>
<smd name="2" x="-3.25" y="3.175" dx="1.65" dy="0.7" layer="1"/>
<smd name="3" x="-3.25" y="1.905" dx="1.65" dy="0.7" layer="1"/>
<smd name="4" x="-3.25" y="0.635" dx="1.65" dy="0.7" layer="1"/>
<smd name="5" x="-3.25" y="-0.635" dx="1.65" dy="0.7" layer="1"/>
<smd name="6" x="-3.25" y="-1.905" dx="1.65" dy="0.7" layer="1"/>
<smd name="7" x="-3.25" y="-3.175" dx="1.65" dy="0.7" layer="1"/>
<smd name="8" x="-3.25" y="-4.445" dx="1.65" dy="0.7" layer="1"/>
<smd name="9" x="3.25" y="-4.445" dx="1.65" dy="0.7" layer="1"/>
<smd name="10" x="3.25" y="-3.175" dx="1.65" dy="0.7" layer="1"/>
<smd name="11" x="3.25" y="-1.905" dx="1.65" dy="0.7" layer="1"/>
<smd name="12" x="3.25" y="-0.635" dx="1.65" dy="0.7" layer="1"/>
<smd name="13" x="3.25" y="0.635" dx="1.65" dy="0.7" layer="1"/>
<smd name="14" x="3.25" y="1.905" dx="1.65" dy="0.7" layer="1"/>
<smd name="15" x="3.25" y="3.175" dx="1.65" dy="0.7" layer="1"/>
<smd name="16" x="3.25" y="4.445" dx="1.65" dy="0.7" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.325" y1="5.55" x2="4.325" y2="5.55" width="0.05" layer="51"/>
<wire x1="4.325" y1="5.55" x2="4.325" y2="-5.55" width="0.05" layer="51"/>
<wire x1="4.325" y1="-5.55" x2="-4.325" y2="-5.55" width="0.05" layer="51"/>
<wire x1="-4.325" y1="-5.55" x2="-4.325" y2="5.55" width="0.05" layer="51"/>
<wire x1="-2.3" y1="5.15" x2="2.3" y2="5.15" width="0.1" layer="51"/>
<wire x1="2.3" y1="5.15" x2="2.3" y2="-5.15" width="0.1" layer="51"/>
<wire x1="2.3" y1="-5.15" x2="-2.3" y2="-5.15" width="0.1" layer="51"/>
<wire x1="-2.3" y1="-5.15" x2="-2.3" y2="5.15" width="0.1" layer="51"/>
<wire x1="-2.3" y1="3.88" x2="-1.03" y2="5.15" width="0.1" layer="51"/>
<wire x1="-2.075" y1="5.15" x2="2.075" y2="5.15" width="0.2" layer="21"/>
<wire x1="2.075" y1="5.15" x2="2.075" y2="-5.15" width="0.2" layer="21"/>
<wire x1="2.075" y1="-5.15" x2="-2.075" y2="-5.15" width="0.2" layer="21"/>
<wire x1="-2.075" y1="-5.15" x2="-2.075" y2="5.15" width="0.2" layer="21"/>
<wire x1="-4.075" y1="5.145" x2="-2.425" y2="5.145" width="0.2" layer="21"/>
</package>
<package name="SOIC127P600X175-14N">
<description>&lt;b&gt;14S1 - SOIC&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.711" y="3.81" dx="1.528" dy="0.65" layer="1"/>
<smd name="2" x="-2.711" y="2.54" dx="1.528" dy="0.65" layer="1"/>
<smd name="3" x="-2.711" y="1.27" dx="1.528" dy="0.65" layer="1"/>
<smd name="4" x="-2.711" y="0" dx="1.528" dy="0.65" layer="1"/>
<smd name="5" x="-2.711" y="-1.27" dx="1.528" dy="0.65" layer="1"/>
<smd name="6" x="-2.711" y="-2.54" dx="1.528" dy="0.65" layer="1"/>
<smd name="7" x="-2.711" y="-3.81" dx="1.528" dy="0.65" layer="1"/>
<smd name="8" x="2.711" y="-3.81" dx="1.528" dy="0.65" layer="1"/>
<smd name="9" x="2.711" y="-2.54" dx="1.528" dy="0.65" layer="1"/>
<smd name="10" x="2.711" y="-1.27" dx="1.528" dy="0.65" layer="1"/>
<smd name="11" x="2.711" y="0" dx="1.528" dy="0.65" layer="1"/>
<smd name="12" x="2.711" y="1.27" dx="1.528" dy="0.65" layer="1"/>
<smd name="13" x="2.711" y="2.54" dx="1.528" dy="0.65" layer="1"/>
<smd name="14" x="2.711" y="3.81" dx="1.528" dy="0.65" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.725" y1="4.62" x2="3.725" y2="4.62" width="0.05" layer="51"/>
<wire x1="3.725" y1="4.62" x2="3.725" y2="-4.62" width="0.05" layer="51"/>
<wire x1="3.725" y1="-4.62" x2="-3.725" y2="-4.62" width="0.05" layer="51"/>
<wire x1="-3.725" y1="-4.62" x2="-3.725" y2="4.62" width="0.05" layer="51"/>
<wire x1="-1.948" y1="4.322" x2="1.948" y2="4.322" width="0.1" layer="51"/>
<wire x1="1.948" y1="4.322" x2="1.948" y2="-4.322" width="0.1" layer="51"/>
<wire x1="1.948" y1="-4.322" x2="-1.948" y2="-4.322" width="0.1" layer="51"/>
<wire x1="-1.948" y1="-4.322" x2="-1.948" y2="4.322" width="0.1" layer="51"/>
<wire x1="-1.948" y1="3.052" x2="-0.678" y2="4.322" width="0.1" layer="51"/>
<wire x1="-1.598" y1="4.322" x2="1.598" y2="4.322" width="0.2" layer="21"/>
<wire x1="1.598" y1="4.322" x2="1.598" y2="-4.322" width="0.2" layer="21"/>
<wire x1="1.598" y1="-4.322" x2="-1.598" y2="-4.322" width="0.2" layer="21"/>
<wire x1="-1.598" y1="-4.322" x2="-1.598" y2="4.322" width="0.2" layer="21"/>
<wire x1="-3.475" y1="4.485" x2="-1.948" y2="4.485" width="0.2" layer="21"/>
</package>
<package name="ECS192833JGNTR">
<description>&lt;b&gt;ECX-32&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.15" y="-0.75" dx="1.3" dy="1.1" layer="1"/>
<smd name="2" x="1.15" y="-0.75" dx="1.3" dy="1.1" layer="1"/>
<smd name="3" x="1.15" y="1.05" dx="1.3" dy="1.1" layer="1"/>
<smd name="4" x="-1.15" y="1.05" dx="1.3" dy="1.1" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.6" y1="1.4" x2="1.6" y2="1.4" width="0.2" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-1.1" width="0.2" layer="51"/>
<wire x1="1.6" y1="-1.1" x2="-1.6" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-1.6" y1="-1.1" x2="-1.6" y2="1.4" width="0.2" layer="51"/>
<wire x1="-2.2" y1="1.8" x2="2.2" y2="1.8" width="0.1" layer="51"/>
<wire x1="2.2" y1="1.8" x2="2.2" y2="-1.8" width="0.1" layer="51"/>
<wire x1="2.2" y1="-1.8" x2="-2.2" y2="-1.8" width="0.1" layer="51"/>
<wire x1="-2.2" y1="-1.8" x2="-2.2" y2="1.8" width="0.1" layer="51"/>
<wire x1="-1.6" y1="0.35" x2="-1.6" y2="-0.05" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-1.1" x2="0.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="1.4" x2="0.3" y2="1.4" width="0.2032" layer="21"/>
<wire x1="1.6" y1="0.35" x2="1.6" y2="-0.05" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="-1.55" x2="-1.2" y2="-1.55" width="0.1" layer="21"/>
<wire x1="-1.2" y1="-1.55" x2="-1.1" y2="-1.55" width="0.127" layer="21" curve="-180"/>
<wire x1="-1.1" y1="-1.55" x2="-1.1" y2="-1.55" width="0.1" layer="21"/>
<wire x1="-1.1" y1="-1.55" x2="-1.2" y2="-1.55" width="0.127" layer="21" curve="-180"/>
</package>
<package name="NR8040T220M">
<description>&lt;b&gt;NR8040&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<text x="-1.019" y="0.137" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.019" y="0.137" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.2" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0.2" layer="51"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.2" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.2" layer="51"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.2" layer="21"/>
<wire x1="-4" y1="-4" x2="4" y2="-4" width="0.2" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.2" layer="21"/>
<wire x1="4" y1="4" x2="-4" y2="4" width="0.2" layer="21"/>
</package>
<package name="DIOM7959X250N">
<description>&lt;b&gt;AP02002&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-3.5" y="0" dx="3.1" dy="2.2" layer="1" rot="R90"/>
<smd name="2" x="3.5" y="0" dx="3.1" dy="2.2" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.85" y1="3.36" x2="4.85" y2="3.36" width="0.05" layer="51"/>
<wire x1="4.85" y1="3.36" x2="4.85" y2="-3.36" width="0.05" layer="51"/>
<wire x1="4.85" y1="-3.36" x2="-4.85" y2="-3.36" width="0.05" layer="51"/>
<wire x1="-4.85" y1="-3.36" x2="-4.85" y2="3.36" width="0.05" layer="51"/>
<wire x1="-3.97" y1="2.952" x2="3.97" y2="2.952" width="0.1" layer="51"/>
<wire x1="3.97" y1="2.952" x2="3.97" y2="-2.952" width="0.1" layer="51"/>
<wire x1="3.97" y1="-2.952" x2="-3.97" y2="-2.952" width="0.1" layer="51"/>
<wire x1="-3.97" y1="-2.952" x2="-3.97" y2="2.952" width="0.1" layer="51"/>
<wire x1="-3.97" y1="1.852" x2="-2.87" y2="2.952" width="0.1" layer="51"/>
<wire x1="3.97" y1="2.952" x2="-4.6" y2="2.952" width="0.2" layer="21"/>
<wire x1="-3.97" y1="-2.952" x2="3.97" y2="-2.952" width="0.2" layer="21"/>
<wire x1="-2" y1="1.9" x2="-2" y2="-1.9" width="0.3048" layer="21"/>
</package>
<package name="CAPC3225X270N">
<description>&lt;b&gt;1210&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.35" y="0" dx="2.72" dy="1.22" layer="1" rot="R90"/>
<smd name="2" x="1.35" y="0" dx="2.72" dy="1.22" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.11" y1="1.51" x2="2.11" y2="1.51" width="0.05" layer="51"/>
<wire x1="2.11" y1="1.51" x2="2.11" y2="-1.51" width="0.05" layer="51"/>
<wire x1="2.11" y1="-1.51" x2="-2.11" y2="-1.51" width="0.05" layer="51"/>
<wire x1="-2.11" y1="-1.51" x2="-2.11" y2="1.51" width="0.05" layer="51"/>
<wire x1="-1.6" y1="1.25" x2="1.6" y2="1.25" width="0.1" layer="51"/>
<wire x1="1.6" y1="1.25" x2="1.6" y2="-1.25" width="0.1" layer="51"/>
<wire x1="1.6" y1="-1.25" x2="-1.6" y2="-1.25" width="0.1" layer="51"/>
<wire x1="-1.6" y1="-1.25" x2="-1.6" y2="1.25" width="0.1" layer="51"/>
<circle x="0" y="0" radius="0.14141875" width="0.2" layer="21"/>
<circle x="0" y="0" radius="0.282840625" width="0.2" layer="21"/>
<circle x="0" y="0" radius="0.360553125" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CSD95379">
<pin name="SKIP#" x="-15.24" y="12.7" length="middle" direction="in"/>
<pin name="N/A" x="-15.24" y="7.62" length="middle" direction="nc"/>
<pin name="VDD" x="-15.24" y="2.54" length="middle" direction="pwr"/>
<pin name="PGND" x="-15.24" y="-2.54" length="middle" direction="pwr"/>
<pin name="VSW" x="-15.24" y="-7.62" length="middle" direction="out"/>
<pin name="VIN" x="15.24" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="BOOT_R" x="15.24" y="-2.54" length="middle" direction="sup" rot="R180"/>
<pin name="BOOT" x="15.24" y="2.54" length="middle" direction="sup" rot="R180"/>
<pin name="N/A2" x="15.24" y="7.62" length="middle" direction="nc" rot="R180"/>
<pin name="PWM" x="15.24" y="12.7" length="middle" direction="in" rot="R180"/>
<pin name="PGND_PAD" x="0" y="-22.86" length="middle" rot="R90"/>
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-10.16" y="19.05" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="16.51" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DMC3025LSD">
<wire x1="-2.3876" y1="-5.207" x2="-2.3876" y2="-10.16" width="0.254" layer="94"/>
<wire x1="1.27" y1="-5.715" x2="-0.7366" y2="-5.715" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="-9.525" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-9.525" x2="1.27" y2="-9.525" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="-5.715" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-5.715" x2="3.81" y2="-5.715" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-9.525" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-9.525" x2="1.27" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-3.81" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-7.62" x2="0.508" y2="-8.128" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-8.128" x2="0.508" y2="-7.112" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-7.112" x2="-0.762" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="0.381" y1="-7.62" x2="1.27" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="0.381" y1="-7.366" x2="-0.508" y2="-7.62" width="0.3048" layer="94"/>
<wire x1="-0.508" y1="-7.62" x2="0.381" y2="-7.874" width="0.3048" layer="94"/>
<wire x1="0.381" y1="-7.874" x2="0.381" y2="-7.62" width="0.3048" layer="94"/>
<wire x1="0.381" y1="-7.62" x2="0.127" y2="-7.62" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-5.715" x2="3.81" y2="-6.858" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-6.858" x2="3.81" y2="-9.525" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-6.858" x2="3.175" y2="-8.255" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-8.255" x2="4.445" y2="-8.255" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-8.255" x2="3.81" y2="-6.858" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-6.858" x2="3.81" y2="-6.858" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-6.858" x2="4.445" y2="-6.858" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-6.858" x2="4.699" y2="-6.604" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-6.858" x2="2.921" y2="-7.112" width="0.1524" layer="94"/>
<circle x="1.27" y="-9.525" radius="0.127" width="0.4064" layer="94"/>
<circle x="1.27" y="-5.715" radius="0.127" width="0.4064" layer="94"/>
<text x="7.112" y="-3.556" size="1.778" layer="94">D2</text>
<text x="6.858" y="-12.7" size="1.778" layer="94">S2</text>
<text x="-9.652" y="-10.668" size="1.778" layer="94">G2</text>
<rectangle x1="-1.524" y1="-10.16" x2="-0.762" y2="-8.89" layer="94"/>
<rectangle x1="-1.524" y1="-6.35" x2="-0.762" y2="-5.08" layer="94"/>
<rectangle x1="-1.524" y1="-8.509" x2="-0.762" y2="-6.731" layer="94"/>
<wire x1="-2.3876" y1="3.937" x2="-2.3876" y2="8.89" width="0.254" layer="94"/>
<wire x1="1.27" y1="4.445" x2="-0.7366" y2="4.445" width="0.1524" layer="94"/>
<wire x1="1.27" y1="6.35" x2="1.27" y2="8.255" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="8.255" x2="1.27" y2="8.255" width="0.1524" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="4.445" width="0.1524" layer="94"/>
<wire x1="1.27" y1="4.445" x2="3.81" y2="4.445" width="0.1524" layer="94"/>
<wire x1="3.81" y1="8.255" x2="1.27" y2="8.255" width="0.1524" layer="94"/>
<wire x1="1.27" y1="8.255" x2="1.27" y2="8.89" width="0.1524" layer="94"/>
<wire x1="3.81" y1="8.255" x2="3.81" y2="7.112" width="0.1524" layer="94"/>
<wire x1="3.81" y1="7.112" x2="3.81" y2="4.445" width="0.1524" layer="94"/>
<wire x1="3.81" y1="7.112" x2="4.445" y2="5.715" width="0.1524" layer="94"/>
<wire x1="4.445" y1="5.715" x2="3.175" y2="5.715" width="0.1524" layer="94"/>
<wire x1="3.175" y1="5.715" x2="3.81" y2="7.112" width="0.1524" layer="94"/>
<wire x1="4.445" y1="7.112" x2="3.81" y2="7.112" width="0.1524" layer="94"/>
<wire x1="3.81" y1="7.112" x2="3.175" y2="7.112" width="0.1524" layer="94"/>
<wire x1="3.175" y1="7.112" x2="2.921" y2="7.366" width="0.1524" layer="94"/>
<wire x1="4.445" y1="7.112" x2="4.699" y2="6.858" width="0.1524" layer="94"/>
<wire x1="1.27" y1="6.35" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="0" y1="5.842" x2="0" y2="6.858" width="0.1524" layer="94"/>
<wire x1="0" y1="6.858" x2="1.27" y2="6.35" width="0.1524" layer="94"/>
<wire x1="0.127" y1="6.35" x2="-0.762" y2="6.35" width="0.1524" layer="94"/>
<wire x1="0.127" y1="6.604" x2="1.016" y2="6.35" width="0.3048" layer="94"/>
<wire x1="1.016" y1="6.35" x2="0.127" y2="6.096" width="0.3048" layer="94"/>
<wire x1="0.127" y1="6.096" x2="0.127" y2="6.35" width="0.3048" layer="94"/>
<wire x1="0.127" y1="6.35" x2="0.381" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-3.81" y2="6.35" width="0.1524" layer="94"/>
<circle x="1.27" y="8.255" radius="0.127" width="0.4064" layer="94"/>
<circle x="1.27" y="4.445" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="16.764" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="13.716" size="1.778" layer="96">&gt;VALUE</text>
<text x="7.112" y="2.54" size="1.778" layer="94" rot="MR180">D1</text>
<text x="6.858" y="11.176" size="1.778" layer="94" rot="MR180">S1</text>
<text x="-9.398" y="9.398" size="1.778" layer="94" rot="MR180">G1</text>
<rectangle x1="-1.524" y1="7.62" x2="-0.762" y2="8.89" layer="94"/>
<rectangle x1="-1.524" y1="3.81" x2="-0.762" y2="5.08" layer="94"/>
<rectangle x1="-1.524" y1="5.461" x2="-0.762" y2="7.239" layer="94"/>
<text x="-3.556" y="11.684" size="1.9304" layer="94" rot="MR180">P-FET</text>
<text x="-3.556" y="-4.572" size="1.9304" layer="94">N-FET</text>
<wire x1="-6.35" y1="12.7" x2="6.35" y2="12.7" width="0.254" layer="94"/>
<wire x1="6.35" y1="-12.7" x2="-6.35" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-12.7" x2="-6.35" y2="12.7" width="0.254" layer="94"/>
<wire x1="6.35" y1="12.7" x2="6.35" y2="-12.7" width="0.254" layer="94"/>
<pin name="D1" x="8.89" y="3.81" visible="off" rot="R180"/>
<pin name="G1" x="-8.89" y="6.35" visible="off" length="middle"/>
<pin name="S1" x="8.89" y="8.89" visible="off" rot="R180"/>
<pin name="S2" x="8.89" y="-10.16" visible="off" rot="R180"/>
<pin name="D2" x="8.89" y="-5.08" visible="off" rot="R180"/>
<pin name="G2" x="-8.89" y="-7.62" visible="off" length="middle"/>
</symbol>
<symbol name="POWERJACK">
<wire x1="-10.16" y1="2.54" x2="-7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.1524" layer="94"/>
<text x="-10.16" y="10.16" size="1.778" layer="96">&gt;Value</text>
<text x="-10.16" y="0" size="1.778" layer="95">&gt;Name</text>
<rectangle x1="-10.16" y1="6.858" x2="0" y2="8.382" layer="94"/>
<pin name="GNDBREAK" x="2.54" y="5.08" visible="off" length="short" rot="R180"/>
<pin name="GND" x="2.54" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="PWR" x="2.54" y="7.62" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="PAM2421AECADJR">
<wire x1="5.08" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-15.24" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-15.24" x2="5.08" y2="-15.24" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-15.24" width="0.254" layer="94"/>
<text x="26.67" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="26.67" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="PGND" x="0" y="0" length="middle"/>
<pin name="VIN" x="0" y="-2.54" length="middle"/>
<pin name="EN" x="0" y="-5.08" length="middle"/>
<pin name="COMP" x="0" y="-7.62" length="middle"/>
<pin name="EP" x="15.24" y="-20.32" length="middle" rot="R90"/>
<pin name="SW" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="SS" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="FB" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="AGND" x="30.48" y="-7.62" length="middle" rot="R180"/>
</symbol>
<symbol name="TL431AIDBZTG4">
<wire x1="-6.35" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-6.35" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-7.62" width="0.254" layer="94"/>
<text x="2.54" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="2.54" y="-10.16" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="CATHODE" x="0" y="7.62" visible="off" length="short" rot="R270"/>
<pin name="REF" x="10.16" y="-1.27" visible="off" length="short" rot="R180"/>
<pin name="ANODE" x="0" y="-10.16" visible="off" length="short" rot="R90"/>
<text x="5.588" y="-2.032" size="1.778" layer="94">R</text>
<text x="1.016" y="4.572" size="1.778" layer="94" rot="R180">C</text>
<text x="-0.508" y="-6.858" size="1.778" layer="94">A</text>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="0" width="0.254" layer="94"/>
<polygon width="0.254" layer="94">
<vertex x="0" y="1.27"/>
<vertex x="-2.54" y="-3.81"/>
<vertex x="2.54" y="-3.81"/>
</polygon>
</symbol>
<symbol name="SRF1280-100M">
<wire x1="2.54" y1="5.08" x2="17.78" y2="5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="-7.62" x2="17.78" y2="5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="-7.62" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<text x="16.51" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="16.51" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pin" length="middle"/>
<pin name="2" x="-2.54" y="-5.08" visible="pin" length="middle"/>
<pin name="3" x="22.86" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="4" x="22.86" y="-5.08" visible="pin" length="middle" rot="R180"/>
<wire x1="3.81" y1="0" x2="6.35" y2="0" width="0.254" layer="94"/>
<wire x1="16.51" y1="0" x2="13.97" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="6.35" y2="-2.54" width="0.254" layer="94"/>
<wire x1="16.51" y1="-2.54" x2="13.97" y2="-2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="0" x2="8.89" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="8.89" y1="0" x2="11.43" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="11.43" y1="0" x2="13.97" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="6.35" y1="-2.54" x2="8.89" y2="-2.54" width="0.254" layer="94" curve="180"/>
<wire x1="8.89" y1="-2.54" x2="11.43" y2="-2.54" width="0.254" layer="94" curve="180"/>
<wire x1="11.43" y1="-2.54" x2="13.97" y2="-2.54" width="0.254" layer="94" curve="180"/>
<wire x1="3.81" y1="-1.27" x2="16.51" y2="-1.27" width="0.254" layer="94" style="shortdash"/>
</symbol>
<symbol name="TLP291-4(GB-TP,E)">
<wire x1="5.08" y1="2.54" x2="38.1" y2="2.54" width="0.254" layer="94"/>
<wire x1="38.1" y1="-20.32" x2="38.1" y2="2.54" width="0.254" layer="94"/>
<wire x1="38.1" y1="-20.32" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<text x="39.37" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="39.37" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="ANODE_1" x="0" y="0" length="middle"/>
<pin name="CATHODE_1" x="0" y="-2.54" length="middle"/>
<pin name="ANODE_2" x="0" y="-5.08" length="middle"/>
<pin name="CATHODE_2" x="0" y="-7.62" length="middle"/>
<pin name="ANODE_3" x="0" y="-10.16" length="middle"/>
<pin name="CATHODE_3" x="0" y="-12.7" length="middle"/>
<pin name="ANODE_4" x="0" y="-15.24" length="middle"/>
<pin name="CATHODE_4" x="0" y="-17.78" length="middle"/>
<pin name="COLLECTOR_1" x="43.18" y="0" length="middle" rot="R180"/>
<pin name="EMITTER_1" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="COLLECTOR_2" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="EMITTER_2" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="COLLECTOR_3" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="EMITTER_3" x="43.18" y="-12.7" length="middle" rot="R180"/>
<pin name="COLLECTOR_4" x="43.18" y="-15.24" length="middle" rot="R180"/>
<pin name="EMITTER_4" x="43.18" y="-17.78" length="middle" rot="R180"/>
</symbol>
<symbol name="ATTINY24A-SSU">
<wire x1="5.08" y1="2.54" x2="54.61" y2="2.54" width="0.254" layer="94"/>
<wire x1="54.61" y1="-35.56" x2="54.61" y2="2.54" width="0.254" layer="94"/>
<wire x1="54.61" y1="-35.56" x2="5.08" y2="-35.56" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-35.56" width="0.254" layer="94"/>
<text x="5.08" y="5.08" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="5.08" y="-38.1" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="VCC" x="0" y="0" length="middle" direction="pwr"/>
<pin name="[PCINT8/XTAL1/CLKI]_PB0" x="0" y="-2.54" length="middle"/>
<pin name="[PCINT9/XTAL2]_PB1" x="0" y="-5.08" length="middle"/>
<pin name="[PCINT11/!RESET!/DW]_PB3" x="0" y="-7.62" length="middle" direction="in"/>
<pin name="[PCINT10/INT0/OC0A/CKOUT]_PB2" x="0" y="-10.16" length="middle"/>
<pin name="[PCINT7/ICP/OC0B/ADC7]_PA7" x="0" y="-12.7" length="middle"/>
<pin name="[PCINT6/OC1A/SDA/MOSI/DI/ADC6]_PA6" x="0" y="-15.24" length="middle"/>
<pin name="GND" x="0" y="-33.02" length="middle" direction="pwr"/>
<pin name="PA0_[ADC0/AREF/PCINT0]" x="0" y="-30.48" length="middle"/>
<pin name="PA1_[ADC1/AIN0/PCINT1]" x="0" y="-27.94" length="middle"/>
<pin name="PA2_[ADC2/AIN1/PCINT2]" x="0" y="-25.4" length="middle"/>
<pin name="PA3_[ADC3/T0/PCINT3]" x="0" y="-22.86" length="middle"/>
<pin name="PA4_[ADC4/USCK/SCL/T1/PCINT4]" x="0" y="-20.32" length="middle"/>
<pin name="PA5_[ADC5/DO/MISO/OC1B/PCINT5]" x="0" y="-17.78" length="middle"/>
</symbol>
<symbol name="ECS-192-8-33-JGN-TR">
<wire x1="3.81" y1="3.81" x2="29.21" y2="3.81" width="0.254" layer="94"/>
<wire x1="29.21" y1="-6.35" x2="29.21" y2="3.81" width="0.254" layer="94"/>
<wire x1="29.21" y1="-6.35" x2="3.81" y2="-6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="-6.35" width="0.254" layer="94"/>
<text x="3.81" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="3.81" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="IN/OUT" x="1.27" y="2.54" length="short"/>
<pin name="GND_1" x="1.27" y="-5.08" length="short"/>
<pin name="OUT/IN" x="31.75" y="2.54" length="short" rot="R180"/>
<pin name="GND_2" x="31.75" y="-5.08" length="short" rot="R180"/>
<wire x1="15.24" y1="0" x2="15.24" y2="-5.08" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="17.78" y2="-5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="-5.08" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="2.54" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="2.54" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="13.97" y1="1.27" x2="13.97" y2="-1.27" width="0.254" layer="94"/>
<wire x1="13.97" y1="-1.27" x2="13.97" y2="-3.81" width="0.254" layer="94"/>
<wire x1="19.05" y1="1.27" x2="19.05" y2="-1.27" width="0.254" layer="94"/>
<wire x1="19.05" y1="-1.27" x2="19.05" y2="-3.81" width="0.254" layer="94"/>
<wire x1="13.97" y1="-1.27" x2="12.7" y2="-1.27" width="0.254" layer="94"/>
<wire x1="19.05" y1="-1.27" x2="20.32" y2="-1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="NR8040T220M">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-175.4"/>
<wire x1="1.27" y1="0" x2="3.81" y2="0" width="0.254" layer="94" curve="-175.4"/>
<wire x1="3.81" y1="0" x2="6.35" y2="0" width="0.254" layer="94" curve="-175.4"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-1.27" y="0" visible="off" length="point"/>
<pin name="2" x="6.35" y="0" visible="off" length="point" rot="R180"/>
</symbol>
<symbol name="B340-13-F">
<wire x1="0" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.524" x2="1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.016" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.524" x2="-1.016" y2="-2.54" width="0.254" layer="94"/>
<text x="2.032" y="2.032" size="1.778" layer="95">&gt;NAME</text>
<text x="1.778" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="K" x="-2.54" y="0" visible="off" length="short"/>
<pin name="A" x="5.08" y="0" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="CL32B106KBJNNNE">
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.508" layer="94"/>
<wire x1="3.302" y1="1.27" x2="3.302" y2="-1.27" width="0.508" layer="94"/>
<wire x1="1.27" y1="0" x2="1.778" y2="0" width="0.254" layer="94"/>
<wire x1="3.302" y1="0" x2="3.81" y2="0" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-1.27" y="0" visible="off" length="short"/>
<pin name="2" x="6.35" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CSD95379Q3M-HOMETCH" prefix="U">
<gates>
<gate name="G$1" symbol="CSD95379" x="2.54" y="-7.62"/>
</gates>
<devices>
<device name="" package="10-VSON-HOMETCH">
<connects>
<connect gate="G$1" pin="BOOT" pad="8"/>
<connect gate="G$1" pin="BOOT_R" pad="7"/>
<connect gate="G$1" pin="N/A" pad="2"/>
<connect gate="G$1" pin="N/A2" pad="9"/>
<connect gate="G$1" pin="PGND" pad="4"/>
<connect gate="G$1" pin="PGND_PAD" pad="11"/>
<connect gate="G$1" pin="PWM" pad="10"/>
<connect gate="G$1" pin="SKIP#" pad="1"/>
<connect gate="G$1" pin="VDD" pad="3"/>
<connect gate="G$1" pin="VIN" pad="6 6_1 6_2 6_3 6_4 6_5 6_6"/>
<connect gate="G$1" pin="VSW" pad="5 5_1 5_2 5_3 P$4 P$5 P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DMC3025LSD" prefix="Q">
<gates>
<gate name="G$1" symbol="DMC3025LSD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO-8">
<connects>
<connect gate="G$1" pin="D1" pad="5 6"/>
<connect gate="G$1" pin="D2" pad="7 8"/>
<connect gate="G$1" pin="G1" pad="4"/>
<connect gate="G$1" pin="G2" pad="2"/>
<connect gate="G$1" pin="S1" pad="3"/>
<connect gate="G$1" pin="S2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="POWER_JACK" prefix="J">
<description>&lt;b&gt;Power Jack&lt;/b&gt;
This is the standard 5.5mm barrel jack for power.&lt;br&gt;
The PTH is the most common, proven, reliable, footprint.&lt;br&gt;
The Slot footprint only works if the mill layer is transmitted to the PCB fab house so be warned.&lt;br&gt;

Mating wall wart : TOL-00298 (and others)</description>
<gates>
<gate name="G$1" symbol="POWERJACK" x="7.62" y="-2.54"/>
</gates>
<devices>
<device name="SMD" package="POWER_JACK_SMD">
<connects>
<connect gate="G$1" pin="GND" pad="P$4"/>
<connect gate="G$1" pin="GNDBREAK" pad="GND"/>
<connect gate="G$1" pin="PWR" pad="VIN0"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08106"/>
<attribute name="SF_ID" value="PRT-12748" constant="no"/>
<attribute name="VALUE" value="5.5x2.1mm Barrel" constant="no"/>
</technology>
</technologies>
</device>
<device name="SLT" package="POWER_JACK_SLOT">
<connects>
<connect gate="G$1" pin="GND" pad="GND@2"/>
<connect gate="G$1" pin="GNDBREAK" pad="GND@1"/>
<connect gate="G$1" pin="PWR" pad="PWR"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="PRT-00119" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH" package="POWER_JACK_PTH">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GNDBREAK" pad="GNDBREAK"/>
<connect gate="G$1" pin="PWR" pad="PWR"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08197"/>
</technology>
</technologies>
</device>
<device name="COMBO" package="POWER_JACK_COMBO">
<connects>
<connect gate="G$1" pin="GND" pad="GND@1"/>
<connect gate="G$1" pin="GNDBREAK" pad="GND"/>
<connect gate="G$1" pin="PWR" pad="POWER"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_LOCK" package="POWER_JACK_PTH_LOCK">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GNDBREAK" pad="GNDBREAK"/>
<connect gate="G$1" pin="PWR" pad="PWR"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08197" constant="no"/>
<attribute name="SF_ID" value="PRT-00119" constant="no"/>
</technology>
</technologies>
</device>
<device name="1" package="POWER_JACK_SMD_OVERPASTE_REDBOARD_0603">
<connects>
<connect gate="G$1" pin="GND" pad="P$4"/>
<connect gate="G$1" pin="GNDBREAK" pad="GND"/>
<connect gate="G$1" pin="PWR" pad="VIN0"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOE" package="POWER_JACK_SMD_OVERPASTE_TOE">
<connects>
<connect gate="G$1" pin="GND" pad="P$4"/>
<connect gate="G$1" pin="GNDBREAK" pad="GND"/>
<connect gate="G$1" pin="PWR" pad="VIN0"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08106" constant="no"/>
<attribute name="SF_ID" value="PRT-12748" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH_BREAD" package="POWER_JACK_PTH_BREAD">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GNDBREAK" pad="GNDBREAK"/>
<connect gate="G$1" pin="PWR" pad="PWR"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="PRT-10811" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="PJ-047AH">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="GNDBREAK" pad="3"/>
<connect gate="G$1" pin="PWR" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PAM2421AECADJR" prefix="IC">
<description>&lt;b&gt;DiodesZetex PAM2421AECADJR, Step Down DC-DC Converter, 3A, Adjustable, 8-Pin SOP&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/PAM2421AECADJR.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="PAM2421AECADJR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X150-9N">
<connects>
<connect gate="G$1" pin="AGND" pad="5"/>
<connect gate="G$1" pin="COMP" pad="4"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="EP" pad="9"/>
<connect gate="G$1" pin="FB" pad="6"/>
<connect gate="G$1" pin="PGND" pad="1"/>
<connect gate="G$1" pin="SS" pad="7"/>
<connect gate="G$1" pin="SW" pad="8"/>
<connect gate="G$1" pin="VIN" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="PAM2421AECADJR" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/pam2421aecadjr/diodes-incorporated" constant="no"/>
<attribute name="DESCRIPTION" value="DiodesZetex PAM2421AECADJR, Step Down DC-DC Converter, 3A, Adjustable, 8-Pin SOP" constant="no"/>
<attribute name="HEIGHT" value="1.5mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Diodes Inc." constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="PAM2421AECADJR" constant="no"/>
<attribute name="RS_PART_NUMBER" value="1697169" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/1697169" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TL431AIDBZTG4" prefix="IC">
<description>&lt;b&gt;PRECISION PROGRAMMABLE REFERENCE&lt;/b&gt;&lt;p&gt;
Source: &lt;a href=""&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TL431AIDBZTG4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P237X112-3N">
<connects>
<connect gate="G$1" pin="ANODE" pad="3"/>
<connect gate="G$1" pin="CATHODE" pad="1"/>
<connect gate="G$1" pin="REF" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="TL431AIDBZTG4" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/tl431aidbztg4/texas-instruments" constant="no"/>
<attribute name="DESCRIPTION" value="PRECISION PROGRAMMABLE REFERENCE" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="TL431AIDBZTG4" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SRF1280-100M" prefix="L">
<description>&lt;b&gt;Bourns SRF1280 Series Type 1280 Shielded Wire-wound SMD Inductor with a Ferrite Core, 10 (Parallel) H, 40 (Series) H&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.bourns.com/docs/Product-Datasheets/SRF1280.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="SRF1280-100M" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SRF12808R2Y">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="SRF1280-100M" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/srf1280-100m/bourns" constant="no"/>
<attribute name="DESCRIPTION" value="Bourns SRF1280 Series Type 1280 Shielded Wire-wound SMD Inductor with a Ferrite Core, 10 (Parallel) H, 40 (Series) H" constant="no"/>
<attribute name="HEIGHT" value="8mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Bourns" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="SRF1280-100M" constant="no"/>
<attribute name="RS_PART_NUMBER" value="7633464P" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/7633464P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TLP291-4(GB-TP,E)" prefix="IC">
<description>&lt;b&gt;Transistor Output Optocouplers PHOTOCOUP QUAD TRANS 16-SOP COMP TLP281-4&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/TLP291-4(GB-TP,E).pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TLP291-4(GB-TP,E)" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P700X230-16N">
<connects>
<connect gate="G$1" pin="ANODE_1" pad="1"/>
<connect gate="G$1" pin="ANODE_2" pad="3"/>
<connect gate="G$1" pin="ANODE_3" pad="5"/>
<connect gate="G$1" pin="ANODE_4" pad="7"/>
<connect gate="G$1" pin="CATHODE_1" pad="2"/>
<connect gate="G$1" pin="CATHODE_2" pad="4"/>
<connect gate="G$1" pin="CATHODE_3" pad="6"/>
<connect gate="G$1" pin="CATHODE_4" pad="8"/>
<connect gate="G$1" pin="COLLECTOR_1" pad="16"/>
<connect gate="G$1" pin="COLLECTOR_2" pad="14"/>
<connect gate="G$1" pin="COLLECTOR_3" pad="12"/>
<connect gate="G$1" pin="COLLECTOR_4" pad="10"/>
<connect gate="G$1" pin="EMITTER_1" pad="15"/>
<connect gate="G$1" pin="EMITTER_2" pad="13"/>
<connect gate="G$1" pin="EMITTER_3" pad="11"/>
<connect gate="G$1" pin="EMITTER_4" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="TLP291-4(GB-TP,E)" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/tlp291-4-gb-tpe/toshiba" constant="no"/>
<attribute name="DESCRIPTION" value="Transistor Output Optocouplers PHOTOCOUP QUAD TRANS 16-SOP COMP TLP281-4" constant="no"/>
<attribute name="HEIGHT" value="2.3mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Toshiba" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="TLP291-4(GB-TP,E)" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATTINY24A-SSU" prefix="IC">
<description>&lt;b&gt;ATTINY24A-SSU, 8 bit AVR Microcontroller 20MHz 2, 128kB, B Flash, 128B RAM, I2C SPI 14-Pin SOIC&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://docs-europe.electrocomponents.com/webdocs/0db4/0900766b80db4e67.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ATTINY24A-SSU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-14N">
<connects>
<connect gate="G$1" pin="GND" pad="14"/>
<connect gate="G$1" pin="PA0_[ADC0/AREF/PCINT0]" pad="13"/>
<connect gate="G$1" pin="PA1_[ADC1/AIN0/PCINT1]" pad="12"/>
<connect gate="G$1" pin="PA2_[ADC2/AIN1/PCINT2]" pad="11"/>
<connect gate="G$1" pin="PA3_[ADC3/T0/PCINT3]" pad="10"/>
<connect gate="G$1" pin="PA4_[ADC4/USCK/SCL/T1/PCINT4]" pad="9"/>
<connect gate="G$1" pin="PA5_[ADC5/DO/MISO/OC1B/PCINT5]" pad="8"/>
<connect gate="G$1" pin="VCC" pad="1"/>
<connect gate="G$1" pin="[PCINT10/INT0/OC0A/CKOUT]_PB2" pad="5"/>
<connect gate="G$1" pin="[PCINT11/!RESET!/DW]_PB3" pad="4"/>
<connect gate="G$1" pin="[PCINT6/OC1A/SDA/MOSI/DI/ADC6]_PA6" pad="7"/>
<connect gate="G$1" pin="[PCINT7/ICP/OC0B/ADC7]_PA7" pad="6"/>
<connect gate="G$1" pin="[PCINT8/XTAL1/CLKI]_PB0" pad="2"/>
<connect gate="G$1" pin="[PCINT9/XTAL2]_PB1" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="ATTINY24A-SSU" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/attiny24a-ssu/microchip-technology" constant="no"/>
<attribute name="DESCRIPTION" value="ATTINY24A-SSU, 8 bit AVR Microcontroller 20MHz 2, 128kB, B Flash, 128B RAM, I2C SPI 14-Pin SOIC" constant="no"/>
<attribute name="HEIGHT" value="1.75mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Microchip" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ATTINY24A-SSU" constant="no"/>
<attribute name="RS_PART_NUMBER" value="9214560" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/9214560" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ECS-192-8-33-JGN-TR" prefix="Y">
<description>&lt;b&gt;CRYSTAL 19.2MHZ 8PF SMD&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.ecsxtal.com/store/pdf/ecx-32.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ECS-192-8-33-JGN-TR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ECS192833JGNTR">
<connects>
<connect gate="G$1" pin="GND_1" pad="2"/>
<connect gate="G$1" pin="GND_2" pad="4"/>
<connect gate="G$1" pin="IN/OUT" pad="1"/>
<connect gate="G$1" pin="OUT/IN" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="ECS-192-8-33-JGN-TR" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="CRYSTAL 19.2MHZ 8PF SMD" constant="no"/>
<attribute name="HEIGHT" value="0mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="ECS Inc." constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ECS-192-8-33-JGN-TR" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NR8040T220M" prefix="L">
<description>&lt;b&gt;TAIYO YUDEN - NR8040T220M - INDUCTOR, SHIELDED, 22UH, 2.2A, SMD, FULL REEL&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/2/NR8040T220M.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="NR8040T220M" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NR8040T220M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="NR8040T220M" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="http://www.arrow.com/en/products/nr8040t220m/taiyo-yuden" constant="no"/>
<attribute name="DESCRIPTION" value="TAIYO YUDEN - NR8040T220M - INDUCTOR, SHIELDED, 22UH, 2.2A, SMD, FULL REEL" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TAIYO YUDEN" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="NR8040T220M" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="B340-13-F" prefix="D">
<description>&lt;b&gt;DIODES INC. - B340-13-F - DIODE, SCHOTTKY, 40V, 3A, SMC&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/2/B340-13-F.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="B340-13-F" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIOM7959X250N">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="71028083" constant="no"/>
<attribute name="ALLIED_PRICE/STOCK" value="https://www.alliedelec.com/diodes-inc-b340-13-f/71028083/" constant="no"/>
<attribute name="ARROW_PART_NUMBER" value="B340-13-F" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/b340-13-f/diodes-incorporated" constant="no"/>
<attribute name="DESCRIPTION" value="DIODES INC. - B340-13-F - DIODE, SCHOTTKY, 40V, 3A, SMC" constant="no"/>
<attribute name="HEIGHT" value="2.5mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Diodes Inc." constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="B340-13-F" constant="no"/>
<attribute name="RS_PART_NUMBER" value="9228109" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/9228109" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CL32B106KBJNNNE" prefix="C">
<description>&lt;b&gt;Samsung Electro-Mechanics 1210 CL 10F Ceramic Multilayer Capacitor, 50 V, +125C, X7R Dielectric, 10% SMD&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://docs-europe.electrocomponents.com/webdocs/13d6/0900766b813d6930.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="CL32B106KBJNNNE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC3225X270N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="CL32B106KBJNNNE" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/cl32b106kbjnnne/samsung-electro-mechanics" constant="no"/>
<attribute name="DESCRIPTION" value="Samsung Electro-Mechanics 1210 CL 10F Ceramic Multilayer Capacitor, 50 V, +125C, X7R Dielectric, 10% SMD" constant="no"/>
<attribute name="HEIGHT" value="2.7mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Samsung Electro-Mechanics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CL32B106KBJNNNE" constant="no"/>
<attribute name="RS_PART_NUMBER" value="7661214P" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/7661214P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dp_devices">
<description>Dangerous Prototypes Standard PCB sizes
http://dangerousprototypes.com</description>
<packages>
<package name="C805">
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.127" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.127" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="12">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.016" layer="27" ratio="12">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
</package>
<package name="C402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="C603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8302" y2="0.4801" layer="51" rot="R180"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<circle x="0" y="0" radius="0.1" width="0.127" layer="21"/>
<wire x1="-0.8" y1="0.75" x2="0.8" y2="0.75" width="0.2" layer="21"/>
<wire x1="-0.8" y1="-0.75" x2="0.8" y2="-0.75" width="0.2" layer="21"/>
</package>
<package name="C1206">
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-2.305" y="1.47" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.405" y="-2.64" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<circle x="0" y="0" radius="0.15" width="0.3048" layer="21"/>
<wire x1="-1.7" y1="1.1" x2="1.7" y2="1.1" width="0.1016" layer="21"/>
<wire x1="-1.7" y1="-1.1" x2="1.7" y2="-1.1" width="0.1016" layer="21"/>
</package>
<package name="C0.1PTH">
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.8575" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="R805">
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
</package>
<package name="R603">
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<wire x1="-0.8" y1="0.75" x2="0.8" y2="0.75" width="0.2" layer="21"/>
<wire x1="0.8" y1="-0.75" x2="-0.8" y2="-0.75" width="0.2" layer="21"/>
</package>
<package name="R402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="R1206">
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="RTH025W">
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.8575" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="TO-220-2">
<wire x1="4.826" y1="-1.778" x2="5.08" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-1.778" x2="-4.826" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.524" x2="-4.826" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.397" x2="5.08" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.524" x2="-5.08" y2="1.397" width="0.1524" layer="21"/>
<circle x="-4.6228" y="-1.1684" radius="0.254" width="0" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-3.3782" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="-1.27" y="-1.27" size="1.27" layer="51" font="vector" ratio="10">1</text>
<text x="3.81" y="-1.27" size="1.27" layer="51" font="vector" ratio="10">2</text>
<rectangle x1="-5.334" y1="1.27" x2="-3.429" y2="2.54" layer="21"/>
<rectangle x1="-3.429" y1="1.778" x2="-1.651" y2="2.54" layer="21"/>
<rectangle x1="-1.651" y1="1.27" x2="-0.889" y2="2.54" layer="21"/>
<rectangle x1="-0.889" y1="1.778" x2="0.889" y2="2.54" layer="21"/>
<rectangle x1="0.889" y1="1.27" x2="1.651" y2="2.54" layer="21"/>
<rectangle x1="1.651" y1="1.778" x2="3.429" y2="2.54" layer="21"/>
<rectangle x1="3.429" y1="1.27" x2="5.334" y2="2.54" layer="21"/>
<rectangle x1="-3.429" y1="1.27" x2="-1.651" y2="1.778" layer="51"/>
<rectangle x1="-0.889" y1="1.27" x2="0.889" y2="1.778" layer="21"/>
<rectangle x1="1.651" y1="1.27" x2="3.429" y2="1.778" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR_NPOL" prefix="C" uservalue="yes">
<description>Non-Polarized capacitor in various packages</description>
<gates>
<gate name="C" symbol="CAP" x="0" y="-2.54"/>
</gates>
<devices>
<device name="-0805" package="C805">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402" package="C402">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="C603">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206" package="C1206">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PTH_0.1&quot;" package="C0.1PTH">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0805" package="R805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="R805"/>
</technologies>
</device>
<device name="-0603" package="R603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402" package="R402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PTH-0.4" package="RTH025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TO-220-2" package="TO-220-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="GND-ISO">
<description>Isolated ground</description>
<pin name="GND-ISO" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-2.032" y1="0" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.524" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="1.27" y1="2.032" x2="1.27" y2="0.508" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND-ISO" prefix="GND-ISO">
<description>Isolated ground</description>
<gates>
<gate name="G$1" symbol="GND-ISO" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microbuilder">
<description>&lt;h2&gt;&lt;b&gt;microBuilder.eu&lt;/b&gt; Eagle Footprint Library&lt;/h2&gt;

&lt;p&gt;Footprints for common components used in our projects and products.  This is the same library that we use internally, and it is regularly updated.  The newest version can always be found at &lt;b&gt;www.microBuilder.eu&lt;/b&gt;.  If you find this library useful, please feel free to purchase something from our online store. Please also note that all holes are optimised for metric drill bits!&lt;/p&gt;

&lt;h3&gt;Obligatory Warning&lt;/h3&gt;
&lt;p&gt;While it probably goes without saying, there are no guarantees that the footprints or schematic symbols in this library are flawless, and we make no promises of fitness for production, prototyping or any other purpose. These libraries are provided for information puposes only, and are used at your own discretion.  While we make every effort to produce accurate footprints, and many of the items found in this library have be proven in production, we can't make any promises of suitability for a specific purpose. If you do find any errors, though, please feel free to contact us at www.microbuilder.eu to let us know about it so that we can update the library accordingly!&lt;/p&gt;

&lt;h3&gt;License&lt;/h3&gt;
&lt;p&gt;This work is placed in the public domain, and may be freely used for commercial and non-commercial work with the following conditions:&lt;/p&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
&lt;/p&gt;</description>
<packages>
<package name="SOT23-WIDE">
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.2032" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.6724" y1="-0.6524" x2="-1.6724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="-1.6724" y1="0.6604" x2="-0.7136" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="1.6724" y1="0.6604" x2="1.6724" y2="-0.6524" width="0.2032" layer="21"/>
<wire x1="0.7136" y1="0.6604" x2="1.6724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="0.2224" y1="-0.6604" x2="-0.2364" y2="-0.6604" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1" dx="1" dy="1.27" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="1" dy="1.27" layer="1"/>
<smd name="1" x="-0.95" y="-1" dx="1" dy="1.27" layer="1"/>
<text x="1.905" y="0" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1.905" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="TO252">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;&lt;p&gt;
TS-003</description>
<wire x1="3.2766" y1="3.8354" x2="3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-2.159" x2="-3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-2.159" x2="-3.2766" y2="3.8354" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="3.835" x2="3.2774" y2="3.8346" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="3.937" x2="-2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="4.6482" x2="-2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="5.1054" x2="2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="5.1054" x2="2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="4.6482" x2="2.5654" y2="3.937" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.937" x2="-2.5654" y2="3.937" width="0.2032" layer="51"/>
<smd name="3" x="0" y="2.5" dx="5.4" dy="6.2" layer="1"/>
<smd name="1" x="-2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<smd name="2" x="2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<text x="-3.81" y="-2.54" size="0.8128" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.7178" y1="-5.1562" x2="-1.8542" y2="-2.2606" layer="51"/>
<rectangle x1="1.8542" y1="-5.1562" x2="2.7178" y2="-2.2606" layer="51"/>
<rectangle x1="-0.4318" y1="-3.0226" x2="0.4318" y2="-2.2606" layer="21"/>
<polygon width="0.2032" layer="51">
<vertex x="-2.5654" y="3.937"/>
<vertex x="-2.5654" y="4.6482"/>
<vertex x="-2.1082" y="5.1054"/>
<vertex x="2.1082" y="5.1054"/>
<vertex x="2.5654" y="4.6482"/>
<vertex x="2.5654" y="3.937"/>
</polygon>
</package>
<package name="SOT23-W">
<description>&lt;b&gt;SOT23&lt;/b&gt; - Wave soldering</description>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.2032" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.6224" y1="-0.3984" x2="-1.6224" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="1.6224" y1="0.6604" x2="1.6224" y2="-0.3984" width="0.2032" layer="21"/>
<wire x1="0.2454" y1="-0.6604" x2="-0.2594" y2="-0.6604" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1.3" dx="2.8" dy="1.4" layer="1"/>
<smd name="2" x="1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<smd name="1" x="-1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<text x="2.032" y="-0.127" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="2.032" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SMADIODE">
<description>&lt;b&gt;SMA Surface Mount Diode&lt;/b&gt;</description>
<wire x1="-2.15" y1="1.3" x2="2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="1.3" x2="2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.3" x2="-2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.3" x2="-2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-3.789" y1="-1.394" x2="-3.789" y2="-1.146" width="0.127" layer="21"/>
<wire x1="-3.789" y1="-1.146" x2="-3.789" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-3.789" y1="1.6" x2="3.816" y2="1.6" width="0.2032" layer="21"/>
<wire x1="3.816" y1="1.6" x2="3.816" y2="1.394" width="0.2032" layer="21"/>
<wire x1="3.816" y1="1.394" x2="3.816" y2="1.3365" width="0.127" layer="21"/>
<wire x1="3.816" y1="1.394" x2="3.816" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="3.816" y1="-1.6" x2="-3.789" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.789" y1="-1.6" x2="-3.789" y2="-1.146" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="-0.508" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="0" x2="0.254" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.762" width="0.2032" layer="21"/>
<smd name="C" x="-2.3495" y="0" dx="2.54" dy="2.54" layer="1"/>
<smd name="A" x="2.3495" y="0" dx="2.54" dy="2.54" layer="1" rot="R180"/>
<text x="-2.54" y="1.905" size="0.8128" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.286" size="0.4064" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.825" y1="-1.1" x2="-2.175" y2="1.1" layer="51"/>
<rectangle x1="2.175" y1="-1.1" x2="2.825" y2="1.1" layer="51" rot="R180"/>
<rectangle x1="-1.75" y1="-1.225" x2="-1.075" y2="1.225" layer="51"/>
</package>
<package name="DO-1N4148">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.2032" layer="21"/>
<pad name="A" x="-3.81" y="0" drill="0.9"/>
<pad name="C" x="3.81" y="0" drill="0.9"/>
<text x="-2.286" y="1.143" size="0.8128" layer="25" ratio="18">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21">&gt;Value</text>
</package>
<package name="SOD-523">
<description>SOD-523 (0.8x1.2mm)

&lt;p&gt;Source: http://www.rohm.com/products/databook/di/pdf/rb751s-40.pdf&lt;/p&gt;</description>
<smd name="K" x="0" y="0.75" dx="0.8" dy="0.6" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.6" layer="1"/>
<text x="0.716" y="0.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="0.716" y="-0.724" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="0.4254" y1="0.6" x2="0.4254" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.4" y1="-0.6" x2="-0.4" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-0.4254" y1="-0.6" x2="-0.4254" y2="0.6" width="0.127" layer="21"/>
<wire x1="-0.4" y1="0.6" x2="0.4" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.05" x2="0.25" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.25" y1="-0.2" x2="-0.25" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-0.25" y1="-0.2" x2="0" y2="0.05" width="0.127" layer="21"/>
<rectangle x1="-0.1" y1="0.45" x2="0.1" y2="0.85" layer="51" rot="R270"/>
<rectangle x1="-0.1" y1="-0.85" x2="0.1" y2="-0.45" layer="51" rot="R270"/>
<rectangle x1="-0.1" y1="-0.2254" x2="0.1" y2="0.5746" layer="21" rot="R270"/>
<polygon width="0.0508" layer="21">
<vertex x="0" y="0.05"/>
<vertex x="0.25" y="-0.2"/>
<vertex x="-0.25" y="-0.2"/>
</polygon>
</package>
<package name="SOD-323">
<description>&lt;b&gt;SOD323&lt;/b&gt; (2.5x1.2mm)</description>
<smd name="C" x="-1.27" y="0" dx="1.35" dy="0.8" layer="1"/>
<smd name="A" x="1.27" y="0" dx="1.35" dy="0.8" layer="1"/>
<text x="-1.1" y="1" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.1" y="-1.792" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-1" y1="0.7" x2="1" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1" y1="0.7" x2="1" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.7" x2="-1" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-1" y1="-0.7" x2="-1" y2="0.7" width="0.2032" layer="51"/>
<wire x1="-0.25" y1="0" x2="0.35" y2="0.4" width="0.2032" layer="21"/>
<wire x1="0.35" y1="0.4" x2="0.35" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="0.35" y1="-0.4" x2="-0.25" y2="0" width="0.2032" layer="21"/>
<rectangle x1="-0.45" y1="-0.5" x2="-0.25" y2="0.5" layer="21"/>
<polygon width="0.2032" layer="21">
<vertex x="-0.1" y="0"/>
<vertex x="0.2" y="0.2"/>
<vertex x="0.2" y="-0.2"/>
</polygon>
</package>
<package name="SOD-123">
<description>&lt;b&gt;SOD-123&lt;/b&gt;
&lt;p&gt;Source: http://www.diodes.com/datasheets/ds30139.pdf&lt;/p&gt;</description>
<smd name="C" x="-1.85" y="0" dx="1.4" dy="1.4" layer="1" rot="R90"/>
<smd name="A" x="1.85" y="0" dx="1.4" dy="1.4" layer="1" rot="R90"/>
<text x="-1.27" y="1.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-0.873" y1="0.7" x2="0.873" y2="0.7" width="0.2032" layer="21"/>
<wire x1="0.873" y1="0.7" x2="0.873" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="0.873" y1="-0.7" x2="-0.873" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-0.873" y1="-0.7" x2="-0.873" y2="0.7" width="0.2032" layer="51"/>
<wire x1="-0.3" y1="0" x2="0.3" y2="0.4" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="-0.3" y2="0" width="0.2032" layer="21"/>
<rectangle x1="-1.723" y1="-0.45" x2="-0.973" y2="0.4" layer="51"/>
<rectangle x1="0.973" y1="-0.45" x2="1.723" y2="0.4" layer="51"/>
<rectangle x1="-0.5" y1="-0.5" x2="-0.3" y2="0.5" layer="21"/>
<polygon width="0.2032" layer="21">
<vertex x="-0.1" y="0"/>
<vertex x="0.2" y="0.2"/>
<vertex x="0.2" y="-0.2"/>
</polygon>
</package>
<package name="1X02_OVAL">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02_ROUND">
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.6764" rot="R90"/>
<text x="-2.6162" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02_SMT">
<description>&lt;p&gt;&lt;b&gt;Pin Headers&lt;/b&gt;&lt;br/&gt;
2 Pin, 0.1"/2.54mm pitch, SMT&lt;/p&gt;</description>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.2032" layer="51"/>
<smd name="1" x="-1.27" y="1.27" dx="1" dy="3.5" layer="1"/>
<smd name="2" x="1.27" y="-1.27" dx="1" dy="3.5" layer="1"/>
<text x="-2.6162" y="3.25" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-4.5" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SOT23-R">
<description>&lt;b&gt;SOT23&lt;/b&gt; - Reflow soldering</description>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-0.7" x2="-1.5" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.6" x2="-0.6" y2="0.6" width="0.2032" layer="21"/>
<wire x1="1.5" y1="0.6" x2="1.5" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.6" y1="0.6" x2="1.5" y2="0.6" width="0.2032" layer="21"/>
<wire x1="0.4" y1="-0.7" x2="-0.4" y2="-0.7" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1.2" dx="0.7" dy="1.1" layer="1"/>
<smd name="2" x="0.95" y="-1.2" dx="0.7" dy="1.1" layer="1"/>
<smd name="1" x="-0.95" y="-1.2" dx="0.7" dy="1.1" layer="1"/>
<text x="1.778" y="-0.127" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1.778" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<text x="0.5" y="-0.4" size="1.016" layer="21" ratio="20" rot="R90">N</text>
</package>
</packages>
<symbols>
<symbol name="MOSFET-P">
<wire x1="-1.778" y1="-0.762" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-3.175" x2="-1.778" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="-1.778" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="1.905" x2="-1.778" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="-1.778" y2="3.175" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.778" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="0.762" x2="0.762" y2="0.508" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0.508" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.508" x2="2.032" y2="0.254" width="0.1524" layer="94"/>
<circle x="0" y="2.54" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-2.54" radius="0.3592" width="0" layer="94"/>
<text x="1.27" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="0.635" y="-3.81" size="0.8128" layer="93">D</text>
<text x="0.635" y="3.175" size="0.8128" layer="93">S</text>
<text x="-3.81" y="1.27" size="0.8128" layer="93">G</text>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="1.27" y="0.508"/>
<vertex x="1.778" y="-0.254"/>
<vertex x="0.762" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="0" y="0"/>
<vertex x="-1.016" y="0.762"/>
<vertex x="-1.016" y="-0.762"/>
</polygon>
</symbol>
<symbol name="MOSFET-N">
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="0.762" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<wire x1="4.318" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.048" y2="0.254" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="3.81" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="3.175" y="3.175" size="0.8128" layer="93">D</text>
<text x="3.175" y="-3.81" size="0.8128" layer="93">S</text>
<text x="-1.27" y="-1.905" size="0.8128" layer="93">G</text>
<pin name="G" x="-2.54" y="0" visible="off" length="short"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.762"/>
<vertex x="2.032" y="-0.762"/>
</polygon>
<text x="0" y="3.81" size="1.27" layer="94">D</text>
<text x="-2.54" y="1.27" size="1.27" layer="94">G</text>
<text x="0" y="-5.08" size="1.27" layer="94">S</text>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.032" y="2.286" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOSFET-P" prefix="Q" uservalue="yes">
<description>&lt;b&gt;P-Channel Mosfet&lt;/b&gt;
&lt;p&gt;&lt;b&gt;LEGEND&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;
&lt;b&gt;VDS&lt;/b&gt;: Voltage Drain-Source&lt;br/&gt;
&lt;b&gt;ID&lt;/b&gt;: Drain Current&lt;br/&gt;
&lt;b&gt;RDS(ON)&lt;/b&gt;: Drain-Source On-State Resistance&lt;br/&gt;
&lt;b&gt;VGS(TH)&lt;/b&gt;: Gate-Source Threshold Voltage&lt;br/&gt;
&lt;b&gt;CISS&lt;/b&gt;: Drain-Source Input Capacitance
&lt;/p&gt;
&lt;p&gt;
&lt;b&gt;SOT-23&lt;/b&gt;
&lt;table border="0" width="90%" cellspacing="0" cellpadding="5"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
&lt;td&gt;Name&lt;/td&gt;
&lt;td&gt;VDS&lt;/td&gt;
&lt;td&gt;ID&lt;/td&gt;
&lt;td&gt;RDS(ON)&lt;/td&gt;
&lt;td&gt;VGS(TH)&lt;/td&gt;
&lt;td&gt;CISS&lt;/td&gt;
&lt;td&gt;Order Number&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;IRLML5103&lt;/td&gt;
&lt;td&gt;30V&lt;/td&gt;
&lt;td&gt;760mA&lt;/td&gt;
&lt;td&gt;600 mOhm&lt;/td&gt;
&lt;td&gt;--&lt;/td&gt;
&lt;td&gt;75pF @ 25V&lt;/td&gt;
&lt;td&gt;Digikey: IRLML5103PBFCT-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;IRLML6401&lt;/td&gt;
&lt;td&gt;12V&lt;/td&gt;
&lt;td&gt;4.3A&lt;/td&gt;
&lt;td&gt;50 mOhm&lt;/td&gt;
&lt;td&gt;950mV @ 250µA&lt;/td&gt;
&lt;td&gt;830pF @ 10V&lt;/td&gt;
&lt;td&gt;Digikey: IRLML6401PBFTR-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;NTR0202PL&lt;/td&gt;
&lt;td&gt;20V&lt;/td&gt;
&lt;td&gt;400mA&lt;/td&gt;
&lt;td&gt;800 mOhm&lt;/td&gt;
&lt;td&gt;2.3V @ 250uA&lt;/td&gt;
&lt;td&gt;70pF @ 5V&lt;/td&gt;
&lt;td&gt;Digikey: NTR0202PLT1GOSTR-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;NTR4101PT1G&lt;/td&gt;
&lt;td&gt;20V&lt;/td&gt;
&lt;td&gt;1.8A&lt;/td&gt;
&lt;td&gt;85 mOhm&lt;/td&gt;
&lt;td&gt;1.2V @ 250uA&lt;/td&gt;
&lt;td&gt;675pF @ 10V&lt;/td&gt;
&lt;td&gt;Digikey: NTR4101PT1GOSCT-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;DMP2004K&lt;/td&gt;
&lt;td&gt;20V&lt;/td&gt;
&lt;td&gt;600mA&lt;/td&gt;
&lt;td&gt;900 mOhm&lt;/td&gt;
&lt;td&gt;1V @ 250uA&lt;/td&gt;
&lt;td&gt;175pF @ 16V&lt;/td&gt;
&lt;td&gt;Digikey: DMP2004KDICT-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;PMV65XP&lt;/td&gt;
&lt;td&gt;20V&lt;/td&gt;
&lt;td&gt;3.9A&lt;/td&gt;
&lt;td&gt;76 mOhm&lt;/td&gt;
&lt;td&gt;950mV @ 1mA&lt;/td&gt;
&lt;td&gt;725pF @ 20V&lt;/td&gt;
&lt;td&gt;Digikey: 568-2358-2-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt; 
&lt;b&gt;TO-252&lt;/b&gt;
&lt;table border="0" width="90%" cellspacing="0" cellpadding="5"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
&lt;td&gt;Name&lt;/td&gt;
&lt;td&gt;VDS&lt;/td&gt;
&lt;td&gt;ID&lt;/td&gt;
&lt;td&gt;RDS(ON)&lt;/td&gt;
&lt;td&gt;VGS(TH)&lt;/td&gt;
&lt;td&gt;CISS&lt;/td&gt;
&lt;td&gt;Order Number&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;AOD417&lt;/td&gt;
&lt;td&gt;30V&lt;/td&gt;
&lt;td&gt;25A&lt;/td&gt;
&lt;td&gt;34 mOhm&lt;/td&gt;
&lt;td&gt;3V @ 250µA&lt;/td&gt;
&lt;td&gt;920pF @ 15V&lt;/td&gt;
&lt;td&gt;Digikey: 785-1106-2-ND&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="MOSFET-P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-R">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WIDE" package="SOT23-WIDE">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO252" package="TO252">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-N" prefix="Q" uservalue="yes">
<description>&lt;b&gt;N-Channel Mosfet&lt;/b&gt;
&lt;p&gt;&lt;b&gt;LEGEND&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;
&lt;b&gt;VDS&lt;/b&gt;: Voltage Drain-Source&lt;br/&gt;
&lt;b&gt;ID&lt;/b&gt;: Drain Current&lt;br/&gt;
&lt;b&gt;RDS(ON)&lt;/b&gt;: Drain-Source On-State Resistance&lt;br/&gt;
&lt;b&gt;VGS(TH)&lt;/b&gt;: Gate-Source Threshold Voltage&lt;br/&gt;
&lt;b&gt;CISS&lt;/b&gt;: Drain-Source Input Capacitance
&lt;/p&gt;
&lt;p&gt;
&lt;b&gt;SOT-23&lt;/b&gt;
&lt;table border="0" width="90%" cellspacing="0" cellpadding="5"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
&lt;td&gt;Name&lt;/td&gt;
&lt;td&gt;VDS&lt;/td&gt;
&lt;td&gt;ID&lt;/td&gt;
&lt;td&gt;RDS(ON)&lt;/td&gt;
&lt;td&gt;VGS(TH)&lt;/td&gt;
&lt;td&gt;CISS&lt;/td&gt;
&lt;td&gt;Order Number&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;2N7002E&lt;/td&gt;
&lt;td&gt;60V&lt;/td&gt;
&lt;td&gt;260mA&lt;/td&gt;
&lt;td&gt;2.5 Ohm&lt;/td&gt;
&lt;td&gt;2.5V @ 250uA&lt;/td&gt;
&lt;td&gt;26.7pF @ 25V&lt;/td&gt;
&lt;td&gt;2N7002ET1GOSTR-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;BSH103&lt;/td&gt;
&lt;td&gt;30V&lt;/td&gt;
&lt;td&gt;850mA&lt;/td&gt;
&lt;td&gt;400 mOhm&lt;/td&gt;
&lt;td&gt;400mV @ 1mA&lt;/td&gt;
&lt;td&gt;83pF @ 24V&lt;/td&gt;
&lt;td&gt;BSH103,235-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;BSS138&lt;/td&gt;
&lt;td&gt;60V&lt;/td&gt;
&lt;td&gt;320mA&lt;/td&gt;
&lt;td&gt;1.6 Ohm&lt;/td&gt;
&lt;td&gt;1.5V @ 250uA&lt;/td&gt;
&lt;td&gt;50pF @ 10V&lt;/td&gt;
&lt;td&gt;Mouser: 771-BSS138PW115&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;DMN2075U-7&lt;/td&gt;
&lt;td&gt;20V&lt;/td&gt;
&lt;td&gt;4.2A&lt;/td&gt;
&lt;td&gt;38 mOhm&lt;/td&gt;
&lt;td&gt;1V @ 250 uA&lt;/td&gt;
&lt;td&gt;594.3pF @ 10V&lt;/td&gt;
&lt;td&gt;DMN2075U-7DICT-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;/table&gt; 
&lt;/p&gt;

&lt;p&gt;* BSS138 good choice for I2C level-shifting&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="MOSFET-N" x="-2.54" y="0"/>
</gates>
<devices>
<device name="REFLOW" package="SOT23-R">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WAVE" package="SOT23-W">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WIDE" package="SOT23-WIDE">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;b&gt;Diode&lt;/b&gt;
&lt;p&gt;
&lt;h3&gt;SMA&lt;/h3&gt;
&lt;table cellpadding="5" width="600"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
  &lt;td&gt;&lt;b&gt;Model&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Volts&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Amps&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Type&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vf&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vr&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Digikey #&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Notes&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;SSA34-E3&lt;/td&gt;
  &lt;td&gt;40V&lt;/td&gt;
  &lt;td&gt;3A&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;480mV @ 3A&lt;/td&gt;
  &lt;td&gt;200uA @ 40V&lt;/td&gt;
  &lt;td&gt;SSA34-E3/61TGITR-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;CDBA120-G&lt;/td&gt;
  &lt;td&gt;20V&lt;/td&gt;
  &lt;td&gt;1A&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;500mV @ 1A&lt;/td&gt;
  &lt;td&gt;500uA @ 20V&lt;/td&gt;
  &lt;td&gt;641-1014-6-ND&lt;/td&gt;
  &lt;td&gt;REEL&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;MBRA210&lt;/td&gt;
  &lt;td&gt;10V&lt;/td&gt;
  &lt;td&gt;2A&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;350mV @ 2A&lt;/td&gt;
  &lt;td&gt;700uA @ 10V&lt;/td&gt;
  &lt;td&gt;MBRA210LT3&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;h3&gt;SOD-123&lt;/h3&gt;
&lt;table cellpadding="5" width="600"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
  &lt;td&gt;&lt;b&gt;Model&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Volts&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Amps&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Type&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vf&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vr&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Order #&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Notes&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;BAT54T1G&lt;/td&gt;
  &lt;td&gt;30V&lt;/td&gt;
  &lt;td&gt;200mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;800mV @ 200mA&lt;/td&gt;
  &lt;td&gt;2uA @ 25V&lt;/td&gt;
  &lt;td&gt;BAT54T1GOSTR-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;B0530W&lt;/td&gt;
  &lt;td&gt;30V&lt;/td&gt;
  &lt;td&gt;500mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;430mV @ 500mA&lt;/td&gt;
  &lt;td&gt;130uA @ 30V&lt;/td&gt;
  &lt;td&gt;B0530W-FDICT-ND&lt;/td&gt;
  &lt;td&gt;REEL&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;MBR120&lt;/td&gt;
  &lt;td&gt;1A&lt;/td&gt;
  &lt;td&gt;20V&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;340mV @ 1A&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;MBR120VLSFT1GOSCT-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;h3&gt;SOD-323&lt;/h3&gt;
&lt;table cellpadding="5" width="600"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
  &lt;td&gt;&lt;b&gt;Model&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Volts&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Amps&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Type&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vf&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vr&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Order #&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Notes&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;PMEG2005EJ&lt;/td&gt;
  &lt;td&gt;20V&lt;/td&gt;
  &lt;td&gt;500mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;355mV @ 500mA&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;568-4110-1-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;ZLLS410&lt;/td&gt;
  &lt;td&gt;10V&lt;/td&gt;
  &lt;td&gt;570mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;380mV @ 570mA&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;ZLLS410CT-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;1N4148WS&lt;/td&gt;
  &lt;td&gt;75V&lt;/td&gt;
  &lt;td&gt;150mA&lt;/td&gt;
  &lt;td&gt;Silicon/Simple&lt;/td&gt;
  &lt;td&gt;1V&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;1N4148WSFSCT-ND&lt;/td&gt;
  &lt;td&gt;REEL&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;h3&gt;SOD-523&lt;/h3&gt;
&lt;table cellpadding="5" width="600"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
  &lt;td&gt;&lt;b&gt;Model&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Volts&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Amps&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Type&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vf&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vr&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Order #&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Notes&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;BAT54XV2&lt;/td&gt;
  &lt;td&gt;30V&lt;/td&gt;
  &lt;td&gt;200mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;0.8V @ 100mA&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;BAT54XV2CT-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;TB751S&lt;/td&gt;
  &lt;td&gt;30V&lt;/td&gt;
  &lt;td&gt;30mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;RB751S-40TE61CT-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;h3&gt;SOT23-R/W&lt;/h3&gt;(R = Solder Paste/Reflow Ovens, W = Hand-Soldering)
&lt;br/&gt;
&lt;table cellpadding="5" width="600"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
  &lt;td&gt;&lt;b&gt;Model&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Volts&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Amps&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Type&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vf&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vr&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Order #&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Notes&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;BAT54FILM&lt;/td&gt;
  &lt;td&gt;40V&lt;/td&gt;
  &lt;td&gt;300mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;497-7162-1-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="SMADIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO-1N4148" package="DO-1N4148">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23_REFLOW" package="SOT23-R">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-523" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23_WIDE" package="SOT23-WIDE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02_OVAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROUND" package="1X02_ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMT" package="1X02_SMT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lio">
<packages>
<package name="SSOP5">
<wire x1="1.45" y1="0.8104" x2="1.45" y2="-0.8104" width="0.2032" layer="21"/>
<wire x1="-1.45" y1="-0.8104" x2="-1.45" y2="0.8104" width="0.2032" layer="21"/>
<circle x="-1.0922" y="-0.37465" radius="0.0359" width="0.254" layer="21"/>
<smd name="1" x="-0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<text x="-0.8255" y="2.159" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9525" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="BD48XXX">
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-7.62" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="5.08" y="-10.16" length="middle" direction="pwr" rot="R90"/>
<pin name="VOUT" x="-5.08" y="-10.16" length="middle" direction="oc" rot="R90"/>
<pin name="VDD" x="0" y="-10.16" length="middle" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BD48XXX" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="BD48XXX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SSOP5">
<connects>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="VDD" pad="2"/>
<connect gate="G$1" pin="VOUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="prg_header">
<description>Connectorless programming header</description>
<packages>
<package name="AVX_9188_6-WAY_MATING_GUIDE">
<smd name="2" x="-2" y="1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<smd name="6" x="2" y="1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<smd name="3" x="-1" y="-1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<smd name="1" x="-3" y="-1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<smd name="4" x="1" y="-1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<smd name="5" x="3" y="-1" dx="0.9" dy="1.7" layer="1" cream="no"/>
<text x="-6.096" y="2.794" size="1.016" layer="25">&gt;NAME</text>
<text x="-6.096" y="-3.81" size="1.016" layer="27">&gt;VALUE</text>
<hole x="-5.334" y="0" drill="1"/>
<hole x="5.334" y="0" drill="1"/>
<circle x="-3.97" y="-0.93" radius="0.14141875" width="0.3048" layer="21"/>
</package>
<package name="AVX_9188_6-WAY_SMT">
<description>&lt;b&gt;AVX 9188 Staggered SOLO Stacker 6-Way&lt;/b&gt;
&lt;p&gt;SMT PCB Footprint&lt;/P&gt;
&lt;a href="http://www.avx.com/docs/Catalogs/9188.pdf"&gt;http://www.avx.com/docs/Catalogs/9188.pdf&lt;/a&gt;</description>
<wire x1="-3.65" y1="2.05" x2="3.65" y2="2.05" width="0.127" layer="21"/>
<wire x1="3.65" y1="2.05" x2="3.65" y2="-2.05" width="0.127" layer="21"/>
<wire x1="3.65" y1="-2.05" x2="-3.65" y2="-2.05" width="0.127" layer="21"/>
<wire x1="-3.65" y1="-2.05" x2="-3.65" y2="2.05" width="0.127" layer="21"/>
<smd name="2" x="-2" y="0.95" dx="0.6" dy="1" layer="1"/>
<smd name="6" x="2" y="0.95" dx="0.6" dy="1" layer="1"/>
<smd name="3" x="-1" y="-0.95" dx="0.6" dy="1" layer="1"/>
<smd name="1" x="-3" y="-0.95" dx="0.6" dy="1" layer="1"/>
<smd name="4" x="1" y="-0.95" dx="0.6" dy="1" layer="1"/>
<smd name="5" x="3" y="-0.95" dx="0.6" dy="1" layer="1"/>
<text x="-3" y="2.5" size="1" layer="25">&gt;NAME</text>
<text x="-3" y="-3.5" size="1" layer="27">&gt;VALUE</text>
</package>
<package name="AVX_9188_6-WAY_TINY">
<smd name="2" x="-2" y="1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<smd name="6" x="2" y="1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<smd name="3" x="-1" y="-1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<smd name="1" x="-3" y="-1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<smd name="4" x="1" y="-1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<smd name="5" x="3" y="-1" dx="0.762" dy="1.6764" layer="1" cream="no"/>
<text x="-6.096" y="2.794" size="1.016" layer="25">&gt;NAME</text>
<text x="-6.096" y="-3.81" size="1.016" layer="27">&gt;VALUE</text>
<hole x="-5.334" y="0" drill="1"/>
<hole x="5.334" y="0" drill="1"/>
<circle x="-3.97" y="-0.93" radius="0.14141875" width="0.3048" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="AVR_ISP">
<wire x1="-5.08" y1="-5.08" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="-4.318" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.064" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="8.89" y="0.635" size="1.27" layer="94">MOSI</text>
<text x="-11.938" y="-2.032" size="1.27" layer="94">RESET</text>
<text x="-11.938" y="0.508" size="1.27" layer="94">SCK</text>
<text x="-11.938" y="3.302" size="1.27" layer="94">MISO</text>
<text x="8.89" y="3.048" size="1.27" layer="94">+5</text>
<text x="8.89" y="-2.032" size="1.27" layer="94">GND</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" direction="pas" function="dot"/>
<pin name="2" x="10.16" y="2.54" visible="pad" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-7.62" y="0" visible="pad" direction="pas" function="dot"/>
<pin name="4" x="10.16" y="0" visible="pad" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-7.62" y="-2.54" visible="pad" direction="pas" function="dot"/>
<pin name="6" x="10.16" y="-2.54" visible="pad" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ISPTOUCH" prefix="J">
<description>&lt;b&gt;ISPtouch for AVR Microcontrollers&lt;/b&gt;
&lt;p&gt;This 6-pin ISP connector for AVR programming does not require any part on the PCB, but an adapter for a common 6-pin programmer.&lt;/p&gt;
</description>
<gates>
<gate name="G$1" symbol="AVR_ISP" x="0" y="0"/>
</gates>
<devices>
<device name="_HEADER" package="AVX_9188_6-WAY_MATING_GUIDE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ADAPTER" package="AVX_9188_6-WAY_SMT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AVX_9188_6-WAY_TINY" package="AVX_9188_6-WAY_TINY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3P-LOC" urn="urn:adsk.eagle:symbol:13887/1" library_version="1">
<wire x1="256.54" y1="24.13" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="256.54" y1="3.81" x2="246.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="3.81" x2="161.29" y2="3.81" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="387.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3P-LOC" urn="urn:adsk.eagle:component:13945/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>A3 Portrait Location</description>
<gates>
<gate name="G$1" symbol="A3P-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="10118194-0001LF">
<packages>
<package name="FRAMATOME_10118194-0001LF">
<wire x1="-3.5" y1="2.85" x2="3.5" y2="2.85" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-1.25" x2="-3.5" y2="-2.1" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-2.1" x2="-3.5" y2="-2.15" width="0.127" layer="21"/>
<wire x1="3.5" y1="-1.25" x2="3.5" y2="-2.15" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-1.45" x2="4.5" y2="-1.45" width="0.127" layer="21"/>
<text x="4.25973125" y="-1.503440625" size="1.2729" layer="51">PCB Edge</text>
<wire x1="-3.5" y1="-2.1" x2="-4" y2="-2.6" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.9" y1="-2.9" x2="-3.1" y2="-2.1" width="0.127" layer="21" curve="90"/>
<wire x1="-3.1" y1="-2.1" x2="-3.1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-3.1" y1="-1.8" x2="-2.8" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-1.8" x2="-2.4" y2="-2.2" width="0.127" layer="21" curve="90"/>
<wire x1="3.5" y1="-1.25" x2="3.5" y2="-2.1" width="0.127" layer="21"/>
<wire x1="3.5" y1="-2.1" x2="3.5" y2="-2.15" width="0.127" layer="21"/>
<wire x1="3.5" y1="-2.1" x2="4" y2="-2.6" width="0.127" layer="21" curve="90"/>
<wire x1="3.9" y1="-2.9" x2="3.1" y2="-2.1" width="0.127" layer="21" curve="-90"/>
<wire x1="3.1" y1="-2.1" x2="3.1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="3.1" y1="-1.8" x2="2.8" y2="-1.8" width="0.127" layer="21"/>
<wire x1="2.8" y1="-1.8" x2="2.4" y2="-2.2" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.4" y1="-2.2" x2="2.3" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-2.5" y1="3.7" x2="3.8" y2="3.7" width="0.127" layer="39"/>
<wire x1="3.8" y1="3.7" x2="3.8" y2="1.2" width="0.127" layer="39"/>
<wire x1="3.8" y1="1.2" x2="4.7" y2="1.2" width="0.127" layer="39"/>
<wire x1="4.7" y1="1.2" x2="4.7" y2="-1.2" width="0.127" layer="39"/>
<wire x1="4.7" y1="-1.2" x2="4.3" y2="-1.2" width="0.127" layer="39"/>
<wire x1="4.3" y1="-1.2" x2="4.3" y2="-3.2" width="0.127" layer="39"/>
<wire x1="4.3" y1="-3.2" x2="-4.3" y2="-3.2" width="0.127" layer="39"/>
<wire x1="-4.3" y1="-3.2" x2="-4.3" y2="-1.2" width="0.127" layer="39"/>
<wire x1="-4.3" y1="-1.2" x2="-4.7" y2="-1.2" width="0.127" layer="39"/>
<wire x1="-4.7" y1="-1.2" x2="-4.7" y2="1.2" width="0.127" layer="39"/>
<wire x1="-4.7" y1="1.2" x2="-3.8" y2="1.2" width="0.127" layer="39"/>
<wire x1="-3.8" y1="1.2" x2="-3.8" y2="3.7" width="0.127" layer="39"/>
<wire x1="-3.8" y1="3.7" x2="-1.8" y2="3.7" width="0.127" layer="39"/>
<text x="-4.50903125" y="-0.90180625" size="1.27255" layer="25" rot="R90">&gt;NAME</text>
<text x="5.904759375" y="0.10008125" size="1.27101875" layer="27" rot="R90">&gt;VALUE</text>
<smd name="1" x="-1.3" y="2.675" dx="0.45" dy="1.35" layer="1" rot="R180"/>
<smd name="2" x="-0.65" y="2.675" dx="0.45" dy="1.35" layer="1" rot="R180"/>
<smd name="3" x="0" y="2.675" dx="0.45" dy="1.35" layer="1" rot="R180"/>
<smd name="4" x="0.65" y="2.675" dx="0.45" dy="1.35" layer="1" rot="R180"/>
<smd name="5" x="1.3" y="2.675" dx="0.45" dy="1.35" layer="1" rot="R180"/>
<pad name="P$6" x="-2.5" y="2.7" drill="0.9"/>
<pad name="P$7" x="2.5" y="2.7" drill="0.9"/>
<pad name="P$8" x="-3.5" y="0" drill="1.2"/>
<pad name="P$9" x="3.5" y="0" drill="1.2"/>
<smd name="P$10" x="-1" y="0" dx="1.5" dy="1.55" layer="1"/>
<smd name="P$11" x="1" y="0" dx="1.5" dy="1.55" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="10118194-0001LF">
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-7.38786875" y="5.604590625" size="1.27376875" layer="95">&gt;NAME</text>
<text x="-7.63473125" y="-7.63473125" size="1.272459375" layer="96">&gt;VALUE</text>
<pin name="D+" x="-12.7" y="2.54" length="middle"/>
<pin name="D-" x="-12.7" y="0" length="middle"/>
<pin name="ID" x="-12.7" y="-2.54" length="middle" direction="pas"/>
<pin name="GND" x="12.7" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD" x="12.7" y="2.54" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10118194-0001LF" prefix="J">
<description>Conn Micro USB Type B RCP 5 POS 0.65mm Solder RA SMD 5 Terminal 1 Port T/R</description>
<gates>
<gate name="G$1" symbol="10118194-0001LF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FRAMATOME_10118194-0001LF">
<connects>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="ID" pad="4"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Warning"/>
<attribute name="DESCRIPTION" value=" Micro Usb, 2.0 Type b, Rcpt, Smt "/>
<attribute name="MF" value="Amphenol"/>
<attribute name="MP" value="10118194-0001LF"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="0.26 USD"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BSS84LT1">
<description>&lt;MOSFET 50V 130mA P-Channel&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOT95P237X111-3N">
<description>&lt;b&gt;BSS84&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.2" y="0.95" dx="1.1" dy="0.7" layer="1"/>
<smd name="2" x="-1.2" y="-0.95" dx="1.1" dy="0.7" layer="1"/>
<smd name="3" x="1.2" y="0" dx="1.1" dy="0.7" layer="1"/>
<text x="0" y="-2.7" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.2" y="2.8" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-0.65" y1="1.46" x2="0.65" y2="1.46" width="0.1" layer="51"/>
<wire x1="0.65" y1="1.46" x2="0.65" y2="-1.46" width="0.1" layer="51"/>
<wire x1="0.65" y1="-1.46" x2="-0.65" y2="-1.46" width="0.1" layer="51"/>
<wire x1="-0.65" y1="-1.46" x2="-0.65" y2="1.46" width="0.1" layer="51"/>
<wire x1="-0.7" y1="0.4" x2="-0.7" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="-1.5" x2="0.6" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="0.6" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="0.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="1.5" x2="-0.7" y2="1.5" width="0.2032" layer="21"/>
<text x="-0.4" y="-0.5" size="1.016" layer="21" ratio="20">P</text>
</package>
</packages>
<symbols>
<symbol name="BSS84LT1">
<wire x1="-2.032" y1="2.54" x2="-2.032" y2="0.254" width="0.254" layer="94"/>
<text x="1.778" y="4.064" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="1.778" y="-4.064" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="G" x="-5.08" y="0" visible="off" length="short"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<wire x1="-2.032" y1="0.254" x2="-2.032" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="2.032" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.778" x2="-1.27" y2="2.032" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.778" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-2.54" x2="1.778" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="1.778" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.778" y1="2.54" x2="1.778" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-0.762" y="-0.762"/>
<vertex x="-0.762" y="0.762"/>
<vertex x="0.254" y="0"/>
</polygon>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0.762" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="2.286" y2="0.508" width="0.1524" layer="94"/>
<wire x1="2.286" y1="0.508" x2="2.54" y2="0.254" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.508" x2="1.778" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.016" x2="-1.27" y2="-1.016" width="0.254" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="1.778" y="0.508"/>
<vertex x="1.27" y="-0.254"/>
<vertex x="2.286" y="-0.254"/>
</polygon>
<wire x1="1.778" y1="-1.27" x2="1.778" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="-5.08" size="1.27" layer="94">D</text>
<text x="-2.54" y="3.81" size="1.27" layer="94">S</text>
<text x="-5.08" y="1.27" size="1.27" layer="94">G</text>
<circle x="0" y="2.54" radius="0.127" width="0.508" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.508" layer="94"/>
<wire x1="-3.81" y1="0" x2="-2.032" y2="0" width="0.127" layer="94"/>
<wire x1="-2.032" y1="0" x2="-2.032" y2="0.254" width="0.127" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BSS84LT1" prefix="Q">
<description>&lt;b&gt;MOSFET 50V 130mA P-Channel&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://eagle.componentsearchengine.com/Datasheets/1/BSS84LT1.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="BSS84LT1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P237X111-3N">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="BSS84LT1" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="MOSFET 50V 130mA P-Channel" constant="no"/>
<attribute name="HEIGHT" value="1.11mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="ON Semiconductor" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="BSS84LT1" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="A750MS108M1AAAE013">
<description>&lt;KEMET Aluminium Polymer Capacitor 1000F 10V dc 10mm A750 Series Aluminium, Through Hole Polymer, 20%&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPPRD500W110D1000H1300">
<description>&lt;b&gt;Bulk&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.5"/>
<pad name="2" x="5" y="0" drill="1.2" diameter="2.5"/>
<text x="2.4" y="7" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="2.6" y="-6.7" size="1.27" layer="27" align="center">&gt;VALUE</text>
<circle x="2.5" y="0" radius="5" width="0.2" layer="25"/>
<circle x="2.5" y="0" radius="5" width="0.1" layer="51"/>
<text x="0.2" y="1.4" size="1.27" layer="21">+</text>
</package>
</packages>
<symbols>
<symbol name="A750MS108M1AAAE013">
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="5.842" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.842" y2="2.54" width="0.254" layer="94"/>
<wire x1="4.572" y1="1.27" x2="3.556" y2="1.27" width="0.254" layer="94"/>
<wire x1="4.064" y1="1.778" x2="4.064" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<text x="-1.27" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-1.27" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="+" x="0" y="0" visible="off" length="short"/>
<pin name="-" x="12.7" y="0" visible="off" length="short" rot="R180"/>
<polygon width="0.254" layer="94">
<vertex x="7.62" y="2.54"/>
<vertex x="7.62" y="-2.54"/>
<vertex x="6.858" y="-2.54"/>
<vertex x="6.858" y="2.54"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="A750MS108M1AAAE013" prefix="C">
<description>&lt;b&gt;KEMET Aluminium Polymer Capacitor 1000F 10V dc 10mm A750 Series Aluminium, Through Hole Polymer, 20%&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://uk.rs-online.com/web/p/products/1734860"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="A750MS108M1AAAE013" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPPRD500W110D1000H1300">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="A750MS108M1AAAE013" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="KEMET Aluminium Polymer Capacitor 1000F 10V dc 10mm A750 Series Aluminium, Through Hole Polymer, 20%" constant="no"/>
<attribute name="HEIGHT" value="13mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="A750MS108M1AAAE013" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="A750KS477M1CAAE013">
<description>&lt;KEMET Aluminium Polymer Capacitor 470F 16V dc 8mm A750 Series Aluminium, Through Hole Polymer, 20% 8 (Dia.) x 12mm&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPPRD350W60D800H1200">
<description>&lt;b&gt;Bulk&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="0.8" diameter="2"/>
<pad name="2" x="3.5" y="0" drill="0.8" diameter="2"/>
<text x="1.7" y="5.6" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="1.6" y="-5.4" size="1.27" layer="27" align="center">&gt;VALUE</text>
<circle x="1.75" y="0" radius="4" width="0.2" layer="25"/>
<circle x="1.75" y="0" radius="4" width="0.1" layer="51"/>
<text x="-0.4" y="1" size="1.27" layer="21">+</text>
<wire x1="0.9" y1="0" x2="1.2" y2="0" width="0.1524" layer="21"/>
<wire x1="2.6" y1="0" x2="2.2" y2="0" width="0.1524" layer="21"/>
<wire x1="1.2" y1="0" x2="1.2" y2="1" width="0.1524" layer="21"/>
<wire x1="1.2" y1="1" x2="1.4" y2="1" width="0.1524" layer="21"/>
<wire x1="1.4" y1="1" x2="1.6" y2="1" width="0.1524" layer="21"/>
<wire x1="1.6" y1="1" x2="1.6" y2="-1" width="0.1524" layer="21"/>
<wire x1="1.6" y1="-1" x2="1.2" y2="-1" width="0.1524" layer="21"/>
<wire x1="1.2" y1="-1" x2="1.2" y2="0" width="0.1524" layer="21"/>
<wire x1="2.2" y1="0" x2="2.2" y2="1" width="0.1524" layer="21"/>
<wire x1="2.2" y1="1" x2="2.1" y2="1" width="0.1524" layer="21"/>
<wire x1="2.1" y1="1" x2="2" y2="1" width="0.1524" layer="21"/>
<wire x1="2" y1="1" x2="1.9" y2="1" width="0.1524" layer="21"/>
<wire x1="1.9" y1="1" x2="1.9" y2="-1" width="0.1524" layer="21"/>
<wire x1="1.9" y1="-1" x2="2" y2="-1" width="0.1524" layer="21"/>
<wire x1="2" y1="-1" x2="2.1" y2="-1" width="0.1524" layer="21"/>
<wire x1="2.1" y1="-1" x2="2.2" y2="-1" width="0.1524" layer="21"/>
<wire x1="2.2" y1="-1" x2="2.2" y2="0" width="0.1524" layer="21"/>
<wire x1="2.2" y1="-1" x2="2.1" y2="1" width="0.1524" layer="21"/>
<wire x1="2.1" y1="1" x2="2.1" y2="-1" width="0.1524" layer="21"/>
<wire x1="2" y1="-1" x2="2" y2="1" width="0.1524" layer="21"/>
<wire x1="4.7" y1="2.6" x2="4.7" y2="-2.6" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="A750KS477M1CAAE013">
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="5.842" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.842" y2="2.54" width="0.254" layer="94"/>
<wire x1="4.572" y1="1.27" x2="3.556" y2="1.27" width="0.254" layer="94"/>
<wire x1="4.064" y1="1.778" x2="4.064" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<text x="8.89" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="8.89" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="+" x="0" y="0" visible="off" length="short"/>
<pin name="-" x="12.7" y="0" visible="off" length="short" rot="R180"/>
<polygon width="0.254" layer="94">
<vertex x="7.62" y="2.54"/>
<vertex x="7.62" y="-2.54"/>
<vertex x="6.858" y="-2.54"/>
<vertex x="6.858" y="2.54"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="A750KS477M1CAAE013" prefix="C">
<description>&lt;b&gt;KEMET Aluminium Polymer Capacitor 470F 16V dc 8mm A750 Series Aluminium, Through Hole Polymer, 20% 8 (Dia.) x 12mm&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://uk.rs-online.com/web/p/products/1726599"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="A750KS477M1CAAE013" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPPRD350W60D800H1200">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="A750KS477M1CAAE013" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="KEMET Aluminium Polymer Capacitor 470F 16V dc 8mm A750 Series Aluminium, Through Hole Polymer, 20% 8 (Dia.) x 12mm" constant="no"/>
<attribute name="HEIGHT" value="12mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="A750KS477M1CAAE013" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Passives">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="PAD.02X.02">
<smd name="P$1" x="0" y="0" dx="0.508" dy="0.508" layer="1"/>
</package>
<package name="PAD.03X.03">
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100" cream="no"/>
</package>
<package name="PAD.03X.05">
<smd name="P$1" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" cream="no"/>
</package>
<package name="PAD.03X.04">
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100" cream="no"/>
</package>
<package name="TP_15TH">
<pad name="P$1" x="0" y="0" drill="0.381" diameter="0.6096" stop="no"/>
<circle x="0" y="0" radius="0.381" width="0" layer="30"/>
</package>
</packages>
<symbols>
<symbol name="TEST-POINT">
<wire x1="2.54" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.762" x2="3.302" y2="-0.762" width="0.1524" layer="94" curve="180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;Name</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;Value</text>
<pin name="1" x="0" y="0" visible="off" length="point" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TEST-POINT" prefix="TP">
<description>Bare copper test points for troubleshooting or ICT</description>
<gates>
<gate name="G$1" symbol="TEST-POINT" x="0" y="0"/>
</gates>
<devices>
<device name="2" package="PAD.02X.02">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3" package="PAD.03X.03">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3X5" package="PAD.03X.05">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3X4" package="PAD.03X.04">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_15TH_THRU" package="TP_15TH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="cap-master">
<description>&lt;b&gt;CAP MASTER! v4.02 - Electrolytic, Film and Tantalum&lt;/b&gt;&lt;br&gt;
&lt;p&gt;This library is a collection of SMD and through hole capacitors by various manufacturers. Some of these include types from Panasonic, Kemet, BC Components and others. Many of the packages were adapted from &lt;b&gt;rcl.lbr&lt;/b&gt; and &lt;b&gt;cap-pan40.lbr&lt;/b&gt; and have been renamed and grouped in a more logical form.&lt;/p&gt;&lt;p&gt;Silkscreen elements are a minimum of 8 mils in width with larger components using 10 mil widths. Most of the SMD components have text sizes of 0 .04" and thru-hole components are 0.05" in size.  A text ratio of 14 is used in both cases. Where practical, the elements are aligned on a 12.5 mil or 6.25 mil grid depending on the pad size and spacing.&lt;/p&gt;
&lt;p&gt;&lt;h4&gt;All capacitors are grouped by the following naming conventions:&lt;/h4&gt;&lt;/p&gt;
&lt;table width="380" border="1" bordercolor="#000000"&gt;
  &lt;tr&gt; 
    &lt;td width="81" bgcolor="#33CCFF"&gt;&lt;div align="center"&gt;&lt;strong&gt;Prefix&lt;/strong&gt;&lt;/div&gt;&lt;/td&gt;
    &lt;td width="289" bgcolor="#33CCFF"&gt;&lt;div align="center"&gt;&lt;strong&gt;Description&lt;/strong&gt;&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CBP-&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Bipolar Electrolytic Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CCA-&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Chip Cap Array Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CNP-&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Non-Polarized Film / Chip Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CP-&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Polarized Electrolytic/Tantalum Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
&lt;/table&gt;
&lt;p&gt;
&lt;small&gt;DISCLAIMER: The author does not warrant that this library is free from error or that it will meet your specific requirements.&lt;/small&gt;&lt;/p&gt;
&lt;author&gt;by Bob Starr (rtzaudio@mindspring.com)&lt;br&gt;
Updated 05/28/2005 - Rev 4.02&lt;/author&gt;&lt;br&gt;&lt;br&gt;</description>
<packages>
<package name="ENP-010-030">
<description>&lt;b&gt;BIPOLAR ELECTROLYTIC&lt;/b&gt;&lt;p&gt;
1.0 mm lead spacing, 3 mm diameter, grid 0.0125 inch</description>
<circle x="0" y="0" radius="1.506" width="0.2032" layer="21"/>
<pad name="1" x="-0.635" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R90"/>
<pad name="2" x="0.635" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R90"/>
<text x="1.5874" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="1.5874" y="0.635" size="1.27" layer="25" ratio="14">&gt;NAME</text>
</package>
<package name="ENP-015-040">
<description>&lt;b&gt;BIPOLAR ELECTROLYTIC&lt;/b&gt;&lt;p&gt;
1.5 mm lead spacing, 4 mm diameter, grid 0.0125 inch</description>
<circle x="0.1588" y="0" radius="1.8581" width="0.2032" layer="21"/>
<pad name="1" x="-0.635" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R90"/>
<pad name="2" x="0.9525" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R90"/>
<text x="2.2225" y="-1.9049" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="2.2225" y="0.635" size="1.27" layer="25" ratio="14">&gt;NAME</text>
</package>
<package name="ENP-020-050">
<description>&lt;b&gt;BIPOLAR ELECTROLYTIC&lt;/b&gt;&lt;p&gt;
2.0 mm lead spacing, 5 mm diameter, grid 0.0125 inch</description>
<circle x="0" y="0" radius="2.7127" width="0.2032" layer="21"/>
<pad name="1" x="-0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<pad name="2" x="0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<text x="2.8575" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="2.8575" y="1.27" size="1.27" layer="25" ratio="14">&gt;NAME</text>
</package>
<package name="ENP-025-060">
<description>&lt;b&gt;BIPOLAR ELECTROLYTIC&lt;/b&gt;&lt;p&gt;
2.5 mm lead spacing, 6 mm diameter, grid 0.0125 inch</description>
<circle x="0" y="0" radius="3.375" width="0.2032" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="3.175" y="1.905" size="1.27" layer="25" ratio="14">&gt;NAME</text>
<text x="3.175" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ENP-035-080">
<description>&lt;b&gt;BIPOLAR ELECTROLYTIC&lt;/b&gt;&lt;p&gt;
3.5 mm lead spacing, 8 mm diameter, grid 0.0125 inch</description>
<circle x="0" y="0" radius="4.1" width="0.2032" layer="51"/>
<pad name="1" x="-1.75" y="0" drill="0.8" diameter="1.95"/>
<pad name="2" x="1.75" y="0" drill="0.8" diameter="1.95"/>
<text x="3.91" y="-3.4925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="3.91" y="2.2225" size="1.27" layer="25" ratio="14">&gt;NAME</text>
<text x="0" y="0" size="0.889" layer="21" ratio="20" rot="R90" align="center">bipol</text>
<wire x1="2.5" y1="3.2" x2="2.5" y2="-3.2" width="0.2032" layer="21" curve="-104.002535"/>
<wire x1="-2.5" y1="-3.2" x2="-2.5" y2="3.2" width="0.2032" layer="21" curve="-104.002535"/>
<wire x1="-0.6" y1="4.1" x2="0.6" y2="4.1" width="0.2032" layer="21" curve="-16.651301"/>
<wire x1="0.6" y1="-4.1" x2="-0.6" y2="-4.1" width="0.2032" layer="21" curve="-16.651301"/>
</package>
<package name="SV-B">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic B&lt;p&gt; 
4 mm dia, grid 0.0125 inch</description>
<wire x1="2.2862" y1="2.2862" x2="2.2862" y2="0.7938" width="0.2032" layer="21"/>
<wire x1="2.2862" y1="-0.7938" x2="2.2862" y2="-2.2863" width="0.2032" layer="21"/>
<wire x1="2.2862" y1="-2.2863" x2="-1.54" y2="-2.2863" width="0.2032" layer="21"/>
<wire x1="-1.54" y1="-2.2863" x2="-2.2863" y2="-1.5401" width="0.2032" layer="21"/>
<wire x1="-2.2863" y1="-1.5401" x2="-2.2863" y2="-0.7938" width="0.2032" layer="21"/>
<wire x1="2.2862" y1="2.2862" x2="-1.54" y2="2.2863" width="0.2032" layer="21"/>
<wire x1="-1.54" y1="2.2863" x2="-2.2863" y2="1.5401" width="0.2032" layer="21"/>
<wire x1="-2.2863" y1="1.5401" x2="-2.2862" y2="0.7938" width="0.2032" layer="21"/>
<wire x1="1.24" y1="1.7" x2="1.24" y2="-1.7" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="2.1298" width="0.1016" layer="51"/>
<smd name="+" x="-1.905" y="0" dx="2.1844" dy="1.0668" layer="1"/>
<smd name="-" x="1.905" y="0" dx="2.1844" dy="1.0668" layer="1"/>
<text x="2.8575" y="0.9525" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="2.8575" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.6988" y1="-0.3175" x2="-2.0638" y2="0.3175" layer="51"/>
<rectangle x1="2.0638" y1="-0.3175" x2="2.6988" y2="0.3175" layer="51"/>
<rectangle x1="-0.3175" y1="-0.635" x2="0.3175" y2="0.635" layer="35"/>
</package>
<package name="SV-C">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic C&lt;p&gt; 
5 mm dia, grid 0.0125 inch</description>
<wire x1="2.6988" y1="2.6988" x2="2.6988" y2="0.9525" width="0.2032" layer="21"/>
<wire x1="2.6987" y1="-0.9525" x2="2.6987" y2="-2.6988" width="0.2032" layer="21"/>
<wire x1="2.6987" y1="-2.6988" x2="-1.7463" y2="-2.6988" width="0.2032" layer="21"/>
<wire x1="-1.7463" y1="-2.6988" x2="-2.6988" y2="-1.7463" width="0.2032" layer="21"/>
<wire x1="-2.6988" y1="-1.7463" x2="-2.6988" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="2.6988" x2="-1.7463" y2="2.6988" width="0.2032" layer="21"/>
<wire x1="-1.7463" y1="2.6988" x2="-2.6988" y2="1.7462" width="0.2032" layer="21"/>
<wire x1="-2.6988" y1="1.7462" x2="-2.6988" y2="0.9525" width="0.2032" layer="21"/>
<wire x1="1.5337" y1="1.9587" x2="1.5337" y2="-1.9588" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="2.5597" width="0.1016" layer="51"/>
<smd name="+" x="-2.2225" y="0" dx="2.54" dy="1.27" layer="1"/>
<smd name="-" x="2.2225" y="0" dx="2.54" dy="1.27" layer="1"/>
<text x="3.175" y="1.27" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="3.175" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.175" y1="-0.3175" x2="-2.54" y2="0.3175" layer="51"/>
<rectangle x1="2.54" y1="-0.3175" x2="3.175" y2="0.3175" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="SV-D">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic D&lt;p&gt; 
6.3 mm dia, grid 0.0125 inch</description>
<wire x1="3.3337" y1="3.3338" x2="3.3337" y2="0.9525" width="0.2032" layer="21"/>
<wire x1="3.3338" y1="-0.9525" x2="3.3338" y2="-3.3338" width="0.2032" layer="21"/>
<wire x1="3.3338" y1="-3.3338" x2="-2.0637" y2="-3.3337" width="0.2032" layer="21"/>
<wire x1="-2.0637" y1="-3.3337" x2="-3.3338" y2="-2.0637" width="0.2032" layer="21"/>
<wire x1="-3.3338" y1="-2.0637" x2="-3.3338" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="3.3337" y1="3.3338" x2="-2.0637" y2="3.3338" width="0.2032" layer="21"/>
<wire x1="-2.0637" y1="3.3338" x2="-3.3338" y2="2.0638" width="0.2032" layer="21"/>
<wire x1="-3.3338" y1="2.0638" x2="-3.3338" y2="0.9525" width="0.2032" layer="21"/>
<wire x1="1.9687" y1="2.4" x2="1.9687" y2="-2.4" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.127" width="0.1016" layer="51"/>
<smd name="+" x="-2.54" y="0" dx="3.302" dy="1.27" layer="1"/>
<smd name="-" x="2.54" y="0" dx="3.302" dy="1.27" layer="1"/>
<text x="3.81" y="1.5875" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="3.81" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3175" x2="3.81" y2="0.3175" layer="51"/>
<rectangle x1="-3.81" y1="-0.3175" x2="-3.175" y2="0.3175" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="CNP-">
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="1.5875" x2="0.635" y2="0" width="0.4064" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.5875" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="1.5875" x2="-0.635" y2="0" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.5875" width="0.4064" layer="94"/>
<text x="1.905" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-4.1275" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.794" y="-1.27" size="0.8636" layer="93">1</text>
<text x="2.286" y="-1.27" size="0.8636" layer="93">2</text>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CBP-" prefix="C" uservalue="yes">
<description>&lt;B&gt;BIPOLAR CAP&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="CNP-" x="0" y="0"/>
</gates>
<devices>
<device name="ENP-010-030" package="ENP-010-030">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ENP-015-040" package="ENP-015-040">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ENP-020-050" package="ENP-020-050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ENP-025-060" package="ENP-025-060">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ENP-035-080" package="ENP-035-080">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-B" package="SV-B">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-C" package="SV-C">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-D" package="SV-D">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U3" library="rakettitiede" deviceset="CSD95379Q3M-HOMETCH" device="" value="CSD95379Q3M"/>
<part name="U2" library="rakettitiede" deviceset="CSD95379Q3M-HOMETCH" device="" value="CSD95379Q3M"/>
<part name="Q5" library="rakettitiede" deviceset="DMC3025LSD" device=""/>
<part name="Q4" library="rakettitiede" deviceset="DMC3025LSD" device=""/>
<part name="C26" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.47u"/>
<part name="C18" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.47u"/>
<part name="GND25" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND26" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND23" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND24" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="Q1" library="microbuilder" deviceset="MOSFET-P" device="TO252" value="DMP4047SK3-13"/>
<part name="U1" library="lio" deviceset="BD48XXX" device="" value="BD4853G"/>
<part name="R2" library="dp_devices" deviceset="RESISTOR" device="-0603" value="10k"/>
<part name="R1" library="dp_devices" deviceset="RESISTOR" device="-0603" value="47k"/>
<part name="R18" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="R20" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="R19" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="R21" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="R14" library="dp_devices" deviceset="RESISTOR" device="-0603" value="TBD"/>
<part name="Q7" library="microbuilder" deviceset="MOSFET-N" device="REFLOW" value="BSS138"/>
<part name="D6" library="microbuilder" deviceset="DIODE" device="SOD-323" value="1N4148"/>
<part name="D8" library="microbuilder" deviceset="DIODE" device="SOD-323" value="1N4148"/>
<part name="D7" library="microbuilder" deviceset="DIODE" device="SOD-323" value="1N4148"/>
<part name="D9" library="microbuilder" deviceset="DIODE" device="SOD-323" value="1N4148"/>
<part name="C5" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.1u"/>
<part name="C6" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.1u"/>
<part name="C3" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.1u"/>
<part name="C2" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="18p"/>
<part name="C1" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="18p"/>
<part name="R16" library="dp_devices" deviceset="RESISTOR" device="-0603" value="TBD"/>
<part name="JP3" library="microbuilder" deviceset="HEADER-1X2" device="" value="9VAC OUTPUT"/>
<part name="JP2" library="microbuilder" deviceset="HEADER-1X2" device="" value="5VDC OUTPUT"/>
<part name="GND11" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND3" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND4" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND6" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND7" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND8" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND9" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND10" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="PRG" library="prg_header" deviceset="ISPTOUCH" device="_HEADER"/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3P-LOC" device=""/>
<part name="J2" library="10118194-0001LF" deviceset="10118194-0001LF" device=""/>
<part name="J1" library="rakettitiede" deviceset="POWER_JACK" device="" value="BARREL 5.5MM"/>
<part name="JP1" library="microbuilder" deviceset="HEADER-1X2" device="ROUND" value="50/60Hz"/>
<part name="R3" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="C19" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.47u"/>
<part name="C27" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.47u"/>
<part name="Q3" library="BSS84LT1" deviceset="BSS84LT1" device="" value="BSS84"/>
<part name="Q2" library="BSS84LT1" deviceset="BSS84LT1" device="" value="BSS84"/>
<part name="IC3" library="rakettitiede" deviceset="PAM2421AECADJR" device=""/>
<part name="IC4" library="rakettitiede" deviceset="TL431AIDBZTG4" device="" value="ATL431"/>
<part name="L4" library="rakettitiede" deviceset="SRF1280-100M" device=""/>
<part name="L5" library="rakettitiede" deviceset="SRF1280-100M" device=""/>
<part name="IC2" library="rakettitiede" deviceset="TLP291-4(GB-TP,E)" device=""/>
<part name="IC1" library="rakettitiede" deviceset="ATTINY24A-SSU" device="">
<attribute name="SPICEPREFIX" value="X"/>
</part>
<part name="Y1" library="rakettitiede" deviceset="ECS-192-8-33-JGN-TR" device="" value="ECS-192-8-33-JGN-TR"/>
<part name="C4" library="A750MS108M1AAAE013" deviceset="A750MS108M1AAAE013" device="" value="1000u"/>
<part name="L3" library="rakettitiede" deviceset="NR8040T220M" device="" value="22u"/>
<part name="D1" library="rakettitiede" deviceset="B340-13-F" device=""/>
<part name="D5" library="rakettitiede" deviceset="B340-13-F" device="" value="SCHOTTKY"/>
<part name="D3" library="rakettitiede" deviceset="B340-13-F" device="" value="SCHOTTKY"/>
<part name="D2" library="rakettitiede" deviceset="B340-13-F" device="" value="SCHOTTKY"/>
<part name="D4" library="rakettitiede" deviceset="B340-13-F" device="" value="SCHOTTKY"/>
<part name="L1" library="rakettitiede" deviceset="NR8040T220M" device="" value="22u"/>
<part name="C24" library="A750KS477M1CAAE013" deviceset="A750KS477M1CAAE013" device="" value="470u"/>
<part name="C9" library="A750KS477M1CAAE013" deviceset="A750KS477M1CAAE013" device="" value="470u"/>
<part name="L2" library="rakettitiede" deviceset="NR8040T220M" device="" value="22u"/>
<part name="C11" library="A750KS477M1CAAE013" deviceset="A750KS477M1CAAE013" device="" value="470u"/>
<part name="GND5" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND2" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND1" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C28" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C29" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C30" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C31" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C20" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C21" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C22" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C23" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C12" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C10" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="Q6" library="microbuilder" deviceset="MOSFET-N" device="REFLOW" value="BSS138"/>
<part name="C7" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C8" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="GND-ISO6" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO7" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO5" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="ISO_GND" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO9" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="C13" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="1u"/>
<part name="GND15" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND17" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND18" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND16" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C15" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="47p"/>
<part name="R11" library="dp_devices" deviceset="RESISTOR" device="-0603" value="51k"/>
<part name="C16" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="2.2n"/>
<part name="GND22" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C14" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="0.1u"/>
<part name="GND20" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND19" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND13" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND14" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R4" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="R5" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="GND12" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R7" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="R6" library="dp_devices" deviceset="RESISTOR" device="-0603" value="1k"/>
<part name="GND-ISO1" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO2" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="TP4" library="SparkFun-Passives" deviceset="TEST-POINT" device="3" value=""/>
<part name="TP6" library="SparkFun-Passives" deviceset="TEST-POINT" device="3" value=""/>
<part name="TP1" library="SparkFun-Passives" deviceset="TEST-POINT" device="3" value=""/>
<part name="TP2" library="SparkFun-Passives" deviceset="TEST-POINT" device="3" value=""/>
<part name="TP3" library="SparkFun-Passives" deviceset="TEST-POINT" device="3" value=""/>
<part name="TP5" library="SparkFun-Passives" deviceset="TEST-POINT" device="3" value=""/>
<part name="TP7" library="SparkFun-Passives" deviceset="TEST-POINT" device="3" value=""/>
<part name="R9" library="dp_devices" deviceset="RESISTOR" device="-0603" value="TBD"/>
<part name="R8" library="dp_devices" deviceset="RESISTOR" device="-0603" value="TBD"/>
<part name="R10" library="dp_devices" deviceset="RESISTOR" device="-0603" value="TBD"/>
<part name="GND21" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND-ISO3" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="GND-ISO4" library="SparkFun-Aesthetics" deviceset="GND-ISO" device="" value="ISO_GND"/>
<part name="R15" library="dp_devices" deviceset="RESISTOR" device="-0603" value="TBD"/>
<part name="C17" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0603" value="TBD"/>
<part name="R12" library="dp_devices" deviceset="RESISTOR" device="-0603" value="TBD"/>
<part name="R13" library="dp_devices" deviceset="RESISTOR" device="-0603" value="TBD"/>
<part name="R17" library="dp_devices" deviceset="RESISTOR" device="-0603" value="10k"/>
<part name="C25" library="cap-master" deviceset="CBP-" device="ENP-035-080" value="100u"/>
<part name="C32" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
<part name="C33" library="rakettitiede" deviceset="CL32B106KBJNNNE" device="" value="10u"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="60.96" y1="146.05" x2="214.63" y2="146.05" width="0.1524" layer="94" style="shortdash"/>
<wire x1="214.63" y1="146.05" x2="214.63" y2="0" width="0.1524" layer="94" style="shortdash"/>
<wire x1="214.63" y1="0" x2="60.96" y2="0" width="0.1524" layer="94" style="shortdash"/>
<wire x1="60.96" y1="0" x2="60.96" y2="146.05" width="0.1524" layer="94" style="shortdash"/>
<wire x1="66.04" y1="285.75" x2="215.9" y2="285.75" width="0.1524" layer="94"/>
<wire x1="215.9" y1="285.75" x2="215.9" y2="229.87" width="0.1524" layer="94"/>
<wire x1="215.9" y1="229.87" x2="66.04" y2="229.87" width="0.1524" layer="94"/>
<wire x1="66.04" y1="229.87" x2="66.04" y2="285.75" width="0.1524" layer="94"/>
<text x="68.58" y="281.94" size="1.778" layer="94">Optoisolator</text>
<wire x1="-31.75" y1="345.44" x2="60.96" y2="345.44" width="0.1524" layer="94"/>
<wire x1="60.96" y1="345.44" x2="60.96" y2="270.51" width="0.1524" layer="94"/>
<wire x1="60.96" y1="270.51" x2="-31.75" y2="270.51" width="0.1524" layer="94"/>
<wire x1="-31.75" y1="270.51" x2="-31.75" y2="345.44" width="0.1524" layer="94"/>
<text x="-30.48" y="341.63" size="1.778" layer="94">5VDC input/output, overvoltage protection</text>
<wire x1="60.96" y1="146.05" x2="-31.75" y2="146.05" width="0.1524" layer="94"/>
<wire x1="-31.75" y1="146.05" x2="-31.75" y2="0" width="0.1524" layer="94"/>
<text x="-29.21" y="142.24" size="1.778" layer="94">Power stages</text>
<text x="60.96" y="142.24" size="1.778" layer="94">Isolated side: Rectifier, H-Bridges + LC-filter sine generator</text>
<wire x1="66.04" y1="345.44" x2="215.9" y2="345.44" width="0.1524" layer="94"/>
<wire x1="215.9" y1="345.44" x2="215.9" y2="289.56" width="0.1524" layer="94"/>
<wire x1="215.9" y1="289.56" x2="66.04" y2="289.56" width="0.1524" layer="94"/>
<wire x1="66.04" y1="289.56" x2="66.04" y2="345.44" width="0.1524" layer="94"/>
<text x="68.58" y="341.63" size="1.778" layer="94">Main controller MCU</text>
<wire x1="-31.75" y1="266.7" x2="60.96" y2="266.7" width="0.1524" layer="94"/>
<wire x1="60.96" y1="266.7" x2="60.96" y2="229.87" width="0.1524" layer="94"/>
<wire x1="60.96" y1="229.87" x2="-31.75" y2="229.87" width="0.1524" layer="94"/>
<wire x1="-31.75" y1="229.87" x2="-31.75" y2="266.7" width="0.1524" layer="94"/>
<text x="-29.21" y="262.89" size="1.778" layer="94">MCU programming header</text>
<text x="127" y="-27.94" size="1.4224" layer="94">NepaPSU is a 5VDC to 5VDC &amp; 9VAC
power supply unit for C64

(C) 2018 Jari Tulilahti &amp; Peter Mitchell
CERN OHL, see LICENSE for more info
</text>
<text x="-19.05" y="307.34" size="1.778" layer="95">MICROUSB</text>
<text x="-15.24" y="13.97" size="1.778" layer="97">10uF caps:
CL32B106KOJNNNE</text>
<text x="-13.97" y="72.39" size="1.778" layer="97">10uF caps:
CL32B106KOJNNNE</text>
<text x="170.18" y="292.1" size="1.778" layer="94">50/60Hz select jumper
(closed = 50Hz)</text>
<text x="102.87" y="86.36" size="2.54" layer="94">+</text>
<text x="78.74" y="86.36" size="2.54" layer="94">-</text>
<text x="174.498" y="336.55" size="1.778" layer="94">XTAL 19.2MHz</text>
<text x="137.16" y="62.23" size="1.524" layer="94">Low-pass filter.
Either populate
C25 or C32 &amp; C33</text>
<text x="193.04" y="11.43" size="1.778" layer="94" rot="MR0">SPWM H-Bridge</text>
<text x="93.98" y="93.98" size="1.778" layer="94">Schottky diode
bridge rectifier</text>
<wire x1="-31.75" y1="0" x2="60.96" y2="0" width="0.1524" layer="94"/>
<wire x1="166.37" y1="139.7" x2="170.18" y2="139.7" width="0.1524" layer="94"/>
<wire x1="170.18" y1="139.7" x2="170.18" y2="143.51" width="0.1524" layer="94"/>
<wire x1="170.18" y1="143.51" x2="171.45" y2="143.51" width="0.1524" layer="94"/>
<wire x1="171.45" y1="143.51" x2="171.45" y2="139.7" width="0.1524" layer="94"/>
<wire x1="171.45" y1="139.7" x2="173.99" y2="139.7" width="0.1524" layer="94"/>
<wire x1="173.99" y1="139.7" x2="173.99" y2="143.51" width="0.1524" layer="94"/>
<wire x1="173.99" y1="143.51" x2="176.53" y2="143.51" width="0.1524" layer="94"/>
<wire x1="176.53" y1="143.51" x2="176.53" y2="139.7" width="0.1524" layer="94"/>
<wire x1="176.53" y1="139.7" x2="177.8" y2="139.7" width="0.1524" layer="94"/>
<wire x1="177.8" y1="139.7" x2="177.8" y2="143.51" width="0.1524" layer="94"/>
<wire x1="177.8" y1="143.51" x2="181.61" y2="143.51" width="0.1524" layer="94"/>
<wire x1="181.61" y1="143.51" x2="181.61" y2="139.7" width="0.1524" layer="94"/>
<wire x1="181.61" y1="143.51" x2="184.15" y2="143.51" width="0.1524" layer="94"/>
<wire x1="184.15" y1="143.51" x2="184.15" y2="139.7" width="0.1524" layer="94"/>
<wire x1="184.15" y1="139.7" x2="185.42" y2="139.7" width="0.1524" layer="94"/>
<wire x1="185.42" y1="139.7" x2="185.42" y2="143.51" width="0.1524" layer="94"/>
<wire x1="185.42" y1="143.51" x2="186.69" y2="143.51" width="0.1524" layer="94"/>
<wire x1="186.69" y1="143.51" x2="186.69" y2="139.7" width="0.1524" layer="94"/>
<wire x1="166.37" y1="134.62" x2="193.04" y2="134.62" width="0.1524" layer="94"/>
<wire x1="193.04" y1="134.62" x2="193.04" y2="138.43" width="0.1524" layer="94"/>
<wire x1="193.04" y1="138.43" x2="194.31" y2="138.43" width="0.1524" layer="94"/>
<wire x1="194.31" y1="138.43" x2="194.31" y2="134.62" width="0.1524" layer="94"/>
<wire x1="194.31" y1="134.62" x2="196.85" y2="134.62" width="0.1524" layer="94"/>
<wire x1="196.85" y1="134.62" x2="196.85" y2="138.43" width="0.1524" layer="94"/>
<wire x1="196.85" y1="138.43" x2="199.39" y2="138.43" width="0.1524" layer="94"/>
<wire x1="199.39" y1="138.43" x2="199.39" y2="134.62" width="0.1524" layer="94"/>
<wire x1="199.39" y1="134.62" x2="200.66" y2="134.62" width="0.1524" layer="94"/>
<wire x1="200.66" y1="134.62" x2="200.66" y2="138.43" width="0.1524" layer="94"/>
<wire x1="200.66" y1="138.43" x2="204.47" y2="138.43" width="0.1524" layer="94"/>
<wire x1="204.47" y1="134.62" x2="204.47" y2="138.43" width="0.1524" layer="94"/>
<wire x1="204.47" y1="138.43" x2="207.01" y2="138.43" width="0.1524" layer="94"/>
<wire x1="207.01" y1="138.43" x2="207.01" y2="134.62" width="0.1524" layer="94"/>
<wire x1="207.01" y1="134.62" x2="208.28" y2="134.62" width="0.1524" layer="94"/>
<wire x1="208.28" y1="134.62" x2="208.28" y2="138.43" width="0.1524" layer="94"/>
<wire x1="208.28" y1="138.43" x2="209.55" y2="138.43" width="0.1524" layer="94"/>
<wire x1="209.55" y1="138.43" x2="209.55" y2="134.62" width="0.1524" layer="94"/>
<wire x1="209.55" y1="134.62" x2="212.09" y2="134.62" width="0.1524" layer="94"/>
<wire x1="186.69" y1="139.7" x2="212.09" y2="139.7" width="0.1524" layer="94"/>
<text x="157.48" y="140.97" size="1.778" layer="94">SPWM1</text>
<text x="157.48" y="135.89" size="1.778" layer="94">SPWM2</text>
<wire x1="-31.75" y1="226.06" x2="-31.75" y2="149.86" width="0.1524" layer="94"/>
<wire x1="-31.75" y1="149.86" x2="214.63" y2="149.86" width="0.1524" layer="94"/>
<wire x1="214.63" y1="149.86" x2="214.63" y2="226.06" width="0.1524" layer="94"/>
<wire x1="214.63" y1="226.06" x2="-31.75" y2="226.06" width="0.1524" layer="94"/>
<text x="-29.21" y="220.98" size="1.778" layer="94">5VDC to 14VDC boost circuit</text>
<wire x1="181.61" y1="280.67" x2="212.09" y2="280.67" width="0.1524" layer="94" style="shortdash"/>
<wire x1="212.09" y1="280.67" x2="212.09" y2="247.65" width="0.1524" layer="94" style="shortdash"/>
<wire x1="212.09" y1="247.65" x2="181.61" y2="247.65" width="0.1524" layer="94" style="shortdash"/>
<wire x1="181.61" y1="247.65" x2="181.61" y2="280.67" width="0.1524" layer="94" style="shortdash"/>
<text x="110.49" y="189.23" size="1.778" layer="94">Feedback from TL431
resistor values TBD</text>
<wire x1="68.58" y1="252.73" x2="68.58" y2="238.76" width="0.1524" layer="94" style="shortdash"/>
<wire x1="68.58" y1="238.76" x2="104.14" y2="238.76" width="0.1524" layer="94" style="shortdash"/>
<wire x1="104.14" y1="238.76" x2="104.14" y2="252.73" width="0.1524" layer="94" style="shortdash"/>
<wire x1="104.14" y1="252.73" x2="68.58" y2="252.73" width="0.1524" layer="94" style="shortdash"/>
<text x="142.24" y="97.79" size="1.778" layer="94">Feedback and
voltage compensation</text>
<wire x1="205.74" y1="71.12" x2="208.28" y2="71.12" width="0.1524" layer="94" curve="-180"/>
<wire x1="210.82" y1="71.12" x2="208.28" y2="71.12" width="0.1524" layer="94" curve="-180"/>
<wire x1="205.74" y1="68.58" x2="210.82" y2="68.58" width="0.1524" layer="94"/>
<text x="-30.48" y="-2.54" size="1.778" layer="94" rot="MR180">D1, D2, D3, D4, D5: Diodes incorporated B340-13-F
C7, C8, C10, C12, C20, C21, C22, C23, C28, C29, C30, C31: Samsung CL32B106KOJNNNE
C9, C11, C24: Kemet A750KS477M1CAAE013
C4: Kemet A750MS108M1AAAE013
C25: Nichicon UVP1E101MPD1TD
L1, L2, L3: Taiyo Yuden NR8040T220M
J1: CUI inc PJ-047AH
J2: Amphenol 10118194-0001LF






</text>
<text x="184.658" y="62.23" size="1.778" layer="94">R17 = bleed</text>
</plain>
<instances>
<instance part="U3" gate="G$1" x="19.05" y="44.45" rot="MR0">
<attribute name="NAME" x="29.21" y="63.5" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="29.21" y="60.96" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="U2" gate="G$1" x="20.32" y="102.87" rot="MR0">
<attribute name="NAME" x="30.48" y="121.92" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="30.48" y="119.38" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="Q5" gate="G$1" x="180.34" y="34.29">
<attribute name="NAME" x="173.99" y="51.054" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.99" y="48.006" size="1.778" layer="96"/>
</instance>
<instance part="Q4" gate="G$1" x="100.33" y="34.29" rot="MR0">
<attribute name="NAME" x="106.68" y="51.054" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="106.68" y="48.006" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C26" gate="C" x="-3.81" y="46.99" rot="R90">
<attribute name="NAME" x="-3.81" y="52.07" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-1.27" y="44.45" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C18" gate="C" x="-2.54" y="105.41" rot="R90">
<attribute name="NAME" x="-2.54" y="110.49" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="0" y="102.87" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND25" gate="1" x="19.05" y="13.97">
<attribute name="VALUE" x="16.51" y="11.43" size="1.778" layer="96"/>
</instance>
<instance part="GND26" gate="1" x="41.91" y="13.97">
<attribute name="VALUE" x="39.37" y="11.43" size="1.778" layer="96"/>
</instance>
<instance part="GND23" gate="1" x="20.32" y="72.39">
<attribute name="VALUE" x="17.78" y="69.85" size="1.778" layer="96"/>
</instance>
<instance part="GND24" gate="1" x="43.18" y="72.39">
<attribute name="VALUE" x="40.64" y="69.85" size="1.778" layer="96"/>
</instance>
<instance part="Q1" gate="G$1" x="35.56" y="332.74">
<attribute name="VALUE" x="36.83" y="327.66" size="1.778" layer="96"/>
<attribute name="NAME" x="36.83" y="336.55" size="1.778" layer="95"/>
</instance>
<instance part="U1" gate="G$1" x="40.64" y="312.42" rot="R270">
<attribute name="NAME" x="35.56" y="320.802" size="1.778" layer="95"/>
<attribute name="VALUE" x="35.56" y="302.26" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="74.93" y="328.93">
<attribute name="NAME" x="71.12" y="330.4286" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="71.12" y="325.628" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R1" gate="G$1" x="22.86" y="332.74">
<attribute name="NAME" x="19.05" y="334.2386" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="19.05" y="329.438" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R18" gate="G$1" x="128.27" y="44.45" rot="MR0">
<attribute name="NAME" x="125.73" y="44.9326" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="131.318" y="45.212" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R20" gate="G$1" x="128.27" y="29.21" rot="MR0">
<attribute name="NAME" x="125.73" y="29.6926" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="131.572" y="29.718" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R19" gate="G$1" x="151.13" y="44.45">
<attribute name="NAME" x="148.59" y="44.9326" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="154.178" y="44.958" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R21" gate="G$1" x="151.13" y="29.21">
<attribute name="NAME" x="148.59" y="29.6926" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="154.432" y="29.718" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R14" gate="G$1" x="205.74" y="119.38" rot="R270">
<attribute name="NAME" x="207.01" y="120.8786" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="207.01" y="116.078" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="Q7" gate="G$1" x="165.1" y="17.78" rot="MR0">
<attribute name="NAME" x="162.052" y="20.955" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="167.64" y="10.668" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="D6" gate="G$1" x="128.27" y="38.1" rot="MR0">
<attribute name="NAME" x="127" y="38.608" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="123.698" y="36.576" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="D8" gate="G$1" x="128.27" y="22.86">
<attribute name="NAME" x="127" y="23.368" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="123.444" y="19.558" size="1.778" layer="96"/>
</instance>
<instance part="D7" gate="G$1" x="151.13" y="38.1">
<attribute name="NAME" x="149.86" y="38.608" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="146.304" y="34.798" size="1.778" layer="96"/>
</instance>
<instance part="D9" gate="G$1" x="151.13" y="22.86" rot="MR0">
<attribute name="NAME" x="149.86" y="23.368" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="156.21" y="19.558" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C5" gate="C" x="19.05" y="284.48">
<attribute name="NAME" x="18.542" y="289.814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="19.05" y="283.464" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C6" gate="C" x="29.21" y="284.48">
<attribute name="NAME" x="28.702" y="289.814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="29.21" y="283.464" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C3" gate="C" x="69.85" y="306.07">
<attribute name="NAME" x="70.358" y="309.118" size="1.778" layer="95"/>
<attribute name="VALUE" x="70.612" y="303.784" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="C" x="200.66" y="327.66" rot="R180">
<attribute name="NAME" x="203.2" y="326.39" size="1.778" layer="95"/>
<attribute name="VALUE" x="203.2" y="323.85" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="C" x="167.64" y="327.66" rot="R180">
<attribute name="NAME" x="165.1" y="327.66" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="165.1" y="325.12" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R16" gate="G$1" x="205.74" y="92.71" rot="R270">
<attribute name="NAME" x="207.01" y="94.2086" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="207.01" y="89.408" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="JP3" gate="G$1" x="200.66" y="68.58">
<attribute name="NAME" x="195.58" y="74.295" size="1.778" layer="95"/>
<attribute name="VALUE" x="190.5" y="77.47" size="1.778" layer="96"/>
</instance>
<instance part="JP2" gate="G$1" x="48.26" y="289.56">
<attribute name="NAME" x="41.91" y="295.275" size="1.778" layer="95"/>
<attribute name="VALUE" x="41.91" y="284.48" size="1.778" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="39.37" y="276.86" rot="MR0">
<attribute name="VALUE" x="41.91" y="274.32" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND3" gate="1" x="7.62" y="302.26">
<attribute name="VALUE" x="5.08" y="299.72" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="17.78" y="302.26" rot="MR0">
<attribute name="VALUE" x="20.32" y="299.72" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND6" gate="1" x="-12.7" y="276.86" rot="MR0">
<attribute name="VALUE" x="-10.16" y="274.32" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND7" gate="1" x="0" y="276.86" rot="MR0">
<attribute name="VALUE" x="2.54" y="274.32" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND8" gate="1" x="10.16" y="276.86" rot="MR0">
<attribute name="VALUE" x="12.7" y="274.32" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND9" gate="1" x="19.05" y="276.86" rot="MR0">
<attribute name="VALUE" x="21.59" y="274.32" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND10" gate="1" x="29.21" y="276.86" rot="MR0">
<attribute name="VALUE" x="31.75" y="274.32" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="PRG" gate="G$1" x="16.51" y="248.92">
<attribute name="NAME" x="12.192" y="254.762" size="1.778" layer="95"/>
<attribute name="VALUE" x="6.096" y="240.03" size="1.778" layer="96"/>
</instance>
<instance part="FRAME1" gate="G$1" x="-38.1" y="-35.56">
<attribute name="DRAWING_NAME" x="179.07" y="-20.32" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="179.07" y="-25.4" size="2.286" layer="94"/>
<attribute name="SHEET" x="192.405" y="-30.48" size="2.54" layer="94"/>
</instance>
<instance part="J2" gate="G$1" x="-12.7" y="314.96">
<attribute name="NAME" x="-20.08786875" y="320.564590625" size="1.778" layer="95"/>
</instance>
<instance part="J1" gate="G$1" x="-15.24" y="325.12">
<attribute name="VALUE" x="-25.4" y="335.28" size="1.778" layer="96"/>
<attribute name="NAME" x="-25.4" y="325.12" size="1.778" layer="95"/>
</instance>
<instance part="JP1" gate="G$1" x="201.93" y="303.53">
<attribute name="NAME" x="195.58" y="309.245" size="1.778" layer="95"/>
<attribute name="VALUE" x="195.58" y="298.45" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="185.42" y="307.34" rot="R180">
<attribute name="NAME" x="189.23" y="305.8414" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="189.23" y="310.642" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="C19" gate="C" x="43.18" y="105.41">
<attribute name="NAME" x="43.942" y="108.204" size="1.778" layer="95"/>
<attribute name="VALUE" x="43.942" y="103.632" size="1.778" layer="96"/>
</instance>
<instance part="C27" gate="C" x="41.91" y="46.99">
<attribute name="NAME" x="42.672" y="49.784" size="1.778" layer="95"/>
<attribute name="VALUE" x="42.418" y="44.704" size="1.778" layer="96"/>
</instance>
<instance part="Q3" gate="G$1" x="163.83" y="49.53" rot="MR0">
<attribute name="NAME" x="163.322" y="53.594" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="159.258" y="43.434" size="1.778" layer="96" rot="MR180" align="center-left"/>
</instance>
<instance part="Q2" gate="G$1" x="116.84" y="49.53">
<attribute name="NAME" x="117.348" y="53.594" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="112.268" y="43.434" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="IC3" gate="G$1" x="45.72" y="204.47">
<attribute name="NAME" x="52.07" y="186.69" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="49.784" y="208.788" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="IC4" gate="G$1" x="182.88" y="100.33">
<attribute name="NAME" x="179.07" y="107.95" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="181.61" y="90.17" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="L4" gate="G$1" x="59.69" y="71.12" rot="R90">
<attribute name="NAME" x="56.642" y="90.424" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="49.53" y="96.774" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="L5" gate="G$1" x="59.69" y="40.64" rot="R90">
<attribute name="NAME" x="56.388" y="60.198" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="49.53" y="34.036" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="IC2" gate="G$1" x="111.76" y="276.86">
<attribute name="NAME" x="116.84" y="280.67" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="116.84" y="255.27" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="IC1" gate="G$1" x="99.06" y="334.01">
<attribute name="NAME" x="104.14" y="339.09" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="104.14" y="295.91" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="Y1" gate="G$1" x="167.64" y="327.66">
<attribute name="NAME" x="171.45" y="319.278" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="167.64" y="334.01" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C4" gate="G$1" x="-12.7" y="292.1" rot="R270">
<attribute name="NAME" x="-13.208" y="288.798" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="-12.954" y="282.956" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="L3" gate="G$1" x="146.05" y="74.93">
<attribute name="NAME" x="147.32" y="77.47" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="146.05" y="72.898" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="D1" gate="G$1" x="91.44" y="215.9" rot="R180">
<attribute name="NAME" x="90.932" y="220.218" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="D5" gate="G$1" x="97.79" y="80.01" rot="R180">
<attribute name="NAME" x="97.79" y="81.788" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="D3" gate="G$1" x="97.79" y="90.17" rot="R180">
<attribute name="NAME" x="96.774" y="88.392" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="D2" gate="G$1" x="87.63" y="90.17" rot="R180">
<attribute name="NAME" x="86.614" y="88.392" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="D4" gate="G$1" x="87.63" y="80.01" rot="R180">
<attribute name="NAME" x="87.63" y="81.788" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="L1" gate="G$1" x="54.61" y="215.9">
<attribute name="NAME" x="55.88" y="218.694" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="55.626" y="214.122" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C24" gate="G$1" x="116.84" y="85.09" rot="R270">
<attribute name="NAME" x="117.348" y="83.058" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="117.348" y="75.692" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C9" gate="G$1" x="157.48" y="214.63" rot="R270">
<attribute name="NAME" x="158.75" y="213.36" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="158.496" y="204.978" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="L2" gate="G$1" x="106.68" y="85.09">
<attribute name="NAME" x="107.95" y="87.63" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="107.95" y="83.566" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C11" gate="G$1" x="13.97" y="200.66" rot="R270">
<attribute name="NAME" x="14.478" y="199.136" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="14.732" y="190.754" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND5" gate="1" x="69.85" y="295.91" rot="MR0">
<attribute name="VALUE" x="72.39" y="293.37" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND2" gate="1" x="200.66" y="318.77" rot="MR0">
<attribute name="VALUE" x="203.2" y="316.23" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND1" gate="1" x="167.64" y="318.77" rot="MR0">
<attribute name="VALUE" x="170.18" y="316.23" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C28" gate="G$1" x="-15.24" y="25.4" rot="R90">
<attribute name="NAME" x="-15.24" y="30.48" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="-15.24" y="25.4" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C29" gate="G$1" x="-8.89" y="25.4" rot="R90">
<attribute name="NAME" x="-8.89" y="30.48" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="-8.89" y="25.4" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C30" gate="G$1" x="-2.54" y="25.4" rot="R90">
<attribute name="NAME" x="-2.54" y="30.48" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="-2.54" y="25.4" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C31" gate="G$1" x="3.81" y="25.4" rot="R90">
<attribute name="NAME" x="3.81" y="30.48" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="3.81" y="25.4" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C20" gate="G$1" x="-13.97" y="85.09" rot="R90">
<attribute name="NAME" x="-13.97" y="90.17" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="-13.97" y="85.09" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C21" gate="G$1" x="-7.62" y="85.09" rot="R90">
<attribute name="NAME" x="-7.62" y="90.17" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="-7.62" y="85.09" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C22" gate="G$1" x="-1.27" y="85.09" rot="R90">
<attribute name="NAME" x="-1.27" y="90.17" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="-1.27" y="85.09" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C23" gate="G$1" x="5.08" y="85.09" rot="R90">
<attribute name="NAME" x="5.08" y="90.17" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="5.08" y="85.09" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C12" gate="G$1" x="22.86" y="196.85" rot="R270">
<attribute name="NAME" x="23.368" y="199.136" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="23.622" y="190.754" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C10" gate="G$1" x="148.59" y="210.82" rot="R270">
<attribute name="NAME" x="149.606" y="213.36" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="149.606" y="204.978" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="Q6" gate="G$1" x="114.3" y="17.78">
<attribute name="NAME" x="117.602" y="21.209" size="1.778" layer="95"/>
<attribute name="VALUE" x="112.014" y="10.668" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="0" y="283.21" rot="R90">
<attribute name="NAME" x="-0.508" y="288.798" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="0" y="282.956" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C8" gate="G$1" x="10.16" y="283.21" rot="R90">
<attribute name="NAME" x="9.652" y="288.798" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="10.16" y="282.448" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="GND-ISO6" gate="G$1" x="86.36" y="19.05" rot="MR0">
<attribute name="VALUE" x="83.82" y="19.05" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND-ISO7" gate="G$1" x="193.04" y="19.05" rot="MR0">
<attribute name="VALUE" x="190.5" y="19.05" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND-ISO5" gate="G$1" x="116.84" y="68.58" rot="MR0">
<attribute name="VALUE" x="114.3" y="68.58" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="ISO_GND" gate="G$1" x="116.84" y="7.62">
<attribute name="VALUE" x="119.38" y="7.62" size="1.778" layer="96"/>
</instance>
<instance part="GND-ISO9" gate="G$1" x="162.56" y="7.62" rot="MR0">
<attribute name="VALUE" x="160.02" y="7.62" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C13" gate="C" x="31.75" y="195.58" rot="R180">
<attribute name="NAME" x="32.512" y="198.374" size="1.778" layer="95"/>
<attribute name="VALUE" x="32.512" y="189.992" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="13.97" y="184.15">
<attribute name="VALUE" x="11.43" y="181.61" size="1.778" layer="96"/>
</instance>
<instance part="GND17" gate="1" x="31.75" y="184.15">
<attribute name="VALUE" x="29.21" y="181.61" size="1.778" layer="96"/>
</instance>
<instance part="GND18" gate="1" x="38.1" y="184.15">
<attribute name="VALUE" x="35.56" y="181.61" size="1.778" layer="96"/>
</instance>
<instance part="GND16" gate="1" x="22.86" y="184.15">
<attribute name="VALUE" x="20.32" y="181.61" size="1.778" layer="96"/>
</instance>
<instance part="C15" gate="C" x="43.18" y="172.72" rot="R180">
<attribute name="NAME" x="42.418" y="175.26" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="42.672" y="169.672" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R11" gate="G$1" x="50.8" y="176.53" rot="R90">
<attribute name="NAME" x="52.832" y="177.0126" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="52.578" y="173.99" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C16" gate="C" x="50.8" y="167.64" rot="MR180">
<attribute name="NAME" x="51.816" y="169.672" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="51.816" y="164.592" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GND22" gate="1" x="43.18" y="157.48">
<attribute name="VALUE" x="40.64" y="154.94" size="1.778" layer="96"/>
</instance>
<instance part="C14" gate="C" x="81.28" y="189.23" rot="MR180">
<attribute name="NAME" x="82.55" y="191.77" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="82.55" y="185.42" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GND20" gate="1" x="81.28" y="180.34">
<attribute name="VALUE" x="78.74" y="177.8" size="1.778" layer="96"/>
</instance>
<instance part="GND19" gate="1" x="60.96" y="180.34">
<attribute name="VALUE" x="58.42" y="177.8" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="148.59" y="198.12">
<attribute name="VALUE" x="146.05" y="195.58" size="1.778" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="157.48" y="198.12">
<attribute name="VALUE" x="154.94" y="195.58" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="102.87" y="276.86">
<attribute name="NAME" x="100.076" y="277.0886" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="106.172" y="277.114" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R5" gate="G$1" x="102.87" y="271.78">
<attribute name="NAME" x="100.076" y="272.0086" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="105.918" y="272.034" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND12" gate="1" x="72.39" y="259.08" rot="MR0">
<attribute name="VALUE" x="74.93" y="256.54" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R7" gate="G$1" x="194.31" y="262.89" rot="R270">
<attribute name="NAME" x="195.326" y="267.7414" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="VALUE" x="195.072" y="257.81" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R6" gate="G$1" x="189.23" y="262.89" rot="R270">
<attribute name="NAME" x="188.722" y="266.1666" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="188.468" y="259.588" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND-ISO1" gate="G$1" x="189.23" y="254" rot="MR0"/>
<instance part="GND-ISO2" gate="G$1" x="194.31" y="254">
<attribute name="VALUE" x="187.96" y="250.19" size="1.27" layer="96"/>
</instance>
<instance part="TP4" gate="G$1" x="110.49" y="261.62" rot="R180">
<attribute name="NAME" x="107.442" y="262.382" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="113.03" y="264.16" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="TP6" gate="G$1" x="110.49" y="259.08" rot="R180">
<attribute name="NAME" x="107.442" y="259.842" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="113.03" y="261.62" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="TP1" gate="G$1" x="86.36" y="311.15" rot="R180">
<attribute name="NAME" x="83.312" y="311.912" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="88.9" y="313.69" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="TP2" gate="G$1" x="86.36" y="306.07" rot="R180">
<attribute name="NAME" x="83.312" y="306.832" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="88.9" y="308.61" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="TP3" gate="G$1" x="86.36" y="303.53" rot="R180">
<attribute name="NAME" x="83.312" y="304.292" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="88.9" y="306.07" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="TP5" gate="G$1" x="156.21" y="261.62">
<attribute name="NAME" x="159.258" y="260.858" size="1.27" layer="95"/>
<attribute name="VALUE" x="153.67" y="259.08" size="1.778" layer="96"/>
</instance>
<instance part="TP7" gate="G$1" x="156.21" y="259.08">
<attribute name="NAME" x="159.258" y="258.318" size="1.27" layer="95"/>
<attribute name="VALUE" x="153.67" y="256.54" size="1.778" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="100.33" y="208.28" rot="R90">
<attribute name="NAME" x="102.362" y="208.7626" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="102.108" y="205.74" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R8" gate="G$1" x="158.75" y="243.84" rot="R180">
<attribute name="NAME" x="156.464" y="245.3386" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="156.464" y="242.316" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="R10" gate="G$1" x="100.33" y="191.77" rot="R90">
<attribute name="NAME" x="102.362" y="192.2526" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="102.108" y="189.23" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND21" gate="1" x="100.33" y="180.34">
<attribute name="VALUE" x="97.79" y="177.8" size="1.778" layer="96"/>
</instance>
<instance part="GND-ISO3" gate="G$1" x="182.88" y="83.82" rot="MR0">
<attribute name="VALUE" x="180.34" y="83.82" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND-ISO4" gate="G$1" x="205.74" y="83.82" rot="MR0">
<attribute name="VALUE" x="203.2" y="83.82" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R15" gate="G$1" x="199.39" y="109.22">
<attribute name="NAME" x="196.85" y="110.7186" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="197.104" y="107.696" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="C17" gate="C" x="190.5" y="109.22" rot="R90">
<attribute name="NAME" x="190.246" y="110.49" size="1.778" layer="95"/>
<attribute name="VALUE" x="190.246" y="108.458" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="R12" gate="G$1" x="171.45" y="120.65" rot="R270">
<attribute name="NAME" x="172.72" y="122.1486" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="172.72" y="117.348" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R13" gate="G$1" x="182.88" y="120.65" rot="R90">
<attribute name="NAME" x="184.15" y="122.1486" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="184.15" y="117.348" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R17" gate="G$1" x="186.69" y="69.85" rot="R270">
<attribute name="NAME" x="185.42" y="72.39" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="185.42" y="69.85" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="C25" gate="G$1" x="173.99" y="69.85" rot="R90">
<attribute name="NAME" x="174.625" y="71.755" size="1.778" layer="95"/>
<attribute name="VALUE" x="174.752" y="66.7385" size="1.778" layer="96"/>
</instance>
<instance part="C32" gate="G$1" x="166.37" y="72.39" rot="R270">
<attribute name="NAME" x="167.64" y="72.39" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="167.64" y="67.31" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C33" gate="G$1" x="162.56" y="72.39" rot="R270">
<attribute name="NAME" x="162.56" y="72.39" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="162.56" y="67.31" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="PSOUT2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VSW"/>
<wire x1="46.99" y1="36.83" x2="34.29" y2="36.83" width="0.1524" layer="91"/>
<wire x1="46.99" y1="36.83" x2="52.07" y2="36.83" width="0.1524" layer="91"/>
<wire x1="52.07" y1="68.58" x2="52.07" y2="64.77" width="0.1524" layer="91"/>
<wire x1="52.07" y1="64.77" x2="52.07" y2="36.83" width="0.1524" layer="91"/>
<wire x1="52.07" y1="68.58" x2="52.07" y2="95.25" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="3"/>
<wire x1="52.07" y1="95.25" x2="57.15" y2="95.25" width="0.1524" layer="91"/>
<wire x1="57.15" y1="95.25" x2="57.15" y2="93.98" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="3"/>
<wire x1="57.15" y1="63.5" x2="57.15" y2="64.77" width="0.1524" layer="91"/>
<wire x1="57.15" y1="64.77" x2="52.07" y2="64.77" width="0.1524" layer="91"/>
<junction x="52.07" y="64.77"/>
</segment>
</net>
<net name="BOOT" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BOOT"/>
<pinref part="C18" gate="C" pin="2"/>
<wire x1="5.08" y1="105.41" x2="0" y2="105.41" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BOOT_R" class="0">
<segment>
<pinref part="C18" gate="C" pin="1"/>
<wire x1="-7.62" y1="105.41" x2="-10.16" y2="105.41" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="105.41" x2="-10.16" y2="100.33" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="BOOT_R"/>
<wire x1="-10.16" y1="100.33" x2="5.08" y2="100.33" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PGND_PAD"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="20.32" y1="80.01" x2="20.32" y2="78.74" width="0.1524" layer="91"/>
<wire x1="20.32" y1="78.74" x2="20.32" y2="74.93" width="0.1524" layer="91"/>
<wire x1="5.08" y1="78.74" x2="20.32" y2="78.74" width="0.1524" layer="91"/>
<junction x="20.32" y="78.74"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="5.08" y1="78.74" x2="-1.27" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="78.74" x2="-7.62" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="78.74" x2="-13.97" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="78.74" x2="-13.97" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="83.82" x2="-7.62" y2="78.74" width="0.1524" layer="91"/>
<junction x="-7.62" y="78.74"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="-1.27" y1="83.82" x2="-1.27" y2="78.74" width="0.1524" layer="91"/>
<junction x="-1.27" y="78.74"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="5.08" y1="83.82" x2="5.08" y2="78.74" width="0.1524" layer="91"/>
<junction x="5.08" y="78.74"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PGND_PAD"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="19.05" y1="21.59" x2="19.05" y2="20.32" width="0.1524" layer="91"/>
<wire x1="19.05" y1="20.32" x2="19.05" y2="16.51" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="19.05" y1="20.32" x2="3.81" y2="20.32" width="0.1524" layer="91"/>
<wire x1="3.81" y1="20.32" x2="-2.54" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="20.32" x2="-8.89" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="20.32" x2="-15.24" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="20.32" x2="-15.24" y2="24.13" width="0.1524" layer="91"/>
<junction x="19.05" y="20.32"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="-8.89" y1="24.13" x2="-8.89" y2="20.32" width="0.1524" layer="91"/>
<junction x="-8.89" y="20.32"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="24.13" x2="-2.54" y2="20.32" width="0.1524" layer="91"/>
<junction x="-2.54" y="20.32"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="3.81" y1="24.13" x2="3.81" y2="20.32" width="0.1524" layer="91"/>
<junction x="3.81" y="20.32"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="PGND"/>
<wire x1="41.91" y1="16.51" x2="41.91" y2="41.91" width="0.1524" layer="91"/>
<wire x1="41.91" y1="41.91" x2="34.29" y2="41.91" width="0.1524" layer="91"/>
<pinref part="C27" gate="C" pin="2"/>
<wire x1="41.91" y1="41.91" x2="41.91" y2="44.45" width="0.1524" layer="91"/>
<junction x="41.91" y="41.91"/>
</segment>
<segment>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="U2" gate="G$1" pin="PGND"/>
<wire x1="43.18" y1="74.93" x2="43.18" y2="100.33" width="0.1524" layer="91"/>
<wire x1="43.18" y1="100.33" x2="35.56" y2="100.33" width="0.1524" layer="91"/>
<pinref part="C19" gate="C" pin="2"/>
<wire x1="43.18" y1="102.87" x2="43.18" y2="100.33" width="0.1524" layer="91"/>
<junction x="43.18" y="100.33"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="30.48" y1="307.34" x2="17.78" y2="307.34" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="17.78" y1="307.34" x2="17.78" y2="304.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="C" pin="2"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="19.05" y1="279.4" x2="19.05" y2="281.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="C" pin="2"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="29.21" y1="279.4" x2="29.21" y2="281.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="39.37" y1="279.4" x2="39.37" y2="289.56" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="2"/>
<wire x1="39.37" y1="289.56" x2="45.72" y2="289.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PRG" gate="G$1" pin="6"/>
<wire x1="26.67" y1="246.38" x2="30.48" y2="246.38" width="0.1524" layer="91"/>
<wire x1="30.48" y1="246.38" x2="30.48" y2="243.84" width="0.1524" layer="91"/>
<wire x1="30.48" y1="243.84" x2="31.75" y2="243.84" width="0.1524" layer="91"/>
<label x="31.75" y="243.84" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="199.39" y1="303.53" x2="191.77" y2="303.53" width="0.1524" layer="91"/>
<wire x1="191.77" y1="303.53" x2="191.77" y2="302.26" width="0.1524" layer="91"/>
<wire x1="191.77" y1="302.26" x2="179.07" y2="302.26" width="0.1524" layer="91"/>
<label x="179.07" y="302.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="C1" gate="C" pin="1"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="167.64" y1="322.58" x2="167.64" y2="321.31" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="GND_1"/>
<wire x1="168.91" y1="322.58" x2="167.64" y2="322.58" width="0.1524" layer="91"/>
<junction x="167.64" y="322.58"/>
</segment>
<segment>
<pinref part="C2" gate="C" pin="1"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="200.66" y1="322.58" x2="200.66" y2="321.31" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="GND_2"/>
<wire x1="199.39" y1="322.58" x2="200.66" y2="322.58" width="0.1524" layer="91"/>
<junction x="200.66" y="322.58"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="99.06" y1="300.99" x2="69.85" y2="300.99" width="0.1524" layer="91"/>
<wire x1="69.85" y1="300.99" x2="69.85" y2="298.45" width="0.1524" layer="91"/>
<pinref part="C3" gate="C" pin="2"/>
<wire x1="69.85" y1="303.53" x2="69.85" y2="300.99" width="0.1524" layer="91"/>
<junction x="69.85" y="300.99"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="10.16" y1="281.94" x2="10.16" y2="279.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="0" y1="279.4" x2="0" y2="281.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND"/>
<wire x1="-12.7" y1="327.66" x2="-8.89" y2="327.66" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="-8.89" y1="327.66" x2="7.62" y2="327.66" width="0.1524" layer="91"/>
<wire x1="7.62" y1="327.66" x2="7.62" y2="312.42" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="GND"/>
<wire x1="7.62" y1="312.42" x2="7.62" y2="304.8" width="0.1524" layer="91"/>
<wire x1="0" y1="312.42" x2="7.62" y2="312.42" width="0.1524" layer="91"/>
<junction x="7.62" y="312.42"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="C13" gate="C" pin="1"/>
<wire x1="31.75" y1="186.69" x2="31.75" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="PGND"/>
<wire x1="45.72" y1="204.47" x2="38.1" y2="204.47" width="0.1524" layer="91"/>
<wire x1="38.1" y1="204.47" x2="38.1" y2="186.69" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="-"/>
<wire x1="13.97" y1="186.69" x2="13.97" y2="187.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="22.86" y1="186.69" x2="22.86" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="C" pin="1"/>
<wire x1="50.8" y1="162.56" x2="50.8" y2="161.29" width="0.1524" layer="91"/>
<pinref part="C15" gate="C" pin="1"/>
<wire x1="50.8" y1="161.29" x2="43.18" y2="161.29" width="0.1524" layer="91"/>
<wire x1="43.18" y1="161.29" x2="43.18" y2="167.64" width="0.1524" layer="91"/>
<wire x1="43.18" y1="161.29" x2="43.18" y2="160.02" width="0.1524" layer="91"/>
<junction x="43.18" y="161.29"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C14" gate="C" pin="1"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="81.28" y1="184.15" x2="81.28" y2="182.88" width="0.1524" layer="91"/>
<wire x1="77.47" y1="184.15" x2="81.28" y2="184.15" width="0.1524" layer="91"/>
<junction x="81.28" y="184.15"/>
<wire x1="77.47" y1="184.15" x2="77.47" y2="196.85" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="AGND"/>
<wire x1="77.47" y1="196.85" x2="76.2" y2="196.85" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="IC3" gate="G$1" pin="EP"/>
<wire x1="60.96" y1="182.88" x2="60.96" y2="184.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="148.59" y1="204.47" x2="148.59" y2="200.66" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="-"/>
<wire x1="157.48" y1="201.93" x2="157.48" y2="200.66" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="CATHODE_2"/>
<wire x1="111.76" y1="269.24" x2="101.6" y2="269.24" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="CATHODE_1"/>
<wire x1="111.76" y1="274.32" x2="101.6" y2="274.32" width="0.1524" layer="91"/>
<wire x1="101.6" y1="274.32" x2="72.39" y2="274.32" width="0.1524" layer="91"/>
<wire x1="72.39" y1="274.32" x2="72.39" y2="269.24" width="0.1524" layer="91"/>
<wire x1="72.39" y1="269.24" x2="72.39" y2="261.62" width="0.1524" layer="91"/>
<wire x1="101.6" y1="269.24" x2="72.39" y2="269.24" width="0.1524" layer="91"/>
<junction x="72.39" y="269.24"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="100.33" y1="186.69" x2="100.33" y2="182.88" width="0.1524" layer="91"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
</net>
<net name="PWM" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PWM"/>
<wire x1="5.08" y1="115.57" x2="0" y2="115.57" width="0.1524" layer="91"/>
<label x="0" y="115.57" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="[PCINT10/INT0/OC0A/CKOUT]_PB2"/>
<wire x1="99.06" y1="323.85" x2="97.79" y2="323.85" width="0.1524" layer="91"/>
<label x="97.79" y="323.85" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NPWM" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PWM"/>
<wire x1="3.81" y1="57.15" x2="-1.27" y2="57.15" width="0.1524" layer="91"/>
<label x="-1.27" y="57.15" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="[PCINT7/ICP/OC0B/ADC7]_PA7"/>
<wire x1="99.06" y1="321.31" x2="97.79" y2="321.31" width="0.1524" layer="91"/>
<label x="97.79" y="321.31" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="5VDC" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SKIP#"/>
<wire x1="35.56" y1="115.57" x2="39.37" y2="115.57" width="0.1524" layer="91"/>
<wire x1="39.37" y1="115.57" x2="39.37" y2="113.03" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VDD"/>
<wire x1="39.37" y1="113.03" x2="39.37" y2="106.68" width="0.1524" layer="91"/>
<wire x1="39.37" y1="106.68" x2="39.37" y2="105.41" width="0.1524" layer="91"/>
<wire x1="39.37" y1="105.41" x2="35.56" y2="105.41" width="0.1524" layer="91"/>
<wire x1="39.37" y1="113.03" x2="43.18" y2="113.03" width="0.1524" layer="91"/>
<junction x="39.37" y="113.03"/>
<label x="43.18" y="116.84" size="1.778" layer="95" xref="yes"/>
<pinref part="C19" gate="C" pin="1"/>
<wire x1="43.18" y1="113.03" x2="43.18" y2="116.84" width="0.1524" layer="91"/>
<wire x1="43.18" y1="110.49" x2="43.18" y2="113.03" width="0.1524" layer="91"/>
<junction x="43.18" y="113.03"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="SKIP#"/>
<wire x1="34.29" y1="57.15" x2="38.1" y2="57.15" width="0.1524" layer="91"/>
<wire x1="38.1" y1="57.15" x2="38.1" y2="54.61" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VDD"/>
<wire x1="38.1" y1="54.61" x2="38.1" y2="46.99" width="0.1524" layer="91"/>
<wire x1="38.1" y1="46.99" x2="34.29" y2="46.99" width="0.1524" layer="91"/>
<junction x="38.1" y="54.61"/>
<wire x1="38.1" y1="54.61" x2="41.91" y2="54.61" width="0.1524" layer="91"/>
<label x="41.91" y="59.69" size="1.778" layer="95" xref="yes"/>
<pinref part="C27" gate="C" pin="1"/>
<wire x1="41.91" y1="54.61" x2="41.91" y2="59.69" width="0.1524" layer="91"/>
<wire x1="41.91" y1="52.07" x2="41.91" y2="54.61" width="0.1524" layer="91"/>
<junction x="41.91" y="54.61"/>
</segment>
<segment>
<wire x1="35.56" y1="323.85" x2="41.91" y2="323.85" width="0.1524" layer="91"/>
<label x="41.91" y="323.85" size="1.778" layer="95" xref="yes"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="35.56" y1="323.85" x2="35.56" y2="327.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PRG" gate="G$1" pin="2"/>
<wire x1="26.67" y1="251.46" x2="30.48" y2="251.46" width="0.1524" layer="91"/>
<wire x1="30.48" y1="251.46" x2="30.48" y2="254" width="0.1524" layer="91"/>
<wire x1="30.48" y1="254" x2="31.75" y2="254" width="0.1524" layer="91"/>
<label x="31.75" y="254" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="VIN"/>
<wire x1="45.72" y1="201.93" x2="43.18" y2="201.93" width="0.1524" layer="91"/>
<pinref part="C13" gate="C" pin="2"/>
<wire x1="43.18" y1="201.93" x2="31.75" y2="201.93" width="0.1524" layer="91"/>
<wire x1="31.75" y1="201.93" x2="31.75" y2="198.12" width="0.1524" layer="91"/>
<wire x1="31.75" y1="201.93" x2="25.4" y2="201.93" width="0.1524" layer="91"/>
<junction x="31.75" y="201.93"/>
<pinref part="C11" gate="G$1" pin="+"/>
<wire x1="25.4" y1="201.93" x2="22.86" y2="201.93" width="0.1524" layer="91"/>
<wire x1="22.86" y1="201.93" x2="13.97" y2="201.93" width="0.1524" layer="91"/>
<wire x1="13.97" y1="201.93" x2="13.97" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="22.86" y1="198.12" x2="22.86" y2="201.93" width="0.1524" layer="91"/>
<junction x="22.86" y="201.93"/>
<wire x1="13.97" y1="201.93" x2="11.43" y2="201.93" width="0.1524" layer="91"/>
<junction x="13.97" y="201.93"/>
<pinref part="IC3" gate="G$1" pin="EN"/>
<wire x1="45.72" y1="199.39" x2="43.18" y2="199.39" width="0.1524" layer="91"/>
<wire x1="43.18" y1="199.39" x2="43.18" y2="201.93" width="0.1524" layer="91"/>
<junction x="43.18" y="201.93"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="43.18" y1="201.93" x2="43.18" y2="215.9" width="0.1524" layer="91"/>
<wire x1="43.18" y1="215.9" x2="53.34" y2="215.9" width="0.1524" layer="91"/>
<label x="11.43" y="201.93" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="+"/>
<wire x1="-17.78" y1="292.1" x2="-12.7" y2="292.1" width="0.1524" layer="91"/>
<label x="-17.78" y="292.1" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="292.1" x2="0" y2="292.1" width="0.1524" layer="91"/>
<junction x="-12.7" y="292.1"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="0" y1="292.1" x2="10.16" y2="292.1" width="0.1524" layer="91"/>
<wire x1="10.16" y1="292.1" x2="19.05" y2="292.1" width="0.1524" layer="91"/>
<wire x1="19.05" y1="292.1" x2="29.21" y2="292.1" width="0.1524" layer="91"/>
<wire x1="29.21" y1="292.1" x2="45.72" y2="292.1" width="0.1524" layer="91"/>
<wire x1="0" y1="292.1" x2="0" y2="289.56" width="0.1524" layer="91"/>
<junction x="0" y="292.1"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="10.16" y1="292.1" x2="10.16" y2="289.56" width="0.1524" layer="91"/>
<junction x="10.16" y="292.1"/>
<pinref part="C5" gate="C" pin="1"/>
<wire x1="19.05" y1="289.56" x2="19.05" y2="292.1" width="0.1524" layer="91"/>
<junction x="19.05" y="292.1"/>
<pinref part="C6" gate="C" pin="1"/>
<wire x1="29.21" y1="289.56" x2="29.21" y2="292.1" width="0.1524" layer="91"/>
<junction x="29.21" y="292.1"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<pinref part="C3" gate="C" pin="1"/>
<wire x1="99.06" y1="334.01" x2="92.71" y2="334.01" width="0.1524" layer="91"/>
<wire x1="92.71" y1="334.01" x2="82.55" y2="334.01" width="0.1524" layer="91"/>
<wire x1="82.55" y1="334.01" x2="69.85" y2="334.01" width="0.1524" layer="91"/>
<wire x1="69.85" y1="334.01" x2="69.85" y2="328.93" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="69.85" y1="328.93" x2="69.85" y2="311.15" width="0.1524" layer="91"/>
<junction x="69.85" y="328.93"/>
<wire x1="82.55" y1="334.01" x2="82.55" y2="336.55" width="0.1524" layer="91"/>
<junction x="82.55" y="334.01"/>
<wire x1="82.55" y1="336.55" x2="81.28" y2="336.55" width="0.1524" layer="91"/>
<label x="81.28" y="336.55" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ISO_GND" class="0">
<segment>
<pinref part="GND-ISO7" gate="G$1" pin="GND-ISO"/>
<wire x1="193.04" y1="21.59" x2="193.04" y2="24.13" width="0.1524" layer="91"/>
<pinref part="Q5" gate="G$1" pin="S2"/>
<wire x1="193.04" y1="24.13" x2="189.23" y2="24.13" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND-ISO9" gate="G$1" pin="GND-ISO"/>
<pinref part="Q7" gate="G$1" pin="S"/>
<wire x1="162.56" y1="10.16" x2="162.56" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ISO_GND" gate="G$1" pin="GND-ISO"/>
<pinref part="Q6" gate="G$1" pin="S"/>
<wire x1="116.84" y1="10.16" x2="116.84" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="189.23" y1="257.81" x2="189.23" y2="256.54" width="0.1524" layer="91"/>
<pinref part="GND-ISO1" gate="G$1" pin="GND-ISO"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="194.31" y1="257.81" x2="194.31" y2="256.54" width="0.1524" layer="91"/>
<pinref part="GND-ISO2" gate="G$1" pin="GND-ISO"/>
</segment>
<segment>
<pinref part="GND-ISO3" gate="G$1" pin="GND-ISO"/>
<pinref part="IC4" gate="G$1" pin="ANODE"/>
<wire x1="182.88" y1="86.36" x2="182.88" y2="90.17" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND-ISO4" gate="G$1" pin="GND-ISO"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="205.74" y1="86.36" x2="205.74" y2="87.63" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND-ISO6" gate="G$1" pin="GND-ISO"/>
<wire x1="91.44" y1="24.13" x2="86.36" y2="24.13" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="S2"/>
<wire x1="86.36" y1="21.59" x2="86.36" y2="24.13" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="82.55" y1="80.01" x2="81.28" y2="80.01" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="81.28" y1="80.01" x2="81.28" y2="85.09" width="0.1524" layer="91"/>
<wire x1="81.28" y1="85.09" x2="81.28" y2="90.17" width="0.1524" layer="91"/>
<wire x1="81.28" y1="90.17" x2="82.55" y2="90.17" width="0.1524" layer="91"/>
<wire x1="81.28" y1="85.09" x2="77.47" y2="85.09" width="0.1524" layer="91"/>
<wire x1="77.47" y1="85.09" x2="77.47" y2="72.39" width="0.1524" layer="91"/>
<junction x="81.28" y="85.09"/>
<wire x1="77.47" y1="72.39" x2="116.84" y2="72.39" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="-"/>
<junction x="116.84" y="72.39"/>
<pinref part="GND-ISO5" gate="G$1" pin="GND-ISO"/>
<wire x1="116.84" y1="72.39" x2="116.84" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="9VAC_UNFILT2" class="0">
<segment>
<pinref part="Q5" gate="G$1" pin="D2"/>
<wire x1="193.04" y1="34.29" x2="195.58" y2="34.29" width="0.1524" layer="91"/>
<pinref part="Q5" gate="G$1" pin="D1"/>
<wire x1="189.23" y1="38.1" x2="193.04" y2="38.1" width="0.1524" layer="91"/>
<wire x1="193.04" y1="38.1" x2="193.04" y2="34.29" width="0.1524" layer="91"/>
<junction x="193.04" y="34.29"/>
<wire x1="193.04" y1="34.29" x2="193.04" y2="29.21" width="0.1524" layer="91"/>
<wire x1="193.04" y1="29.21" x2="189.23" y2="29.21" width="0.1524" layer="91"/>
<wire x1="195.58" y1="34.29" x2="195.58" y2="60.96" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="2"/>
<wire x1="198.12" y1="68.58" x2="189.23" y2="68.58" width="0.1524" layer="91"/>
<wire x1="189.23" y1="68.58" x2="189.23" y2="64.77" width="0.1524" layer="91"/>
<wire x1="189.23" y1="64.77" x2="186.69" y2="64.77" width="0.1524" layer="91"/>
<wire x1="186.69" y1="64.77" x2="173.99" y2="64.77" width="0.1524" layer="91"/>
<wire x1="173.99" y1="64.77" x2="166.37" y2="64.77" width="0.1524" layer="91"/>
<wire x1="166.37" y1="64.77" x2="162.56" y2="64.77" width="0.1524" layer="91"/>
<wire x1="162.56" y1="64.77" x2="161.29" y2="64.77" width="0.1524" layer="91"/>
<wire x1="161.29" y1="64.77" x2="161.29" y2="60.96" width="0.1524" layer="91"/>
<wire x1="161.29" y1="60.96" x2="195.58" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<junction x="186.69" y="64.77"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="173.99" y1="67.31" x2="173.99" y2="64.77" width="0.1524" layer="91"/>
<junction x="173.99" y="64.77"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="162.56" y1="64.77" x2="162.56" y2="66.04" width="0.1524" layer="91"/>
<junction x="162.56" y="64.77"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="166.37" y1="64.77" x2="166.37" y2="66.04" width="0.1524" layer="91"/>
<junction x="166.37" y="64.77"/>
</segment>
</net>
<net name="5VDC_PSU" class="0">
<segment>
<wire x1="17.78" y1="326.39" x2="17.78" y2="312.42" width="0.1524" layer="91"/>
<wire x1="30.48" y1="312.42" x2="17.78" y2="312.42" width="0.1524" layer="91"/>
<wire x1="17.78" y1="326.39" x2="17.78" y2="332.74" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="17.78" y1="332.74" x2="17.78" y2="337.82" width="0.1524" layer="91"/>
<wire x1="17.78" y1="337.82" x2="33.02" y2="337.82" width="0.1524" layer="91"/>
<wire x1="33.02" y1="337.82" x2="35.56" y2="337.82" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<junction x="17.78" y="332.74"/>
<pinref part="U1" gate="G$1" pin="VDD"/>
<pinref part="J1" gate="G$1" pin="PWR"/>
<wire x1="-12.7" y1="332.74" x2="-2.54" y2="332.74" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="332.74" x2="1.27" y2="332.74" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="VDD"/>
<wire x1="1.27" y1="332.74" x2="17.78" y2="332.74" width="0.1524" layer="91"/>
<wire x1="0" y1="317.5" x2="1.27" y2="317.5" width="0.1524" layer="91"/>
<wire x1="1.27" y1="317.5" x2="1.27" y2="332.74" width="0.1524" layer="91"/>
<junction x="1.27" y="332.74"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="PRG" gate="G$1" pin="5"/>
<wire x1="8.89" y1="246.38" x2="3.81" y2="246.38" width="0.1524" layer="91"/>
<wire x1="3.81" y1="246.38" x2="3.81" y2="243.84" width="0.1524" layer="91"/>
<wire x1="3.81" y1="243.84" x2="2.54" y2="243.84" width="0.1524" layer="91"/>
<label x="2.54" y="243.84" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="IC1" gate="G$1" pin="[PCINT11/!RESET!/DW]_PB3"/>
<wire x1="80.01" y1="328.93" x2="86.36" y2="328.93" width="0.1524" layer="91"/>
<wire x1="86.36" y1="328.93" x2="87.63" y2="328.93" width="0.1524" layer="91"/>
<wire x1="87.63" y1="328.93" x2="87.63" y2="326.39" width="0.1524" layer="91"/>
<label x="86.36" y="326.39" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="87.63" y1="326.39" x2="99.06" y2="326.39" width="0.1524" layer="91"/>
<wire x1="86.36" y1="326.39" x2="87.63" y2="326.39" width="0.1524" layer="91"/>
<junction x="87.63" y="326.39"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="PRG" gate="G$1" pin="3"/>
<wire x1="8.89" y1="248.92" x2="2.54" y2="248.92" width="0.1524" layer="91"/>
<label x="2.54" y="248.92" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PA4_[ADC4/USCK/SCL/T1/PCINT4]"/>
<wire x1="99.06" y1="313.69" x2="97.79" y2="313.69" width="0.1524" layer="91"/>
<label x="97.79" y="313.69" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BOOT2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="BOOT"/>
<pinref part="C26" gate="C" pin="2"/>
<wire x1="3.81" y1="46.99" x2="-1.27" y2="46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BOOT_R2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="BOOT_R"/>
<wire x1="3.81" y1="41.91" x2="-11.43" y2="41.91" width="0.1524" layer="91"/>
<wire x1="-11.43" y1="41.91" x2="-11.43" y2="46.99" width="0.1524" layer="91"/>
<pinref part="C26" gate="C" pin="1"/>
<wire x1="-11.43" y1="46.99" x2="-8.89" y2="46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TRANSOUT2" class="0">
<segment>
<wire x1="69.85" y1="68.58" x2="69.85" y2="36.83" width="0.1524" layer="91"/>
<wire x1="69.85" y1="36.83" x2="64.77" y2="36.83" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="2"/>
<wire x1="64.77" y1="36.83" x2="64.77" y2="38.1" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="64.77" y1="68.58" x2="69.85" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="92.71" y1="80.01" x2="91.44" y2="80.01" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="K"/>
<wire x1="90.17" y1="80.01" x2="91.44" y2="80.01" width="0.1524" layer="91"/>
<wire x1="69.85" y1="68.58" x2="91.44" y2="68.58" width="0.1524" layer="91"/>
<wire x1="91.44" y1="68.58" x2="91.44" y2="80.01" width="0.1524" layer="91"/>
<junction x="69.85" y="68.58"/>
<junction x="91.44" y="80.01"/>
</segment>
</net>
<net name="SW_50_60" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="180.34" y1="307.34" x2="179.07" y2="307.34" width="0.1524" layer="91"/>
<label x="179.07" y="307.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PA2_[ADC2/AIN1/PCINT2]"/>
<wire x1="99.06" y1="308.61" x2="97.79" y2="308.61" width="0.1524" layer="91"/>
<label x="97.79" y="308.61" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="XTAL1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="[PCINT8/XTAL1/CLKI]_PB0"/>
<wire x1="99.06" y1="331.47" x2="97.79" y2="331.47" width="0.1524" layer="91"/>
<label x="97.79" y="331.47" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="Y1" gate="G$1" pin="OUT/IN"/>
<pinref part="C2" gate="C" pin="2"/>
<wire x1="199.39" y1="330.2" x2="200.66" y2="330.2" width="0.1524" layer="91"/>
<wire x1="200.66" y1="330.2" x2="201.93" y2="330.2" width="0.1524" layer="91"/>
<junction x="200.66" y="330.2"/>
<label x="201.93" y="330.2" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="XTAL2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="[PCINT9/XTAL2]_PB1"/>
<wire x1="99.06" y1="328.93" x2="97.79" y2="328.93" width="0.1524" layer="91"/>
<label x="97.79" y="328.93" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="Y1" gate="G$1" pin="IN/OUT"/>
<pinref part="C1" gate="C" pin="2"/>
<wire x1="168.91" y1="330.2" x2="167.64" y2="330.2" width="0.1524" layer="91"/>
<wire x1="167.64" y1="330.2" x2="166.37" y2="330.2" width="0.1524" layer="91"/>
<junction x="167.64" y="330.2"/>
<label x="166.37" y="330.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="14VDC" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VIN"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="5.08" y1="95.25" x2="-1.27" y2="95.25" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="95.25" x2="-7.62" y2="95.25" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="95.25" x2="-13.97" y2="95.25" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="95.25" x2="-13.97" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="-7.62" y1="91.44" x2="-7.62" y2="95.25" width="0.1524" layer="91"/>
<junction x="-7.62" y="95.25"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="-1.27" y1="91.44" x2="-1.27" y2="95.25" width="0.1524" layer="91"/>
<junction x="-1.27" y="95.25"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="5.08" y1="91.44" x2="5.08" y2="95.25" width="0.1524" layer="91"/>
<junction x="5.08" y="95.25"/>
<wire x1="-13.97" y1="95.25" x2="-15.24" y2="95.25" width="0.1524" layer="91"/>
<junction x="-13.97" y="95.25"/>
<wire x1="-15.24" y1="95.25" x2="-16.51" y2="95.25" width="0.1524" layer="91"/>
<label x="-16.51" y="95.25" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VIN"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="3.81" y1="36.83" x2="-2.54" y2="36.83" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="36.83" x2="-8.89" y2="36.83" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="36.83" x2="-15.24" y2="36.83" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="36.83" x2="-15.24" y2="31.75" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="-8.89" y1="31.75" x2="-8.89" y2="36.83" width="0.1524" layer="91"/>
<junction x="-8.89" y="36.83"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="31.75" x2="-2.54" y2="36.83" width="0.1524" layer="91"/>
<junction x="-2.54" y="36.83"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="3.81" y1="31.75" x2="3.81" y2="36.83" width="0.1524" layer="91"/>
<junction x="3.81" y="36.83"/>
<wire x1="-15.24" y1="36.83" x2="-16.51" y2="36.83" width="0.1524" layer="91"/>
<junction x="-15.24" y="36.83"/>
<wire x1="-16.51" y1="36.83" x2="-17.78" y2="36.83" width="0.1524" layer="91"/>
<label x="-17.78" y="36.83" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="K"/>
<wire x1="93.98" y1="215.9" x2="100.33" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="100.33" y1="215.9" x2="123.19" y2="215.9" width="0.1524" layer="91"/>
<wire x1="123.19" y1="215.9" x2="148.59" y2="215.9" width="0.1524" layer="91"/>
<wire x1="148.59" y1="215.9" x2="157.48" y2="215.9" width="0.1524" layer="91"/>
<wire x1="157.48" y1="215.9" x2="166.37" y2="215.9" width="0.1524" layer="91"/>
<wire x1="148.59" y1="212.09" x2="148.59" y2="215.9" width="0.1524" layer="91"/>
<junction x="148.59" y="215.9"/>
<pinref part="C9" gate="G$1" pin="+"/>
<wire x1="157.48" y1="214.63" x2="157.48" y2="215.9" width="0.1524" layer="91"/>
<junction x="157.48" y="215.9"/>
<label x="166.37" y="215.9" size="1.778" layer="95" xref="yes"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="100.33" y1="213.36" x2="100.33" y2="215.9" width="0.1524" layer="91"/>
<junction x="100.33" y="215.9"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="153.67" y1="243.84" x2="152.4" y2="243.84" width="0.1524" layer="91"/>
<label x="152.4" y="243.84" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="157.48" y1="44.45" x2="156.21" y2="44.45" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="D7" gate="G$1" pin="C"/>
<wire x1="157.48" y1="44.45" x2="157.48" y2="40.64" width="0.1524" layer="91"/>
<wire x1="157.48" y1="40.64" x2="157.48" y2="38.1" width="0.1524" layer="91"/>
<wire x1="157.48" y1="38.1" x2="153.67" y2="38.1" width="0.1524" layer="91"/>
<pinref part="Q5" gate="G$1" pin="G1"/>
<wire x1="171.45" y1="40.64" x2="163.83" y2="40.64" width="0.1524" layer="91"/>
<junction x="157.48" y="40.64"/>
<pinref part="Q3" gate="G$1" pin="D"/>
<wire x1="163.83" y1="40.64" x2="157.48" y2="40.64" width="0.1524" layer="91"/>
<wire x1="163.83" y1="44.45" x2="163.83" y2="40.64" width="0.1524" layer="91"/>
<junction x="163.83" y="40.64"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="K"/>
<wire x1="100.33" y1="80.01" x2="101.6" y2="80.01" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="K"/>
<wire x1="101.6" y1="80.01" x2="101.6" y2="85.09" width="0.1524" layer="91"/>
<wire x1="101.6" y1="85.09" x2="101.6" y2="90.17" width="0.1524" layer="91"/>
<wire x1="101.6" y1="90.17" x2="100.33" y2="90.17" width="0.1524" layer="91"/>
<wire x1="101.6" y1="85.09" x2="105.41" y2="85.09" width="0.1524" layer="91"/>
<junction x="101.6" y="85.09"/>
<pinref part="L2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="TRANSOUT1" class="0">
<segment>
<wire x1="71.12" y1="64.77" x2="73.66" y2="64.77" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="4"/>
<wire x1="71.12" y1="64.77" x2="64.77" y2="64.77" width="0.1524" layer="91"/>
<wire x1="64.77" y1="64.77" x2="64.77" y2="63.5" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="4"/>
<wire x1="64.77" y1="93.98" x2="64.77" y2="95.25" width="0.1524" layer="91"/>
<wire x1="64.77" y1="95.25" x2="66.04" y2="95.25" width="0.1524" layer="91"/>
<wire x1="66.04" y1="95.25" x2="72.39" y2="95.25" width="0.1524" layer="91"/>
<wire x1="72.39" y1="95.25" x2="73.66" y2="95.25" width="0.1524" layer="91"/>
<wire x1="73.66" y1="95.25" x2="73.66" y2="64.77" width="0.1524" layer="91"/>
<wire x1="73.66" y1="95.25" x2="91.44" y2="95.25" width="0.1524" layer="91"/>
<junction x="73.66" y="95.25"/>
<pinref part="D2" gate="G$1" pin="K"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="91.44" y1="90.17" x2="92.71" y2="90.17" width="0.1524" layer="91"/>
<wire x1="91.44" y1="90.17" x2="90.17" y2="90.17" width="0.1524" layer="91"/>
<wire x1="91.44" y1="95.25" x2="91.44" y2="93.98" width="0.1524" layer="91"/>
<wire x1="91.44" y1="93.98" x2="91.44" y2="90.17" width="0.1524" layer="91"/>
<junction x="91.44" y="90.17"/>
</segment>
</net>
<net name="ISO_12V7" class="0">
<segment>
<wire x1="191.77" y1="43.18" x2="191.77" y2="57.15" width="0.1524" layer="91"/>
<pinref part="Q5" gate="G$1" pin="S1"/>
<wire x1="189.23" y1="43.18" x2="191.77" y2="43.18" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="S"/>
<wire x1="163.83" y1="54.61" x2="163.83" y2="57.15" width="0.1524" layer="91"/>
<wire x1="163.83" y1="57.15" x2="161.29" y2="57.15" width="0.1524" layer="91"/>
<wire x1="191.77" y1="57.15" x2="163.83" y2="57.15" width="0.1524" layer="91"/>
<wire x1="163.83" y1="57.15" x2="156.21" y2="57.15" width="0.1524" layer="91"/>
<junction x="163.83" y="57.15"/>
<pinref part="Q2" gate="G$1" pin="S"/>
<wire x1="116.84" y1="54.61" x2="116.84" y2="57.15" width="0.1524" layer="91"/>
<wire x1="88.9" y1="43.18" x2="88.9" y2="57.15" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="S1"/>
<wire x1="88.9" y1="43.18" x2="91.44" y2="43.18" width="0.1524" layer="91"/>
<wire x1="116.84" y1="57.15" x2="88.9" y2="57.15" width="0.1524" layer="91"/>
<wire x1="116.84" y1="57.15" x2="124.46" y2="57.15" width="0.1524" layer="91"/>
<junction x="116.84" y="57.15"/>
<wire x1="156.21" y1="57.15" x2="129.54" y2="57.15" width="0.1524" layer="91"/>
<wire x1="129.54" y1="57.15" x2="124.46" y2="57.15" width="0.1524" layer="91"/>
<wire x1="125.73" y1="85.09" x2="129.54" y2="85.09" width="0.1524" layer="91"/>
<wire x1="129.54" y1="85.09" x2="130.81" y2="85.09" width="0.1524" layer="91"/>
<wire x1="123.19" y1="85.09" x2="125.73" y2="85.09" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="+"/>
<wire x1="116.84" y1="85.09" x2="123.19" y2="85.09" width="0.1524" layer="91"/>
<junction x="116.84" y="85.09"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="113.03" y1="85.09" x2="116.84" y2="85.09" width="0.1524" layer="91"/>
<label x="130.81" y="85.09" size="1.778" layer="95" xref="yes"/>
<wire x1="129.54" y1="85.09" x2="129.54" y2="57.15" width="0.1524" layer="91"/>
<junction x="129.54" y="85.09"/>
<junction x="129.54" y="57.15"/>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="171.45" y1="125.73" x2="171.45" y2="127" width="0.1524" layer="91"/>
<wire x1="171.45" y1="127" x2="182.88" y2="127" width="0.1524" layer="91"/>
<wire x1="182.88" y1="127" x2="182.88" y2="125.73" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="205.74" y1="124.46" x2="205.74" y2="127" width="0.1524" layer="91"/>
<wire x1="205.74" y1="127" x2="182.88" y2="127" width="0.1524" layer="91"/>
<junction x="182.88" y="127"/>
<wire x1="171.45" y1="127" x2="129.54" y2="127" width="0.1524" layer="91"/>
<wire x1="129.54" y1="127" x2="129.54" y2="85.09" width="0.1524" layer="91"/>
<junction x="171.45" y="127"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="COLLECTOR_1"/>
<wire x1="154.94" y1="276.86" x2="196.85" y2="276.86" width="0.1524" layer="91"/>
<wire x1="196.85" y1="276.86" x2="198.12" y2="276.86" width="0.1524" layer="91"/>
<label x="198.12" y="276.86" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="COLLECTOR_2"/>
<wire x1="154.94" y1="271.78" x2="198.12" y2="271.78" width="0.1524" layer="91"/>
<label x="198.12" y="271.78" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ISO_SPWM1" class="0">
<segment>
<wire x1="135.89" y1="41.91" x2="135.89" y2="44.45" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="130.81" y1="38.1" x2="135.89" y2="38.1" width="0.1524" layer="91"/>
<wire x1="135.89" y1="38.1" x2="135.89" y2="41.91" width="0.1524" layer="91"/>
<wire x1="138.43" y1="41.91" x2="135.89" y2="41.91" width="0.1524" layer="91"/>
<junction x="135.89" y="41.91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="133.35" y1="29.21" x2="135.89" y2="29.21" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="C"/>
<wire x1="135.89" y1="29.21" x2="135.89" y2="26.67" width="0.1524" layer="91"/>
<wire x1="135.89" y1="26.67" x2="135.89" y2="22.86" width="0.1524" layer="91"/>
<wire x1="130.81" y1="22.86" x2="135.89" y2="22.86" width="0.1524" layer="91"/>
<wire x1="138.43" y1="41.91" x2="138.43" y2="34.29" width="0.1524" layer="91"/>
<wire x1="138.43" y1="34.29" x2="138.43" y2="26.67" width="0.1524" layer="91"/>
<wire x1="138.43" y1="26.67" x2="135.89" y2="26.67" width="0.1524" layer="91"/>
<junction x="135.89" y="26.67"/>
<pinref part="Q3" gate="G$1" pin="G"/>
<wire x1="168.91" y1="49.53" x2="170.18" y2="49.53" width="0.1524" layer="91"/>
<pinref part="Q7" gate="G$1" pin="G"/>
<wire x1="170.18" y1="49.53" x2="170.18" y2="17.78" width="0.1524" layer="91"/>
<wire x1="170.18" y1="17.78" x2="167.64" y2="17.78" width="0.1524" layer="91"/>
<wire x1="170.18" y1="17.78" x2="170.18" y2="2.54" width="0.1524" layer="91"/>
<junction x="170.18" y="17.78"/>
<wire x1="170.18" y1="2.54" x2="138.43" y2="2.54" width="0.1524" layer="91"/>
<wire x1="138.43" y1="2.54" x2="138.43" y2="26.67" width="0.1524" layer="91"/>
<junction x="138.43" y="26.67"/>
<wire x1="138.43" y1="41.91" x2="138.43" y2="49.53" width="0.1524" layer="91"/>
<junction x="138.43" y="41.91"/>
<wire x1="138.43" y1="49.53" x2="138.43" y2="52.07" width="0.1524" layer="91"/>
<label x="138.43" y="52.07" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="135.89" y1="44.45" x2="133.35" y2="44.45" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="EMITTER_2"/>
<wire x1="154.94" y1="269.24" x2="189.23" y2="269.24" width="0.1524" layer="91"/>
<wire x1="189.23" y1="269.24" x2="189.23" y2="267.97" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="189.23" y1="269.24" x2="198.12" y2="269.24" width="0.1524" layer="91"/>
<junction x="189.23" y="269.24"/>
<label x="198.12" y="269.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="123.19" y1="29.21" x2="121.92" y2="29.21" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="A"/>
<wire x1="121.92" y1="29.21" x2="121.92" y2="26.67" width="0.1524" layer="91"/>
<wire x1="121.92" y1="26.67" x2="121.92" y2="22.86" width="0.1524" layer="91"/>
<wire x1="121.92" y1="22.86" x2="125.73" y2="22.86" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="G2"/>
<wire x1="109.22" y1="26.67" x2="116.84" y2="26.67" width="0.1524" layer="91"/>
<junction x="121.92" y="26.67"/>
<pinref part="Q6" gate="G$1" pin="D"/>
<wire x1="116.84" y1="26.67" x2="121.92" y2="26.67" width="0.1524" layer="91"/>
<wire x1="116.84" y1="22.86" x2="116.84" y2="26.67" width="0.1524" layer="91"/>
<junction x="116.84" y="26.67"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="123.19" y1="44.45" x2="121.92" y2="44.45" width="0.1524" layer="91"/>
<wire x1="121.92" y1="44.45" x2="121.92" y2="40.64" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="121.92" y1="40.64" x2="121.92" y2="38.1" width="0.1524" layer="91"/>
<wire x1="121.92" y1="38.1" x2="125.73" y2="38.1" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="G1"/>
<wire x1="109.22" y1="40.64" x2="116.84" y2="40.64" width="0.1524" layer="91"/>
<junction x="121.92" y="40.64"/>
<pinref part="Q2" gate="G$1" pin="D"/>
<wire x1="116.84" y1="40.64" x2="121.92" y2="40.64" width="0.1524" layer="91"/>
<wire x1="116.84" y1="44.45" x2="116.84" y2="40.64" width="0.1524" layer="91"/>
<junction x="116.84" y="40.64"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="156.21" y1="29.21" x2="157.48" y2="29.21" width="0.1524" layer="91"/>
<wire x1="157.48" y1="29.21" x2="157.48" y2="26.67" width="0.1524" layer="91"/>
<pinref part="D9" gate="G$1" pin="A"/>
<wire x1="157.48" y1="26.67" x2="157.48" y2="22.86" width="0.1524" layer="91"/>
<wire x1="157.48" y1="22.86" x2="153.67" y2="22.86" width="0.1524" layer="91"/>
<pinref part="Q5" gate="G$1" pin="G2"/>
<wire x1="157.48" y1="26.67" x2="162.56" y2="26.67" width="0.1524" layer="91"/>
<junction x="157.48" y="26.67"/>
<pinref part="Q7" gate="G$1" pin="D"/>
<wire x1="162.56" y1="26.67" x2="171.45" y2="26.67" width="0.1524" layer="91"/>
<wire x1="162.56" y1="22.86" x2="162.56" y2="26.67" width="0.1524" layer="91"/>
<junction x="162.56" y="26.67"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="152.4" y1="74.93" x2="162.56" y2="74.93" width="0.1524" layer="91"/>
<junction x="173.99" y="74.93"/>
<wire x1="162.56" y1="74.93" x2="166.37" y2="74.93" width="0.1524" layer="91"/>
<wire x1="166.37" y1="74.93" x2="173.99" y2="74.93" width="0.1524" layer="91"/>
<wire x1="173.99" y1="74.93" x2="186.69" y2="74.93" width="0.1524" layer="91"/>
<wire x1="186.69" y1="74.93" x2="189.23" y2="74.93" width="0.1524" layer="91"/>
<wire x1="189.23" y1="74.93" x2="189.23" y2="71.12" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="1"/>
<wire x1="189.23" y1="71.12" x2="198.12" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<junction x="186.69" y="74.93"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="173.99" y1="74.93" x2="173.99" y2="72.39" width="0.1524" layer="91"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="166.37" y1="73.66" x2="166.37" y2="74.93" width="0.1524" layer="91"/>
<junction x="166.37" y="74.93"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="162.56" y1="73.66" x2="162.56" y2="74.93" width="0.1524" layer="91"/>
<junction x="162.56" y="74.93"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VOUT"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="29.21" y1="317.5" x2="30.48" y2="317.5" width="0.1524" layer="91"/>
<wire x1="27.94" y1="332.74" x2="29.21" y2="332.74" width="0.1524" layer="91"/>
<wire x1="29.21" y1="332.74" x2="29.21" y2="317.5" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="30.48" y1="335.28" x2="29.21" y2="335.28" width="0.1524" layer="91"/>
<wire x1="29.21" y1="335.28" x2="29.21" y2="332.74" width="0.1524" layer="91"/>
<junction x="29.21" y="332.74"/>
</segment>
</net>
<net name="BOOST_COMP" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="COMP"/>
<pinref part="C15" gate="C" pin="2"/>
<wire x1="45.72" y1="196.85" x2="43.18" y2="196.85" width="0.1524" layer="91"/>
<wire x1="43.18" y1="196.85" x2="43.18" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="43.18" y1="182.88" x2="43.18" y2="175.26" width="0.1524" layer="91"/>
<wire x1="50.8" y1="181.61" x2="50.8" y2="182.88" width="0.1524" layer="91"/>
<wire x1="50.8" y1="182.88" x2="43.18" y2="182.88" width="0.1524" layer="91"/>
<junction x="43.18" y="182.88"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="C16" gate="C" pin="2"/>
<wire x1="50.8" y1="171.45" x2="50.8" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="60.96" y1="215.9" x2="77.47" y2="215.9" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="SW"/>
<wire x1="77.47" y1="215.9" x2="86.36" y2="215.9" width="0.1524" layer="91"/>
<wire x1="76.2" y1="204.47" x2="77.47" y2="204.47" width="0.1524" layer="91"/>
<wire x1="77.47" y1="204.47" x2="77.47" y2="215.9" width="0.1524" layer="91"/>
<junction x="77.47" y="215.9"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="SS"/>
<wire x1="76.2" y1="201.93" x2="77.47" y2="201.93" width="0.1524" layer="91"/>
<pinref part="C14" gate="C" pin="2"/>
<wire x1="77.47" y1="201.93" x2="81.28" y2="201.93" width="0.1524" layer="91"/>
<wire x1="81.28" y1="201.93" x2="81.28" y2="191.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ISO_SPWM2" class="0">
<segment>
<pinref part="D9" gate="G$1" pin="C"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="146.05" y1="29.21" x2="144.78" y2="29.21" width="0.1524" layer="91"/>
<wire x1="144.78" y1="29.21" x2="144.78" y2="26.67" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="144.78" y1="44.45" x2="146.05" y2="44.45" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="A"/>
<wire x1="148.59" y1="38.1" x2="144.78" y2="38.1" width="0.1524" layer="91"/>
<wire x1="144.78" y1="38.1" x2="144.78" y2="41.91" width="0.1524" layer="91"/>
<wire x1="144.78" y1="41.91" x2="144.78" y2="44.45" width="0.1524" layer="91"/>
<junction x="144.78" y="41.91"/>
<wire x1="144.78" y1="41.91" x2="142.24" y2="41.91" width="0.1524" layer="91"/>
<wire x1="142.24" y1="41.91" x2="142.24" y2="34.29" width="0.1524" layer="91"/>
<wire x1="142.24" y1="34.29" x2="142.24" y2="26.67" width="0.1524" layer="91"/>
<wire x1="142.24" y1="26.67" x2="144.78" y2="26.67" width="0.1524" layer="91"/>
<junction x="144.78" y="26.67"/>
<wire x1="144.78" y1="26.67" x2="144.78" y2="22.86" width="0.1524" layer="91"/>
<wire x1="144.78" y1="22.86" x2="148.59" y2="22.86" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="111.76" y1="49.53" x2="110.49" y2="49.53" width="0.1524" layer="91"/>
<pinref part="Q6" gate="G$1" pin="G"/>
<wire x1="110.49" y1="49.53" x2="110.49" y2="17.78" width="0.1524" layer="91"/>
<wire x1="110.49" y1="17.78" x2="111.76" y2="17.78" width="0.1524" layer="91"/>
<wire x1="110.49" y1="17.78" x2="110.49" y2="5.08" width="0.1524" layer="91"/>
<wire x1="110.49" y1="5.08" x2="142.24" y2="5.08" width="0.1524" layer="91"/>
<junction x="110.49" y="17.78"/>
<wire x1="142.24" y1="5.08" x2="142.24" y2="26.67" width="0.1524" layer="91"/>
<junction x="142.24" y="26.67"/>
<wire x1="142.24" y1="41.91" x2="142.24" y2="52.07" width="0.1524" layer="91"/>
<junction x="142.24" y="41.91"/>
<label x="142.24" y="52.07" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="EMITTER_1"/>
<wire x1="154.94" y1="274.32" x2="193.04" y2="274.32" width="0.1524" layer="91"/>
<wire x1="193.04" y1="274.32" x2="194.31" y2="274.32" width="0.1524" layer="91"/>
<wire x1="194.31" y1="274.32" x2="194.31" y2="269.24" width="0.1524" layer="91"/>
<wire x1="194.31" y1="269.24" x2="194.31" y2="267.97" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="194.31" y1="274.32" x2="198.12" y2="274.32" width="0.1524" layer="91"/>
<junction x="194.31" y="274.32"/>
<label x="198.12" y="274.32" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="FEEDBACK" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="FB"/>
<wire x1="76.2" y1="199.39" x2="100.33" y2="199.39" width="0.1524" layer="91"/>
<label x="110.49" y="199.39" size="1.778" layer="95" xref="yes"/>
<wire x1="100.33" y1="199.39" x2="110.49" y2="199.39" width="0.1524" layer="91"/>
<junction x="100.33" y="199.39"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="100.33" y1="196.85" x2="100.33" y2="199.39" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="100.33" y1="203.2" x2="100.33" y2="199.39" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="EMITTER_3"/>
<wire x1="154.94" y1="264.16" x2="166.37" y2="264.16" width="0.1524" layer="91"/>
<wire x1="166.37" y1="264.16" x2="166.37" y2="247.65" width="0.1524" layer="91"/>
<wire x1="166.37" y1="247.65" x2="152.4" y2="247.65" width="0.1524" layer="91"/>
<label x="152.4" y="247.65" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPWM1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="[PCINT6/OC1A/SDA/MOSI/DI/ADC6]_PA6"/>
<wire x1="99.06" y1="318.77" x2="97.79" y2="318.77" width="0.1524" layer="91"/>
<label x="97.79" y="318.77" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PRG" gate="G$1" pin="4"/>
<wire x1="26.67" y1="248.92" x2="30.48" y2="248.92" width="0.1524" layer="91"/>
<label x="30.48" y="248.92" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="97.79" y1="271.78" x2="95.25" y2="271.78" width="0.1524" layer="91"/>
<label x="95.25" y="271.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPWM2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA5_[ADC5/DO/MISO/OC1B/PCINT5]"/>
<wire x1="99.06" y1="316.23" x2="97.79" y2="316.23" width="0.1524" layer="91"/>
<label x="97.79" y="316.23" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PRG" gate="G$1" pin="1"/>
<wire x1="8.89" y1="251.46" x2="3.81" y2="251.46" width="0.1524" layer="91"/>
<wire x1="3.81" y1="251.46" x2="3.81" y2="254" width="0.1524" layer="91"/>
<wire x1="3.81" y1="254" x2="2.54" y2="254" width="0.1524" layer="91"/>
<label x="2.54" y="254" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="97.79" y1="276.86" x2="95.25" y2="276.86" width="0.1524" layer="91"/>
<label x="95.25" y="276.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NC1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA3_[ADC3/T0/PCINT3]"/>
<wire x1="99.06" y1="311.15" x2="86.36" y2="311.15" width="0.1524" layer="91"/>
<pinref part="TP1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="NC2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA1_[ADC1/AIN0/PCINT1]"/>
<wire x1="99.06" y1="306.07" x2="86.36" y2="306.07" width="0.1524" layer="91"/>
<pinref part="TP2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="NC3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA0_[ADC0/AREF/PCINT0]"/>
<wire x1="99.06" y1="303.53" x2="86.36" y2="303.53" width="0.1524" layer="91"/>
<pinref part="TP3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<wire x1="199.39" y1="306.07" x2="191.77" y2="306.07" width="0.1524" layer="91"/>
<wire x1="191.77" y1="306.07" x2="191.77" y2="307.34" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="191.77" y1="307.34" x2="190.5" y2="307.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="ANODE_1"/>
<wire x1="111.76" y1="276.86" x2="107.95" y2="276.86" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="ANODE_2"/>
<wire x1="111.76" y1="271.78" x2="107.95" y2="271.78" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="COLLECTOR_4"/>
<wire x1="154.94" y1="261.62" x2="156.21" y2="261.62" width="0.1524" layer="91"/>
<pinref part="TP5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="EMITTER_4"/>
<wire x1="154.94" y1="259.08" x2="156.21" y2="259.08" width="0.1524" layer="91"/>
<pinref part="TP7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="ANODE_4"/>
<wire x1="111.76" y1="261.62" x2="110.49" y2="261.62" width="0.1524" layer="91"/>
<pinref part="TP4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CATHODE_4"/>
<wire x1="111.76" y1="259.08" x2="110.49" y2="259.08" width="0.1524" layer="91"/>
<pinref part="TP6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PSOUT1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VSW"/>
<wire x1="39.37" y1="95.25" x2="35.56" y2="95.25" width="0.1524" layer="91"/>
<wire x1="48.26" y1="95.25" x2="48.26" y2="71.12" width="0.1524" layer="91"/>
<wire x1="39.37" y1="95.25" x2="48.26" y2="95.25" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="57.15" y1="68.58" x2="57.15" y2="67.31" width="0.1524" layer="91"/>
<wire x1="57.15" y1="67.31" x2="48.26" y2="67.31" width="0.1524" layer="91"/>
<wire x1="48.26" y1="67.31" x2="48.26" y2="71.12" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="1"/>
<wire x1="57.15" y1="38.1" x2="57.15" y2="31.75" width="0.1524" layer="91"/>
<wire x1="57.15" y1="31.75" x2="48.26" y2="31.75" width="0.1524" layer="91"/>
<wire x1="48.26" y1="31.75" x2="48.26" y2="67.31" width="0.1524" layer="91"/>
<junction x="48.26" y="67.31"/>
</segment>
</net>
<net name="ISO_FEEDBACK_A" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="ANODE_3"/>
<wire x1="111.76" y1="266.7" x2="99.06" y2="266.7" width="0.1524" layer="91"/>
<wire x1="99.06" y1="266.7" x2="99.06" y2="247.65" width="0.1524" layer="91"/>
<wire x1="99.06" y1="247.65" x2="95.25" y2="247.65" width="0.1524" layer="91"/>
<label x="95.25" y="247.65" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="171.45" y1="115.57" x2="171.45" y2="114.3" width="0.1524" layer="91"/>
<wire x1="171.45" y1="114.3" x2="163.83" y2="114.3" width="0.1524" layer="91"/>
<label x="163.83" y="114.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ISO_FEEDBACK_C" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CATHODE_3"/>
<wire x1="111.76" y1="264.16" x2="101.6" y2="264.16" width="0.1524" layer="91"/>
<wire x1="101.6" y1="264.16" x2="101.6" y2="243.84" width="0.1524" layer="91"/>
<wire x1="101.6" y1="243.84" x2="95.25" y2="243.84" width="0.1524" layer="91"/>
<label x="95.25" y="243.84" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="CATHODE"/>
<wire x1="182.88" y1="107.95" x2="182.88" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C17" gate="C" pin="1"/>
<wire x1="182.88" y1="109.22" x2="185.42" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="182.88" y1="109.22" x2="182.88" y2="115.57" width="0.1524" layer="91"/>
<junction x="182.88" y="109.22"/>
<wire x1="182.88" y1="109.22" x2="163.83" y2="109.22" width="0.1524" layer="91"/>
<label x="163.83" y="109.22" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="C17" gate="C" pin="2"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="193.04" y1="109.22" x2="194.31" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="204.47" y1="109.22" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="205.74" y1="109.22" x2="205.74" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="205.74" y1="97.79" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<junction x="205.74" y="109.22"/>
<pinref part="IC4" gate="G$1" pin="REF"/>
<wire x1="205.74" y1="99.06" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<wire x1="193.04" y1="99.06" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<junction x="205.74" y="99.06"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="COLLECTOR_3"/>
<wire x1="154.94" y1="266.7" x2="168.91" y2="266.7" width="0.1524" layer="91"/>
<wire x1="168.91" y1="266.7" x2="168.91" y2="248.92" width="0.1524" layer="91"/>
<wire x1="168.91" y1="248.92" x2="168.91" y2="243.84" width="0.1524" layer="91"/>
<wire x1="168.91" y1="243.84" x2="163.83" y2="243.84" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="N/A"/>
<wire x1="35.56" y1="110.49" x2="36.83" y2="110.49" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="N/A2"/>
<wire x1="5.08" y1="110.49" x2="3.81" y2="110.49" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="N/A2"/>
<wire x1="3.81" y1="52.07" x2="2.54" y2="52.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="N/A"/>
<wire x1="34.29" y1="52.07" x2="35.56" y2="52.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="9VAC_UNFILT1" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="D2"/>
<pinref part="Q4" gate="G$1" pin="D1"/>
<wire x1="91.44" y1="38.1" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
<wire x1="88.9" y1="38.1" x2="88.9" y2="34.29" width="0.1524" layer="91"/>
<wire x1="88.9" y1="34.29" x2="86.36" y2="34.29" width="0.1524" layer="91"/>
<junction x="88.9" y="34.29"/>
<wire x1="88.9" y1="34.29" x2="88.9" y2="29.21" width="0.1524" layer="91"/>
<wire x1="88.9" y1="29.21" x2="91.44" y2="29.21" width="0.1524" layer="91"/>
<wire x1="86.36" y1="34.29" x2="86.36" y2="62.23" width="0.1524" layer="91"/>
<wire x1="86.36" y1="62.23" x2="118.11" y2="62.23" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="118.11" y1="62.23" x2="132.08" y2="62.23" width="0.1524" layer="91"/>
<wire x1="144.78" y1="74.93" x2="135.89" y2="74.93" width="0.1524" layer="91"/>
<wire x1="135.89" y1="74.93" x2="135.89" y2="62.23" width="0.1524" layer="91"/>
<wire x1="135.89" y1="62.23" x2="132.08" y2="62.23" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="102,1,3.81,41.91,BOOT_R,BOOT_R2,,,,"/>
<approved hash="102,1,3.81,46.99,BOOT,BOOT2,,,,"/>
<approved hash="102,1,86.36,21.59,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,193.04,21.59,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,116.84,71.12,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,116.84,10.16,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,162.56,10.16,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,189.23,256.54,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,194.31,256.54,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,182.88,86.36,GND-ISO,ISO_GND,,,,"/>
<approved hash="102,1,205.74,86.36,GND-ISO,ISO_GND,,,,"/>
<approved hash="201,1,3.81,46.99,BOOT,BOOT2\, BOOT,,,,"/>
<approved hash="201,1,3.81,41.91,BOOT_R,BOOT_R2\, BOOT_R,,,,"/>
<approved hash="103,1,34.29,52.07,U3,N/A,N$31,,,"/>
<approved hash="104,1,34.29,46.99,U3,VDD,5VDC,,,"/>
<approved hash="104,1,34.29,41.91,U3,PGND,GND,,,"/>
<approved hash="103,1,3.81,52.07,U3,N/A2,N$30,,,"/>
<approved hash="103,1,35.56,110.49,U2,N/A,N$26,,,"/>
<approved hash="104,1,35.56,105.41,U2,VDD,5VDC,,,"/>
<approved hash="104,1,35.56,100.33,U2,PGND,GND,,,"/>
<approved hash="103,1,5.08,110.49,U2,N/A2,N$27,,,"/>
<approved hash="104,1,30.48,312.42,U1,VDD,5VDC_PSU,,,"/>
<approved hash="104,1,0,317.5,J2,VDD,5VDC_PSU,,,"/>
<approved hash="104,1,99.06,334.01,IC1,VCC,5VDC,,,"/>
<approved hash="106,1,35.56,110.49,N$26,,,,,"/>
<approved hash="106,1,5.08,110.49,N$27,,,,,"/>
<approved hash="106,1,3.81,52.07,N$30,,,,,"/>
<approved hash="106,1,34.29,52.07,N$31,,,,,"/>
<approved hash="110,1,194.31,269.24,ISO_SPWM1,ISO_SPWM2,,,,"/>
<approved hash="110,1,194.31,269.24,ISO_SPWM1,ISO_SPWM2,,,,"/>
<approved hash="113,1,91.971,158.011,FRAME1,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
